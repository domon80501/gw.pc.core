﻿using GW.PC.Models.Queues;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.Queues.EF
{
    public class PaymentCallbackRequestService : EntityService<PaymentCallbackRequest>
    {
        public async Task<PaymentCallbackRequest> Pop()
        {
            using (var ctx = new QueuesContext())
            {
                var request = await ctx.PaymentCallbackRequests.FirstOrDefaultAsync();

                if (request != null)
                {
                    ctx.PaymentCallbackRequests.Remove(request);

                    await ctx.SaveChangesAsync();
                }

                return request;
            }
        }
    }
}
