﻿using GW.PC.Models.Queues;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GW.PC.Services.Queues.EF
{
    public abstract class EntityService<T>
        where T : class, IEntity
    {
        public virtual async Task<T> Add(T item)
        {
            using (var ctx = new QueuesContext())
            {
                ctx.Set<T>().Add(item);

                await ctx.SaveChangesAsync();

                return item;
            }
        }

        public virtual async Task AddRange(IEnumerable<T> items)
        {
            using (var ctx = new QueuesContext())
            {
                ctx.Set<T>().AddRange(items);

                await ctx.SaveChangesAsync();
            }
        }

        public virtual async Task<IEnumerable<T>> GetAll()
        {
            using (var ctx = new QueuesContext())
            {
                return await ctx.Set<T>().AsNoTracking().ToListAsync();
            }
        }

        public virtual async Task<IEnumerable<T>> GetAll(params string[] includes)
        {
            using (var ctx = new QueuesContext())
            {
                IQueryable<T> query = ctx.Set<T>();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }

                return await query.ToListAsync();
            }
        }

        public virtual async Task<T> Find(int id)
        {
            using (var ctx = new QueuesContext())
            {
                return await ctx.Set<T>().FindAsync(id);
            }
        }

        public virtual async Task<T> Find(int id, params string[] includes)
        {
            if (id <= 0)
            {
                return null;
            }

            using (var ctx = new QueuesContext())
            {
                IQueryable<T> source = ctx.Set<T>();

                foreach (var include in includes)
                {
                    source = source.Include(include);
                }

                return await source.SingleOrDefaultAsync(o => o.Id == id);
            }
        }

        public virtual async Task<T> Update(T item)
        {
            using (var ctx = new QueuesContext())
            {
                ctx.Entry(item).State = EntityState.Modified;

                await ctx.SaveChangesAsync();

                return item;
            }
        }

        protected virtual async Task<T> EnsureEntity(int id, QueuesContext ctx)
        {
            var entity = await ctx.Set<T>().FindAsync(id);
            if (entity == null)
            {
                //throw new Exception(Constants.Messages.NotFound);
            }

            return entity;
        }

        protected virtual async Task<TEntity> EnsureEntity<TEntity>(int id, QueuesContext ctx, params string[] includes)
            where TEntity : class, IEntity
        {
            IQueryable<TEntity> query = ctx.Set<TEntity>();

            if (includes != null)
            {
                foreach (var include in includes)
                {
                    query = query.Include(include);
                }
            }

            var entity = await query.SingleOrDefaultAsync(e => e.Id == id);
            if (entity == null)
            {
                //throw new Exception(Constants.Messages.NotFound);
            }

            return entity;
        }

        protected async Task CheckUniqueness(params Expression<Func<T, bool>>[] predicates)
        {
            using (var ctx = new QueuesContext())
            {
                foreach (var predicate in predicates)
                {
                    if (await ctx.Set<T>().AnyAsync(predicate))
                    {
                        //throw new Exception(Constants.Messages.ObjectDuplicated);
                    }
                }
            }
        }

        protected void CheckNull<TEntity>(TEntity entity)
        {
            if (entity == null)
            {
                //throw new Exception(Constants.Messages.NotFound);
            }
        }

        protected void CheckNull<TEntity>(TEntity entity, string parameterName)
        {
            if (string.IsNullOrEmpty(entity?.ToString()))
            {
                //throw new Exception(string.Format(Constants.MessageTemplates.NullObject, parameterName));
            }
        }
    }
}
