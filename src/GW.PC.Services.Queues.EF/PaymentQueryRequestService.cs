﻿using GW.PC.Models.Queues;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.Queues.EF
{
    public class PaymentQueryRequestService : EntityService<PaymentQueryRequest>
    {
        public async Task<PaymentQueryRequest> Pop()
        {
            using (var ctx = new QueuesContext())
            {
                var request = await ctx.PaymentQueryRequests.FirstOrDefaultAsync();

                if (request != null)
                {
                    ctx.PaymentQueryRequests.Remove(request);

                    await ctx.SaveChangesAsync();
                }

                return request;
            }
        }
    }
}
