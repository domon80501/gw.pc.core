﻿using GW.PC.Models.Queues;
using System.Data.Entity;

namespace GW.PC.Services.Queues.EF
{
    public class QueuesContext : DbContext
    {
        public DbSet<MerchantCallbackRequest> MerchantCallbackRequests { get; set; }
        public DbSet<PaymentCallbackRequest> PaymentCallbackRequests { get; set; }
        public DbSet<PaymentQueryRequest> PaymentQueryRequests { get; set; }
    }
}
