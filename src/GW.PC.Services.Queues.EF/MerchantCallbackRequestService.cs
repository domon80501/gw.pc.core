﻿using GW.PC.Models.Queues;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.Queues.EF
{
    public class MerchantCallbackRequestService : EntityService<MerchantCallbackRequest>
    {
        public async Task<MerchantCallbackRequest> Pop()
        {
            using (var ctx = new QueuesContext())
            {
                var request = await ctx.MerchantCallbackRequests.FirstOrDefaultAsync();

                if (request != null)
                {
                    ctx.MerchantCallbackRequests.Remove(request);

                    await ctx.SaveChangesAsync();
                }

                return request;
            }
        }
    }
}
