﻿namespace GW.PC.Models.Queues.Enums
{
    public enum PaymentType
    {
        Deposit = 1,
        Withdrawal,
    }
}
