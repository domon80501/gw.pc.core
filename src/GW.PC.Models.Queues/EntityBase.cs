﻿namespace GW.PC.Models.Queues
{
    public abstract class EntityBase : IEntity
    {
        public int Id { get; set; }
    }
}
