﻿using System;

namespace GW.PC.Models.Queues
{
    public interface ICreatedAt
    {
        DateTime CreatedAt { get; set; }
    }
}
