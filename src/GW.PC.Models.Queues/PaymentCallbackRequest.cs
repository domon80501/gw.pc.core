﻿using GW.PC.Core;
using System;

namespace GW.PC.Models.Queues
{
    public class PaymentCallbackRequest : EntityBase, ICreatedAt
    {
        public string RequestId { get; set; }        
        public int PaymentChannelId { get; set; }
        public string HttpMethod { get; set; }
        public string Url { get; set; }
        public string Headers { get; set; }
        public PaymentType PaymentType { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
