﻿namespace GW.PC.Models.Queues
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
