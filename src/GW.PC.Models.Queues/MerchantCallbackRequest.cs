﻿using GW.PC.Core;
using System;

namespace GW.PC.Models.Queues
{
    public class MerchantCallbackRequest : EntityBase, ICreatedAt
    {
        public string RequestId { get; set; }
        public string MerchantCallbackUrl { get; set; }
        public string MerchantUsername { get; set; }
        public string MerchantOrderNumber { get; set; }
        public string PaymentCenterOrderNumber { get; set; }
        public string PaymentOrderNumber { get; set; }
        public PaymentType PaymentType { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public string RequestedAmount { get; set; }
        public string ActualAmount { get; set; }
        public TransactionStatus Status { get; set; }
        public int SendCount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime NextSendingAt { get; set; }
    }
}
