﻿using GW.PC.Core;
using System;

namespace GW.PC.Models.Queues
{
    public class PaymentQueryRequest : EntityBase, ICreatedAt
    {
        public string RequestId { get; set; }
        public string MerchantUsername { get; set; }        
        public PaymentType PaymentType { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public string MerchantOrderNumber { get; set; }        
        public int SendCount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime NextSendingAt { get; set; }
    }
}
