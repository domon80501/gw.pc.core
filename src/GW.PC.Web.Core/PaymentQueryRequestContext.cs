﻿using System.Collections.Generic;

namespace GW.PC.Web.Core
{
    public class PaymentQueryRequestContext
    {
        public IDictionary<string, string> Data { get; set; }
    }
}
