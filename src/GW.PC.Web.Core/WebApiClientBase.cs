﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace GW.PC.Web.Core
{
    public abstract class WebApiClientBase
    {
        protected string RoutePrefix = null;

        protected abstract HttpClient GetHttpClient();

        protected async Task<string> PostAsJsonAsyncRaw<TModel>(string uri, TModel model)
        {
            var response = await GetHttpClient().PostAsJsonAsync($"{RoutePrefix}/{uri}", model);

            return await response.ReadRaw();
        }

        protected async Task<TResult> GetAsync<TResult>(string uri)
        {
            var response = await GetHttpClient().GetAsync($"{RoutePrefix}/{uri}");

            return await response.ReadResult<TResult>();
        }

        protected async Task PostAsJsonAsyncNoResult<TModel>(string uri, TModel model)
        {
            var response = await GetHttpClient().PostAsJsonAsync($"{RoutePrefix}/{uri}", model);

            await response.ReadResult();
        }

        protected async Task<TResult> PostAsJsonAsync<TModel, TResult>(string uri, TModel model)
        {
            var response = await GetHttpClient().PostAsJsonAsync($"{RoutePrefix}/{uri}", model);

            return await response.ReadResult<TResult>();
        }

        protected async Task<string> PostAsJsonAsync<TModel>(string uri, TModel model)
        {
            var response = await GetHttpClient().PostAsJsonAsync($"{RoutePrefix}/{uri}", model);

            return await response.ReadString();
        }

        protected async Task PostAsync(string uri)
        {
            var response = await GetHttpClient().PostAsync($"{RoutePrefix}/{uri}", null);
        }

        protected async Task<TResult> PostAsJsonAsyncRawResult<TModel, TResult>(string uri, TModel model)
        {
            var response = await GetHttpClient().PostAsJsonAsync($"{RoutePrefix}/{uri}", model);

            return await response.ReadRawResult<TResult>();
        }

        protected async Task PutAsJsonAsyncNoResult<TModel>(string uri, TModel model)
        {
            var response = await GetHttpClient().PutAsJsonAsync($"{RoutePrefix}/{uri}", model);

            await response.ReadResult();
        }

        protected async Task PatchAsJsonAsyncNoResult<TModel>(string uri, TModel model)
        {
            var myContent = JsonConvert.SerializeObject(model);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await GetHttpClient().SendAsync(new HttpRequestMessage(new HttpMethod("PATCH"), $"{RoutePrefix}/{uri}")
            {
                Content = byteContent
            });

            await response.ReadResult();
        }
    }
}
