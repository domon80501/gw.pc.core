﻿using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GW.PC.Web.Core
{
    public class ApiResultMessageHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);

            if (response.IsSuccessStatusCode)
            {
                ApiResult result = null;

                if (response.Content == null)
                {
                    result = ApiResult.Success();
                }
                else
                {
                    var value = (response.Content as ObjectContent)?.Value;
                    if (value == null || value is ApiResult)
                    {
                        return response;
                    }
                    else
                    {
                        result = ApiResult.Success(value);
                    }
                }

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(result, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    }),
                    Encoding.UTF8,
                    "application/json")
                };
            }
            else
            {
                // exception filter is a better place for error handling.

                return response;
            }
        }
    }
}
