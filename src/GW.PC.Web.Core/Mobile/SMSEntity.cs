﻿namespace GW.PC.Web.Core
{
    public class SMSEntity
    {
        public string mobile { get; set; }
        public string smsId { get; set; }
        public string customSmsId{ get; set; }
    }
}
