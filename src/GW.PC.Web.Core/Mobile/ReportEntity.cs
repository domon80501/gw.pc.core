﻿namespace GW.PC.Web.Core
{
    public class ReportEntity
    {
        public string mobile { get; set; }
        public string smsId { get; set; }
        public string customSmsId { get; set; }
        public string state { get; set; }
        public string desc { get; set; }
        public string receiveTime { get; set; }
        public string submitTime { get; set; }
        public string extendedCode { get; set; }
      
    }
}
