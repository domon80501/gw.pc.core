﻿namespace GW.PC.Web.Core
{
    public  class MoEntity
    {
        public string mobile { get; set; }
        public string content { get; set; }
        public string moTime { get; set; }
        public string extendedCode { get; set; }
    }
}
