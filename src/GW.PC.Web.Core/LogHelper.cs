﻿using GW.PC.Core;
using System.Web;

namespace GW.PC.Web.Core
{
    public static class LogHelper
    {
        #region Merchant Request
        public static void LogMerchantQueryRequestReceived(string guid, string content)
        {
            SerilogLogger.Logger.LogInformation($"{guid} Merchant query request received. From: {HttpContext.Current.Request.Url} Request context: {content}");
        }

        public static void LogMerchantQueryResponded(string guid, string content)
        {
            SerilogLogger.Logger.LogInformation($"{guid} Merchant query responded. Response context: {content}");
        }

        public static void LogMerchantDepositRequestReceived(string guid, string content)
        {
            SerilogLogger.Logger.LogInformation($"{guid} Merchant deposit request received. From: {HttpContext.Current.Request.Url} Request context: {content}");
        }

        public static void LogMerchantDepositResponded(string guid, string content)
        {
            SerilogLogger.Logger.LogInformation($"{guid} Merchant deposit responded. Response context: {content}");
        }

        public static void LogMerchantWithdrawalRequestReceived(string guid, string content)
        {
            SerilogLogger.Logger.LogInformation($"{guid} Merchant withdrawal request received. From: {HttpContext.Current.Request.Url} Request context: {content}");
        }

        public static void LogMerchantWithdrawalResponded(string guid, string content)
        {
            SerilogLogger.Logger.LogInformation($"{guid} Merchant withdrawal responded. Response context: {content}");
        }        

        public static void LogMerchantRequestError(string guid, string content)
        {
            SerilogLogger.Logger.LogError($"{guid} Merchant request error. Exception: {content}");
        }
        #endregion

        #region Payment Callback

        public static void LogReadPaymentCallbackHeader(string name, string content)
        {
            SerilogLogger.Logger.LogInformation($"{name} callback http-headers. Header-Context: { content}");
        }

        public static void LogDepositCallbackReceived(string name, string content)
        {
            SerilogLogger.Logger.LogInformation($"{name} deposit callback received. Url: {HttpContext.Current.Request.Url}. Content: { content}");
        }

        public static void LogDepositCallbackResponded(string name, string content)
        {
            SerilogLogger.Logger.LogInformation($"{name} deposit callback responded: {content}");
        }

        public static void LogDepositCallbackError(string name, string content)
        {
            SerilogLogger.Logger.LogError($"{name} deposit callback error: {content}");
        }

        public static void LogWithdrawCallbackReceived(string name, string content)
        {
            SerilogLogger.Logger.LogInformation($"{name} withdraw callback received. Url: {HttpContext.Current.Request.Url}. Content: {content}");
        }

        public static void LogWithdrawCallbackResponded(string name, string content)
        {
            SerilogLogger.Logger.LogInformation($"{name} withdraw callback responded: {content}");
        }

        public static void LogWithdrawCallbackError(string name, string content)
        {
            SerilogLogger.Logger.LogError($"{name} withdraw callback error: {content}");
        }

        #endregion
    }
}