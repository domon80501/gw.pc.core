﻿namespace GW.PC.Web.Core
{
    public abstract class SearchCriteriaModel
    {
        public SearchCriteriaModel()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            IsAscending = false;
        }

        /// <summary>
        /// 搜索第几页的数据，从1开始。
        /// </summary>
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public bool IsAscending { get; set; }
    }
}
