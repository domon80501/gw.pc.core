﻿using System;
using System.Collections.Generic;

namespace GW.PC.Web.Core
{
    public class PaymentCallbackRequestContext
    {
        public Guid RequestId { get; set; }
        public int PaymentChannelId { get; set; }
        public string PaymentType { get; set; }
        public string PaymentMethod { get; set; }
        public string Content { get; set; }
    }
}
