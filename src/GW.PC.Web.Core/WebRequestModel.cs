﻿namespace GW.PC.Web.Core
{
    public class WebRequestModel
    {
        public string IP { get; set; }
        public string IPAddress { get; set; }
    }
}
