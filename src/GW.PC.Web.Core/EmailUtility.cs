﻿using GW.PC.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace GW.PC.Web.Core
{
    /// <summary>
    /// Summary description for MailHelper
    /// </summary>
    public static class EmailUtility
    {
        private static string key = "163win88";

        public struct SMTPMailInfo
        {
            public List<string> lstTo;
            public List<string> lstCC;
            public List<string> lstBCC;
            public string sSubject;
            public string sContent;
            public List<string> lstAttachment;
            public List<string> lstEmbeddedPic;
        }

        public static AppSettingNames GetRandomAccount(AppSettingNames[] arr)
        {
            Random ran = new Random();
            int n = ran.Next(arr.Length - 1);

            return arr[n];
        }
        //
        public static string SendMail(SMTPMailInfo udtSMTPMail, string emailCompany)
        {
            string sSMTP = "", sUserAcct = "", sPassword = "", smtpPort = "25", errorMessage = "";
            sSMTP = Constants.AppSettings.Get(AppSettingNames.SMTPGateway);
            var arr = new[] {
                AppSettingNames.SMTPAccount,
                AppSettingNames.SMTPAccount02,
                AppSettingNames.SMTPAccount03,
                AppSettingNames.SMTPAccount04 };
            var key = GetRandomAccount(arr);
            sUserAcct = Constants.AppSettings.Get(key);
            sPassword = Constants.AppSettings.Get(AppSettingNames.SMTPPassword);
            smtpPort = Constants.AppSettings.Get(AppSettingNames.SMTPPort);
            int.TryParse(smtpPort, out int iSmtpPort);
            MailMessage mailMsg = new MailMessage();
            try
            {
                #region  lstTo
                if (udtSMTPMail.lstTo != null)
                {
                    if (udtSMTPMail.lstTo.Count() == 0)
                    {
                        return errorMessage;
                    }
                    else
                    {
                        foreach (var ToRecipient in udtSMTPMail.lstTo)
                        {
                            if (ToRecipient.Trim().Length > 0)
                            {
                                if (mailMsg.From == null)
                                {
                                    mailMsg = new MailMessage(sUserAcct, ToRecipient);
                                }
                                else
                                {
                                    mailMsg.To.Add(ToRecipient);
                                }
                            }
                        }
                    }
                }
                else
                {
                    return errorMessage;
                }
                #endregion

                #region lstCC
                if (udtSMTPMail.lstCC != null)
                {
                    if (udtSMTPMail.lstCC.Count() > 0)
                    {
                        foreach (var ToRecipient in udtSMTPMail.lstCC)
                        {
                            if (ToRecipient.Trim().Length > 0)
                            {
                                mailMsg.CC.Add(ToRecipient);
                            }
                        }
                    }
                }
                #endregion

                mailMsg.BodyEncoding = Encoding.UTF8;
                mailMsg.Subject = udtSMTPMail.sSubject.Trim();
                mailMsg.Body = udtSMTPMail.sContent.Trim();
                mailMsg.Priority = MailPriority.High;
                mailMsg.IsBodyHtml = true;

                #region attachment
                if (udtSMTPMail.lstAttachment != null)
                {
                    if (udtSMTPMail.lstAttachment.Count() > 0)
                    {
                        foreach (var attachment in udtSMTPMail.lstAttachment)
                        {
                            if (attachment.Trim().Length > 0)
                            {
                                Attachment msgAttach = new Attachment(attachment);
                                mailMsg.Attachments.Add(msgAttach);

                            }
                        }
                    }
                }
                #endregion

                SmtpClient smtpClient = new SmtpClient()
                {
                    Host = sSMTP,
                    Credentials = new NetworkCredential(sUserAcct, sPassword),
                    EnableSsl = true,
                    Port = iSmtpPort
                };

                smtpClient.Send(mailMsg);
            }
            catch (Exception ex)
            {
                return errorMessage = ex.Message;
            }

            return errorMessage;
        }

        public static string EncryString(string account, string email)
        {
            string result = "", sYear = "", sMonth = "", sDay = "", sHour = "", sMin = "", sTimeString = "", sValue = "";
            if (!email.Equals(""))
            {
                DateTime dtNow = DateTime.Now;
                sYear = dtNow.Year.ToString();
                sMonth = dtNow.Month.ToString();
                sDay = dtNow.Day.ToString();
                sHour = dtNow.Hour.ToString();
                sMin = dtNow.Minute.ToString();
                sTimeString = sYear + "&*!" + sMonth + "&*!" + sDay + "&*!" + sHour + "&*!" + sMin;
                sValue = sTimeString + "$#$" + account + "$#$" + email;
                result = OldEncrypt.EncryptDES(sValue, key);

            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="encryString"></param>
        /// <returns>decryString 
        /// "-1"-过期
        /// "-2"-时间有错误
        ///  </returns>
        public static void DecryptString(string encryString, ref string account, ref string email)
        {
            string decryString = "", sAccount = "", sEmail, sTime = "", sYear = "", sMonth = "", sDay = "", sHour = "", sMin = "";
            string result = "0";
            int iYear, iMonth, iDay, iHour, iMin;
            double diffHours;
            try
            {
                if (!encryString.Equals(""))
                {
                    decryString = OldEncrypt.DecryptDES(encryString, key);
                    if (!decryString.Equals(""))
                    {
                        string[] stringArrays = decryString.Split(new string[] { "$#$" }, StringSplitOptions.None);
                        sTime = stringArrays[0];
                        sAccount = stringArrays[1];
                        sEmail = stringArrays[2];

                        string[] time = sTime.Split(new string[] { "&*!" }, StringSplitOptions.None);
                        sYear = time[0];
                        sMonth = time[1];
                        sDay = time[2];
                        sHour = time[3];
                        sMin = time[4];

                        bool bYear = int.TryParse(sYear, out iYear);
                        bool bMonth = int.TryParse(sMonth, out iMonth);
                        bool bDay = int.TryParse(sDay, out iDay);
                        bool bHour = int.TryParse(sHour, out iHour);
                        bool bMin = int.TryParse(sMin, out iMin);

                        if (bYear && bMonth && bDay && bHour && bMin)
                        {
                            DateTime dt = new DateTime(iYear, iMonth, iDay, iHour, iMin, 0);
                            diffHours = DateTime.Now.Subtract(dt).TotalHours;
                            if (diffHours > 24)
                            {
                                throw new BusinessException("验证链接已过期");
                            }
                            else
                            {
                                account = sAccount;
                                email = sEmail;
                            }
                        }
                        else
                        {
                            throw new BusinessException("绑定邮箱错误");
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new BusinessException("绑定邮箱错误");
            }
        }
    }
}