﻿namespace GW.PC.Web.Core
{
    public class GeneralOperationModel
    {
        public int Id { get; set; }
        public int OperatorId { get; set; }
        public string OperatorUsername { get; set; }
    }
}
