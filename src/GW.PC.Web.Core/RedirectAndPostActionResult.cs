﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web.Mvc;
using GW.PC.Core;

namespace GW.PC.Web.Core
{
    public class RedirectAndPostActionResult : ActionResult
    {
        string url = string.Empty;
        Dictionary<string, object> postData = new Dictionary<string, object>();
        string completeResponse = string.Empty;

        public RedirectAndPostActionResult(string url, string jsonData)
        {
            this.url = url;
            this.postData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData);
        }

        public RedirectAndPostActionResult(string url, Dictionary<string, object> postData)
        {
            this.url = url;
            this.postData = postData ?? new Dictionary<string, object>();
        }

        public RedirectAndPostActionResult(string response)
        {
            completeResponse = response;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (completeResponse.Length > 0)
            {
                context.HttpContext.Response.Write(completeResponse);
            }
            else
            {
                for (int i = 0; i < this.postData.Count; i++)
                {
                    SerilogLogger.Logger.LogInformation($"Post data: {this.postData.Keys.ElementAt(i)} - {this.postData.Values.ElementAt(i)}");
                }

                var strHtml = BuildPostForm(this.url, this.postData);

                context.HttpContext.Response.Write(strHtml);
            }
        }

        private string BuildPostForm(string url, Dictionary<string, object> PostData)
        {
            string formId = "__PostForm";

            StringBuilder strForm = new StringBuilder();
            strForm.Append(string.Format("<form id=\"{0}\" name=\"{0}\" action=\"{1}\" method=\"POST\">", formId, url));
            foreach (var item in PostData)
            {
                strForm.Append(string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"/>", item.Key, item.Value));
            }
            strForm.Append("</form>");

            StringBuilder strScript = new StringBuilder();
            strScript.Append("<script language=\"javascript\">");
            strScript.Append(string.Format("var v{0}=document.{0};", formId));
            strScript.Append(string.Format("v{0}.submit();", formId));
            strScript.Append("</script>");

            return strForm.ToString() + strScript.ToString();
        }
    }
}
