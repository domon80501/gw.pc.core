﻿using System;
using System.Collections.Generic;
using System.Linq;
using X.PagedList;

namespace GW.PC.Web.Core
{
    public class SerializablePagedList<T>
    {
        protected SerializablePagedList() { }

        public SerializablePagedList(IPagedList<T> list)
        {
            if (list == null)
            {
                Data = Enumerable.Empty<T>();
            }
            else
            {
                Data = list;

                PageCount = list.PageCount;
                TotalItemCount = list.TotalItemCount;
                PageNumber = list.PageNumber;
                PageSize = list.PageSize;
                HasPreviousPage = list.HasPreviousPage;
                HasNextPage = list.HasNextPage;
                IsFirstPage = list.IsFirstPage;
                IsLastPage = list.IsLastPage;
                FirstItemOnPage = list.FirstItemOnPage;
                LastItemOnPage = list.LastItemOnPage;

                var startPageNumber = PageNumber - 5;
                var endPageNumber = PageNumber + 4;
                if (startPageNumber <= 0)
                {
                    endPageNumber -= (startPageNumber - 1);
                    startPageNumber = 1;
                }
                if (endPageNumber > PageCount)
                {
                    endPageNumber = PageCount;
                    if (endPageNumber > 10)
                    {
                        startPageNumber = endPageNumber - 9;
                    }
                }

                StartPageNumber = startPageNumber;
                EndPageNumber = endPageNumber;
            }
        }

        public SerializablePagedList(IEnumerable<T> subset, int totalItemCount, int pageNumber, int pageSize)
        {
            Data = subset;

            TotalItemCount = totalItemCount;
            PageNumber = pageNumber;
            PageSize = pageSize;

            PageCount = TotalItemCount > 0 ?
                (int)Math.Ceiling(TotalItemCount / (double)PageSize) :
                0;
            HasPreviousPage = PageNumber > 1;
            HasNextPage = PageNumber < PageCount;
            IsFirstPage = PageNumber == 1;
            IsLastPage = PageNumber >= PageCount;
            FirstItemOnPage = (PageNumber - 1) * PageSize + 1;
            var numberOfLastItemOnPage = FirstItemOnPage + PageSize - 1;
            LastItemOnPage = numberOfLastItemOnPage > TotalItemCount ?
                TotalItemCount :
                numberOfLastItemOnPage;

            var startPageNumber = PageNumber - 5;
            var endPageNumber = PageNumber + 4;
            if (startPageNumber <= 0)
            {
                endPageNumber -= (startPageNumber - 1);
                startPageNumber = 1;
            }
            if (endPageNumber > PageCount)
            {
                endPageNumber = PageCount;
                if (endPageNumber > 10)
                {
                    startPageNumber = endPageNumber - 9;
                }
            }

            StartPageNumber = startPageNumber;
            EndPageNumber = endPageNumber;
        }

        public SerializablePagedList<TResult> Cast<TResult>(Func<T, TResult> selector)
        {
            return new SerializablePagedList<TResult>
            {
                Data = Data.Select(selector).ToList(),
                FirstItemOnPage = FirstItemOnPage,
                HasNextPage = HasNextPage,
                HasPreviousPage = HasPreviousPage,
                IsFirstPage = IsFirstPage,
                IsLastPage = IsLastPage,
                LastItemOnPage = LastItemOnPage,
                PageCount = PageCount,
                PageNumber = PageNumber,
                PageSize = PageSize,
                TotalItemCount = TotalItemCount,
                StartPageNumber = StartPageNumber,
                EndPageNumber = EndPageNumber
            };
        }

        public SerializablePagedList<T> OrderBy<TKey>(Func<T, TKey> orderBy, bool asc)
        {
            Data = asc ? Data.OrderBy(orderBy) : Data.OrderByDescending(orderBy);

            return this;
        }

        public IEnumerable<T> Data { get; set; }

        public int PageCount { get; set; }
        public int TotalItemCount { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
        public bool IsFirstPage { get; set; }
        public bool IsLastPage { get; set; }
        public int FirstItemOnPage { get; set; }
        public int LastItemOnPage { get; set; }
        /// <summary>
        /// 每页显示一共10页链接，前5后4。该属性表示10页中第1页号码。
        /// </summary>
        public int StartPageNumber { get; set; }
        /// <summary>
        /// /// 每页显示一共10页链接，前5后4。该属性表示10页中第10页号码。
        /// </summary>
        public int EndPageNumber { get; set; }
    }
}
