﻿using GW.PC.Core;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace GW.PC.Web.Core
{
    public static class HttpResponseMessageExtensions
    {
        public static async Task EnsureSuccessStatusCodeAsync(this HttpResponseMessage message)
        {
            if (!message.IsSuccessStatusCode)
            {
                throw new Exception(await message.Content?.ReadAsStringAsync());
            }
        }

        /// <summary>
        /// For api actions that have no return value.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static async Task ReadResult(this HttpResponseMessage message)
        {
            if (message.IsSuccessStatusCode)
            {
                var result = await message.Content?.ReadAsAsync<ApiResult>();
                if (!result.Succeeded)
                {
                    throw new ApiException(await GetApiErrors(message, result));
                }
            }
            else
            {
                throw await BuildException(message);
            }
        }

        /// <summary>
        /// For api actions that return a string value.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static async Task<string> ReadString(this HttpResponseMessage message)
        {
            if (message.IsSuccessStatusCode)
            {
                var result = await message.Content?.ReadAsAsync<string>();

                return result;
            }
            else
            {
                throw await BuildException(message);
            }
        }

        /// <summary>
        /// For api actions that return a <typeparamref name="TResult"/> value. 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="message"></param>
        /// <returns></returns>
        public static async Task<TResult> ReadResult<TResult>(this HttpResponseMessage message)
        {
            if (message.IsSuccessStatusCode)
            {
                var result = await message.Content?.ReadAsAsync<ApiResult<TResult>>();

                if (result == null)
                {
                    return default(TResult);
                }

                if (result.Succeeded)
                {
                    return result.Result;
                }
                else
                {
                    throw new ApiException(await GetApiErrors(message, result));
                }
            }
            else
            {
                throw await BuildException(message);
            }
        }

        /// <summary>
        /// For api actions that return raw http response, such as game validation api.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static async Task<string> ReadRaw(this HttpResponseMessage message)
        {
            if (message.IsSuccessStatusCode)
            {
                return await message.Content?.ReadAsAsync<string>();
            }
            else
            {
                throw await BuildException(message);
            }
        }

        public static async Task<TResult> ReadRawResult<TResult>(this HttpResponseMessage message)
        {
            if (message.IsSuccessStatusCode)
            {
                var result = await message.Content?.ReadAsAsync<TResult>();
                if (result == null)
                {
                    return default(TResult);
                }
                return result;
            }

            throw new Exception(await message.Content?.ReadAsStringAsync());
        }

        private static async Task<Exception> BuildException(HttpResponseMessage message)
        {
            if (message.StatusCode == HttpStatusCode.InternalServerError)
            {
                var result = await message.Content?.ReadAsAsync<ApiResult>();
                if (result.IsBusinessError)
                {
                    return new BusinessException(result.FlattenedErrors);
                }
                else
                {
                    return new ApiException(result.FlattenedErrors);
                }
            }
            else
            {
                return new Exception(await message.Content?.ReadAsStringAsync());
            }
        }

        private static async Task<string> GetApiErrors(HttpResponseMessage message, ApiResult result)
        {
            if (string.IsNullOrEmpty(result.FlattenedErrors))
            {
                return await message.Content?.ReadAsStringAsync();
            }
            else
            {
                return result.FlattenedErrors;
            }
        }
    }
}
