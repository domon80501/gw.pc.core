﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace GW.PC.Web.Core
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString FormControlTextBoxFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string extraClasses = null,
            bool @readonly = false)
        {
            var attributes = new Dictionary<string, object>
            {
                { "class", $"form-control {extraClasses}" }
            };

            if (@readonly)
            {
                attributes.Add("readonly", "readonly");
            }

            return htmlHelper.TextBoxFor(expression, attributes);
        }
    }
}
