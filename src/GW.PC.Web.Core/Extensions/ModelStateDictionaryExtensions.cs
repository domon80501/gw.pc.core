﻿using System.Linq;

namespace GW.PC.Web.Core
{
    public static class ModelStateDictionaryExtensions
    {
        public static string FirstError(this ModelStateDictionary modelState)
        {
            return modelState.First(
                o => o.Value.Errors.Count > 0)
                .Value.Errors.First().ErrorMessage;
        }
    }
}
