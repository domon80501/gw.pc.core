﻿using System.IO;
using System.Text;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using GW.PC.Core;
using System.Web;

namespace GW.PC.Web.Core
{
    public static class BankCardUtility
    {
        private const string requestUrl = "https://api06.aliyun.venuscn.com/ocr/bank-card";
        private const string method = "POST";
        private const string appcode = "ef8795bf2b564d189975ae7e280ba711";


        public static Task<OcrBankCardModel> IdentifyBankCard(string rawText)
        {            
            string bodys = $"pic={rawText}";
            
            string url = requestUrl;
            HttpWebRequest httpRequest = null;
            HttpWebResponse httpResponse = null;

            if (requestUrl.Contains("https://"))
            {
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                httpRequest = (HttpWebRequest)WebRequest.CreateDefault(new Uri(url));
            }
            else
            {
                httpRequest = (HttpWebRequest)WebRequest.Create(url);
            }
            httpRequest.Method = method;
            httpRequest.Headers.Add("Authorization", "APPCODE " + appcode);

            //根据API的要求，定义相对应的Content-Type
            httpRequest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            if (0 < bodys.Length)
            {
                byte[] data = Encoding.UTF8.GetBytes(bodys);
                using (Stream streamRequest = httpRequest.GetRequestStream())
                {
                    streamRequest.Write(data, 0, data.Length);
                }
            }
            try
            {
                httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            }
            catch (WebException ex)
            {
                NLogLogger.Logger.Debug($"IdentifyBankCard Request Error : {ex.Message}");
                throw new BusinessException("请求失败:验证功能维护中", "1");
            }

            Stream streamResponse = httpResponse.GetResponseStream();
            StreamReader reader = new StreamReader(streamResponse, Encoding.GetEncoding("utf-8"));

            var response = reader.ReadToEnd();
            var result = JsonConvert.DeserializeObject<OcrBankCardModel>(response);

            if (result.Status!="200")
            {
                NLogLogger.Logger.Debug($"IdentifyBankCard Response Error : {result.Message}");
                throw new BusinessException($"请求失败:{result.Message}", "1");
            }

            return Task.FromResult(result);
        }

        public static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        public class OcrBankCardModel
        {
            [JsonProperty("ret")]
            public string Status { get; set; }
            [JsonProperty("msg")]
            public string Message { get; set; }
            [JsonProperty("log_id")]
            public string LogID { get; set; }
            [JsonProperty("data")]
            public OcrBankCardContent Content { get; set; }
        }

        public class OcrBankCardContent
        {
            [JsonProperty("number")]
            public string BankCard { get; set; }
            [JsonProperty("bank")]
            public string BankName { get; set; }
            [JsonProperty("type")]
            public string CardType { get; set; }
        }
    }
}