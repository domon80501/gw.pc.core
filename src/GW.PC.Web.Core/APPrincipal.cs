﻿using System;
using System.Security.Principal;

namespace GW.PC.Web.Core
{
    public class APPrincipal : IPrincipal
    {
        public APPrincipal(string username)
        {
            this.Identity = new GenericIdentity(username);
        }

        public IIdentity Identity { get; private set; }

        public int UserId { get; set; }

        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }
    }
}