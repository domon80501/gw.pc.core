﻿using System.Web.Mvc;

namespace GW.PC.Web.Core
{
    public class MvcValidateModelAttribute : ActionFilterAttribute
    {
        public string FailureViewName { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.Controller.ViewData.ModelState.IsValid)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest() ||
                    string.IsNullOrEmpty(FailureViewName))
                {
                    filterContext.HttpContext.Response.StatusCode = 500;
                    filterContext.Result = new ContentResult
                    {
                        Content = filterContext.Controller.ViewData.ModelState.FirstError()
                    };
                }
                else
                {
                    filterContext.Result = new ViewResult
                    {
                        ViewName = FailureViewName,
                        ViewData = filterContext.Controller.ViewData
                    };
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
