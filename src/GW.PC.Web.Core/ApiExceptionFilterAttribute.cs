﻿using GW.PC.Core;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Web.Http.Filters;

namespace GW.PC.Web.Core
{
    public class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var baseException = actionExecutedContext.Exception.GetBaseException();
            if (!(baseException is BusinessException))
            {
                NLogLogger.Logger.Error(baseException.ToString());
            }

            var message = baseException is SocketException ?
                    Constants.Messages.ApiConnectionError : baseException.Message;

            actionExecutedContext.Response =
                actionExecutedContext.Request.CreateResponse(
                    HttpStatusCode.InternalServerError,
                    new ErrorResult
                    {
                        Succeeded = false,
                        IsBusinessError = actionExecutedContext.Exception is BusinessException,
                        Errors = new[] { message }
                    });
        }
    }
}
