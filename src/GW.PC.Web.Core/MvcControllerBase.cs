﻿using GW.PC.Core;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GW.PC.Web.Core
{
    /// <summary>
    /// Base class for all AP Mvc controllers.
    /// </summary>
    public abstract class MvcControllerBase : Controller
    {
        public virtual new APPrincipal User
        {
            get
            {
                return HttpContext.User as APPrincipal;
            }
        }

        protected virtual Task CallApi<T>(Func<T, Task> func, T model)
            where T : WebRequestModel
        {
            model.IP = Request.UserHostAddress;
            model.IPAddress = IPLoacator.GetAddress(
                model.IP,
                Server.MapPath(Constants.AppSettings.Get(
                    AppSettingNames.IPAddressDatabaseFilePath)));

            return func(model);
        }

        protected virtual Task<TResult> CallApi<T, TResult>(Func<T, Task<TResult>> func, T model)
            where T : WebRequestModel
        {
            model.IP = Request.UserHostAddress;
            model.IPAddress = IPLoacator.GetAddress(
                model.IP,
                Server.MapPath(Constants.AppSettings.Get(
                    AppSettingNames.IPAddressDatabaseFilePath)));

            return func(model);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            var baseException = filterContext.Exception?.GetBaseException();
            if (baseException != null)
            {
                if (!(baseException is BusinessException))
                {
                    NLogLogger.Logger.Error(baseException.ToString());
                }

                var message = baseException is SocketException ?
                    Constants.Messages.ApiConnectionError : baseException.Message;

                if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                {
                    Response.StatusCode = baseException is BusinessException ?
                        (int)HttpStatusCode.BadRequest :
                        (int)HttpStatusCode.InternalServerError;
                    filterContext.Result = Content(message);
                }
                else
                {
                    if (baseException is BusinessException)
                    {
                        filterContext.Controller.ViewData.ModelState.AddModelError("", message);

                        HandleBusinessException(filterContext, message);
                    }
                    else
                    {
                        HandleGeneralException(filterContext, message);
                    }
                }
            }
        }

        private void HandleBusinessException(ExceptionContext filterContext, string message)
        {
            var method = filterContext.Controller.GetType().GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
                .SingleOrDefault(
                    m => m.Name.ToLower() == filterContext.RouteData.Values["action"].ToString().ToLower() &&
                        m.GetCustomAttributes(typeof(HttpPostAttribute), true).Length > 0);

            if (method?.GetCustomAttributes(typeof(MvcValidateModelAttribute), true).ElementAtOrDefault(0) is MvcValidateModelAttribute attribute)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = attribute.FailureViewName,
                    ViewData = filterContext.Controller.ViewData
                };
            }
            else
            {
                HandleGeneralException(filterContext, message);
            }
        }

        private void HandleGeneralException(ExceptionContext filterContext, string message)
        {
            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary
                    {
                        { "Error", message }
                    }
            };
        }
    }
}
