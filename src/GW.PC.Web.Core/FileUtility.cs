﻿using GW.PC.Core;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;

namespace GW.PC.Web.Core
{
    public static class FileUtility
    {
        const string PURGE_URL = "https://client.cdn77.com/api/v2.0/data/purge";
        const string UPLOAD_BASE_DIR = "ftp://push-9.cdn77.com/www";
        static string siteId = "15763";
        static string ftpUser = "user_bwrxl9c4";
        static string ftpPasswd = "iHn0ky0Oh4X03t0meOH8";
        static string cdn77Account = "pywsofttech@gmail.com";
        static string operationKey = "DIPYtgBTpXxvm89wcbzd7kjFEf0JSHRG";

        public static bool UploadAndPurge(string directory, string filename, string content)
        {
            return UploadAndPurge(directory, filename, Encoding.UTF8.GetBytes(content));
        }

        public static bool UploadAndPurge(string directory, string filename, byte[] content)
        {
            try
            {
                Upload(directory, filename, content);

                return true;
            }
            catch (Exception ex)
            {
                NLogLogger.Logger.Error($"Upload error: {ex.ToString()}");

                return false;
            }
            finally
            {
                PurgeCache(directory + filename);
            }
        }

        public static bool PurgeCache(string fileToBePurged)
        {
            // Purging Cache from CDN77
            string url = PURGE_URL;

            using (WebClient wc = new WebClient())
            {
                var reqparm = new NameValueCollection
                {
                    { "cdn_id", siteId },
                    { "login", cdn77Account },
                    { "passwd", operationKey },
                    { "url[]", fileToBePurged }
                };

                byte[] responsebytes = wc.UploadValues(url, "POST", reqparm);
                string responsebody = Encoding.UTF8.GetString(responsebytes);

                return true;
            }
        }

        public static bool Upload(string directory, string filename, string content)
        {
            return Upload(directory, filename, Encoding.UTF8.GetBytes(content));
        }

        public static bool Upload(string directory, string filename, byte[] content)
        {
            // Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create($"{UPLOAD_BASE_DIR}/{directory}/{filename}");
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(ftpUser, ftpPasswd);
            request.ContentLength = content.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(content, 0, content.Length);
            requestStream.Close();
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            return true;
        }
    }
}
