﻿namespace GW.PC.Web.Core
{
    public static class MvcControllerRoutePrefixes
    {
        public const string File = "files";
        public const string Customer = "customers";
        public const string CustomerGrade = "grades";
        public const string CustomerRelevance = "relevances";
        public const string Deposit = "deposits";
        public const string GameTransfer = "gametransfers";
        public const string Game = "games";
        public const string User = "users";
        public const string Role = "roles";
        public const string Permission = "permissions";
        public const string Province = "provinces";
        public const string Bonus = "bonus";
        public const string Tag = "tags";
        public const string UserLog = "userlogs";
        public const string Alarm = "alarms";
        public const string PaymentProvider = "paymentproviders";
        public const string PaymentChannel = "paymentchannels";
        public const string Bank = "banks";
        public const string Announcement = "announcements";
        public const string FollowUp = "followups";
        public const string CustomerLog = "customerlogs";
        public const string CustomerMessage = "customermessages";
        public const string CustomerQuery = "customerqueries";
        public const string DepositReminder = "depositreminders";
        public const string CustomerWithdrawal = "customerwithdrawals";
        public const string BalanceAdjustments = "balanceadjustments";
        public const string Rakeback = "rakebacks";
        public const string Agent = "agents";
        public const string AgentSettlement = "agentsettlements";
        public const string AgentAdjustment = "agentadjustments";
        public const string AgentWithdrawal = "agentwithdrawals";
        public const string AgentCustomerReport = "agentcustomerreports";
        public const string AgentReport = "agentreports";
        public const string AgentAnalysisReport = "agentanalysisreports";
        public const string AgentPromotion = "agentpromotions";
        public const string Promotion = "promotions";
        public const string FinancialReport = "financialreports";
        public const string RakebackConfiguration = "rakebackconfigurations";
        public const string WalletTransaction = "wallettransactions";
        public const string WebPaymentGatewayController = "wpg";
        public const string CustomerGameData = "customergamedata";
        public const string OnlineToBankCardDepositReminder = "otbcdepositreminders";
        public const string PaymentChannelTransfer = "pctransfers";
        public const string PaymentChannelAdjustment = "pcadjustments";
        public const string PaymentChannelTransaction = "pctransactions";
        public const string EntityLog = "entitylogs";
        public const string PromotionCategory = "promotioncategories";
        public const string Mission = "missions";
    }
}
