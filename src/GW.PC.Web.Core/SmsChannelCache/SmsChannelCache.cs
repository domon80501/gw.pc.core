﻿using GW.PC.Core;
using StackExchange.Redis;
using System;
using System.Linq;

namespace GW.PC.Web.Core
{
    public class SmsChannelCache
    {
        private const String globalKey = "global";
        private const int retryMinutes = 1;

        private static Lazy<ConnectionMultiplexer> _lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            return ConnectionMultiplexer.Connect(Constants.ConnectionStrings.Get("RedisConnectionString"));
        });

        private static ConnectionMultiplexer _connection
        {
            get
            {
                try
                {
                    return _lazyConnection.Value;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        private static IDatabase _database => _connection?.GetDatabase(1);

        public static bool UsdRedis => _connection == null ? false : true;

        public bool IsGlobalFrozen(out string message)
        {
            message = string.Empty;
            if (_database.HashExists("global", "count"))
            {
                var global = GetGlobalCache("global");
                int count = global.Item1 + 1;
                bool isFrozen = global.Item2;
                DateTime expire = global.Item3;

                if (DateTime.Now > expire)
                {
                    SetCache(globalKey, 1, false, DateTime.Now.AddHours(24));
                    return false;
                }

                if (isFrozen)
                {
                    message = $"讯息量己超过使用上限, 请于{expire.ToDateTimeString()}之后再使用";
                    return true;
                }

                if (count > 10000)
                {
                    SetFrozen(globalKey);
                    message = $"讯息量己超过使用上限, 请于{expire.ToDateTimeString()}之后再使用";
                    return true;
                }

                SetCache(globalKey, count);
                return false;
            }
            else
            {
                SetCache(globalKey, 1, false, DateTime.Now.AddHours(24));
                return false;
            }
        }

        public int GetChannel(string key, int initChannel, int totalChannel, out string message)
        {
            message = string.Empty;
            if (_database.HashExists(key, "count"))
            {
                var channel = GetCache(key);
                int count = channel.Item1 + 1;
                int lastChannel = channel.Item2 + 1;
                bool isFrozen = channel.Item3;
                DateTime expire = channel.Item4;
                DateTime lastUpdate = channel.Item5;

                if (lastUpdate.AddMinutes(retryMinutes) > DateTime.Now)
                {
                    message = $"请于{retryMinutes.ToString()}分钟之后再使用";
                    return initChannel;
                }

                if (DateTime.Now > expire)
                {
                    SetCache(key, 1, initChannel, false, DateTime.Now.AddMinutes(30));
                    return initChannel;
                }

                if (isFrozen)
                {
                    message = $"手机号:{ key}己被冻结，请在{expire.ToDateTimeString()}之后再使用";
                    return initChannel;
                }

                if (count > 30)
                {
                    SetFrozen(key);
                    message = $"IP:{key}己被冻结，请在{expire.ToDateTimeString()}之后再使用";
                    return initChannel;
                }


                int result = lastChannel > totalChannel ? lastChannel - totalChannel : lastChannel;
                SetCache(key, count, result, false);
                return result;

            }
            else
            {
                SetCache(key, 1, initChannel, false, DateTime.Now.AddMinutes(30));
                return initChannel;
            }
        }

        public void SetLastModifyTime(string mobile)
        {
            _database.HashSet(mobile, new HashEntry[] {
                    new HashEntry("lastUpdateAt",DateTime.Now.ToDateTimeString())
                });
        }

        private void SetCache(string key, int count, int lastChannel, bool isFrozen, DateTime expire)
        {
            _database.HashSet(key, new HashEntry[] {
                    new HashEntry("count",count),
                    new HashEntry("lastChannel", lastChannel),
                    new HashEntry("isFrozen",isFrozen),
                    new HashEntry("expire", expire.ToDateTimeString())
                });
        }

        private void SetCache(string key, int count, int lastChannel, bool isFrozen)
        {
            _database.HashSet(key, new HashEntry[] {
                    new HashEntry("count",count),
                    new HashEntry("lastChannel", lastChannel),
                    new HashEntry("isFrozen",isFrozen)
                });
        }

        private void SetCache(string key, int count, bool isFrozen, DateTime expire)
        {
            _database.HashSet(key, new HashEntry[] {
                    new HashEntry("count", count),
                    new HashEntry("isFrozen",isFrozen),
                    new HashEntry("expire", expire.ToDateTimeString())
                });
        }

        private void SetCache(string key, int count)
        {
            _database.HashSet(key, "count", count);
        }

        private void SetFrozen(string key)
        {
            _database.HashSet(key, "isFrozen", true);
        }

        private Tuple<int, int, bool, DateTime, DateTime> GetCache(string key)
        {
            var values = _database.HashGetAll(key);
            var resultCount = (int)values.FirstOrDefault(x => x.Name == "count").Value;
            var resultLastChannel = (int)values.FirstOrDefault(x => x.Name == "lastChannel").Value;
            var resultIsFrozen = (bool)values.FirstOrDefault(x => x.Name == "isFrozen").Value;
            var resultExpire = Convert.ToDateTime(values.FirstOrDefault(x => x.Name == "expire").Value);
            var resultLastUpdateAt = Convert.ToDateTime(values.FirstOrDefault(x => x.Name == "lastUpdateAt").Value);
            return Tuple.Create(resultCount, resultLastChannel, resultIsFrozen, resultExpire, resultLastUpdateAt);
        }

        private Tuple<int, bool, DateTime> GetGlobalCache(string key)
        {
            var values = _database.HashGetAll(key);
            var resultCount = (int)values.FirstOrDefault(x => x.Name == "count").Value;
            var resultIsFrozen = (bool)values.FirstOrDefault(x => x.Name == "isFrozen").Value;
            var resultExpire = Convert.ToDateTime(values.FirstOrDefault(x => x.Name == "expire").Value);
            return Tuple.Create(resultCount, resultIsFrozen, resultExpire);
        }
    }
}
