﻿namespace GW.PC.Web.Core
{
    public static class WebApiControllerRoutePrefixes
    {
        public const string Customer = "api/customers";
        public const string Agent = "api/agents";
        public const string AgentCustomer = "api/agentcustomers";
        public const string AgentSettlement = "api/agentsettlements";
        public const string AgentAdjustment = "api/agentadjustments";
        public const string AgentWithdrawal = "api/agentwithdrawals";
        public const string AgentCustomerReport = "api/agentcustomerreports";
        public const string AgentReport = "api/agentreports";
        public const string AgentAnalysisReport = "api/agentanalysisreports";
        public const string AgentPromotion = "api/agentpromotions";
        public const string FinancialReport = "api/financialreports";
        public const string User = "api/users";
        public const string Role = "api/roles";
        public const string Bonus = "api/bonus";
        public const string Announcement = "api/announcements";
        public const string Permission = "api/permissions";
        public const string CustomerMessage = "api/customermessages";
        public const string CustomerQuery = "api/customerqueries";
        public const string Tag = "api/tags";
        public const string PaymentProvider = "api/paymentproviders";
        public const string Bank = "api/banks";
        public const string Alarm = "api/alarms";
        public const string CustomerRelevance = "api/customerrelevance";
        public const string FollowUp = "api/followups";
        public const string CustomerLog = "api/logs";
        public const string UserLog = "api/userlogs";
        public const string CustomerGrade = "api/customergrades";
        public const string Deposit = "api/deposits";
        public const string PaymentChannel = "api/paymentchannels";
        public const string OnlinePaymentDeposit = "api/opd";
        public const string AlipayDeposit = "api/ad";
        public const string WeChatDeposit = "api/wd";
        public const string OnlineBankingDeposit = "api/obd";
        public const string PrepaidCardDeposit = "api/pcd";
        public const string QQWalletDeposit = "api/qqd";
        public const string OnlineToBankCardDeposit = "api/otbcd";
        public const string JDWalletDeposit = "api/jdd";
        public const string DepositReminder = "api/depositreminders";
        public const string CustomerWithdrawal = "api/customerwithdrawals";
        public const string BalanceAdjustment = "api/balanceadjustments";
        public const string Rakeback = "api/rakebacks";
        public const string Gaming = "api/gaming";
        public const string GameTransfer = "api/gametransfers";
        public const string GamingCallback = "api/gcb";
        public const string Sportsbook = "api/sports";
        public const string Bbin = "api/bbin";
        public const string AG = "api/ag";
        public const string EA = "api/ea";
        public const string QT = "api/qt";
        public const string KG = "api/kg";
        public const string PT = "api/pt";
        public const string LB = "api/lb";
        public const string LT = "api/lt";
        public const string PaymentGateway = "api/pg";
        public const string PaymentCallbackGateway = "api/pcg";
        public const string Promotion = "api/promotions";
        public const string RakebackConfiguration = "api/rakebackconfigurations";
        public const string Province = "api/provinces";
        public const string WalletTransaction = "api/wallettransactions";
        public const string CustomerGameData = "api/customergamedata";
        public const string OnlineToBankCardDepositReminder = "api/otbcdepositreminders";
        public const string WithdrawCallbackGateway = "api/wcg";
        public const string PaymentChannelTransfer = "api/pctransfers";
        public const string PaymentChannelAdjustment = "api/pcadjustments";
        public const string PaymentChannelTransaction = "api/pctransactions";
        public const string QuickPayDeposit = "api/qpd";
        public const string Queue = "api/queues";
        public const string EntityLogs = "api/entitylogs";
        public const string PaymentCallback = "api/paymentcallback";
        public const string PromotionCategory = "api/promotioncategories";
        public const string Mission = "api/missions";

        //PaymentCenterWebApi
        public const string PaymentCenterWebApiDeposit = "api/deposits";
        public const string MerchantCallbackGateway = "api/mcg";





        #region Admin RoutePrefix
        public const string GWAdminApiAccount = "api/admin/account";
        public const string GWAdminApiMerchant = "api/admin/merchant";
        public const string GWAdminApiDeposit = "api/admin/trade/deposit";
        public const string GWAdminApiWithdraw = "api/admin/trade/withdrawal";
        public const string GWAdminApiTradeFeature = "api/admin/trade/deposit/feature";
        public const string GWAdminApiSystem = "api/admin/system";
        public const string GWAdminApiSystemExtension = "api/admin/systemextension";
        #endregion

    }
}
