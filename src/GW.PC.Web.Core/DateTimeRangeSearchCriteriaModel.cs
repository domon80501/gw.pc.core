﻿using System;
using System.Globalization;

namespace GW.PC.Web.Core
{
    public abstract class DateTimeRangeSearchCriteriaModel : SearchCriteriaModel
    {
        public string DateTimeRange { get; set; }
        public string DateTimeFormat { get; set; }

        public DateTime Start
        {
            get
            {
                try
                {
                    var pieces = DateTimeRange.Split('-');
                    if (pieces.Length == 2 && DateTime.TryParseExact(pieces[0].Trim(), DateTimeFormat, null, DateTimeStyles.None, out DateTime dtStart))
                    {
                        return dtStart;
                    }

                    return DateTime.MinValue;
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
        }

        public DateTime End
        {
            get
            {
                try
                {
                    var pieces = DateTimeRange.Split('-');
                    if (pieces.Length == 2 && DateTime.TryParseExact(pieces[1].Trim(), DateTimeFormat, null, DateTimeStyles.None, out DateTime dtEnd))
                    {
                        return dtEnd.AddMilliseconds(999);
                    }

                    return DateTime.MaxValue;
                }
                catch
                {
                    return DateTime.MaxValue;
                }
            }
        }
    }
}