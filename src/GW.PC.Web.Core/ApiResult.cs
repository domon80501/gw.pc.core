﻿using GW.PC.Core;
using System.Collections.Generic;

namespace GW.PC.Web.Core
{
    public class ApiResult
    {
        public ApiResult()
        {
            ResultCode = Constants.PCAdminMessages.SuccessCode;
            ResultMessage = string.Empty;
            IsBusinessError = false;
        }

        public string ResultCode { get; set; }
        public string ResultMessage { get; set; }
        public bool Succeeded { get { return ResultCode == "PC-000-000"; } }
        public bool IsBusinessError { get; set; }
        public string FlattenedErrors => string.Join(";", Errors ?? new[] { string.Empty });
        public IEnumerable<string> Errors { get; set; }

        public object Result { get; set; }

        public ApiResult<T> Success<T>(T value = default(T))
        {
            return new ApiResult<T>(this)
            {
                Result = value
            };
        }
    }

    public class ApiResult<T> : ApiResult
    {
        public ApiResult(ApiResult baseModel)
        {
            if(baseModel != null)
            {
                ResultCode = baseModel.ResultCode;
                ResultMessage = baseModel.ResultMessage;
                IsBusinessError = baseModel.IsBusinessError;
                Errors = baseModel.Errors;
            }
            else
            {
                ResultCode = Constants.PCAdminMessages.SuccessCode;
                ResultMessage = string.Empty;
                IsBusinessError = false;
            }
          
        }

        public T Result { get; set; }
    }
}