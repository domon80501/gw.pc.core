﻿using GW.PC.Core;
using GW.PC.Web.Core.SmsService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Web.Core
{
    /// <summary>
    /// Summary description for PhoneHelper
    /// </summary>
    public static class MobileUtility
    {
        private const string AppId = "6SDK-EMY-6688-KKWRT";
        private const string SecretKey = "6D10C0349E99EBC8";
        private const string Host = "bjmtn.b2m.cn";

        private const String host_toushi = "http://yzx.market.alicloudapi.com";
        private const String path_toushi = "/yzx/sendSms";
        private const String method_toushi = "POST";
        private const String appcode_toushi = "d5b52c64fcbe482f8f4ffe8a9f78598c";

        private const String host_fegine = "http://smsmsgs.market.alicloudapi.com";
        private const String path_fegine = "/smsmsgs";
        private const String method_fegine = "GET";
        private const String appcode_fegine = "d5b52c64fcbe482f8f4ffe8a9f78598c";

        private static SDKClientClient sdkService = new SDKClientClient();

        private static Dictionary<int, Func<string, string, string>> ChannelDic
        {
            get
            {
                return new Dictionary<int, Func<string, string, string>>()
                {
                    {1, (string mobile, string content)=>{ return SendByEmay(mobile,content); } },
                    {2, (string mobile, string content)=>{ return SendByToushi(mobile,content); } },
                    {3, (string mobile, string content)=>{ return SendByFegine(mobile,content); } }
                };
            }
        }

        private static string MessageSample(int sampleCode)
        {
            string sampleString = "";
            switch (sampleCode)
            {
                case 1:
                    sampleString = "【爱拼网】尊敬的爱拼用户，您好，您的验证码是#string,请尽快验证。";
                    break;
                case 2:
                    sampleString = "【爱拼网】尊敬的爱拼用户，您已经成功提取金额为#string，感谢您的支持。";
                    break;
                case 3:
                    sampleString = "【Ray】您好，您的本次验证码是:#string，请勿向他人泄露您的验证码。";
                    break;
                default:
                    break;
            }
            return sampleString;
        }

        public static int SendMessage(string[] phoneNumbers, int sampleCode, ref string randomCode)
        {
            string phoneAccount = "", phonePassword = "";
            phoneAccount = Constants.AppSettings.Get(AppSettingNames.SmsAccount);
            phonePassword = Constants.AppSettings.Get(AppSettingNames.SmsPassword);
            Random rnd = new Random();
            randomCode = rnd.Next(1000, 9999).ToString();
            string content = MessageSample(sampleCode);
            content = content.Replace("#string", randomCode);

            int result = sdkService.sendSMS(phoneAccount, phonePassword, "", phoneNumbers, content, "", "GBK", 3, Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff")));

            return result;
        }

        public static Task Send(string mobile, string content)
        {
            var account = Constants.AppSettings.Get(AppSettingNames.SmsAccount);
            var password = Constants.AppSettings.Get(AppSettingNames.SmsPassword);

            return sdkService.sendSMSAsync(
               account,
               password,
               "",
               new[] { mobile },
               $"【爱拼网】{content}",
               "",
               "GBK",
               3,
               Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff")));
        }

        public static string SendByEmay(string mobile, string content, bool useTemplate = true)
        {
            NLogLogger.Logger.Debug($"SendByEmay 参数 : mobile:{mobile} / content: {content} / useTemplate: {useTemplate} ");
            var contentBody = $"【Ray】{content}";
            if (useTemplate)
            {
                contentBody = MessageSample(3);
                contentBody = contentBody.Replace("#string", content);
            }

            NLogLogger.Logger.Debug($"SendByEmay - contentBody : {contentBody}");

            string result = "";
            Hashtable headerhs = new Hashtable();
            Byte[] byteArray = null;
            string jsondata = "";
            string url = "http://" + Host + "/inter/sendSingleSMS";
            headerhs.Add("appId", AppId);
            var data = new
            {
                mobile = mobile,
                content = contentBody,
                requestTime = DateTime.Now.Ticks.ToString(),
                requestValidPeriod = 30
            };
            jsondata = JsonConvert.SerializeObject(data);
            headerhs.Add("gzip", "on");//先压缩成byte再加密

            try
            {
                byteArray = HttpHelper.postdata(url, AESHelper.AESEncrypt(GzipHelper.GZipCompressString(jsondata), SecretKey), headerhs, Encoding.UTF8, SecretKey);

                if (byteArray != null)
                {
                    result = GzipHelper.DecompressString(AESHelper.AESDecrypt(byteArray, SecretKey));
                    if (result != "")
                    {
                        if (result.IndexOf("ERROR") != -1)
                        {
                            NLogLogger.Logger.Debug($"SendByEmay - request error : {result}");
                            return result;
                        }
                        else
                        {
                            result = result.Replace('\"', '"');

                            JObject jo = (JObject)JsonConvert.DeserializeObject(result);
                            if (jo != null)
                            {
                                SMSEntity SMS = new SMSEntity
                                {
                                    customSmsId = jo["customSmsId"].ToString().Replace("\"", ""),
                                    mobile = jo["mobile"].ToString().Replace("\"", ""),
                                    smsId = jo["smsId"].ToString().Replace("\"", "")
                                };
                            }

                            return string.Empty;
                        }
                    }
                }

                return "手机号码错误，请重新输入。";
            }
            catch (Exception ex)
            {
                NLogLogger.Logger.Debug($"SendByEmay Error : {ex.Message}");
                return "手机号码错误，请重新输入。";
            }


        }

        public static string SendByToushi(string mobile, string content)
        {
            NLogLogger.Logger.Debug($"SendByToushi 参数 : mobile:{mobile} / content: {content}");
            String querys = $"mobile={mobile}&param=code%3A{content}&tpl_id=TP1803033";
            String bodys = "";
            String url = host_toushi + path_toushi;
            HttpWebRequest httpRequest = null;
            HttpWebResponse httpResponse = null;

            if (0 < querys.Length)
            {
                url = url + "?" + querys;
            }

            if (host_toushi.Contains("https://"))
            {
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                httpRequest = (HttpWebRequest)WebRequest.CreateDefault(new Uri(url));
            }
            else
            {
                httpRequest = (HttpWebRequest)WebRequest.Create(url);
            }
            httpRequest.Method = method_toushi;
            httpRequest.Headers.Add("Authorization", "APPCODE " + appcode_toushi);
            if (0 < bodys.Length)
            {
                byte[] data = Encoding.UTF8.GetBytes(bodys);
                using (var stream = httpRequest.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
            }
            try
            {
                httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            }
            catch (WebException ex)
            {
                NLogLogger.Logger.Debug($"SendByToushi Error : {ex.Message}");
                return "手机号码错误，请重新输入。";
            }

            var st = httpResponse.GetResponseStream();
            var reader = new StreamReader(st, Encoding.GetEncoding("utf-8"));

            JObject resContent = JsonConvert.DeserializeObject<JObject>(reader.ReadToEnd());

            if (resContent["return_code"].ToString() != "00000")
            {
                return "手机号码错误，请重新输入。";
            }

            return string.Empty;
        }

        public static string SendByFegine(string mobile, string content)
        {
            NLogLogger.Logger.Debug($"SendByFegine 参数 : mobile:{mobile} / content: {content}");
            String querys = $"param={content}&phone={mobile}&sign=40741&skin=9027";
            String bodys = "";
            String url = host_fegine + path_fegine;
            HttpWebRequest httpRequest = null;
            HttpWebResponse httpResponse = null;

            if (0 < querys.Length)
            {
                url = url + "?" + querys;
            }

            if (host_fegine.Contains("https://"))
            {
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                httpRequest = (HttpWebRequest)WebRequest.CreateDefault(new Uri(url));
            }
            else
            {
                httpRequest = (HttpWebRequest)WebRequest.Create(url);
            }
            httpRequest.Method = method_fegine;
            httpRequest.Headers.Add("Authorization", "APPCODE " + appcode_fegine);
            if (0 < bodys.Length)
            {
                byte[] data = Encoding.UTF8.GetBytes(bodys);
                using (Stream stream = httpRequest.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
            }
            try
            {
                httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            }
            catch (WebException ex)
            {
                NLogLogger.Logger.Debug($"SendByFegine Error : {ex.Message}");
                return "手机号码错误，请重新输入。";
            }

            Stream st = httpResponse.GetResponseStream();
            StreamReader reader = new StreamReader(st, Encoding.GetEncoding("utf-8"));

            JObject resContent = JsonConvert.DeserializeObject<JObject>(reader.ReadToEnd());

            if (resContent["Code"].ToString() != "OK")
            {
                return "手机号码错误，请重新输入。";
            }

            return string.Empty;
        }

        public static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        public static string AutoSend(string mobile, string content, string ip)
        {
            var result = string.Empty;
            if (int.TryParse(Constants.AppSettings.Get(AppSettingNames.SmsChannel), out int smsChannel))
            {
                if (SmsChannelCache.UsdRedis)
                {
                    NLogLogger.Logger.Debug($"Use Redis Cache Mobile={mobile}, IP={ip}");

                    var smsChannelCache = new SmsChannelCache();
                    try
                    {
                        if (smsChannelCache.IsGlobalFrozen(out string message))
                        {
                            throw new BusinessException($"{message}");
                        }

                        int ipChannel = 0;
                        string ipMessage = string.Empty;
                        if (ip != string.Empty && ip != null)
                        {
                            ipChannel = smsChannelCache.GetChannel(ip, smsChannel, ChannelDic.Count, out ipMessage);
                        }

                        int mobileChannel = smsChannelCache.GetChannel(mobile, smsChannel, ChannelDic.Count, out string mobileMessage);

                        if (mobileMessage != string.Empty && ipMessage != string.Empty)
                        {
                            throw new BusinessException($"{mobileMessage}({ipMessage})");
                        }
                        else if (mobileMessage != string.Empty || ipMessage != string.Empty)
                        {
                            throw new BusinessException(mobileMessage != string.Empty ? mobileMessage : ipMessage);
                        }
                        else
                        {
                            result = ChannelDic[mobileChannel].Invoke(mobile, content);
                            if(string.IsNullOrEmpty(result))
                                smsChannelCache.SetLastModifyTime(mobile);
                            NLogLogger.Logger.Debug($"mobile/sendcode取得smsChannel : {mobileChannel}");
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    result = AutoSend(mobile, content);
                }
            }
            else
            {
                NLogLogger.Logger.Debug($"mobile/sendcode取得smsChannel : 失敗");
            }

            return result;
        }

        public static string AutoSend(string mobile, string content)
        {
            var smsChannel = Constants.AppSettings.Get(AppSettingNames.SmsChannel);
            NLogLogger.Logger.Debug($"mobile/sendcode取得smsChannel : {smsChannel}");

            var result = string.Empty;


            switch (smsChannel)
            {
                case Constants.SmsChannels.emay:
                    result = SendByEmay(mobile, content);
                    break;
                case Constants.SmsChannels.toushi:
                    result = SendByToushi(mobile, content);
                    break;
                case Constants.SmsChannels.fegine:
                    result = SendByFegine(mobile, content);
                    break;
                default:
                    result = "短信通道维护中";
                    break;
            }

            return result;
        }
    }
}