﻿using Newtonsoft.Json;
using System;
using System.Linq;

namespace GW.PC.Web.Core
{
    public static class WebUtility
    {
        public static T DeserializeQueryString<T>(string text, string separator = "&")
            where T : new()
        {
            var result = new T();

            var properties = typeof(T).GetProperties();
            var items = text.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var item in items)
            {
                var pieces = item.Split('=');
                var key = pieces[0];
                var value = pieces[1];

                foreach (var property in properties)
                {
                    if (property.GetCustomAttributes(typeof(JsonPropertyAttribute), false).SingleOrDefault()
                        is JsonPropertyAttribute attr)
                    {
                        if (attr.PropertyName.ToLower() == key.ToLower())
                        {
                            property.SetValue(result, value);
                        }
                    }
                    else
                    {
                        if (property.Name.ToLower() == key.ToLower())
                        {
                            property.SetValue(result, value);
                        }
                    }
                }
            }

            return result;
        }

        public static object DeserializeQueryString(string text, Type type)
        {
            var result = Activator.CreateInstance(type);

            var properties = type.GetProperties();
            var items = text.Split('&');
            foreach (var item in items)
            {
                var key = item.Substring(0, item.IndexOf('='));
                var value = item.Substring(item.IndexOf('=') + 1);

                foreach (var property in properties)
                {
                    if (property.GetCustomAttributes(typeof(JsonPropertyAttribute), false).SingleOrDefault()
                        is JsonPropertyAttribute attr)
                    {
                        if (attr.PropertyName.ToLower() == key.ToLower())
                        {
                            property.SetValue(result, value);
                        }
                    }
                    else
                    {
                        if (property.Name.ToLower() == key.ToLower())
                        {
                            property.SetValue(result, value);
                        }
                    }
                }
            }

            return result;
        }
    }
}
