﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace GW.PC.Web.Core
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new ErrorResult
                    {
                        Succeeded = false,
                        IsBusinessError = true,
                        Errors = actionContext.ModelState.Values.SelectMany(
                            o => o.Errors.Select(
                                e => e.ErrorMessage))
                    });
            }

            base.OnActionExecuting(actionContext);
        }
    }
}
