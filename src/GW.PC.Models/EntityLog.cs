﻿using Newtonsoft.Json;
using System;

namespace GW.PC.Models
{
    public class EntityLog : EntityBase, ICreatedAt
    {
        public EntityLogTargetType TargetType { get; set; }
        public int? TargetId { get; set; }
        public EntityLogContent? Content { get; set; }
        public string Data { get; set; }
        public string Details { get; set; }

        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            try
            {
                var template = EntityLogContents.Get(Content);
                if (string.IsNullOrEmpty(template))
                {
                    return string.Empty;
                }
                else
                {
                    if (string.IsNullOrEmpty(Data))
                    {
                        return template;
                    }
                    else
                    {
                        return string.Format(template, JsonConvert.DeserializeObject<object[]>(Data));
                    }
                }
            }
            catch (Exception)
            {
                return Data;
            }
        }
    }
}
