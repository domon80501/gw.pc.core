﻿using System;
using System.Collections.Generic;

namespace GW.PC.Models
{
    public class Customer : CreatableEntity, IUsername
    {
        public string Username { get; set; }
        public CustomerAccountType AccountType { get; set; }
        /// <summary>
        /// 从兼容性考虑，保存旧系统的密码md5值。
        /// </summary>
        public string MD5Password { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// 1 - 男；2 - 女
        /// </summary>
        public Gender Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Email { get; set; }
        public bool EmailBound { get; set; }
        public string Mobile { get; set; }
        public bool MobileBound { get; set; }
        public string QQ { get; set; }
        public bool QQBound { get; set; }
        public string WeChat { get; set; }
        public bool WeChatBound { get; set; }
        /// <summary>
        /// 注册网址
        /// </summary>
        public string RegistrationUrl { get; set; }
        /// <summary>
        /// 注册IP
        /// </summary>
        public string RegistrationIP { get; set; }
        /// <summary>
        /// 注册地址
        /// </summary>
        public string RegistrationAddress { get; set; }
        public AccountStatus Status { get; set; }
        public DateTime? LastLoginAt { get; set; }
        public bool AllowWithdraw { get; set; }
        public bool AllowRakeback { get; set; }
        public bool AllowPromotion { get; set; }
        public DateTime? LastDisallowWithdrawAt { get; set; }
        /// <summary>
        /// 为数据迁移的临时字段，迁移完成后应通过Migration删除该字段
        /// </summary>
        public string TempPasswordKey { get; set; }
        /// <summary>
        /// 从性能角度考虑，不采用标准外键形式保存玩家在各游戏的数据，而是
        /// 在该字段里保存游戏数据的json表示形式。
        /// </summary>
        public string TempGameData { get; set; }
        public string Notes { get; set; }
        public int FailedLoginCount { get; set; }
        public string DocumentReferrer { get; set; }
        public string UTMSource { get; set; }
        public string UTMMedium { get; set; }
        public string UTMCampaign { get; set; }
        public string UTMContent { get; set; }
        public string UTMTerm { get; set; }

        public int? ReferrerId { get; set; }
        public int? AgentId { get; set; }
        public int GradeId { get; set; }
        public int WalletId { get; set; }
        public int? AssigneeId { get; set; }
        public int? LastDisallowedWithdrawById { get; set; }

        public virtual Customer Referrer { get; set; }
        public virtual Agent Agent { get; set; }
        public virtual CustomerGrade Grade { get; set; }
        public virtual Wallet Wallet { get; set; }
        public virtual User Assignee { get; set; }
        public virtual User LastDisallowedWithdrawBy { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
    }
}
