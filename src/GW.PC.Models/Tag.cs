﻿using System.Collections.Generic;

namespace GW.PC.Models
{
    public class Tag : CreatableEntity
    {
        public string Name { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}
