﻿using System.Collections.Generic;

namespace GW.PC.Models
{
    public class PermissionGroup : EntityBase
    {
        public string Name { get; set; }
        /// <summary>
        /// 1-based.
        /// </summary>
        public int Level { get; set; }

        public int? ParentId { get; set; }

        public virtual PermissionGroup Parent { get; set; }
        public virtual ICollection<PermissionGroup> PermissionGroups { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
