﻿namespace GW.PC.Models
{
    public class CustomerRelevance : CreatableEntity
    {
        public string PrincipalCustomerUsername { get; set; }
        public string AssociateCustomerUsername { get; set; }
        public CustomerRelevanceType Type { get; set; }
        public string PrincipalValue { get; set; }
        public string AssociateValue { get; set; }
    }
}
