﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;

namespace GW.PC.Models
{
    public class OnlineBankingDeposit : Deposit
    {
        /// <summary>
        /// Nullable to support old bad data.
        /// </summary>
        public int? PaymentProviderBankId { get; set; }        
        public string PayerName { get; set; }
        public string PayeeName { get; set; }
        public string PayeeAccountNumber { get; set; }
        public string Remark { get; set; }
        /// <summary>
        /// 网银手续费返还
        /// </summary>
        public decimal CommissionRefund { get; set; }

        public virtual PaymentProviderBank PaymentProviderBank { get; set; }

        public override PaymentMethod PaymentMethod => PaymentMethod.OnlineBanking;
        public override EntityLogTargetType EntityLogTargetType => EntityLogTargetType.OnlineBankingDeposit;
        public override HttpPayment BuildHttpPayment()
        {
            var payment = base.BuildHttpPayment();

            payment.Source = HttpPaymentSource.OnlineBankingDeposit;

            return payment;
        }
    }
}
