﻿namespace GW.PC.Models
{
    public class Province : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
