﻿using GW.PC.Models.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace GW.PC.Models
{
    public class NewPromotion : CreatableEntity, IEntityStatus
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime StartsAt { get; set; }
        public DateTime? EndsAt { get; set; }
        public int Order { get; set; }
        public string Platforms { get; set; }
        public string Url { get; set; }
        public EntityStatus Status { get; set; }
        public bool PopUp { get; set; }
        
        public virtual ICollection<Mission> Missions { get; set; }

        private readonly IEnumerable<PlatformsDto> platformsDto = null;
        public IEnumerable<PlatformsDto> PlatformsDto
        {
            get
            {
                return platformsDto ?? JsonConvert.DeserializeObject<IEnumerable<PlatformsDto>>(Platforms);
            }
        }
    }
}
