﻿namespace GW.PC.Models
{
    public class RakebackConfiguration : CreatableEntity, IEntityStatus
    {
        public string Configurations { get; set; }
        public EntityStatus Status { get; set; }

        public int CustomerGradeId { get; set; }

        public virtual CustomerGrade CustomerGrade { get; set; }
    }

    public class RakebackConfigurationItem
    {
        public string GameName { get; set; }
        public decimal Percentage { get; set; }
        public decimal MaxAmount { get; set; }
    }
}
