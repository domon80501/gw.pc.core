﻿namespace GW.PC.Models
{
    public class City : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }

        public int ProvinceId { get; set; }

        public virtual Province Province { get; set; }
    }
}
