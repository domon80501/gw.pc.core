﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public class SystemConfiguration : EntityBase
    {
        public string Value { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
