﻿using System;

namespace GW.PC.Models
{
    public class PaymentChannelTransaction : EntityBase, ICreatedAt
    {
        public PaymentChannelTransactionType Type { get; set; }
        public decimal BeforeBalance { get; set; }
        public decimal Amount { get; set; }
        public decimal Commission { get; set; }
        public decimal AfterBalance { get; set; }
        public string Notes { get; set; }
        public DateTime CreatedAt { get; set; }

        public int PaymentChannelId { get; set; }

        public virtual PaymentChannel PaymentChannel { get; set; }
    }
}
