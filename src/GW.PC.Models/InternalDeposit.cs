﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;

namespace GW.PC.Models
{
    public class InternalDeposit : Deposit
    {
        public decimal DepositCommission { get; set; }

        public int DepositPaymentChannelId { get; set; }
        public int? PaymentProviderBankId { get; set; }

        public virtual PaymentChannel DepositPaymentChannel { get; set; }
        public virtual PaymentProviderBank PaymentProviderBank { get; set; }

        public override PaymentMethod PaymentMethod => PaymentMethod.Internal;
        public override EntityLogTargetType EntityLogTargetType => EntityLogTargetType.InternalDeposit;
        public override HttpPayment BuildHttpPayment()
        {
            var payment = base.BuildHttpPayment();

            payment.Source = HttpPaymentSource.InternalDeposit;

            return payment;
        }
    }
}
