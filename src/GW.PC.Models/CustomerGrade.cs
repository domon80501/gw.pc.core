﻿namespace GW.PC.Models
{
    public class CustomerGrade : CreatableEntity
    {
        public string Name { get; set; }
        /// <summary>
        /// 0-based.
        /// </summary>
        public int Level { get; set; }
        public decimal MonthlyAmountToUpgrade { get; set; }
        public decimal OneTimeAmountToUpgrade { get; set; }
        public decimal BonusAfterUpgrade { get; set; }
        public decimal OneTimeWithdrawalAmountLimit { get; set; }
        public int? DailyWithdrawalCountLimit { get; set; }

        /// <summary>
        /// Backward compatibility
        /// </summary>
        public int Code { get; set; }
    }
}
