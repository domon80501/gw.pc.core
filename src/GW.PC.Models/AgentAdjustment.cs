﻿namespace GW.PC.Models
{
    public class AgentAdjustment : AuditableEntity, IUsername
    {
        public string Username { get; set; }
        public AgentAdjustmentType Type { get; set; }
        public decimal Amount { get; set; }
        public string Notes { get; set; }
        public AuditStatus Status { get; set; }
    }
}
