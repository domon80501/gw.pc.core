﻿using System;

namespace GW.PC.Models
{
    public class HttpPayment : EntityBase, ICreatedAt
    {
        public HttpPaymentSource Source { get; set; }
        public int SourceId { get; set; }
        public bool? Succeeded { get; set; }
        public string Response { get; set; }
        public DateTime? ResponseReceivedAt { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
