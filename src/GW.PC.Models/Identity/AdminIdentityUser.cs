﻿using GW.PC.Core;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GW.PC.Models
{
    public class AdminIdentityUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AdminIdentityUser> manager)
        {
            // 注意 authenticationType 必須符合 CookieAuthenticationOptions.AuthenticationType 中定義的項目
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);




            var identityJson = JsonConvert.SerializeObject(userIdentity);
            SerilogLogger.Logger.LogInformation($"TimmyAuth: userIdentity: {identityJson}");



            // 在這裡新增自訂使用者宣告
            return userIdentity;
        }
    }
}
