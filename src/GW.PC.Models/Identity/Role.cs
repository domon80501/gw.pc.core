﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace GW.PC.Models
{
    public class AdminIdentityRole : IdentityRole
    {
        public AdminIdentityRole() { }
    }
}
