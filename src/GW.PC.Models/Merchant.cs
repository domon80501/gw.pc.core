﻿using System.Collections.Generic;

namespace GW.PC.Models
{
    public class Merchant : CreatableEntity, IUsername
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public EntityStatus Status { get; set; }

        public string MerchantKey { get; set; }

        public virtual ICollection<PaymentProvider> PaymentProviders { get; set; }
        public virtual ICollection<PaymentProviderBank> PaymentProviderBanks { get; set; }
        public virtual ICollection<PaymentChannel> PaymentChannels { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
