﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    /// <summary>
    /// 表示各种支付服务提供商，每个提供商可能提供一条或多条不同支付渠道
    /// （微信，支付宝，在线，网银等）。
    /// </summary>
    public class PaymentProvider : CreatableEntity, IEntityStatus
    {
        public string Name { get; set; }
        public EntityStatus Status { get; set; }
        public string AllowedIPs { get; set; }
        public decimal Balance { get; set; }
        public int MerchantId { get; set; }
       
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual Merchant Merchant { get; set; }
        public virtual ICollection<PaymentChannel> PaymentChannels { get; set; }
        public virtual ICollection<PaymentProviderBank> PaymentProviderBanks { get; set; }
    }
}
