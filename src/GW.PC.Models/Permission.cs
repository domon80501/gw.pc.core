﻿using System.Collections.Generic;

namespace GW.PC.Models
{
    public class Permission : EntityBase, IEntityStatus
    {
        public string Name { get; set; }
        public PermissionCode Code { get; set; }
        public EntityStatus Status { get; set; }

        public int ParentId { get; set; }

        public virtual PermissionGroup Parent { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
