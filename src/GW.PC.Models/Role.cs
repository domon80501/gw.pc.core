﻿using System.Collections.Generic;

namespace GW.PC.Models
{
    public class Role : CreatableEntity, IEntityStatus
    {
        public string Name { get; set; }
        public EntityStatus Status { get; set; }

        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
