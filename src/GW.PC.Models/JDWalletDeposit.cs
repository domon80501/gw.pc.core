﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;

namespace GW.PC.Models
{
    public class JDWalletDeposit : Deposit
    {
        public override PaymentMethod PaymentMethod => PaymentMethod.JDWallet;
        public override EntityLogTargetType EntityLogTargetType => EntityLogTargetType.JDWalletDeposit;

        public override HttpPayment BuildHttpPayment()
        {
            var payment = base.BuildHttpPayment();

            payment.Source = HttpPaymentSource.JDWalletDeposit;

            return payment;
        }
    }
}
