﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;

namespace GW.PC.Models
{
    public class OnlinePaymentDeposit : Deposit
    {
        /// <summary>
        /// Nullable to support old bad data.
        /// </summary>
        public int? PaymentProviderBankId { get; set; }
        public virtual PaymentProviderBank PaymentProviderBank { get; set; }
        public override PaymentMethod PaymentMethod => PaymentMethod.OnlinePayment;

        public override EntityLogTargetType EntityLogTargetType => EntityLogTargetType.OnlinePaymentDeposit;
        public override HttpPayment BuildHttpPayment()
        {
            var payment = base.BuildHttpPayment();

            payment.Source = HttpPaymentSource.OnlinePaymentDeposit;

            return payment;
        }
    }
}
