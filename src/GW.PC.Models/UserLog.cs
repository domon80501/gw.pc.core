﻿using System;

namespace GW.PC.Models
{
    public class UserLog : EntityBase, ICreatedAt
    {
        public string Username { get; set; }
        public UserLogType Type { get; set; }
        public string IP { get; set; }
        public string Address { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
