﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public class CustomerDepositPeriod : EntityBase, ICreatedAt, IUsername
    {
        public DateTime CreatedAt { get; set; }
        public string Username { get; set; }
        public DateTime? EndsAt { get; set; }
        public decimal TotalAmount { get; set; }
        public bool Completed { get; set; }
        public decimal ValidBetAmount { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
