﻿using System;

namespace GW.PC.Models
{
    public class GameTransaction : EntityBase, ICreatedAt, IUsername
    {
        public string Username { get; set; }
        public int GameId { get; set; }
        public string GameOrderNumber { get; set; }
        public GameTransactionType Type { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedAt { get; set; }
        public decimal Balance { get; set; }
    }
}
