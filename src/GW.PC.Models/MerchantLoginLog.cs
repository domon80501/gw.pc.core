﻿using System;

namespace GW.PC.Models
{
    public class MerchantLoginLog : EntityBase, ICreatedAt
    {
        public string Username { get; set; }
        public DateTime CreatedAt { get; set; }
        public string DeviceIdentifier { get; set; }
        public bool Succeeded { get; set; }
    }
}
