﻿using System;

namespace GW.PC.Models
{
    public class CustomerEA : CustomerGameBase
    {
        /// <summary>
        /// 每次登录游戏生成的token，游戏商会回调AP服务验证该token。
        /// </summary>
        public string LoginToken { get; set; }

        /// <summary>
        /// 登录token有效期，为生成token时的服务器时间+20分钟。
        /// </summary>
        public DateTime? LoginTokenExpiredAt { get; set; }
    }
}
