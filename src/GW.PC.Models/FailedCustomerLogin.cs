﻿using System;

namespace GW.PC.Models
{
    public class FailedCustomerLogin : EntityBase, ICreatedAt
    {
        public string Username { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
