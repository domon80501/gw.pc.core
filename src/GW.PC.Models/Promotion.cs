﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    /// <summary>
    /// 优惠活动
    /// </summary>
    public class Promotion: CreatableEntity, IEntityStatus
    {
        [Required]
        public string Name { get; set; }
        public PromotionEvent Event { get; set; }
        public PromotionType PromotionType { get; set; }
        public int? TimesLimit { get; set; }
        public PromotionDepositCheck DepositCheck { get; set; }
        public decimal? DepositCheckAmount { get; set; }
        public BonusCalculationType BonusCalculationType { get; set; }
        public decimal BonusCalculationValue { get; set; }
        /// <summary>
        /// 提款投注倍数
        /// </summary>
        public int WithdrawalBetMultiplier { get; set; }
        public decimal? MaxBonusAmount { get; set; }
        public string CustomerGrades { get; set; }
        public EntityStatus Status { get; set; }
    }
}
