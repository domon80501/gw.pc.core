﻿using System.Collections.Generic;

namespace GW.PC.Models
{
    public class FollowUp : CreatableEntity
    {
        public string CustomerUsername { get; set; }
        public FollowUpType Type { get; set; }
        public string Content { get; set; }
        public bool IsSolved { get; set; }
        public string AttachmentFileNames { get; set; }

        public virtual ICollection<FollowUpComment> Comments { get; set; }
        public virtual ICollection<User> Receivers { get; set; }
    }
}
