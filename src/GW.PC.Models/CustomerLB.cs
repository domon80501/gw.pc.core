﻿using System;

namespace GW.PC.Models
{
    public class CustomerLB : CustomerGameBase
    {
        public string LoginToken { get; set; }
        public DateTime? LoginTokenExpiredAt { get; set; }
    }
}