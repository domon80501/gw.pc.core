﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum CustomerMessageType
    {
        [Display(Name = "系统通知")]
        SystemNotification = 1,
        [Display(Name = "商户消息")]
        MerchantMessage,
        [Display(Name = "客户消息")]
        CustomerMessage
    }
}