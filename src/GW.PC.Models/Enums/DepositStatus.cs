﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum DepositStatus
    {
        Unknown,
        [Display(Name = "支付中")]
        PendingPayment = 1,
        [Display(Name = "自动到账")]
        AutoSuccess,
        [Display(Name = "手动到账")]
        ManualSuccess,
        [Display(Name = "支付失败")]
        PaymentFailed,
        [Display(Name = "等待审核催到账")]
        AwaitReminderApproval,
        [Display(Name = "到账")]
        Success = 101
    }
}