﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum AgentSettlementStatus
    {
        [Display(Name = "未开始")]
        NotStarted,
        [Display(Name = "正在结算")]
        InProgress,
        [Display(Name = "等待审核")]
        AwaitApproval,
        [Display(Name = "审核通过")]
        Approved,
        [Display(Name = "审核拒绝")]
        Declined,
        [Display(Name = "生成结算失败")]
        Failed,
        [Display(Name = "失败已关闭")]
        FailedAndClosed
    }
}
