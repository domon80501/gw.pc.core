﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum GameTransferStatus
    {
        /// <summary>
        /// 刚创建，准备进行游戏接口调用
        /// </summary>
        [Display(Name = "转账中")]
        InProgress = 1,
        /// <summary>
        /// 成功
        /// </summary>
        [Display(Name = "成功")]
        Successful = 4,
        /// <summary>
        /// 失败
        /// </summary>
        [Display(Name = "失败")]
        Failed = 5,
        [Display(Name = "失败已关闭")]
        FailedAndClosed,
        [Display(Name = "等待转账")]
        Pending,
    }
}