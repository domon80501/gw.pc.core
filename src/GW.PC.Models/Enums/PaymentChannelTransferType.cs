﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum PaymentChannelTransferType
    {
        [Display(Name = "手动")]
        Manual,
        [Display(Name = "代付")]
        Auto
    }
}
