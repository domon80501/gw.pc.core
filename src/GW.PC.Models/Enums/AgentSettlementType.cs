﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum AgentSettlementType
    {
        [Display(Name = "投注额")]
        BetAmount,
        [Display(Name = "输赢")]
        ProfitAmount
    }
}
