﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum PaymentPlatform
    {
        [Display(Name = "所有平台")]
        All = 0,
        [Display(Name = "电脑")]
        PC,
        [Display(Name = "手机")]
        Mobile,
        [Display(Name = "APP")]
        App,
    }
}
