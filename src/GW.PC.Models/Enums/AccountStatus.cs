﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum AccountStatus
    {
        [Display(Name = "正常")]
        Normal = 1,
        [Display(Name = "冻结")]
        Frozen = 2,
        [Display(Name = "等待审核")]
        AwaitApproval = 3,
        [Display(Name = "审核拒绝")]
        Declined = 4
    }
}