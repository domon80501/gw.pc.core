﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum Gender
    {
        [Display(Name = "无")]
        NotSet = 0,
        [Display(Name = "男")]
        Male = 1,
        [Display(Name = "女")]
        Female = 2,
    }
}