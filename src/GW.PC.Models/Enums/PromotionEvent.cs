﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum PromotionEvent
    {
        [Display(Name = "无")]
        None = 0,
        [Display(Name = "存款优惠")]
        Deposit,
        [Display(Name = "升级优惠")]
        Upgrade,
        [Display(Name = "绑定优惠")]
        Binding
    }
}
