﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum WithdrawalStatus
    {
        /// <summary>
        /// 等待审核
        /// </summary>
        [Display(Name = "等待审核")]
        AwaitingApproval = 1,

        /// <summary>
        /// 等待系统支付
        /// </summary>
        [Display(Name = "等待系统支付")]
        AwaitingAutoPayment = 2,

        /// <summary>
        /// 系统支付中
        /// </summary>
        [Display(Name = "系统支付中")]
        AutoPaymentInProgress = 4,

        /// <summary>
        /// 系统支付成功
        /// </summary>
        [Display(Name = "系统支付成功")]
        AutoPaymentSuccessful = 5,

        /// <summary>
        /// 系统支付失败
        /// </summary>
        [Display(Name = "系统支付失败")]
        AutoPaymentFailed = 6,

        /// <summary>
        /// 补单成功
        /// </summary>
        [Display(Name = "补单成功")]
        ManualConfirmed = 8,

        /// <summary>
        /// 拒绝提款
        /// </summary>
        [Obsolete]
        [Display(Name = "拒绝提款")]
        Declined = 9,

        /// <summary>
        /// 系统失败补单
        /// </summary>
        [Display(Name = "补单失败")]
        AutoPaymentManualConfirmedFailed = 10,

        [Display(Name = "失败已关闭")]
        FailedAndClosed = 11,

        [Display(Name = "重试系统支付成功")]
        RetryAutoPaymentSuccessful = 12,

        [Display(Name = "重试补单成功")]
        RetryManualConfirmed = 13
    }
}