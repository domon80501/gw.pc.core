﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Models
{
    public enum MissionReferralRequiredAmountType
    {
        [Display(Name = "存款额")]
        Deposit,
        [Display(Name = "总投注额")]
        TotalBet
    }
}
