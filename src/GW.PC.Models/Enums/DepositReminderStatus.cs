﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum DepositReminderStatus
    {
        [Display(Name = "等待审核")]
        AwaitApproval = 1,
        [Display(Name = "审核成功")]
        Approved,
        [Display(Name = "审核失败")]
        Declined,
        [Display(Name = "自动到账")]
        DepositAutoSucceeded,
        [Display(Name = "支付失败")]
        DepositFailed
    }
}