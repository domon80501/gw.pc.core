﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Models
{
    public enum CustomerMissionStatus
    {
        [Display(Name = "未达成")]
        NotQualified,
        [Display(Name = "可申请")]
        Qualified,
        [Display(Name = "已领取")]
        Applied
    }
}
