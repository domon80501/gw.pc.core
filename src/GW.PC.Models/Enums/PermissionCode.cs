﻿
namespace GW.PC.Models
{
    /// <summary>
    /// The only changes to this enum definition should be addition or attributing.
    /// Name changes or reordering should be strictly prohibited.
    /// </summary>
    public enum PermissionCode
    {
        NotSet = 0,

        #region Merchants
        Merchant_Access,
        Merchant_Create,
        //Merchant_Export,
        Merchant_Detail_Edit,
        Merchant_Detail_Status_Edit,
        #endregion

        #region Orders

        // Merchant Withdrawal
        Order_MerchantWithdrawal_Access,
        Order_MerchantWithdrawal_Query,
        Order_MerchantWithdrawal_Manual,

        // Deposit
        Order_Deposit_Access,
        Order_Deposit_Query,
        Order_Deposit_Manual,


        #region OnlinePayment
        Order_OnlinePaymentDeposit_Access,
        #endregion

        #endregion

        #region Roles
        System_Role_Access,
        System_Role_Create,
        //System_Role_Export,
        System_Role_Detail_Edit,
        System_Role_Detail_Status_Edit,
        #endregion

        #region Users
        System_User_Access,
        System_User_Create,
        //System_User_Export,
        System_User_Detail_Edit,
        System_User_Detail_Status_Edit,
        #endregion

        #region EntityLog
        System_EntityLog_Access,
        #endregion

        #region PaymentProviders
        System_PaymentProvider_Access,
        System_PaymentProvider_Create,
        //System_PaymentProvider_Export,
        System_PaymentProvider_Detail_Edit,
        System_PaymentProvider_Detail_Status_Edit,
        #endregion

        #region PaymentTypes
        System_PaymentType_Access,
        #endregion

        #region PaymentMethods
        System_PaymentMethod_Access,
        #endregion

        #region PaymentPlatforms
        System_PaymentPlatform_Access,
        #endregion

        #region Paymentchannels
        System_Paymentchannel_Access,
        System_Paymentchannel_Create,
        //System_Paymentchannel_Export,
        System_Paymentchannel_Detail_Edit,
        System_Paymentchannel_Detail_Status_Edit,
        #endregion

        #region Banks
        System_Bank_Access,
        System_Bank_Create,
        //System_Bank_Export,
        System_Bank_Detail_Edit,
        System_Bank_Detail_Status_Edit,
        #endregion

        #region PaymentProviderBanks
        System_PaymentProviderBank_Access,
        System_PaymentProviderBank_Create,
        //System_PaymentProviderBank_Export,
        System_PaymentProviderBank_Detail_Edit,
        System_PaymentProviderBank_Detail_Status_Edit,
        #endregion
        
        //#region Customer
        //Customer_Access,
        //Customer_Create,
        //Customer_Export,
        //Customer_Detail_TopTags_Edit,
        //Customer_Detail_Status_Edit,
        //Customer_Detail_Allow_Edit,
        //Customer_Detail_Agent_Edit,
        //Customer_Detail_ContactInfo_View,
        //Customer_Detail_ContactInfo_Bind,
        //Customer_Detail_PersonalInfo_Edit,
        //Customer_Detail_Password_Edit,
        //Customer_Detail_AllowGameLogin_Edit,
        //Customer_Crm_Access,
        //Customer_Crm_FollowUp_Add,
        //Customer_Detail_FollowUp_Edit,
        //Customer_Crm_Delete,
        //Customer_Query_Access,
        //Customer_Query_Export,
        //Customer_Relevance_Access,
        //Customer_Relevance_Create,
        //Customer_Message_Access,
        //Customer_Message_Add,
        //Customer_Message_Export,
        //Customer_Log_Access,
        //Customer_Log_Export,
        //Customer_Complaint_Access,
        //Customer_Complaint_Export,
        //#endregion

        //#region Finance
        //WeChatDeposit_Access,
        //WeChatDeposit_ManualConfirm,
        //WeChatDeposit_Export,
        //AlipayDeposit_Access,
        //AlipayDeposit_ManualConfirm,
        //AlipayDeposit_Export,
        //QQDeposit_Access,
        //QQDeposit_ManualConfirm,
        //QQDeposit_Export,
        //OnlineBankingDeposit_Access,
        //OnlineBankingDeposit_Export,
        //DepositReminder_Access,
        //DepositReminder_Audit,
        //DepositReminder_Export,
        //QuickPay_Access,
        //QuickPay_ManualConfirm,
        //QuickPay_Export,
        //OnlinePaymentDeposit_Access,
        //OnlinePaymentDeposit_ManualConfirm,
        //OnlinePaymentDeposit_Export,
        //PrepaidCardDeposit_Access,
        //PrepaidCardDeposit_ManualConfirm,
        //PrepaidCardDeposit_Export,
        //Customer_Withdrawal_Access,
        //[Obsolete]
        //Customer_Withdrawal_Audit,
        //[Obsolete]
        //Customer_Withdrawal_FinancialOperation,
        //Customer_Withdrawal_Export,
        //Customer_Withdrawal_Access_EN,
        //Customer_Withdrawal_Decline_EN,
        //Customer_Withdrawal_FinancialOperation_EN,
        //Customer_Withdrawal_Export_EN,
        //WithdrawalAudit_Access,
        //[Obsolete]
        //WithdrawalAudit_Audit,
        //WithdrawalAudit_Export,
        //Bonus_Access,
        //Bonus_Create,
        //Bonus_Audit,
        //Bonus_Export,
        //GameTransfer_Access,
        //GameTransfer_ManualConfirm,
        //GameTransfer_Export,
        //Rakeback_Access,
        //Rakeback_Create,
        //Rakeback_Audit,
        //Rakeback_Export,
        //BalanceAdjustment_Access,
        //BalanceAdjustment_Create,
        //BalanceAdjustment_Audit,
        //BalanceAdjustment_Export,
        //Customer_WalletTransaction_Access,
        //Customer_WalletTransaction_Export,
        //#endregion

        //#region Marketing
        //Marketing_Promotion_Access,
        //Marketing_Promotion_Create,
        //Marketing_Promotion_Edit,
        //Marketing_Promotion_Status_Edit,
        //Marketing_Promotion_Export,
        //Marketing_FinancialReport_Access,
        //Marketing_FinancialReport_Export,
        //Marketing_Rakeback_Access,
        //Marketing_Rakeback_Add,
        //Marketing_Rakeback_Edit,
        //Marketing_Rakeback_Audit,
        //Marketing_Rakeback_Export,
        //#endregion

        //#region Agent
        //Agent_Access,
        //Agent_Create,
        //Agent_Status_Edit,
        //Agent_Export,
        //Agent_Detail_Edit,
        //Agent_Password_Edit,
        //Agent_PromotionUrl_Edit,
        //Agent_BankCards_Add,
        //Agent_BankCards_Edit,
        //Agent_Commission_Edit,
        //Agent_Settlement_Access,
        //Agent_Settlement_Add,
        //Agent_Settlement_Export,
        //Agent_Settlement_Audit,
        //Agent_Adjustment_Access,
        //Agent_Adjustment_Add,
        //Agent_Adjustment_Audit,
        //Agent_Adjustment_Export,
        //Agent_Withdrawal_Access,
        //[Obsolete]
        //Agent_Withdrawal_Add,
        //[Obsolete]
        //Agent_Withdrawal_Audit,
        //Agent_Withdrawal_Export,
        //[Obsolete]
        //Agent_Withdrawal_Access_EN,
        //[Obsolete]
        //Agent_Withdrawal_Add_EN,
        //[Obsolete]
        //Agent_Withdrawal_Audit_EN,
        //[Obsolete]
        //Agent_Withdrawal_Export_EN,
        //Agent_CustomerReport_Access,
        //Agent_CustomerReport_Export,
        //Agent_Report_Access,
        //Agent_Report_Export,
        //Agent_CustomerAnalysisReport_Access,
        //Agent_CustomerAnalysisReport_Export,
        //Agent_AnalysisReport_Access,
        //Agent_AnalysisReport_Export,
        //Agent_Promotion_Access,
        //Agent_Promotion_Export,
        //Agent_Promotion_Add,
        //Agent_Promotion_Delete,
        //Agent_Promotion_Edit,
        //#endregion

        //#region System
        //System_PaymentChannel_Access,
        //System_PaymentChannel_Add,
        //System_PaymentChannel_Audit,
        //System_PaymentChannel_Edit,
        //System_PaymentChannel_Export,
        //System_PaymentChannel_Credentials_View,
        //System_PaymentProvider_Access,
        //System_PaymentProvider_Add,
        //System_PaymentProvider_Audit,
        //System_PaymentProvider_Edit,
        //System_PaymentProvider_Export,
        //System_PaymentProviderBank_Access,
        //System_PaymentProviderBank_Add,
        //System_PaymentProviderBank_Audit,
        //System_PaymentProviderBank_Edit,
        //System_PaymentProviderBank_Export,
        //System_Bank_Access,
        //System_Bank_Add,
        //System_Bank_Edit,
        //System_Bank_Export,
        //System_User_Access,
        //System_User_Add,
        //System_User_Audit,
        //System_User_Edit,
        //System_User_Export,
        //System_Role_Access,
        //System_Role_Add,
        //System_Role_Audit,
        //System_Role_Edit,
        //System_Role_Export,
        //System_Permission_Access,
        //System_Game_Access,
        //System_Game_Add,
        //System_Game_Status_Edit,
        //System_Game_AllowTransfer_Edit,
        //System_Game_Edit,
        //System_Game_Export,
        //System_Tag_Access,
        //System_Tag_Add,
        //System_Tag_Edit,
        //System_Tag_Status_Edit,
        //System_Tag_Export,
        //System_Announcement_Access,
        //System_Announcement_Add,
        //System_Announcement_Audit,
        //System_Announcement_Edit,
        //System_Announcement_Export,
        //System_UserLog_Access,
        //System_UserLog_Export,
        //System_Alarm_Access,
        //System_Alarm_AllAlarms,
        //#endregion

        //Customer_Withdrawal_Audit_Approve,
        //Customer_Withdrawal_Audit_Decline,
        ///// <summary>
        ///// 人工支付成功
        ///// </summary>
        //Customer_Withdrawal_Finance_ManualSucceed,
        ///// <summary>
        ///// 补单
        ///// </summary>
        //Customer_Withdrawal_Finance_ManualConfirm,
        //Customer_Withdrawal_Finance_Decline,

        //Customer_GameData_Access,
        //Customer_GameData_Create,
        //Customer_GameData_Discard,

        //Agent_Withdrawal_Audit_Approve,
        //Agent_Withdrawal_Audit_Decline,
        //Agent_Withdrawal_Finance_ManualSucceed,
        //Agent_Withdrawal_Finance_ManualConfirm,
        //Agent_Withdrawal_Finance_Decline,

        //OnlineToBankCardDeposit_Access,
        //OnlineToBankCardDeposit_Export,

        //OnlineToBankCardDepositReminder_Access,
        //OnlineToBankCardDepositReminder_Audit,
        //OnlineToBankCardDepositReminder_Export,

        //AllDeposit_Access,
        //AllDeposit_Export,

        //PaymentChannel_Stats_Access,
        //PaymentChannelTransfer_Access,
        //PaymentChannelTransfer_Add,
        //PaymentChannelTransfer_Export,
        //PaymentChannelAdjustment_Access,
        //PaymentChannelAdjustment_Add,
        //PaymentChannelAdjustment_Edit,
        //PaymentChannelAdjustment_Export,
        //PaymentChannelTransaction_Access,
        //PaymentChannelTransaction_Export,

        //InternalDeposit_Access,
        //InternalDeposit_Add,
        //InternalDeposit_Export,
        //InternalDeposit_ManualConfirm,

        //PaymentChannel_Discard,

        //OnlineBankingDeposit_ManualConfirm,
        //OnlineToBankCardDeposit_ManualConfirm,
        //Customer_Withdrawal_Retry,
        //Agent_Withdrawal_Retry,
        //Agent_Withdrawal_ManualConfirm,

        //JDWalletDeposit_Access,
        //JDWalletDeposit_ManualConfirm,
        //JDWalletDeposit_Export,

        //Marketing_Mission_Access,
        //Marketing_Mission_Edit,
        //System_PromotionCategory_Access,
        //System_PromotionCategory_Edit,
    }
}
