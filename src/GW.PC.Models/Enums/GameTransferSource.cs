﻿namespace GW.PC.Models
{
    public enum GameTransferSource
    {
        Unknown,
        CustomerWithdrawalRefund,
        WeChatDeposit,
        AlipayDeposit,
        OnlinePaymentDeposit,
        PrepaidCardDeposit,
        QQWalletDeposit,
        QuickPayDeposit,
        CustomerWithdrawal,
        BalanceAdjustmentApproved,
        BonusApproved,
        RakebackApproved,
        OnlineBankingDeposit,
        OnlineTobankCardDeposit,
        JDWalletDeposit,
    }
}
