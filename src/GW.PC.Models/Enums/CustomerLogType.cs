﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum CustomerLogType
    {
        [Display(Name = "登录网页")]
        Login = 1,
        [Display(Name = "存款请求")]
        DepositRequest,
        [Display(Name = "游戏转账")]
        FundTransfer,
        [Display(Name = "提款请求")]
        WithdrawRequest,
        [Display(Name = "更新密码")]
        UpdatePassword,
        [Display(Name = "更新资料")]
        UpdateProfile,
        [Display(Name = "客户留言")]
        SentMessage,
        [Display(Name = "登录游戏")]
        GameLogin,
        [Display(Name = "查看记录")]
        View,
        [Display(Name = "页面浏览")]
        Browse,
        [Display(Name = "阅读消息")]
        ReadMessage,
        [Display(Name = "客户注册")]
        Register,
        [Display(Name = "提款绑定")]
        WithdrawBinding,
        [Display(Name = "退出登录")]
        Logout
    }
}