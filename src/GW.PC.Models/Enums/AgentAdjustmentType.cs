﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum AgentAdjustmentType
    {
        [Display(Name = "钱包调整")]
        Balance,
        [Display(Name = "结算调整")]
        Settlement
    }
}