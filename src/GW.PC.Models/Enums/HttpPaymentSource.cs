﻿namespace GW.PC.Models
{
    public enum HttpPaymentSource
    {
        WeChatDeposit,
        AlipayDeposit,
        OnlinePaymentDeposit,
        PrepaidCardDeposit,
        QQWalletDeposit,
        QuickPayDeposit,
        InternalDeposit,
        OnlineBankingDeposit,
        OnlineTobankCardDeposit,
        CustomerWithdrawal,
        PaymentChannelTransfer,
        PaymentChannelAdjustment,
        AgentWithdrawal,
        JDWalletDeposit,
        MerchantWithdrawal,
    }
}
