﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum EntityLogTargetType
    {
        Unknown,
        [Display(Name = "代理结算")]
        AgentSettlement,
        [Display(Name = "游戏转账")]
        GameTransfer,
        [Display(Name = "客户提款")]
        CustomerWithdrawal,
        [Display(Name = "微信存款")]
        WeChatDeposit,
        [Display(Name = "支付宝存款")]
        AlipayDeposit,
        [Display(Name = "在线存款")]
        OnlinePaymentDeposit,
        [Display(Name = "网银存款")]
        OnlineBankingDeposit,
        [Display(Name = "支付宝转卡存款")]
        OnlineToBankCardDeposit,
        [Display(Name = "充值卡存款")]
        PrepaidCardDeposit,
        [Display(Name = "QQ钱包存款")]
        QQWalletDeposit,
        [Display(Name = "快捷存款")]
        QuickPayDeposit,
        [Display(Name = "灌钱存款")]
        InternalDeposit,
        [Display(Name = "资金通道互转")]
        PaymentChannelTransfer,
        [Display(Name = "资金通道微调")]
        PaymentChannelAdjustment,
        [Display(Name = "代理提款")]
        AgentWithdrawal,
        [Display(Name = "京东钱包存款")]
        JDWalletDeposit,
        [Display(Name = "商户提款")]
        MerchantWithdrawal,
    }
}
