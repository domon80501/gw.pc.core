﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum FollowUpType
    {
        [Display(Name = "投诉")]
        Complaint = 1,
        [Display(Name = "电话")]
        Telephone,
        [Display(Name = "需求")]
        Requirement
    }
}