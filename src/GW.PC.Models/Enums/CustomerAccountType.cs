﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum CustomerAccountType
    {
        [Display(Name = "正常")]
        Normal = 1,
        [Display(Name = "测试")]
        Testing = 2
    }
}