﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum PaymentChannelAdjustmentType
    {
        [Display(Name = "手动")]
        Manual,
        [Display(Name = "代付")]
        Auto
    }
}
