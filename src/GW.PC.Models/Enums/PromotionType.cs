﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum PromotionType
    {
        [Display(Name = "玩家手动")]
        CustomerApply,
        [Display(Name = "用户新增")]
        ManualAdd,
        [Display(Name = "系统自动")]
        SystemAuto
    }
}
