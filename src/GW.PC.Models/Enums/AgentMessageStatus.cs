﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum AgentMessageStatus
    {
        /// <summary>
        /// Used to store data from old system ONLY!
        /// </summary>
        [Display(Name = "无")]
        None,
        [Display(Name = "未读")]
        Unread = 1,
        [Display(Name = "已读")]
        Read,
    }
}