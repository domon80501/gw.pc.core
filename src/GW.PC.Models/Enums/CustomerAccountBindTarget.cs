﻿namespace GW.PC.Models
{
    public enum CustomerAccountBindTarget
    {
        None,
        Mobile = 1,
        Email = 2,
        QQ = 3,
        WeChat = 4
    }
}