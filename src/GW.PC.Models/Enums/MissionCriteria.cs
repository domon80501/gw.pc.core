﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum MissionCriteria
    {
        [Display(Name = "注册时间")]
        RegisteredAt = 1,
        [Display(Name = "存款额")]
        DepositAmount,
        [Display(Name = "有效投注额")]
        ValidBetAmount,
        [Display(Name = "总投注额")]
        TotalBetAmount,
        [Display(Name = "输赢")]
        ProfitAmount,
        [Display(Name = "参加次数")]
        ParticipationCount,
        [Display(Name = "推荐客户")]
        Referral,
        [Display(Name = "游戏及平台")]
        Game,
        [Display(Name = "客户等级")]
        CustomerGrade
    }
}