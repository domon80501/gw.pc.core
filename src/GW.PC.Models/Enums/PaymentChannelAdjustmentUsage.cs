﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum PaymentChannelAdjustmentUsage
    {
        [Display(Name = "调入调出")]
        Transfer = 1,
        [Display(Name = "置换货币")]
        CurrencyExchange,
        [Display(Name = "平台费用")]
        PlatformCost,
        [Display(Name = "运维费用")]
        OperationCost,
        [Display(Name = "市场费用")]
        MarketingCost,
        [Display(Name = "财务费用")]
        FinanceCost,
        [Display(Name = "薪资费用")]
        SalaryCost,
        [Display(Name = "其它")]
        Others = 99
    }
}
