﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum CustomerRelevanceType
    {
        [Display(Name = "电话")]
        Mobile = 1,
        [Display(Name = "姓名")]
        Name,
        [Display(Name = "注册IP")]
        RegisteredIP,
        [Display(Name = "转账IP")]
        TransferIP,
        [Display(Name = "取款IP")]
        WithdrawIP,
        [Display(Name = "存款IP")]
        DepositIP,
        [Display(Name = "邮箱")]
        Email,
        [Display(Name = "QQ")]
        QQ
    }
}