﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum PaymentChannelTransactionType
    {
        [Display(Name = "商户存款")]
        MerchantDeposit = 1,
        [Display(Name = "商户提款")]
        MerchantWithdrawal,
        [Display(Name = "通道互转")]
        PaymentChannelTransfer,
        [Display(Name = "通道微调")]
        PaymentChannelAdjustment,
        [Display(Name = "灌钱")]
        InternalDeposit,
        [Display(Name = "客户存款")]
        CustomerDeposit,
        [Display(Name = "客户提款")]
        CustomerWithdrawal,
    }
}