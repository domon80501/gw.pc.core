﻿namespace GW.PC.Models
{
    public enum MerchantAccountBindTarget
    {
        None,
        Mobile = 1,
        Email = 2,
        QQ = 3,
        WeChat = 4
    }
}