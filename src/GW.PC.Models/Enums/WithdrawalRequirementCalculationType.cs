﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Models
{
    public enum WithdrawalRequirementCalculationType
    {
        [Display(Name = "红利*倍数")]
        BonusMultiplier,
        [Display(Name = "(红利*本金)*倍数")]
        BonusPrincipalMultiplier,
        [Display(Name = "本金*倍数")]
        PrincipalMultiplier
    }
}
