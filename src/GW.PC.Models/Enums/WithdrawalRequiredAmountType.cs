﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Models
{
    public enum WithdrawalRequiredAmountType
    {
        [Display(Name = "总投注额")]
        TotalBet,
        [Display(Name = "有效投注额")]
        ValidBet,
        [Display(Name = "输赢")]
        Profit
    }
}
