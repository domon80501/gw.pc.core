﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum RakebackType
    {
        [Display(Name = "提前")]
        Advanced = 1,
        [Display(Name = "正常")]
        Regular,
        [Display(Name = "额外")]
        Split
    }
}
