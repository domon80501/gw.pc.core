﻿namespace GW.PC.Models
{
    public enum GameTransactionType
    {
        Win = 1,
        BetRefund,
        Bet
    }
}
