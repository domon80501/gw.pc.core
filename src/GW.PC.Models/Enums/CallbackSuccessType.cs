﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum CallbackSuccessType
    {
        success = 1,
        SUCCESS = 2,
        Success = 3,
        ok = 4,
        OK = 5,
        Ok = 6,
        JBP = 7,
    }
}