﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum AgentMessageType
    {
        [Display(Name = "系统通知")]
        SystemNotification = 1
    }
}