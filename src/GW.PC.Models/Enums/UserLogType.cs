﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum UserLogType
    {
        [Display(Name = "登录系统")]
        Login = 1,
        [Display(Name = "查看记录")]
        View,
        [Display(Name = "进行审核")]
        Audit,
        [Display(Name = "提款支付")]
        Withdraw,
        [Display(Name = "删除记录")]
        Delete,
        [Display(Name = "新增记录")]
        Add,
        [Display(Name = "取消操作")]
        Cancel,
        [Display(Name = "查询记录")]
        Search,
        [Display(Name = "修改记录")]
        Edit,
        [Display(Name = "修改权限")]
        EditPermission,
        [Display(Name = "停用操作")]
        Disable,
        [Display(Name = "启用操作")]
        Enable,
        [Display(Name = "作废操作")]
        Deprecate,
        [Display(Name = "设置操作")]
        Configure,
        [Display(Name = "退出系统")]
        Logout
    }
}