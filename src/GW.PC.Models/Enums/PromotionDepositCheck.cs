﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum PromotionDepositCheck
    {
        [Display(Name = "无")]
        None,
        [Display(Name = "累积")]
        Accumulate,
        [Display(Name = "大于等于")]
        LargerThanOrEqual
    }
}
