﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum WalletTransactionType
    {
        [Display(Name = "无")]
        Unknown = 0,
        [Display(Name = "微信存款")]
        WeChatDeposit,
        [Display(Name = "支付宝存款")]
        AlipayDeposit,
        [Display(Name = "网银存款")]
        OnlineBankingDeposit,
        [Display(Name = "在线存款")]
        OnlinePaymentDeposit,
        [Display(Name = "充值卡存款")]
        PrepaidCardDeposit,
        [Display(Name = "QQ钱包存款")]
        QQWalletDeposit,
        [Display(Name = "快捷支付存款")]
        QuickPayDeposit,
        [Display(Name = "转入游戏")]
        GameDeposit,
        [Display(Name = "转出游戏")]
        GameWithdrawal,
        [Display(Name = "账户微调")]
        BalanceAdjustment,
        [Display(Name = "提款")]
        CustomerWithdrawal,
        [Display(Name = "红利")]
        Bonus,
        [Display(Name = "返水")]
        Rakeback,
        [Display(Name = "手续费返还")]
        OnlineToBankCardCommissionRefund,
        [Display(Name = "支付转卡存款")]
        OnlineToBankCardDeposit,
        [Display(Name = "代理提款")]
        AgentWithdrawal,
        [Display(Name = "代理结算")]
        AgentSettlement,
        [Display(Name = "关闭客户提款返还")]
        CustomerWithdrawalRefund,
        [Display(Name = "关闭代理提款返还")]
        AgentWithdrawalRefund,
        AgentAdjustment,
        [Display(Name = "京东钱包存款")]
        JDWalletDeposit,
        [Display(Name = "游戏投注")]
        GameBet,
        [Display(Name = "游戏奖金")]
        GameWin,
        [Display(Name = "游戏投注退回")]
        GameBetRefund,
    }
}