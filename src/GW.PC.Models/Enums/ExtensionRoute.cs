﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum ExtensionRoute
    {
        Default = 1,
        JBP = 2,
        CSF = 3,
    }
}