﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum FinancialReportType
    {
        [Display(Name = "日报表")]
        Day = 1,
        [Display(Name = "月报表")]
        Month,
        [Display(Name = "年报表")]
        Year
    }
}