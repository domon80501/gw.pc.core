
namespace GW.PC.Models.Enums
{
    public enum SystemOption
    {
        PaymentType = 1,
        PaymentMethod = 2,
        EntityStatus = 3,
        PaymentPlatform = 4,
        DepositStatus = 5,
        EntityLogTargetType = 6,
        ExtensionRoute = 7,
        CallbackSuccessType = 8,
        WithdrawalStatus = 9,

        Bank = 98,
        Merchant = 99,
        PaymentProvider = 100,
        WithdrawalPaymentChannel = 101,
        PaymentChannel = 102,
        Role = 103,
    }
}
