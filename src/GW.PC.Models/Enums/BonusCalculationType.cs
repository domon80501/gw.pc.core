﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum BonusCalculationType
    {
        [Display(Name = "百分比")]
        Percentage,
        [Display(Name = "定额")]
        FixedAmount,
        [Display(Name = "自定义")]
        Custom = 99
    }
}
