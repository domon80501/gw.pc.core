﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum PaymentChannelTransferStatus
    {
        [Display(Name = "创建成功")]
        Created = 1,
        [Display(Name = "系统支付中")]
        AutoPaymentInProgress,
        [Display(Name = "系统支付成功")]
        AutoPaymentSuccessful,
        [Display(Name = "系统支付失败")]
        AutoPaymentFailed,
        [Display(Name = "补单成功")]
        ManualConfirmed,
        [Display(Name = "补单失败")]
        AutoPaymentManualConfirmedFailed,
    }
}
