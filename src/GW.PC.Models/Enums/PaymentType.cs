﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public enum PaymentType
    {
        [Display(Name = "存款")]
        Deposit = 1,
        [Display(Name = "提款")]
        Withdrawal,
        [Display(Name = "中转")]
        InternalTransfer,
        [Display(Name = "费用")]
        Cost,
        [Display(Name = "存储")]
        Repository,
        [Display(Name = "灌钱")]
        InternalDeposit
    }
}
