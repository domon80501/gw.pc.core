﻿using System;

namespace GW.PC.Models
{
    public class CustomerLT : CustomerGameBase
    {
        public string LoginToken { get; set; }
        public DateTime? LoginTokenExpiredAt { get; set; }
    }
}