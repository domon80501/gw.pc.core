﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public class FinancialReport : EntityBase, ICreatedAt
    {
        public FinancialReportType Type { get; set; }

        public DateTime Date { get; set; }
        public decimal DepositAmount { get; set; }
        public decimal WithdrawalAmount { get; set; }
        public decimal Fee { get; set; }
        public decimal Bonus { get; set; }
        public decimal Rakeback { get; set; }
        public decimal Profit { get; set; }
        public decimal WeChatDepositAmount { get; set; }
        public int WeChatSuccessfulCount { get; set; }
        public int WeChatTotalCount { get; set; }
        public decimal AlipayDepositAmount { get; set; }
        public int AlipaySuccessfulCount { get; set; }
        public int AlipayTotalCount { get; set; }
        public decimal OnlinePaymentDepositAmount { get; set; }
        public int OnlinePaymentSuccessfulCount { get; set; }
        public int OnlinePaymentTotalCount { get; set; }
        public decimal OnlineBankingDepositAmount { get; set; }
        public int OnlineBankingSuccessfulCount { get; set; }
        public int OnlineBankingTotalCount { get; set; }
        public decimal PrepardCardDepositAmount { get; set; }
        public int PrepardCardSuccessfulCount { get; set; }
        public int PrepardCardTotalCount { get; set; }
        public decimal QuickPayDepositAmount { get; set; }
        public int QuickPaySuccessfulCount { get; set; }
        public int QuickPayTotalCount { get; set; }
        public decimal QQWalletDepositAmount { get; set; }
        public int QQWalletSuccessfulCount { get; set; }
        public int QQWalletTotalCount { get; set; }
        public decimal JDWalletDepositAmount { get; set; }
        public int JDWalletSuccessfulCount { get; set; }
        public int JDWalletTotalCount { get; set; }
        public DateTime CreatedAt { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
