﻿using System;

namespace GW.PC.Models
{
    /// <summary>
    /// Represents all types of customer wallet balance changes. Refer to the WalletTransactionType
    /// enum for details. All business-related information specific to each type of
    /// transaction is defined in individual models, GameTransfer, Bonus, etc.
    /// </summary>
    public class WalletTransaction : EntityBase, ICreatedAt, IUsername
    {
        public string Username { get; set; }
        public WalletTransactionType Type { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public DateTime CreatedAt { get; set; }
        public int? SourceId { get; set; }
    }
}
