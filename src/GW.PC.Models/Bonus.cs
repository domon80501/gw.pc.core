﻿namespace GW.PC.Models
{
    /// <summary>
    /// 红利
    /// </summary>
    public class Bonus : AuditableEntity, IUsername
    {
        public string Username { get; set; }
        public decimal? DepositAmount { get; set; }
        public string Notes { get; set; }
        public decimal BonusAmount { get; set; }
        /// <summary>
        /// 提款投注倍数
        /// </summary>
        public int? WithdrawalBetMultiplier { get; set; }
        public AuditStatus Status { get; set; }
        public string SystemOrderNumber { get; set; }

        public int? MissionId { get; set; }

        public virtual Mission Mission { get; set; }
    }
}
