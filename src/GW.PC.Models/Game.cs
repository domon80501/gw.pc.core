﻿using Newtonsoft.Json;
using System;

namespace GW.PC.Models
{
    public class Game : CreatableEntity, IEntityStatus
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public EntityStatus Status { get; set; }
        public string Arguments { get; set; }
        /// <summary>
        /// AP在各游戏商的账号前缀，尽量避免和其它网站在游戏商账号冲突，
        /// 但并不能100%保证。
        /// </summary>
        public string UsernamePrefix { get; set; }
        /// <summary>
        /// 返水计算周期开始时间点
        /// </summary>
        public DateTime? RakebackAt { get; set; }
        public bool AllowTransfer { get; set; }

        public T ParseArgument<T>()
        {
            return JsonConvert.DeserializeObject<T>(Arguments);
        }
    }
}
