﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Models
{
    public class CustomerMission : EntityBase
    {
        public string Username { get; set; }
        public string CurrentValue { get; set; }
        public string RequiredValue { get; set; }
        public CustomerMissionStatus Status { get; set; }
        public DateTime? AppliedAt { get; set; }

        public int MissionId { get; set; }

        public virtual Mission Mission { get; set; }
    }
}
