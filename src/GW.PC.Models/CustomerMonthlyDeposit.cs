﻿using System;

namespace GW.PC.Models
{
    public class CustomerMonthlyDeposit : EntityBase, IUsername, ICreatedAt
    {
        public string Username { get; set; }
        public DateTime Month { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
    }
}
