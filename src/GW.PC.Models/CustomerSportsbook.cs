﻿using System;

namespace GW.PC.Models
{
    /// <summary>
    /// 表示玩家最后一次登录体育游戏的相关信息
    /// </summary>
    public class CustomerSportsbook : CustomerGameBase
    {
        /// <summary>
        /// 每次登录体育游戏生成的token，体育会回调AP服务验证该token。
        /// </summary>
        public string LoginToken { get; set; }

        /// <summary>
        /// 登录token有效期，为生成token时的服务器时间+20分钟。
        /// </summary>
        public DateTime? LoginTokenExpiredAt { get; set; }
    }
}
