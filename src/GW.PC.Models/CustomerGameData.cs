﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GW.PC.Models
{
    [Table("CustomerGameData")]
    public class CustomerGameData : CreatableEntity, IEntityStatus
    {
        public string Username { get; set; }
        public DateTime PeriodStartsAt { get; set; }
        public DateTime PeriodEndsAt { get; set; }
        public decimal TotalBetAmount { get; set; }
        public decimal ProfitAmount { get; set; }
        public EntityStatus Status { get; set; }
        public DateTime? DiscardedAt { get; set; }
        public decimal ValidBetAmount { get; set; }

        public int GameId { get; set; }
        public int? DiscardedById { get; set; }

        public virtual Game Game { get; set; }
        public virtual User DiscardedBy { get; set; }
    }
}
