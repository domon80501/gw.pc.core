﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    /// <summary>
    /// Represents a customer wallet.
    /// </summary>
    public class Wallet : CreatableEntity, IUsername
    {
        public string Username { get; set; }
        public decimal Balance { get; set; }
        public decimal FrozenAmount { get; set; }
        public DateTime? FirstDepositAt { get; set; }
        public DateTime? LastDepositAt { get; set; }
        public DateTime? FirstWithdrawalAt { get; set; }
        public DateTime? LastWithdrawalAt { get; set; }
        public int TotalDepositCount { get; set; }
        public decimal TotalDepositAmount { get; set; }
        public int TotalWithdrawCount { get; set; }
        public decimal TotalWithdrawAmount { get; set; }
        public decimal TotalBonusAmount { get; set; }
        public decimal TotalRakebackAmount { get; set; }
        public DateTime? AmountLastUpdatedAt { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ICollection<BankCard> BankCards { get; set; }
    }
}