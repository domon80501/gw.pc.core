﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Models
{
    public class ReferralRecognition : EntityBase, ICreatedAt
    {
        public string ClientIP { get; set; }
        public string Device { get; set; }
        public string AgentPromotionCode { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public string Url { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
