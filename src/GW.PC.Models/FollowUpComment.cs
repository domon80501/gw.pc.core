﻿namespace GW.PC.Models
{
    public class FollowUpComment : CreatableEntity
    {
        public string Content { get; set; }
        public string AttachmentFileNames { get; set; }

        public int FollowUpId { get; set; }

        public virtual FollowUp FollowUp { get; set; }
    }
}
