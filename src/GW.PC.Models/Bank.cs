﻿using System.Collections.Generic;

namespace GW.PC.Models
{
    /// <summary>
    /// Represents the banks and prepaid card operator that are used for 
    /// customer payment(can't find an appropriate class name to mean the both).
    /// </summary>
    public class Bank : CreatableEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        /// <summary>
        /// 用于网银存款方式中，将客户跳转到相应页面登录网银。
        /// </summary>
        public string Url { get; set; }

        public virtual ICollection<PaymentProviderBank> PaymentProviderBanks { get; set; }
    }
}
