﻿
using GW.PC.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public class PaymentChannelAdjustment : CreatableEntity
    {
        public PaymentChannelAdjustmentUsage Usage { get; set; }
        public decimal Amount { get; set; }
        public decimal Commission { get; set; }
        public string Notes { get; set; }
        public PaymentChannelAdjustmentStatus Status { get; set; }
        public string SystemOrderNumber { get; set; }
        public string MerchantOrderNumber { get; set; }
        public string PayeeCardNumber { get; set; }
        public string PayeeName { get; set; }
        public int? PayeeBankId { get; set; }
        public DateTime? PaidAt { get; set; }
        public DateTime? ManualConfirmedAt { get; set; }
        public string PaymentNotes { get; set; }
        public PaymentChannelAdjustmentType Type { get; set; }

        public int PaymentChannelId { get; set; }
        public int? ManualConfirmedById { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual PaymentChannel PaymentChannel { get; set; }
        public virtual User ManualConfirmedBy { get; set; }
        public virtual Bank PayeeBank { get; set; }

        public HttpPayment BuildHttpPayment() => new HttpPayment
        {
            CreatedAt = DateTime.Now.ToE8(),
            Source = HttpPaymentSource.PaymentChannelAdjustment,
            SourceId = Id
        };
    }
}
