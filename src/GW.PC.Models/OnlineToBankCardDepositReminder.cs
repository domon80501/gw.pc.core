﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public class OnlineToBankCardDepositReminder : EntityBase, ICreatedAt, IAuditable
    {
        public string Username { get; set; }
        public string PayerName { get; set; }
        public decimal Amount { get; set; }
        public string PayeeCardNumber { get; set; }
        public DateTime DepositedAt { get; set; }
        public DepositReminderStatus Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? AuditedAt { get; set; }
        public int? AuditedById { get; set; }

        public int OnlineToBankCardDepositId { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual OnlineToBankCardDeposit OnlineToBankCardDeposit { get; set; }
        public virtual User AuditedBy { get; set; }
    }
}
