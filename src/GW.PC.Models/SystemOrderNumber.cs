﻿namespace GW.PC.Models
{
    public class SystemOrderNumber : EntityBase
    {
        public string Value { get; set; }
    }
}
