﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;

namespace GW.PC.Models
{
    public class QuickPayDeposit : Deposit
    {
        public override PaymentMethod PaymentMethod => PaymentMethod.QuickPay;

        public override EntityLogTargetType EntityLogTargetType => EntityLogTargetType.QuickPayDeposit;
        public override HttpPayment BuildHttpPayment()
        {
            var payment = base.BuildHttpPayment();

            payment.Source = HttpPaymentSource.QuickPayDeposit;

            return payment;
        }
    }
}
