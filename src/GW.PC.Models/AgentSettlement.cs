﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public class AgentSettlement : AuditableEntity
    {
        public string Username { get; set; }
        public DateTime PeriodStartsAt { get; set; }
        public DateTime PeriodEndsAt { get; set; }
        public decimal DirectCommission { get; set; }
        public decimal SubordinateCommissions { get; set; }
        public string Notes { get; set; }
        public AgentSettlementStatus Status { get; set; }
        public decimal? SettlementPercentage { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ICollection<SubordinateAgentSettlement> SubordinateAgentSettlements { get; set; }
    }
}
