﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;

namespace GW.PC.Models
{
    public class PrepaidCardDeposit : Deposit
    {
        public string CardSerialNumber { get; set; }
        public string CardPassword { get; set; }
        public string CardStatus { get; set; }

        public int PaymentProviderBankId { get; set; }

        public virtual PaymentProviderBank PaymentProviderBank { get; set; }

        public override PaymentMethod PaymentMethod => PaymentMethod.PrepaidCard;

        public override EntityLogTargetType EntityLogTargetType => EntityLogTargetType.PrepaidCardDeposit;
        public override HttpPayment BuildHttpPayment()
        {
            var payment = base.BuildHttpPayment();

            payment.Source = HttpPaymentSource.PrepaidCardDeposit;

            return payment;
        }
    }
}
