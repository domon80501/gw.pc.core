﻿using System;

namespace GW.PC.Models
{
    public class CustomerLogin : EntityBase
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public DateTime? ExpiresAt { get; set; }
    }
}
