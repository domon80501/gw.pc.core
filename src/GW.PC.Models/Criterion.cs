﻿namespace GW.PC.Models
{
    public class Criterion : CreatableEntity
    {
        public string Name { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public bool IsDatabaseField { get; set; }
        public string DataType { get; set; }
        public CriterionComparisonOperator ComparisonOperator { get; set; }
        public string Value { get; set; }
        public string BuilderName { get; set; }
    }
}
