﻿namespace GW.PC.Models
{
    public class DirectAgentSettlement : AuditableEntity, IUsername
    {
        public string Username { get; set; }
        public decimal BaseAmount { get; set; }
        public decimal DepositFeeAmount { get; set; }
        public decimal WithdrawalFeeAmount { get; set; }
        public decimal PlatformFeeAmount { get; set; }
        public decimal RakeAmount { get; set; }
        public decimal BonusAmount { get; set; }
        public decimal AdjustmentAmount { get; set; }
        public decimal TotalCommission { get; set; }

        public int SettlementId { get; set; }

        public AgentSettlement Settlement { get; set; }
    }
}
