﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;

namespace GW.PC.Models
{
    public class AlipayDeposit : Deposit
    {
        public override PaymentMethod PaymentMethod => PaymentMethod.Alipay;        
        public override EntityLogTargetType EntityLogTargetType => EntityLogTargetType.AlipayDeposit;
        //public override Merchant Merchant { get => base.Merchant; set => base.Merchant = value; }

        public override HttpPayment BuildHttpPayment()
        {
            var payment = base.BuildHttpPayment();

            payment.Source = HttpPaymentSource.AlipayDeposit;

            return payment;
        }
    }
}
