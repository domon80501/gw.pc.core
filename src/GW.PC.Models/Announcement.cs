﻿using System;

namespace GW.PC.Models
{
    /// <summary>
    /// 网站公告
    /// </summary>
    public class Announcement : CreatableEntity, IEntityStatus
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime? ExpiresAt { get; set; }
        public EntityStatus Status { get; set; }
    }
}
