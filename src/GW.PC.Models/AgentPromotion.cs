﻿namespace GW.PC.Models
{
    public class AgentPromotion : CreatableEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string AttachmentFileNames { get; set; }
        public string Orders { get; set; }
    }
}
