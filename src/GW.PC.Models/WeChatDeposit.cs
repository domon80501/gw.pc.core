﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;

namespace GW.PC.Models
{
    public class WeChatDeposit : Deposit
    {
        public override PaymentMethod PaymentMethod => PaymentMethod.WeChat;
        public override EntityLogTargetType EntityLogTargetType => EntityLogTargetType.WeChatDeposit;
        public override HttpPayment BuildHttpPayment()
        {
            var payment = base.BuildHttpPayment();

            payment.Source = HttpPaymentSource.WeChatDeposit;

            return payment;
        }
    }
}
