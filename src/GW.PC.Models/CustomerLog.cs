﻿using System;

namespace GW.PC.Models
{
    public class CustomerLog : EntityBase, IUsername, ICreatedAt
    {
        public string Username { get; set; }
        public CustomerLogType Type { get; set; }
        public string IP { get; set; }
        public string Address { get; set; }
        public string Url { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
