﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;

namespace GW.PC.Models
{
    public class OnlineToBankCardDeposit : Deposit
    {
        public string PayerName { get; set; }
        public string PayeeName { get; set; }
        public string PayeeCardNumber { get; set; }
        public string Remark { get; set; }
        public decimal CommissionRefund { get; set; }

        public override PaymentMethod PaymentMethod => PaymentMethod.OnlineToBankCard;
        public override EntityLogTargetType EntityLogTargetType => EntityLogTargetType.OnlineToBankCardDeposit;
        public override HttpPayment BuildHttpPayment()
        {
            var payment = base.BuildHttpPayment();

            payment.Source = HttpPaymentSource.OnlineTobankCardDeposit;

            return payment;
        }
    }
}
