﻿namespace GW.PC.Models
{
    /// <summary>
    /// 表示玩家在AG游戏的相关信息
    /// </summary>
    public class CustomerAG : CustomerGameBase
    {
        /// <summary>
        /// AG游戏登录要求使用第一次登录游戏时设置的密码（蠢!），所以要保留初始密码。
        /// 客户的前台登录密码修改时，这个值永远保持不变。
        /// </summary>
        public string Password { get; set; }
    }
}
