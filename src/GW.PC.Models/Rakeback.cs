﻿using System;

namespace GW.PC.Models
{
    public class Rakeback : AuditableEntity, IUsername
    {
        public string Username { get; set; }
        public RakebackType Type { get; set; }
        public decimal ValidBetAmount { get; set; }
        public decimal ProfitAmount { get; set; }
        public decimal RakebackAmount { get; set; }
        public decimal RakebackPercentage { get; set; }
        public DateTime PeriodStartsAt { get; set; }
        public DateTime PeriodEndsAt { get; set; }
        /// <summary>
        /// Nullable to support old data.
        /// </summary>
        public int? GameId { get; set; }
        public AuditStatus Status { get; set; }

        public virtual Game Game { get; set; }
    }
}
