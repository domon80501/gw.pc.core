﻿using GW.PC.Core;

namespace GW.PC.Models
{
    public class PaymentProviderBank : CreatableEntity, IEntityStatus
    {
        /// <summary>
        /// 支付服务商定义的银行代码
        /// </summary>
        public string Code { get; set; }
        public string Name { get; set; }
        public PaymentType PaymentType { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public int Order { get; set; }
        public EntityStatus Status { get; set; }

        public int PaymentProviderId { get; set; }
        public int BankId { get; set; }
        public int MerchantId { get; set; }

        public virtual PaymentProvider PaymentProvider { get; set; }
        public virtual Bank Bank { get; set; }
        public virtual Merchant Merchant { get; set; }
    }
}
