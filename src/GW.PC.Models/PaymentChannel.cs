﻿using GW.PC.Core;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace GW.PC.Models
{
    /// <summary>
    /// 表示各条支付渠道。一个支付服务商可能有多条支付渠道，用于不同支付方式。
    /// </summary>
    public class PaymentChannel : CreatableEntity, IEntityStatus
    {
        public string Name { get; set; }
        /// <summary>
        /// 用于收款还是出款
        /// </summary>
        public PaymentType PaymentType { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        /// <summary>
        /// 从本次累计周期开始时间到现在为止的收款总额
        /// </summary>
        public decimal CurrentPeriodDepositAmount { get; set; }
        /// <summary>
        /// 总存款上限
        /// </summary>
        public decimal TotalLimit { get; set; }
        /// <summary>
        /// 单次最低限额
        /// </summary>
        public decimal MinAmount { get; set; }
        /// <summary>
        /// 单次最高限额
        /// </summary>
        public decimal MaxAmount { get; set; }
        /// <summary>
        /// 商户在支付方的唯一标识
        /// </summary>
        public string MerchantUsername { get; set; }
        /// <summary>
        /// 商户在支付方的Key
        /// </summary>
        public string MerchantKey { get; set; }
        /// <summary>
        /// 请求支付的地址
        /// </summary>
        public string RequestUrl { get; set; }
        /// <summary>
        /// 支付方回调商户的地址
        /// </summary>
        public string CallbackUrl { get; set; }
        public string AssemblyName { get; set; }
        public string HandlerType { get; set; }
        public EntityStatus Status { get; set; }
        public DateTime? LastUsedAt { get; set; }
        public string Arguments { get; set; }
        public PaymentPlatform PaymentPlatform { get; set; }
        public string Notes { get; set; }
        /// <summary>
        /// 手续费百分比
        /// </summary>
        public decimal? CommissionPercentage { get; set; }
        /// <summary>
        /// 手续费固定数值
        /// </summary>
        public decimal? CommissionConstants { get; set; }
        /// <summary>
        /// 手续费最低数值
        /// </summary>
        public decimal? CommissionMinValue { get; set; }
        public decimal? MinimumWithdrawInterval { get; set; }
        public string QueryUrl { get; set; }

        public int ProviderId { get; set; }
        public int MerchantId { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual PaymentProvider Provider { get; set; }
        public virtual Merchant Merchant { get; set; }

        public bool IsBankCard => PaymentType == PaymentType.InternalTransfer ||
            PaymentType == PaymentType.Cost ||
            PaymentType == PaymentType.Repository;

        public ExtensionRoute ExtensionRoute { get; set; }

        public CallbackSuccessType CallbackSuccessType { get; set; }        

        public string GetArgument(string name)
        {
            try
            {
                var args = JObject.Parse(Arguments);

                return args[name].ToString();
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<IDictionary<string, int>> Amounts
        {
            get
            {
                try
                {
                    return GetArgument("Amounts")?.Split('|')
                        .Select(o => new Dictionary<string, int>
                        {
                           { o.Split(':')[0], Convert.ToInt32(o.Split(':')[1]) }
                        });
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}
