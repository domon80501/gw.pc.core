﻿using System.Collections.Generic;

namespace GW.PC.Models
{
    public class User : CreatableEntity, IEntityStatus
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public EntityStatus Status { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<Merchant> Merchants { get; set; }
    }
}
