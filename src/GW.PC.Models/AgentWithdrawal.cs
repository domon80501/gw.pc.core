﻿using GW.PC.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public class AgentWithdrawal : EntityBase, ICreatedAt
    {
        public string Username { get; set; }
        public string PayeeCardNumber { get; set; }
        public string PayeeName { get; set; }
        public string PayeeCardAddress { get; set; }
        public decimal Amount { get; set; }
        public string WithdrawalIP { get; set; }
        public string WithdrawalAddress { get; set; }
        public int? PayeeBankId { get; set; }
        public DateTime? PaidAt { get; set; }
        public WithdrawalStatus Status { get; set; }
        public int? AuditedById { get; set; }
        public DateTime? AuditedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public string DeclineNotes { get; set; }
        public string MerchantOrderNumber { get; set; }
        public string AgentMessage { get; set; }
        public string SystemOrderNumber { get; set; }
        public bool HasChildren { get; set; }
        public DateTime? ManualConfirmedAt { get; set; }
        public string ManualConfirmNotes { get; set; }
        public decimal Commission { get; set; }

        public int? PaymentChannelId { get; set; }
        public int? ParentId { get; set; }
        public int? ReferenceId { get; set; }
        public int? ManualConfirmedById { get; set; }

        public virtual Bank PayeeBank { get; set; }
        public virtual User AuditedBy { get; set; }
        public virtual PaymentChannel PaymentChannel { get; set; }
        public virtual User ManualConfirmedBy { get; set; }
        public virtual AgentWithdrawal Parent { get; set; }
        public virtual AgentWithdrawal Reference { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public HttpPayment BuildHttpPayment() => new HttpPayment
        {
            CreatedAt = DateTime.Now.ToE8(),
            Source = HttpPaymentSource.AgentWithdrawal,
            SourceId = Id
        };
    }
}
