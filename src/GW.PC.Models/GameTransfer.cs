﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    /// <summary>
    /// 游戏转账
    /// </summary>
    public class GameTransfer : EntityBase, IUsername, ICreatedAt
    {
        public string Username { get; set; }
        public decimal? BeforeBalanceFrom { get; set; }
        public decimal? AfterBalanceFrom { get; set; }
        public decimal? BeforeBalanceTo { get; set; }
        public decimal? AfterBalanceTo { get; set; }
        /// <summary>
        /// 运营商订单号
        /// </summary>
        public string MerchantOrderNumber { get; set; }
        /// <summary>
        /// 游戏订单号
        /// </summary>
        public string GameOrderNumber { get; set; }
        public string TransferIP { get; set; }
        public string TransferAddress { get; set; }
        public GameTransferStatus Status { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? CompletedAt { get; set; }
        public string Notes { get; set; }
        public GameTransferSource? Source { get; set; }
        public int? SourceId { get; set; }
        public DateTime? ClosedAt { get; set; }
        [Obsolete]
        public bool HasFrozen { get; set; }

        public int? AuditedById { get; set; }
        public int? FromId { get; set; }
        public int? ToId { get; set; }
        public int? ClosedById { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual User AuditedBy { get; set; }
        public virtual Game From { get; set; }
        public virtual Game To { get; set; }
        public virtual User ClosedBy { get; set; }

        public bool IsValid
        {
            get
            {
                return (IsDeposit && this.FromId == null && this.ToId != null) ||
                    (!IsDeposit && this.FromId != null && this.ToId == null);
            }
        }

        public bool IsDeposit
        {
            get
            {
                return FromId == null;
            }
        }

        public string GameName
        {
            get
            {
                if (From != null)
                {
                    return From.Name;
                }
                else if (To != null)
                {
                    return To.Name;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }
}
