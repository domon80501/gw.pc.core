﻿using System;

namespace GW.PC.Models
{
    public class MobileVerificationCode : EntityBase, ICreatedAt
    {
        public string Mobile { get; set; }
        public string Code { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? VerifiedAt { get; set; }
    }
}
