﻿using System;

namespace GW.PC.Models
{
    /// <summary>
    /// 客户和商户之间的站内消息
    /// </summary>
    public class CustomerMessage : EntityBase, ICreatedAt
    {
        public CustomerMessageType Type { get; set; }
        public CustomerMessageStatus Status { get; set; }
        public string FromUsername { get; set; }
        public string ToUsername { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int? ParentId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ReadAt { get; set; }
        /// <summary>
        /// 旧数据id，用于建立外键关联
        /// </summary>
        public int? OldId { get; set; }
        /// <summary>
        /// 旧数据Refid，用于建立外键关联
        /// </summary>
        public int? OldRefId { get; set; }

        public virtual CustomerMessage Parent { get; set; }
    }
}
