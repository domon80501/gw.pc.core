﻿using System;

namespace GW.PC.Models
{
    public class BankCard : EntityBase, IEntityStatus, ICreatedAt
    {
        public string AccountName { get; set; }
        public string CardNumber { get; set; }
        public string RegistrationAddress { get; set; }
        public EntityStatus Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool? Verified { get; set; }

        public int BankId { get; set; }
        public int? CityId { get; set; }
        public int? WalletId { get; set; }
        public int? AgentId { get; set; }

        public virtual Bank Bank { get; set; }
        public virtual City City { get; set; }
        public virtual Wallet Wallet { get; set; }
        public virtual Agent Agent { get; set; }
    }
}
