﻿using System;
using System.Collections.Generic;

namespace GW.PC.Models
{
    public class Agent : AuditableEntity, IUsername
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string RegistrationIP { get; set; }
        public string RegistrationAddress { get; set; }
        public string LastLoginIP { get; set; }
        public string LastLoginAddress { get; set; }
        public DateTime? LastLoginAt { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string QQ { get; set; }
        public string WeChat { get; set; }
        /// <summary>
        /// 推广网址，最多可以有4个，以|隔开
        /// </summary>
        public string PromotionWebsites { get; set; }
        public string PromotionMethod { get; set; }
        public string Remarks { get; set; }
        public decimal Balance { get; set; }
        public decimal FrozenAmount { get; set; }
        public decimal DepositFeePercentage { get; set; }
        public decimal WithdrawalFeePercentage { get; set; }
        public decimal ParentBonusPercentage { get; set; }
        public decimal PlatformFeePercentage { get; set; }
        public decimal AgentPercentage { get; set; }
        public decimal? AgentDynamicPercentage { get; set; }
        public bool AllowCustomerBonus { get; set; }
        public bool AllowCustomerRakeback { get; set; }
        public AccountStatus Status { get; set; }
        public DateTime? ActivatedAt { get; set; }
        public string GameSettings { get; set; }
        public DateTime? CustomerAmountLastUpdatedAt { get; set; }
        public string SafeCode { get; set; }
        public string PromotionCode { get; set; }
        public string Introduction { get; set; }
        public string Website { get; set; }

        public int? ParentId { get; set; }

        public virtual Agent Parent { get; set; }
        public virtual ICollection<BankCard> BankCards { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }

        /// <summary>
        /// 为数据迁移的临时字段，迁移完成后应通过Migration删除该字段
        /// </summary>
        public string TempPasswordKey { get; set; }
    }
}
