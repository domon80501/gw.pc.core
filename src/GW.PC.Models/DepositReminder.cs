﻿using GW.PC.Core;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    /// <summary>
    /// 催到账
    /// </summary>
    public class DepositReminder : AuditableEntity
    {
        public PaymentMethod PaymentMethod { get; set; }
        public int DepositId { get; set; }
        public string Username { get; set; }
        public string MerchantOrderNumber { get; set; }
        public decimal ActualAmount { get; set; }
        public DepositReminderStatus Status { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
