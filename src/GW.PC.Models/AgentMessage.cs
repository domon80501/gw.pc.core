﻿using System;

namespace GW.PC.Models
{
    /// <summary>
    /// 代理和系统之间的站内消息
    /// </summary>
    public class AgentMessage : EntityBase, ICreatedAt
    {
        public AgentMessageType Type { get; set; }
        public AgentMessageStatus Status { get; set; }
        public string ToUsername { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ReadAt { get; set; }
    }
}
