﻿using GW.PC.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models.Infrastructure
{
    public class Deposit : AuditableEntity, IUsername
    {
        public string Username { get; set; }
        public PaymentPlatform PaymentPlatform { get; set; }
        public decimal RequestedAmount { get; set; }
        public decimal? ActualAmount { get; set; }
        /// <summary>
        /// 商户订单号
        /// </summary>
        public string MerchantOrderNumber { get; set; }
        /// <summary>
        /// 支付中心订单号
        /// </summary>
        public string PaymentCenterOrderNumber { get; set; }
        /// <summary>
        /// 支付平台订单号
        /// </summary>
        public string PaymentOrderNumber { get; set; }
        public string DepositIP { get; set; }
        public string DepositAddress { get; set; }
        public DateTime? CompletedAt { get; set; }
        public DepositStatus Status { get; set; }
        /// <summary>
        /// 支付服务商通知支付结果的原始数据
        /// </summary>
        public string CallbackRawText { get; set; }
        public decimal Commission { get; set; }
        public string RequestRawText { get; set; }

        /// <summary>
        /// Nullable for old data migration purpose.
        /// </summary>
        public int? PaymentChannelId { get; set; }
        public int? MerchantId { get; set; }
        public int? PromotionId { get; set; }
        public string SystemOrderNumber { get; set; }
        public string CallbackUrl { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual PaymentChannel PaymentChannel { get; set; }

        public virtual PaymentMethod PaymentMethod => 0;

        public virtual EntityLogTargetType EntityLogTargetType => EntityLogTargetType.Unknown;

        public virtual Merchant Merchant { get; set; }

        public virtual HttpPayment BuildHttpPayment() => new HttpPayment
        {
            CreatedAt = DateTime.Now.ToE8(),
            SourceId = Id
        };

        public string RedirectionUrl { get; set; }
        
        public MerchantCallbackStatus MerchantCallbackStatus { get; set; }
    }
}
