﻿namespace GW.PC.Models.Infrastructure
{
    public class AGArgument
    {
        public string CAgent { get; set; }
        public string ApiBaseAddress { get; set; }
        public string WebsiteBaseAddress { get; set; }
        public string MD5Key { get; set; }
        public string DesKey { get; set; }
        public string Lang { get; set; }
        public string OddType { get; set; }
        public string DM { get; set; }
    }
}
