﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Models.Infrastructure
{
    public class PlatformsDto
    {
        [JsonProperty("platformName")]
        public string Platform { get; set; }
        [JsonProperty("platformImages")]
        public List<PlatformImageDto> Images { get; set; }
        [JsonProperty("platformDetail")]
        public string Description { get; set; }
    }

    public class PlatformImageDto
    {
        [JsonProperty("index")]
        public int Order { get; set; }
        public string ActualFileName { get; set; }
        [JsonProperty("fileName")]
        public string OriginalFileName { get; set; }
        [JsonProperty("fileData")]
        public string Data { get; set; }
    }
}
