﻿using System;

namespace GW.PC.Models.Infrastructure
{
    public class AgentAggregateTransaction
    {
        /// <summary>
        /// 收款人姓名
        /// </summary>
        public string PayeeName { get; set; }
        /// <summary>
        /// 金额(佣金/提款)
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 收款人卡號
        /// </summary>
        public string PayeeCardNumber { get; set; }
        /// <summary>
        /// 收款银行名
        /// </summary>
        public string PayeeBankName { get; set; }
        /// <summary>
        /// 钱包馀额
        /// </summary>
        public decimal? Balance { get; set; }        
        /// <summary>
        /// 提款商户单号 
        /// </summary>
        public string MerchantOrderNumber { get; set; }
        /// <summary>
        /// 拒絕原因
        /// </summary>
        public string DeclineMessage { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? AuditedAt { get; set; }
        public DateTime? CommissionDate { get; set; }

        /// <summary>
        /// 交易類型
        /// </summary>
        public WalletTransactionType TransactionType;
        public WithdrawalStatus Status { get; set; }
    }
}