﻿namespace GW.PC.Models.Infrastructure
{
    public class AgentGameSettingItem
    {
        public int GameId { get; set; }
        public AgentSettlementType? SettlementType { get; set; }
        public decimal? SettlementPercentage { get; set; }
    }
}
