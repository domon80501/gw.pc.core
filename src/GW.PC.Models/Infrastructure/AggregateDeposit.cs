﻿using System;

namespace GW.PC.Models.Infrastructure
{
    public class AggregateDeposit
    {
        public int Id { get; set; }
        /// <summary>
        /// 申请金额
        /// </summary>
        public decimal RequestedAmount { get; set; }
        /// <summary>
        /// 实际到账金额
        /// </summary>
        public decimal? ActualAmount { get; set; }
        /// <summary>
        /// 手续费返还
        /// </summary>
        public decimal? CommissionRefund { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? CompletedAt { get; set; }
        public WalletTransactionType Type { get; set; }
        public DepositStatus Status { get; set; }
    }
}