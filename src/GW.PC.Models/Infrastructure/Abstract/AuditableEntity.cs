﻿using System;

namespace GW.PC.Models
{
    public abstract class AuditableEntity : CreatableEntity, IAuditable
    {
        public int? AuditedById { get; set; }
        public DateTime? AuditedAt { get; set; }

        public User AuditedBy { get; set; }
    }
}
