﻿using System;

namespace GW.PC.Models
{
    public abstract class CreatableEntity : EntityBase, ICreatable
    {
        /// <summary>
        /// Null means the creator is the system.
        /// </summary>
        public int? CreatedById { get; set; }
        public DateTime CreatedAt { get; set; }

        public User CreatedBy { get; set; }
    }
}
