﻿namespace GW.PC.Models
{
    public abstract class EntityBase : IEntity
    {
        public int Id { get; set; }
    }
}
