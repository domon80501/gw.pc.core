﻿namespace GW.PC.Models
{
    /// <summary>
    /// 表示所有玩家在各游戏中的数据基类，各游戏详细数据类都从该类继承。
    /// </summary>
    public abstract class CustomerGameBase : EntityBase
    {
        public string Username { get; set; }
        public string GameUsername { get; set; }
        public string ClientIP { get; set; }
        public bool AllowLogin { get; set; }
    }
}
