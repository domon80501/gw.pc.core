﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace GW.PC.Models
{
    public static class EntityLogContents
    {
        public static string Get(EntityLogContent? content)
        {
            if (content.HasValue)
            {
                try
                {
                    return contents[content.Value] ?? string.Empty;
                }
                catch (KeyNotFoundException e)
                {
                    return $"找不到相对应的Content: {content.ToString()}";
                }
                
            }
            else
            {
                return string.Empty;
            }
        }

        [Obsolete("Don't use")]
        public static string SetData(params object[] values)
        {
            if (values?.Length > 0)
            {
                var data = JsonConvert.SerializeObject(values);
                if (data.Length > 4000)
                {
                    data = data.Substring(0, 4000);
                }

                return data;
            }
            else
            {
                return string.Empty;
            }
        }

        private static readonly Dictionary<EntityLogContent, string> contents = new Dictionary<EntityLogContent, string>
        {
            { EntityLogContent.AgentSettlement_Audit, "{0}在{1}进行了审核{2}" },
            { EntityLogContent.AgentSettlement_Recreate, "{0}在{1}进行了重新结算" },
            { EntityLogContent.GameTransfer_Failed, "游戏转账{0}失败" },
            { EntityLogContent.GameTransfer_Created, "游戏转账{1}创建成功，来源：{0}" },
            { EntityLogContent.GameTransfer_SendingRequest, "正在发送游戏转账{0}请求" },
            { EntityLogContent.GameTransfer_ResponseReceived, "收到游戏转账{0}响应" },
            { EntityLogContent.GameTransfer_Succeeded, "游戏转账{0}成功完成" },
            { EntityLogContent.GameTransfer_AfterBalanceUpdateFailed, "游戏转账{1}后更新金额失败：{0}" },
            { EntityLogContent.Deposit_Created, "{0}存款创建成功" },
            { EntityLogContent.Deposit_PaymentRequestSending, "正在发送存款支付请求" },
            { EntityLogContent.Deposit_PaymentResponseReceived, "收到存款支付响应" },
            { EntityLogContent.Deposit_PaymentResponseParse, "解析支付请求响应结果" },
            { EntityLogContent.Deposit_CallbackRequestReceived, "收到存款回调" },
            { EntityLogContent.Deposit_CallbackResponseSending, "正在发送存款回调响应" },
            { EntityLogContent.Deposit_QueryRequestSending, "正在发送存款查询请求" },
            { EntityLogContent.Deposit_QueryResponseReceived, "收到存款查询响应" },
            { EntityLogContent.Deposit_QueryResponseParse, "解析查询响应结果" },
            { EntityLogContent.Deposit_PaymentHttpRequestSending, "正在发送支付请求" },
            { EntityLogContent.CustomerWithdrawal_Created, "客户提款{3}创建成功，当前状态：{0}，钱包金额：{1}，冻结金额：{2}" },
            { EntityLogContent.CustomerWithdrawal_AmountFrozen, "客户提款{3}已冻结，当前状态：{0}，钱包金额{1}，冻结金额{2}" },
            { EntityLogContent.CustomerWithdrawal_AutoAudit_Approved_NoLastDepositPeriod, "客户提款{1}自动审核：通过，无存款订单但有存款记录，当前状态：{0}" },
            { EntityLogContent.CustomerWithdrawal_AutoAudit_Approved_LastDepositPeriodCompleted, "客户提款{1}自动审核：通过，最近存款订单已完成，当前状态：{0}" },
            { EntityLogContent.CustomerWithdrawal_AutoAudit_Approved_TotalBetAmountQualifiedDeposit, "客户提款{1}自动审核：通过，总投注金额通过，要求：{2}，实际：{3}，当前状态：{0}" },
            { EntityLogContent.CustomerWithdrawal_AutoAudit_Declined_TotalBetAmountNotQualifiedDeposit, "客户提款{1}自动审核：总投注金额拒绝，要求：{2}，实际：{3}，当前提款状态：{0}" },
            { EntityLogContent.CustomerWithdrawal_Failed, "客户提款{3}失败，当前状态：{0}，钱包金额：{1}，冻结金额：{2}" },
            { EntityLogContent.CustomerWithdrawal_PaymentRequestSending, "正在发送客户提款{0}支付请求" },
            { EntityLogContent.CustomerWithdrawal_PaymentResponseReceived, "收到客户提款{0}支付响应" },
            { EntityLogContent.Deposit_Failed, "客户存款失败" },
            { EntityLogContent.CustomerWithdrawal_SuccessfulPaymentResponse, "客户提款{0}支付响应：成功" },
            { EntityLogContent.Deposit_SuccessfulPaymentResponse, "存款支付响应：成功" },
            { EntityLogContent.CustomerWithdrawal_QueryRequestSending, "正在发送客户提款{0}查询请求" },
            { EntityLogContent.CustomerWithdrawal_QueryResponseReceived, "收到客户提款{0}查询响应" },
            { EntityLogContent.CustomerWithdrawal_CallbackRequestReceived, "收到客户提款{0}回调" },
            { EntityLogContent.CustomerWithdrawal_CallbackResponseSending, "正在发送客户提款{0}回调响应" },
            { EntityLogContent.GameTransfer_Started, "开始游戏转账，金额{0}" },
            { EntityLogContent.GameTransfer_AfterBalanceUpdated, "游戏转账{2}后金额更新成功：转出后金额：{0}，转入后金额：{1}" },
            { EntityLogContent.CustomerWithdrawal_DividingCompleted, "大额提款拆分完成，提交额度：{0}，系统限额设置：{1}，拆分为{2}个子提款，金额分别为：{3}" },
            { EntityLogContent.CustomerWithdrawal_RetryCreated, "客户提款{3}重试创建成功，当前状态：{0}，钱包金额：{1}，冻结金额{2}" },
            { EntityLogContent.CustomerWithdrawal_FailedAndClosed, "客户提款{0}失败已关闭，钱包金额：{1}，冻结金额：{2}" },
            { EntityLogContent.PaymentChannelTransfer_Created, "通道互转{0}创建成功，当前状态：{1}" },
            { EntityLogContent.PaymentChannelTransfer_PaymentRequestSending, "通道互转{0}正在发送支付请求" },
            { EntityLogContent.PaymentChannelTransfer_PaymentResponseReceived, "通道互转{0}收到支付响应" },
            { EntityLogContent.PaymentChannelTransfer_Failed, "通道互转{0}失败，当前状态：{1}" },
            { EntityLogContent.PaymentChannelTransfer_SuccessfulPaymentResponse, "通道互转{0}支付响应：成功" },
            { EntityLogContent.PaymentChannelTransfer_CallbackRequestReceived, "通道互转{0}收到回调" },
            { EntityLogContent.PaymentChannelTransfer_CallbackResponseSending, "通道互转{0}正在发送回调响应" },
            { EntityLogContent.PaymentChannelAdjustment_Created, "通道微调{0}创建成功，当前状态：{1}" },
            { EntityLogContent.PaymentChannelAdjustment_PaymentRequestSending, "通道微调{0}正在发送支付请求" },
            { EntityLogContent.PaymentChannelAdjustment_PaymentResponseReceived, "通道微调{0}收到支付响应" },
            { EntityLogContent.PaymentChannelAdjustment_Failed, "通道微调{0}失败，当前状态：{1}" },
            { EntityLogContent.PaymentChannelAdjustment_SuccessfulPaymentResponse, "通道微调{0}支付响应：成功" },
            { EntityLogContent.PaymentChannelAdjustment_CallbackRequestReceived, "通道微调{0}收到回调" },
            { EntityLogContent.PaymentChannelAdjustment_CallbackResponseSending, "通道微调{0}正在发送回调响应" },
            { EntityLogContent.PaymentChannelTransfer_QueryRequestSending, "正在发送通道互转{0}查询请求" },
            { EntityLogContent.PaymentChannelTransfer_QueryResponseReceived, "收到通道互转{0}查询响应" },
            { EntityLogContent.AgentWithdrawal_Created, "代理提款{3}创建成功，金额已冻结，当前状态：{0}，钱包金额：{1}，冻结金额：{2}" },
            { EntityLogContent.AgentWithdrawal_Failed, "代理提款{3}失败，当前状态：{0}，钱包金额：{1}，冻结金额：{2}" },
            { EntityLogContent.AgentWithdrawal_PaymentRequestSending, "正在发送代理提款{0}支付请求" },
            { EntityLogContent.AgentWithdrawal_PaymentResponseReceived, "收到代理提款{0}支付响应" },
            { EntityLogContent.AgentWithdrawal_QueryRequestSending, "正在发送代理提款{0}查询请求" },
            { EntityLogContent.AgentWithdrawal_QueryResponseReceived, "收到代理提款{0}查询响应" },
            { EntityLogContent.AgentWithdrawal_CallbackRequestReceived, "收到代理提款{0}回调" },
            { EntityLogContent.AgentWithdrawal_CallbackResponseSending, "正在发送代理提款{0}回调响应" },
            { EntityLogContent.AgentWithdrawal_DividingCompleted, "大额提款拆分完成，提交额度：{0}，系统限额设置：{1}，拆分为{2}个子提款，金额分别为：{3}" },
            { EntityLogContent.AgentWithdrawal_RetryCreated, "代理提款{3}重试创建成功，当前状态：{0}，钱包金额：{1}，冻结金额{2}" },
            { EntityLogContent.AgentWithdrawal_FailedAndClosed, "代理提款{0}失败已关闭，钱包金额：{1}，冻结金额：{2}" },
            { EntityLogContent.AgentWithdrawal_SuccessfulPaymentResponse, "代理提款{0}支付响应：成功" },
            { EntityLogContent.CustomerWithdrawal_AutoAudit_Approved_NoLastBonusPeriod, "客户提款{1}自动审核：通过，无红利订单但有红利记录，当前状态：{0}" },
            { EntityLogContent.CustomerWithdrawal_AutoAudit_Approved_LastBonusPeriodCompleted, "客户提款{1}自动审核：通过，最近红利订单已完成，当前状态：{0}" },
            { EntityLogContent.CustomerWithdrawal_AutoAudit_Approved_ValidBetAmountQualifiedDeposit, "客户提款{1}自动审核：有效投注金额通过，要求：{2}，实际：{3}，当前提款状态：{0}" },
            { EntityLogContent.CustomerWithdrawal_AutoAudit_Declined_ValidBetAmountNotQualifiedDeposit, "客户提款{1}自动审核：有效投注金额拒绝，要求：{2}，实际：{3}，当前提款状态：{0}" },

            

 

        };
    }
}
