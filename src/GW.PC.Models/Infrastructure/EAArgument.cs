﻿namespace GW.Models.Infrastructure
{
    public class EAArgument
    {
        public string WebLoginUrl { get; set; }
        public string MobileLoginUrl { get; set; }
        public string GetBalanceUrl { get; set; }
        public string DepositUrl { get; set; }
        public string WithdrawUrl { get; set; }
        public string VendorId { get; set; }
        public string CurrencyId { get; set; }
    }
}
