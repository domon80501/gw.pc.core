﻿namespace GW.PC.Models.Infrastructure
{
    public class KGArgument
    {
        public string LoginUrl { get; set; }
        public string VendorSite { get; set; }
        public string GetBalanceUrl { get; set; }
        public string PrepareTransferUrl { get; set; }
        public string TransferUrl { get; set; }
        public string VendorId { get; set; }
        public string Currency { get; set; }
        public string AllowStake { get; set; }
        public string Language { get; set; }
    }
}
