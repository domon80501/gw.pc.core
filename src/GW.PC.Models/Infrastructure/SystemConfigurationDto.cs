﻿using System.Collections.Generic;

namespace GW.PC.Models
{
    public class SystemConfigurationDto
    {
        public Global Global { get; set; }
        public WithdrawalConfiguration Withdrawal { get; set; }
        public AgentConfiguration Agent { get; set; }
    }

    public class Global
    {
        public bool IsSyncingGameTransactions { get; set; }
    }

    public class WithdrawalConfiguration
    {
        public AutoAudit AutoAudit { get; set; }
        public decimal? DivideThreshold { get; set; }
    }

    public class AutoAudit
    {
        public bool Enabled { get; set; }
        public decimal BetAmountQualifiedPercentage { get; set; }
        public decimal BonusAmountQualifiedPercentage { get; set; }
    }

    public class AgentConfiguration
    {
        public decimal DefaultPercentage { get; set; }
        public decimal DefaultSettlementPercentage { get; set; }
        public AgentDynamicPercentageConfiguration DynamicPercentage { get; set; }
        public IEnumerable<AgentPromotionUrlConfiguration> PromotionUrls { get; set; }
        public string PromotionDomain { get; set; }
        public string PromotionAppDomain { get; set; }
    }

    public class AgentDynamicPercentageConfiguration
    {
        public Stage Stages { get; set; }
        public Percentage Percentages { get; set; }

        public class Stage : Number
        {
        }
        public class Percentage : Number
        {
        }
        public class Number
        {
            public decimal One { get; set; }
            public decimal Two { get; set; }
            public decimal Three { get; set; }
            public decimal Four { get; set; }
            public decimal Five { get; set; }
        }
    }

    public class AgentPromotionUrlConfiguration
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public bool AddCode { get; set; }
        public int Order { get; set; }
    }
}
