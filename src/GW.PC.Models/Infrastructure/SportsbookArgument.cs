﻿namespace GW.PC.Models.Infrastructure
{
    public class SportsbookArgument
    {
        public string Key { get; set; }
        public string MerchantId { get; set; }
        public string ApiBaseAddress { get; set; }
        public string SiteBaseAddress { get; set; }
        public string OddsTypeId { get; set; }
        public string LangCode { get; set; }
        public string TimeZone { get; set; }
        public string CurrencyCode { get; set; }
    }
}
