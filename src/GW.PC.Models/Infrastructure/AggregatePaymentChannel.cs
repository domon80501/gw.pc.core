﻿namespace GW.PC.Models.Infrastructure
{
    public class AggregatePaymentChannel
    {
        public string Name { get; set; }
        public string MerchantUsername { get; set; }
    }
}
