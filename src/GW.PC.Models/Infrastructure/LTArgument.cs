﻿namespace GW.PC.Models.Infrastructure
{
    public class LTArgument
    {
        public string ApiBaseAddress { get; set; }
        public string WebSiteBaseAddress { get; set; }
        public string SecretKey { get; set; }
        public string OperatorId { get; set; }
        public string SiteCode { get; set; }
        public string ProductCode { get; set; }
        public string Currency { get; set; }
    }
}
