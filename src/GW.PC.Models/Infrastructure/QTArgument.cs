﻿namespace GW.PC.Models.Infrastructure
{
    public class QTArgument
    {
        public string ApiBaseAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Currency { get; set; }
        public string Country { get; set; }
        public string Language { get; set; }
    }
}
