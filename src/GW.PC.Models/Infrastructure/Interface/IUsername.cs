﻿namespace GW.PC.Models
{
    /// <summary>
    /// Represents any model that has a Username property.
    /// </summary>
    public interface IUsername
    {
        string Username { get; set; }
    }
}
