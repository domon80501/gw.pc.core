﻿using System;

namespace GW.PC.Models
{
    public interface ICreatedAt
    {
        DateTime CreatedAt { get; set; }
    }
}
