﻿namespace GW.PC.Models
{
    public interface IEntityStatus
    {
        EntityStatus Status { get; set; }
    }
}
