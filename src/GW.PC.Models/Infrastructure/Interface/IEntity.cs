﻿namespace GW.PC.Models
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
