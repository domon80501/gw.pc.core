﻿using System;

namespace GW.PC.Models
{
    public interface IAuditable
    {
        int? AuditedById { get; set; }
        DateTime? AuditedAt { get; set; }
    }
}
