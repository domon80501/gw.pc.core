﻿namespace GW.PC.Models
{
    public interface ICreatable : ICreatedAt
    {
        int? CreatedById { get; set; }
    }
}
