﻿namespace GW.PC.Models.Infrastructure
{
    public class GameDataItem
    {
        public string GameName { get; set; }
        public decimal GameBetAmount { get; set; }
        public decimal GameProfitAmount { get; set; }
    }
}
