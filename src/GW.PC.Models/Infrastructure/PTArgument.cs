﻿namespace GW.PC.Models.Infrastructure
{
    public class PTArgument
    {
        public string ApiBaseAddress { get; set; }
        public string KioskName { get; set; }
        public string KioskAdminName { get; set; }
        public string MerchantId { get; set; }
        public string EntityKey { get; set; }
        public string CertificateFilePath { get; set; }
        public string CertificatePassword { get; set; }
    }
}
