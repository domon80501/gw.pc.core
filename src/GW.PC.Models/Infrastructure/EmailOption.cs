﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GW.PC.Models
{
    [ComplexType]
    public class EmailOption
    {
        public string SenderAddress { get; set; }
    }
}
