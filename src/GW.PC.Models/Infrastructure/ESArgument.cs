﻿namespace GW.PC.Models.Infrastructure
{
    public class ESArgument
    {
        public string ApiBaseAddress { get; set; }
        public string AppKey { get; set; }
        public string Secret { get; set; }
    }
}
