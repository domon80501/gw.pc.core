﻿namespace GW.PC.Models.Infrastructure
{
    public class BbinArgument
    {
        public string Website { get; set; }
        public string Uppername { get; set; }
        public string LoginBaseAddress { get; set; }
        public string LoginKey { get; set; }
        public string ApiBaseAddress { get; set; }
        public string RegisterKey { get; set; }
        public string GetBalanceKey { get; set; }
        public string TransferKey { get; set; }
    }
}
