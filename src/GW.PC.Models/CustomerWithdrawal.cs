﻿using GW.PC.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public class CustomerWithdrawal : EntityBase, ICreatedAt
    {
        public string Username { get; set; }
        public string PayeeCardNumber { get; set; }
        public string PayeeName { get; set; }
        public string PayeeCardAddress { get; set; }
        public decimal Amount { get; set; }
        public string WithdrawalIP { get; set; }
        public string WithdrawalAddress { get; set; }
        public int? PayeeBankId { get; set; }
        public int? PaidById { get; set; }
        public DateTime? PaidAt { get; set; }
        public WithdrawalStatus Status { get; set; }
        public int? AuditedById { get; set; }
        public DateTime? AuditedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        /// <summary>
        /// Processing time from creation to completion, in seconds
        /// </summary>
        public int? ProcessingTime { get; set; }
        /// <summary>
        /// 拒绝原因
        /// </summary>
        public string DeclineNotes { get; set; }
        public decimal Commission { get; set; }
        public int? PaymentChannelId { get; set; }
        public string MerchantOrderNumber { get; set; }
        /// <summary>
        /// 补单人员
        /// </summary>
        public int? ManualConfirmedById { get; set; }
        /// <summary>
        /// 补单时间
        /// </summary>
        public DateTime? ManualConfirmedAt { get; set; }
        /// <summary>
        /// 补单原因
        /// </summary>
        public string ManualConfirmNotes { get; set; }
        public string CustomerMessage { get; set; }
        public int? ParentId { get; set; }
        public bool HasChildren { get; set; }
        public int? ReferenceId { get; set; }
        public string SystemOrderNumber { get; set; }

        public virtual Bank PayeeBank { get; set; }
        public virtual User PaidBy { get; set; }
        public virtual User AuditedBy { get; set; }
        public virtual PaymentChannel PaymentChannel { get; set; }
        public virtual User ManualConfirmedBy { get; set; }
        public virtual CustomerWithdrawal Parent { get; set; }
        public virtual CustomerWithdrawal Reference { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public HttpPayment BuildHttpPayment() => new HttpPayment
        {
            CreatedAt = DateTime.Now.ToE8(),
            Source = HttpPaymentSource.CustomerWithdrawal,
            SourceId = Id
        };
    }
}
