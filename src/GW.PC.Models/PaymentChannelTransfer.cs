﻿using GW.PC.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Models
{
    public class PaymentChannelTransfer : CreatableEntity
    {
        public decimal Amount { get; set; }
        public decimal FromCommission { get; set; }
        public decimal ToCommission { get; set; }
        public PaymentChannelTransferStatus Status { get; set; }
        public string PayeeCardNumber { get; set; }
        public string PayeeName { get; set; }
        public int? PayeeBankId { get; set; }
        public DateTime? PaidAt { get; set; }
        public DateTime? ManualConfirmedAt { get; set; }
        public string Notes { get; set; }
        public string SystemOrderNumber { get; set; }
        public string MerchantOrderNumber { get; set; }

        public int FromId { get; set; }
        public int ToId { get; set; }
        public int? ManualConfirmedById { get; set; }

        public virtual PaymentChannel From { get; set; }
        public virtual PaymentChannel To { get; set; }
        public virtual User ManualConfirmedBy { get; set; }
        public virtual Bank PayeeBank { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public HttpPayment BuildHttpPayment() => new HttpPayment
        {
            CreatedAt = DateTime.Now.ToE8(),
            Source = HttpPaymentSource.PaymentChannelTransfer,
            SourceId = Id
        };
    }
}
