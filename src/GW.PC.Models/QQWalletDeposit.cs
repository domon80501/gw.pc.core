﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;

namespace GW.PC.Models
{
    public class QQWalletDeposit : Deposit
    {
        public override PaymentMethod PaymentMethod => PaymentMethod.QQWallet;        
        public override EntityLogTargetType EntityLogTargetType => EntityLogTargetType.QQWalletDeposit;
        public override HttpPayment BuildHttpPayment()
        {
            var payment = base.BuildHttpPayment();

            payment.Source = HttpPaymentSource.QQWalletDeposit;

            return payment;
        }
    }
}
