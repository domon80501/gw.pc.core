﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace GW.PC.Models
{
    public class Mission : CreatableEntity, IEntityStatus
    {
        public string Name { get; set; }
        public EntityStatus Status { get; set; }

        public MissionCriteria PrimaryCriteria { get; set; }
        public string SecondaryCriteria { get; set; }

        public DateTime? CriteriaRegisteredAtStart { get; set; }
        public DateTime? CriteriaRegisteredAtEnd { get; set; }
        public decimal? CriteriaDepositAmountFrom { get; set; }
        public decimal? CriteriaDepositAmountTo { get; set; }
        public DateTime? CriteriaDepositAtStart { get; set; }
        public DateTime? CriteriaDepositAtEnd { get; set; }
        public decimal? CriteriaValidBetAmountFrom { get; set; }
        public decimal? CriteriaValidBetAmountTo { get; set; }
        public DateTime? CriteriaValidBetAtStart { get; set; }
        public DateTime? CriteriaValidBetAtEnd { get; set; }
        public decimal? CriteriaTotalBetAmountFrom { get; set; }
        public decimal? CriteriaTotalBetAmountTo { get; set; }
        public DateTime? CriteriaTotalBetAtStart { get; set; }
        public DateTime? CriteriaTotalBetAtEnd { get; set; }
        public decimal? CriteriaProfitAmountFrom { get; set; }
        public decimal? CriteriaProfitAmountTo { get; set; }
        public DateTime? CriteriaProfitAtStart { get; set; }
        public DateTime? CriteriaProfitAtEnd { get; set; }
        public int? MaxParticipationCount { get; set; }
        public DateTime? ParticipatedAtStart { get; set; }
        public DateTime? ParticipatedAtEnd { get; set; }
        public int? MinReferralCount { get; set; }
        public MissionReferralRequiredAmountType? ReferralRequiredAmountType { get; set; }
        public decimal? ReferralRequiredAmount { get; set; }
        public DateTime? ReferralAtStart { get; set; }
        public DateTime? ReferralAtEnd { get; set; }
        public string CustomerGrades { get; set; }
        public int? GamePlatformId { get; set; }
        public int? GameId { get; set; }

        public string Requirement { get; set; }
        public string Content { get; set; }
        public bool AutoAuditBonus { get; set; }

        public WithdrawalRequiredAmountType WithdrawalRequiredAmountType { get; set; }
        public decimal? Principal { get; set; }
        public decimal? Bonus { get; set; }
        public int? Multiplier { get; set; }
        public WithdrawalRequirementCalculationType WithdrawalCalculationType { get; set; }
        public int Order { get; set; }

        public int PromotionCategoryId { get; set; }
        public int PromotionId { get; set; }

        public virtual PromotionCategory PromotionCategory { get; set; }
        [ForeignKey("PromotionId")]
        public virtual NewPromotion Promotion { get; set; }

        public IEnumerable<MissionCriteria> SecondaryCriteriaEnums
        {
            get
            {
                if (string.IsNullOrEmpty(SecondaryCriteria))
                {
                    return null;
                }
                else
                {
                    return SecondaryCriteria.Split(';').Select(c => (MissionCriteria)Convert.ToInt32(c));
                }
            }
        }

        public bool ContainsSecondaryCriteria(MissionCriteria criteria)
        {
            if (string.IsNullOrEmpty(SecondaryCriteria))
            {
                return false;
            }
            else
            {
                return SecondaryCriteriaEnums.Contains(criteria);
            }
        }
    }
}
