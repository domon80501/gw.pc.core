﻿using System;
using System.Collections.Generic;

namespace GW.PC.Models
{
    public class Alarm : CreatableEntity
    {
        public string Content { get; set; }
        public bool Read { get; set; }
        public DateTime StartedAt { get; set; }

        public int? FollowUpId { get; set; }

        public virtual FollowUp FollowUp { get; set; }
        public virtual ICollection<User> Receivers { get; set; }
    }
}
