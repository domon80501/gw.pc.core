﻿namespace GW.PC.Models
{
    public class PromotionCategory : CreatableEntity, IEntityStatus
    {
        public string Name { get; set; }
        public int SequenceNumber { get; set; }
        public EntityStatus Status { get; set; }
    }
}
