﻿namespace GW.PC.Models
{
    /// <summary>
    /// 账户微调
    /// </summary>
    public class BalanceAdjustment : AuditableEntity, IUsername
    {
        public string Username { get; set; }
        public decimal Amount { get; set; }
        public string Notes { get; set; }
        public AuditStatus Status { get; set; }
        public decimal BeforeBalance { get; set; }
        public decimal AfterBalance { get; set; }
        public string SystemOrderNumber { get; set; }
    }
}
