﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using GW.PC.Core;
using GW.PC.Models;

namespace GW.PC.Services.EF
{
    public class ReferralRecognitionService : EntityService<ReferralRecognition>
    {
        public override async Task<ReferralRecognition> Add(ReferralRecognition item)
        {
            using (var ctx = new GWContext())
            {
                if (!string.IsNullOrEmpty(item.AgentPromotionCode) && !await ctx.Agents.AnyAsync(a => a.PromotionCode == item.AgentPromotionCode))
                {
                    throw new BusinessException($"Invalid agent id: {item.AgentPromotionCode}");
                }

                if (item.CustomerId.HasValue && !await ctx.Customers.AnyAsync(c => c.Id == item.CustomerId))
                {
                    throw new BusinessException($"Invalid customer id: {item.CustomerId}");
                }

                var recognitions = await ctx.ReferralRecognitions.Where(
                    r => r.ClientIP == item.ClientIP)
                    .ToListAsync();
                ReferralRecognition recognition = null;

                if (recognitions.Count > 1)
                {
                    throw new BusinessException($"Multiple items found with the same key value: {item.ClientIP}+{item.Device}");
                }

                if (recognitions.Count == 0)
                {
                    recognition = item;

                    ctx.ReferralRecognitions.Add(recognition);
                }

                if (recognitions.Count == 1)
                {
                    recognition = recognitions.Single();

                    recognition.AgentPromotionCode = item.AgentPromotionCode;
                    recognition.CustomerId = item.CustomerId;
                    recognition.LastModifiedAt = DateTime.Now.ToE8();
                    recognition.Url = item.Url;
                }

                await ctx.SaveChangesAsync();

                return recognition;
            }
        }
    }
}
