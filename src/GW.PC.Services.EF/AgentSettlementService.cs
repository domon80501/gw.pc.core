﻿using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Models.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.PC.Services.EF
{
    public class AgentSettlementService : CreatableEntityService<AgentSettlement>
    {
        public async Task<AgentSettlement> Next()
        {
            using (var ctx = new GWContext())
            {
                return await ctx.AgentSettlements.FirstOrDefaultAsync(
                    s => s.Status == AgentSettlementStatus.NotStarted);
            }
        }

        public async Task Generate(DateTime periodStartsAt, DateTime periodEndsAt, int createdById)
        {
            using (var ctx = new GWContext())
            {
                var usernames = await ctx.Agents.Select(a => a.Username).ToListAsync();
                var existing = await ctx.AgentSettlements.Where(
                    s => s.PeriodStartsAt == periodStartsAt &&
                    s.PeriodEndsAt == periodEndsAt)
                    .Select(s => s.Username)
                    .Distinct()
                    .ToListAsync();

                var settlements = usernames.Except(existing)
                    .Select(u => new AgentSettlement
                    {
                        CreatedAt = DateTime.Now.ToE8(),
                        CreatedById = createdById,
                        PeriodStartsAt = periodStartsAt,
                        PeriodEndsAt = periodEndsAt,
                        Username = u,
                        Status = AgentSettlementStatus.NotStarted
                    });

                ctx.AgentSettlements.AddRange(settlements);

                await ctx.SaveChangesAsync();
            }
        }

        /// <summary>
        /// 该代理:	（总业务 - (存款手续费* 代理比例) - (提款手续费* 代理比例) - (平台费* 代理比例) - (返水* 代理比例) - (红利* 代理比例)) + 其他 ＝  结算佣金
        /// </summary>
        /// <returns></returns>
        public async Task Settle(
            Agent agent,
            IEnumerable<Agent> subAgents,
            AgentSettlement settlement,
            IEnumerable<CustomerGameData> agentGameData,
            IEnumerable<CustomerGameData> subAgentGameData)
        {
            using (var ctx = new GWContext())
            {
                var businessData = await ctx.Database.SqlQuery<AgentSettlementBusinessDto>("AgentSettlement_BusinessData @username,@periodStartsAt,@periodEndsAt",
                    new[]
                    {
                        new SqlParameter("@username", settlement.Username),
                        new SqlParameter("@periodStartsAt", settlement.PeriodStartsAt),
                        new SqlParameter("@periodEndsAt", settlement.PeriodEndsAt)
                    })
                    .ToListAsync();
                var agentAdjustment = await ctx.AgentAdjustments.Where(
                    aa => aa.Username == settlement.Username &&
                    aa.Type == AgentAdjustmentType.Settlement &&
                    aa.Status == AuditStatus.Approved &&
                    aa.AuditedAt >= settlement.PeriodStartsAt &&
                    aa.AuditedAt < settlement.PeriodEndsAt)
                    .ToListAsync();

                var now = DateTime.Now;
                var agentSerivce = new AgentService();
                var netProfit = await agentSerivce.GetNetProfitLastMonth(agent.Username);
                agent.AgentDynamicPercentage = await agentSerivce.GetAgentDynamicPercentage(netProfit);
                await agentSerivce.Update(agent);

                var calcPercentage = (((agent.AgentDynamicPercentage ?? 0) > agent.AgentPercentage ? (agent.AgentDynamicPercentage ?? 0) : agent.AgentPercentage));

                ctx.DirectAgentSettlements.Add(BuildDirectAgentSettlement(
                        businessData.Single(d => d.Username == settlement.Username),
                        agentGameData,
                        agent,
                        agentAdjustment.Where(aa => aa.Username == settlement.Username),
                        settlement,
                        now,
                        calcPercentage));

                ctx.SubordinateAgentSettlements.AddRange(BuildSubAgentSettlementDetails(
                    businessData.Where(d => d.Username != settlement.Username),
                    subAgentGameData,
                    subAgents,
                    settlement,
                    now));

                settlement.Status = AgentSettlementStatus.AwaitApproval;
                settlement.SettlementPercentage = calcPercentage;
                ctx.Entry(settlement).State = EntityState.Modified;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task Audit(int id, bool result, int auditedById)
        {
            using (var ctx = new GWContext())
            {
                var settlement = await EnsureEntity(id, ctx);
                var auditedBy = await EnsureEntity<User>(auditedById, ctx);

                if (settlement.Status != AgentSettlementStatus.AwaitApproval)
                {
                    throw new BusinessException(
                        string.Format(
                            Constants.MessageTemplates.InvalidCurrentStatus,
                            settlement.Status.GetDisplayName()));
                }

                var now = DateTime.Now.ToE8();

                settlement.AuditedById = auditedById;
                settlement.AuditedAt = now;

                if (result)
                {
                    settlement.Status = AgentSettlementStatus.Approved;

                    var agent = await EnsureEntity<Agent>(settlement.Username, ctx);
                    var amount = settlement.DirectCommission + settlement.SubordinateCommissions;

                    agent.Balance += amount;

                    AddWalletTransaction(ctx, amount, agent.Balance, WalletTransactionType.AgentSettlement, agent.Username, settlement.Id);

                    await new AgentMessageService().Add(new AgentMessage
                    {
                        Type = AgentMessageType.SystemNotification,
                        Status = AgentMessageStatus.Unread,
                        ToUsername = agent.Username,
                        Title = $"{settlement.PeriodStartsAt.ToString("MM")}佣金到账通知",
                        Content = $"{settlement.PeriodStartsAt.ToString("MM")}月佣金{(settlement.DirectCommission + settlement.SubordinateCommissions).ToString("F2")}元，已经到账户，请至佣金提款里查看。",
                        CreatedAt = now
                    });
                }
                else
                {
                    settlement.Status = AgentSettlementStatus.Declined;
                }

                EntityLogService.Add(
                    ctx,
                    EntityLogContent.AgentSettlement_Audit,
                    EntityLogTargetType.AgentSettlement,
                    settlement.Id,
                    null,
                    auditedBy.Username, result ? "通过" : "拒绝");

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<AgentSettlement> Retry(int id, int operatorId)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var settlement = await EnsureEntity(id, ctx);

                    if (settlement.Status != AgentSettlementStatus.Declined &&
                        settlement.Status != AgentSettlementStatus.Failed)
                    {
                        throw new BusinessException(string.Format(
                            Constants.MessageTemplates.InvalidCurrentStatus,
                            settlement.Status.GetDisplayName()));
                    }

                    settlement.Status = AgentSettlementStatus.FailedAndClosed;

                    if (await ctx.AgentSettlements.AnyAsync(
                        s => s.Username == settlement.Username &&
                        s.PeriodStartsAt == settlement.PeriodStartsAt &&
                        s.PeriodEndsAt == settlement.PeriodEndsAt &&
                        s.Status != AgentSettlementStatus.FailedAndClosed &&
                        s.Id != id))
                    {
                        throw new BusinessException("该代理有未关闭结算，无法重新结算");
                    }

                    var user = await EnsureEntity<User>(operatorId, ctx);
                    var now = DateTime.Now.ToE8();

                    var newSettlement = new AgentSettlement
                    {
                        CreatedAt = now,
                        PeriodStartsAt = settlement.PeriodStartsAt,
                        PeriodEndsAt = settlement.PeriodEndsAt,
                        Username = settlement.Username,
                        Status = AgentSettlementStatus.NotStarted,
                        CreatedById = operatorId
                    };
                    ctx.AgentSettlements.Add(newSettlement);

                    EntityLogService.Add(
                        ctx,
                        EntityLogContent.AgentSettlement_Recreate,
                        EntityLogTargetType.AgentSettlement,
                        id,
                        null,
                        user.Username, now.ToDateTimeString());

                    await ctx.SaveChangesAsync();

                    EntityLogService.Add(
                        ctx,
                        EntityLogContent.AgentSettlement_Created,
                        EntityLogTargetType.AgentSettlement,
                        newSettlement.Id,
                        null,
                        user.Username);

                    scope.Complete();

                    return settlement;
                }
            }
        }

        public async Task Fail(AgentSettlement settlement, string notes)
        {
            using (var ctx = new GWContext())
            {
                settlement.Status = AgentSettlementStatus.Failed;
                settlement.Notes = notes.Length > 500 ? notes.Substring(0, 500) : notes;

                ctx.Entry(settlement).State = EntityState.Modified;

                await ctx.SaveChangesAsync();
            }
        }

        private DirectAgentSettlement BuildDirectAgentSettlement(
            AgentSettlementBusinessDto agentBusinessData,
            IEnumerable<CustomerGameData> agentGameData,
            Agent agent,
            IEnumerable<AgentAdjustment> agentAdjustments,
            AgentSettlement settlement,
            DateTime now,
            decimal calcPercentage)
        {
            var ds = new DirectAgentSettlement
            {
                Username = agent.Username,
                CreatedAt = now,
                CreatedById = settlement.CreatedById,
                SettlementId = settlement.Id
            };
            var gameSettings = JsonConvert.DeserializeObject<IEnumerable<AgentGameSettingItem>>(agent.GameSettings);

            // 淨輸贏(总业务)：SUM(输赢额or投注额 * 结算比例)
            ds.BaseAmount = agentGameData.Sum(g =>
            {
                var gs = gameSettings.SingleOrDefault(s => s.GameId == g.GameId);
                if (gs == null)
                {
                    throw new BusinessException($"找不到代理游戏数据结算设置{g.GameId}");
                }

                return (gs.SettlementType == AgentSettlementType.BetAmount ? g.TotalBetAmount : g.ProfitAmount) * gs.SettlementPercentage;
            }) ?? 0;

            // 总存款 * 存款手续费比例 * 代理比例
            ds.DepositFeeAmount = agentBusinessData.DepositAmount * agent.DepositFeePercentage * calcPercentage;
            // 总提款 * 提款手续费比例 * 代理比例
            ds.WithdrawalFeeAmount = agentBusinessData.WithdrawalAmount * agent.WithdrawalFeePercentage * calcPercentage;
            // 平台费：SUM（输赢 * 平台费比例） * 代理比例
            ds.PlatformFeeAmount = agentGameData.Sum(g => g.ProfitAmount) * agent.PlatformFeePercentage * calcPercentage;
            if (ds.PlatformFeeAmount < 0)
            {
                ds.PlatformFeeAmount = 0;
            }
            // 返水 * 代理比例
            ds.RakeAmount = agentBusinessData.RakebackAmount * calcPercentage;
            // 红利 * 代理比例
            ds.BonusAmount = agentBusinessData.BonusAmount * calcPercentage;
            // 结算微调
            ds.AdjustmentAmount = agentAdjustments.Sum(aa => aa.Amount);

            // 结算佣金
            ds.TotalCommission = ds.BaseAmount - ds.DepositFeeAmount - ds.WithdrawalFeeAmount
                    - ds.PlatformFeeAmount - ds.RakeAmount - ds.BonusAmount + ds.AdjustmentAmount;

            settlement.DirectCommission = ds.TotalCommission;

            return ds;
        }

        private IEnumerable<SubordinateAgentSettlement> BuildSubAgentSettlementDetails(
            IEnumerable<AgentSettlementBusinessDto> subAgentBusinessData,
            IEnumerable<CustomerGameData> subAgentGameData,
            IEnumerable<Agent> subAgents,
            AgentSettlement settlement,
            DateTime now)
        {
            var list = new List<SubordinateAgentSettlement>();

            foreach (var item in subAgentBusinessData)
            {
                var agent = subAgents.Single(a => a.Username == item.Username);
                var gameSettings = JsonConvert.DeserializeObject<IEnumerable<AgentGameSettingItem>>(agent.GameSettings);

                var subDetail = new SubordinateAgentSettlement
                {
                    Username = agent.Username,
                    CreatedAt = now,
                    CreatedById = settlement.CreatedById,
                    ParentBonusPercentage = agent.ParentBonusPercentage,
                    SettlementId = settlement.Id
                };

                // 淨輸贏(总业务)：SUM(输赢额or投注额)
                subDetail.BaseAmount = subAgentGameData.Sum(g =>
                {
                    var gs = gameSettings.SingleOrDefault(s => s.GameId == g.GameId);
                    if (gs == null)
                    {
                        throw new BusinessException($"找不到代理游戏数据结算设置{g.GameId}");
                    }

                    return gs.SettlementType == AgentSettlementType.BetAmount ? g.TotalBetAmount : g.ProfitAmount;
                });
                // 总存款 * 存款手续费比例 * 代理比例
                subDetail.DepositFeeAmount = item.DepositAmount * agent.DepositFeePercentage;
                // 总提款 * 提款手续费比例 * 代理比例
                subDetail.WithdrawalFeeAmount = item.WithdrawalAmount * agent.WithdrawalFeePercentage;
                // 平台费：SUM（输赢 * 平台费比例） * 代理比例
                subDetail.PlatformFeeAmount = subAgentGameData.Sum(g => g.ProfitAmount) * agent.PlatformFeePercentage;
                if (subDetail.PlatformFeeAmount < 0)
                {
                    subDetail.PlatformFeeAmount = 0;
                }
                // 返水 * 代理比例
                subDetail.RakeAmount = item.RakebackAmount;
                // 红利 * 代理比例
                subDetail.BonusAmount = item.BonusAmount;

                // 结算佣金
                subDetail.TotalCommission = (subDetail.BaseAmount - subDetail.DepositFeeAmount - subDetail.WithdrawalFeeAmount
                    - subDetail.PlatformFeeAmount - subDetail.RakeAmount - subDetail.BonusAmount) * agent.ParentBonusPercentage;

                list.Add(subDetail);
            }

            settlement.SubordinateCommissions = list.Sum(s => s.TotalCommission);
            if (settlement.SubordinateCommissions < 0)
            {
                settlement.SubordinateCommissions = 0;
            }

            return list;
        }

        private class AgentSettlementBusinessDto
        {
            public string Username { get; set; }
            public decimal DepositAmount { get; set; }
            public decimal BonusAmount { get; set; }
            public decimal RakebackAmount { get; set; }
            public decimal WithdrawalAmount { get; set; }
        }
    }
}
