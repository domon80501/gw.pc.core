﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class PCDbInitializer : DropCreateDatabaseIfModelChanges<PCContext>
    {
        protected override void Seed(PCContext context)
        {
            new SeedInit().SetSeed(context);
        }
    }

    internal sealed class Configuration : DbMigrationsConfiguration<PCContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(PCContext context)
        {
            new SeedInit().SetSeed(context);
        }
    }

    public class SeedInit
    {
        public async Task SetSeed(PCContext context)
        {
            #region Permission Group
            PermissionGroup p1 = new PermissionGroup { Name = "商户管理", Level = 1 };
            PermissionGroup p1sub1 = new PermissionGroup { Name = "商户列表", Level = 2, Parent = p1 };

            PermissionGroup p2 = new PermissionGroup { Name = "交易管理", Level = 1 };
            PermissionGroup p2sub1 = new PermissionGroup { Name = "支付宝存款列表", Level = 2, Parent = p2 };
            PermissionGroup p2sub2 = new PermissionGroup { Name = "微信存款列表", Level = 2, Parent = p2 };
            PermissionGroup p2sub3 = new PermissionGroup { Name = "网银支付存款列表", Level = 2, Parent = p2 };
            PermissionGroup p2sub4 = new PermissionGroup { Name = "充值卡存款列表", Level = 2, Parent = p2 };
            PermissionGroup p2sub5 = new PermissionGroup { Name = "QQ钱包存款列表", Level = 2, Parent = p2 };
            PermissionGroup p2sub6 = new PermissionGroup { Name = "银联支付存款列表", Level = 2, Parent = p2 };
            PermissionGroup p2sub7 = new PermissionGroup { Name = "支付宝转卡存款列表", Level = 2, Parent = p2 };
            PermissionGroup p2sub8 = new PermissionGroup { Name = "京东钱包存款列表", Level = 2, Parent = p2 };
           // PermissionGroup p2sub9 = new PermissionGroup { Name = "灌钱存款列表", Level = 2, Parent = p2 };

            PermissionGroup p3 = new PermissionGroup { Name = "系统管理", Level = 1 };
            PermissionGroup p3sub1 = new PermissionGroup { Name = "角色列表", Level = 2, Parent = p3 };
           // PermissionGroup p3sub2 = new PermissionGroup { Name = "权限列表", Level = 2, Parent = p3 };
            PermissionGroup p3sub3 = new PermissionGroup { Name = "使用者列表", Level = 2, Parent = p3 };
            PermissionGroup p3sub4 = new PermissionGroup { Name = "支付渠道列表", Level = 2, Parent = p3 };
            PermissionGroup p3sub5 = new PermissionGroup { Name = "银行列表", Level = 2, Parent = p3 };
            PermissionGroup p3sub6 = new PermissionGroup { Name = "支付服务商列表", Level = 2, Parent = p3 };
            PermissionGroup p3sub7 = new PermissionGroup { Name = "支付银行列表", Level = 2, Parent = p3 };
            PermissionGroup p3sub8 = new PermissionGroup { Name = "业务日志", Level = 2, Parent = p3 };

            //PermissionGroup p4 = new PermissionGroup { Name = "系统功能", Level = 1 };

            //PermissionGroup p4sub1 = new PermissionGroup { Name = "支付类型选单", Level = 2, Parent = p4 };
            //PermissionGroup p4sub2 = new PermissionGroup { Name = "支付方式选单", Level = 2, Parent = p4 };
            //PermissionGroup p4sub3 = new PermissionGroup { Name = "支付平台选单", Level = 2, Parent = p4 };

            context.PermissionGroups.AddOrUpdate(
                r => new { r.Name },
                p1, p1sub1, p2sub2, p2sub3, p2sub4, p2sub5, p2sub6, p2sub7, p2sub8,
                p2, p2sub1,
                p3, p3sub1, p3sub3, p3sub5, p3sub6, p3sub7, p3sub8
                //p4, p4sub1, p4sub2, p4sub3
                );

            #endregion

            #region Permissions

            context.Permissions.AddOrUpdate(
                new Permission { Name = "商户列表_查询", Parent = p1sub1, Status = EntityStatus.Enabled, Code = PermissionCode.Merchant_Access },
                new Permission { Name = "商户资料_新增", Parent = p1sub1, Status = EntityStatus.Enabled, Code = PermissionCode.Merchant_Create },
                new Permission { Name = "商户资料_修改", Parent = p1sub1, Status = EntityStatus.Enabled, Code = PermissionCode.Merchant_Detail_Edit },
                new Permission { Name = "商户资料_修改商户状态", Parent = p1sub1, Status = EntityStatus.Enabled, Code = PermissionCode.Merchant_Detail_Status_Edit },

                //new Permission { Name = "支付宝存款列表_查询", Parent = p2sub1, Status = EntityStatus.Enabled, Code = PermissionCode.Order_AlipayDeposit_Access },
                //new Permission { Name = "支付宝存款列表_查询订单", Parent = p2sub1, Status = EntityStatus.Enabled, Code = PermissionCode.Order_AlipayDeposit_Query },
                //new Permission { Name = "支付宝存款列表_补发商户回调", Parent = p2sub1, Status = EntityStatus.Enabled, Code = PermissionCode.Order_AlipayDeposit_Manual },

                //new Permission { Name = "微信存款列表_查询", Parent = p2sub2, Status = EntityStatus.Enabled, Code = PermissionCode.Order_WechatDeposit_Access },
                //new Permission { Name = "微信存款列表_查询订单", Parent = p2sub2, Status = EntityStatus.Enabled, Code = PermissionCode.Order_WechatDeposit_Query },
                //new Permission { Name = "微信存款列表_补发商户回调", Parent = p2sub2, Status = EntityStatus.Enabled, Code = PermissionCode.Order_WechatDeposit_Manual },

                //new Permission { Name = "网银支付存款列表_查询", Parent = p2sub3, Status = EntityStatus.Enabled, Code = PermissionCode.Order_OnlineBankingDeposit_Access },
                //new Permission { Name = "网银支付存款列表_查询订单", Parent = p2sub3, Status = EntityStatus.Enabled, Code = PermissionCode.Order_OnlineBankingDeposit_Query },
                //new Permission { Name = "网银支付存款列表_补发商户回调", Parent = p2sub3, Status = EntityStatus.Enabled, Code = PermissionCode.Order_OnlineBankingDeposit_Manual },

                //new Permission { Name = "充值卡存款列表_查询", Parent = p2sub4, Status = EntityStatus.Enabled, Code = PermissionCode.Order_PrepaidCardDeposit_Access },
                //new Permission { Name = "充值卡存款列表_查询订单", Parent = p2sub4, Status = EntityStatus.Enabled, Code = PermissionCode.Order_PrepaidCardDeposit_Query },
                //new Permission { Name = "充值卡存款列表_补发商户回调", Parent = p2sub4, Status = EntityStatus.Enabled, Code = PermissionCode.Order_PrepaidCardDeposit_Manual },

                //new Permission { Name = "QQ钱包存款列表_查询", Parent = p2sub5, Status = EntityStatus.Enabled, Code = PermissionCode.Order_QQWalletDeposit_Access },
                //new Permission { Name = "QQ钱包存款列表_查询订单", Parent = p2sub5, Status = EntityStatus.Enabled, Code = PermissionCode.Order_QQWalletDeposit_Query },
                //new Permission { Name = "QQ钱包存款列表_补发商户回调", Parent = p2sub5, Status = EntityStatus.Enabled, Code = PermissionCode.Order_QQWalletDeposit_Manual },

                //new Permission { Name = "银联支付存款列表_查询", Parent = p2sub6, Status = EntityStatus.Enabled, Code = PermissionCode.Order_QuickPayDeposit_Access },
                //new Permission { Name = "银联支付存款列表_查询订单", Parent = p2sub6, Status = EntityStatus.Enabled, Code = PermissionCode.Order_QuickPayDeposit_Query },
                //new Permission { Name = "银联支付存款列表_补发商户回调", Parent = p2sub6, Status = EntityStatus.Enabled, Code = PermissionCode.Order_QuickPayDeposit_Manual },

                //new Permission { Name = "支付宝转卡存款列表_查询", Parent = p2sub7, Status = EntityStatus.Enabled, Code = PermissionCode.Order_OnlineToBankCardDeposit_Access },
                //new Permission { Name = "支付宝转卡存款列表_查询订单", Parent = p2sub7, Status = EntityStatus.Enabled, Code = PermissionCode.Order_OnlineToBankCardDeposit_Query },
                //new Permission { Name = "支付宝转卡存款列表_补发商户回调", Parent = p2sub7, Status = EntityStatus.Enabled, Code = PermissionCode.Order_OnlineToBankCardDeposit_Manual },

                //new Permission { Name = "京东钱包存款列表_查询", Parent = p2sub8, Status = EntityStatus.Enabled, Code = PermissionCode.Order_JDWalletDeposit_Access },
                //new Permission { Name = "京东钱包存款列表_查询订单", Parent = p2sub8, Status = EntityStatus.Enabled, Code = PermissionCode.Order_JDWalletDeposit_Query },
                //new Permission { Name = "京东钱包存款列表_补发商户回调", Parent = p2sub8, Status = EntityStatus.Enabled, Code = PermissionCode.Order_JDWalletDeposit_Manual },

                //new Permission { Name = "灌钱存款列表_查询", Parent = p2sub9, Status = EntityStatus.Enabled, Code = PermissionCode.Order_InternalDeposit_Access },
                //new Permission { Name = "灌钱存款列表_查询订单", Parent = p2sub9, Status = EntityStatus.Enabled, Code = PermissionCode.Order_InternalDeposit_Query },
                //new Permission { Name = "灌钱存款列表_补发商户回调", Parent = p2sub9, Status = EntityStatus.Enabled, Code = PermissionCode.Order_InternalDeposit_Manual },

                new Permission { Name = "存款列表_查询", Parent = p2sub1, Status = EntityStatus.Enabled, Code = PermissionCode.Order_Deposit_Access },
                new Permission { Name = "存款列表_查询订单", Parent = p2sub1, Status = EntityStatus.Enabled, Code = PermissionCode.Order_Deposit_Query },
                new Permission { Name = "存款列表_补发商户回调", Parent = p2sub1, Status = EntityStatus.Enabled, Code = PermissionCode.Order_Deposit_Manual },

                new Permission { Name = "提款列表_查询", Parent = p2sub1, Status = EntityStatus.Enabled, Code = PermissionCode.Order_MerchantWithdrawal_Access },

                new Permission { Name = "角色列表_查询", Parent = p3sub1, Status = EntityStatus.Enabled, Code = PermissionCode.System_Role_Access },
                new Permission { Name = "角色资料_新增", Parent = p3sub1, Status = EntityStatus.Enabled, Code = PermissionCode.System_Role_Create },
                new Permission { Name = "角色资料_修改", Parent = p3sub1, Status = EntityStatus.Enabled, Code = PermissionCode.System_Role_Detail_Edit },
                new Permission { Name = "角色资料_修改角色状态", Parent = p3sub1, Status = EntityStatus.Enabled, Code = PermissionCode.System_Role_Detail_Status_Edit },

                new Permission { Name = "使用者列表_查询", Parent = p3sub3, Status = EntityStatus.Enabled, Code = PermissionCode.System_User_Access },
                new Permission { Name = "使用者资料_新增", Parent = p3sub3, Status = EntityStatus.Enabled, Code = PermissionCode.System_User_Create },
                new Permission { Name = "使用者资料_修改", Parent = p3sub3, Status = EntityStatus.Enabled, Code = PermissionCode.System_User_Detail_Edit },
                new Permission { Name = "使用者资料_修改使用者状态", Parent = p3sub3, Status = EntityStatus.Enabled, Code = PermissionCode.System_User_Detail_Status_Edit },

                new Permission { Name = "支付渠道列表_查询", Parent = p3sub4, Status = EntityStatus.Enabled, Code = PermissionCode.System_Paymentchannel_Access },
                new Permission { Name = "支付渠道资料_新增", Parent = p3sub4, Status = EntityStatus.Enabled, Code = PermissionCode.System_Paymentchannel_Create },
                new Permission { Name = "支付渠道资料_修改", Parent = p3sub4, Status = EntityStatus.Enabled, Code = PermissionCode.System_Paymentchannel_Detail_Edit },
                new Permission { Name = "支付渠道资料_修改状态", Parent = p3sub4, Status = EntityStatus.Enabled, Code = PermissionCode.System_Paymentchannel_Detail_Status_Edit },

                new Permission { Name = "银行总表列表_查询", Parent = p3sub5, Status = EntityStatus.Enabled, Code = PermissionCode.System_Bank_Access },
                new Permission { Name = "银行总表资料_新增", Parent = p3sub5, Status = EntityStatus.Enabled, Code = PermissionCode.System_Bank_Create },
                new Permission { Name = "银行总表资料_修改", Parent = p3sub5, Status = EntityStatus.Enabled, Code = PermissionCode.System_Bank_Detail_Edit },
                new Permission { Name = "银行总表资料_修改状态", Parent = p3sub5, Status = EntityStatus.Enabled, Code = PermissionCode.System_Bank_Detail_Status_Edit },

                new Permission { Name = "支付服务商列表_查询", Parent = p3sub6, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentProvider_Access },
                new Permission { Name = "支付服务商资料_新增", Parent = p3sub6, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentProvider_Create },
                new Permission { Name = "支付服务商资料_修改", Parent = p3sub6, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentProvider_Detail_Edit },
                new Permission { Name = "支付服务商资料_修改状态", Parent = p3sub6, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentProvider_Detail_Status_Edit },

                new Permission { Name = "支付银行列表_查询", Parent = p3sub7, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentProviderBank_Access },
                new Permission { Name = "支付银行资料_新增", Parent = p3sub7, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentProviderBank_Create },
                //new Permission { Name = "支付银行资料_详细", Parent = p3sub7, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentProviderBank_Export },
                new Permission { Name = "支付银行资料_修改", Parent = p3sub7, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentProviderBank_Detail_Edit },
                new Permission { Name = "支付银行资料_修改状态", Parent = p3sub7, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentProviderBank_Detail_Status_Edit },

                new Permission { Name = "业务日志_查询", Parent = p3sub8, Status = EntityStatus.Enabled, Code = PermissionCode.System_EntityLog_Access }

                //new Permission { Name = "支付类型列表_取得", Parent = p3sub5, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentType_Access },
                //new Permission { Name = "支付方式列表_取得", Parent = p3sub6, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentMethod_Access },
                //new Permission { Name = "支付平台列表_取得", Parent = p3sub7, Status = EntityStatus.Enabled, Code = PermissionCode.System_PaymentPlatform_Access },
                );
            #endregion

            // commit
            context.SaveChanges();

            #region user and role
            User user = new User()
            {
                Username = "Admin",
                CreatedAt = DateTime.Now.ToE8(),
                Status = EntityStatus.Enabled,
                Password = PasswordUtility.Hash("admin123")
            };

            context.Users.AddOrUpdate(
                r => new { r.Username },
                user
                );

            Role role = new Role()
            {
                Name = "Administrators",
                CreatedAt = DateTime.Now.ToE8(),
                CreatedBy = user,
                Status = EntityStatus.Enabled,
            };

            role.Users = new List<User>();
            role.Users.Add(user);
            role.Permissions = new List<Permission>();

            foreach (var ii in context.Permissions)
            {
                role.Permissions.Add(ii);
            }

            context.Roles.AddOrUpdate(
              role
              );

            #endregion

            context.Banks.AddOrUpdate(
                r => new { r.Name, r.Code },
                new Bank { Name = "工商银行", Code = "ICBC", Url = "http://www.icbc.com.cn/icbc/", CreatedAt = DateTime.Now },
                new Bank { Name = "建设银行", Code = "CCB", Url = "http://www.ccb.com/cn/home/indexv3.html", CreatedAt = DateTime.Now },
                new Bank { Name = "农业银行", Code = "ABC", Url = "http://www.abchina.com/cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "招商银行", Code = "CMB", Url = "http://www.cmbchina.com/", CreatedAt = DateTime.Now },
                new Bank { Name = "民生银行", Code = "CMBC", Url = "http://www.cmbc.com.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "交通银行", Code = "BOCO", Url = "http://www.bankcomm.com/", CreatedAt = DateTime.Now },
                new Bank { Name = "邮政银行", Code = "PSBC", Url = "http://www.psbc.com/cn/index.html", CreatedAt = DateTime.Now },
                new Bank { Name = "中国银行", Code = "BOC", Url = "http://www.boc.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "兴业银行", Code = "CIB", Url = "http://www.cib.com.cn/cn/index.html", CreatedAt = DateTime.Now },
                new Bank { Name = "光大银行", Code = "CEB", Url = "http://www.cebbank.com/", CreatedAt = DateTime.Now },
                new Bank { Name = "平安银行", Code = "PAB", Url = "http://www.pingan.com/", CreatedAt = DateTime.Now },
                new Bank { Name = "北京银行", Code = "BOB", Url = "http://www.bankofbeijing.com.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "中信银行", Code = "ECITIC", Url = "http://www.citicbank.com/", CreatedAt = DateTime.Now },
                new Bank { Name = "广发银行", Code = "CGB", Url = "http://www.cgbchina.com.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "浦发银行", Code = "SPDB", Url = "http://www.spdb.com.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "上海银行", Code = "BOS", Url = "http://www.bosc.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "宁波银行", Code = "NBCB", Url = "http://www.nbcb.com.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "东亚银行", Code = "HKBEA", Url = "http://www.hkbea.com.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "绵阳市商业银行", Code = "MCCB", Url = "https://www.mycc-bank.com/", CreatedAt = DateTime.Now },
                new Bank { Name = "渤海银行", Code = "CBHB", Url = "http://www.cbhb.com.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "南京银行", Code = "NJCB", Url = "http://www.njcb.com.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "北京农商银行", Code = "BRCB", Url = "http://www.bjrcb.com/", CreatedAt = DateTime.Now },
                new Bank { Name = "杭州银行", Code = "HCCB", Url = "http://www.hzbank.com.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "上海农商银行", Code = "SRCB", Url = "http://www.srcb.com", CreatedAt = DateTime.Now },
                new Bank { Name = "华夏银行", Code = "HXB", Url = "http://www.hxb.com.cn/home/cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "长沙银行", Code = "CSCB", Url = "http://www.cscb.cn/", CreatedAt = DateTime.Now },
                new Bank { Name = "易充32卡", Code = "YCCARDTW", Url = null, CreatedAt = DateTime.Now },
                new Bank { Name = "易充玖佰卡", Code = "YCJIUB", Url = null, CreatedAt = DateTime.Now },
                new Bank { Name = "易充天宏卡", Code = "YCTH", Url = null, CreatedAt = DateTime.Now },
                new Bank { Name = "易充纵游卡", Code = "YCZY", Url = null, CreatedAt = DateTime.Now }
                );

            context.Merchants.AddOrUpdate(
                r => r.Username,
                new Merchant { Username = "ap001", Name = "apAdmin", MerchantKey = "ap123qwe", CreatedAt = DateTime.Now, Status = EntityStatus.Enabled },
                new Merchant { Username = "rb001", Name = "rbAdmin", MerchantKey = "rb123qwe", CreatedAt = DateTime.Now, Status = EntityStatus.Enabled }
            );

            context.PaymentProviders.AddOrUpdate(
                r => r.Name,
                new PaymentProvider { Id = 1, Name = "蜻蜓", Status = EntityStatus.Enabled, CreatedAt = DateTime.Now, Balance = 5000, MerchantId = 2 },
                new PaymentProvider { Name = "testProvider", Status = EntityStatus.Enabled, CreatedAt = DateTime.Now, Balance = 5000, MerchantId = 2 }
                );

            context.PaymentProviderBanks.AddOrUpdate(
               r => r.Name,
               new PaymentProviderBank { Code = "ICBC", BankId = 1, CreatedAt = DateTime.Now, Order = 1, Status = EntityStatus.Enabled, Name = "工商银行", PaymentType = PaymentType.Withdrawal, PaymentMethod = PaymentMethod.OnlineBanking, PaymentProviderId = 1, MerchantId = 2 },
               new PaymentProviderBank { Code = "ICBC", BankId = 1, CreatedAt = DateTime.Now, Order = 1, Status = EntityStatus.Enabled, Name = "工商银行", PaymentType = PaymentType.Deposit, PaymentMethod = PaymentMethod.OnlineBanking, PaymentProviderId = 1, MerchantId = 2 },
               new PaymentProviderBank { Code = "ICBC", BankId = 1, CreatedAt = DateTime.Now, Order = 1, Status = EntityStatus.Enabled, Name = "工商银行", PaymentType = PaymentType.Deposit, PaymentMethod = PaymentMethod.OnlinePayment, PaymentProviderId = 1, MerchantId = 2 },
               new PaymentProviderBank { Code = "YCZY", BankId = 1, CreatedAt = DateTime.Now, Order = 1, Status = EntityStatus.Enabled, Name = "易充纵游卡", PaymentType = PaymentType.Deposit, PaymentMethod = PaymentMethod.PrepaidCard, PaymentProviderId = 1, MerchantId = 2 }
               );

            context.PaymentChannels.AddOrUpdate(
                r => r.Name,
                new PaymentChannel
                {
                    Name = "FakeWechat",
                    Status = EntityStatus.Enabled,
                    MerchantUsername = "1001677743",
                    MinAmount = 1,
                    MaxAmount = 500,
                    MerchantKey = "TestErrorKey",
                    CallbackUrl = "http://localhost:60322/api/pcg/1/1/1/1/4",
                    RequestUrl = "http://localhost:60034/api/fake",
                    QueryUrl = "http://localhost:60034/api/fakequery",
                    PaymentType = PaymentType.Deposit,
                    PaymentMethod = PaymentMethod.WeChat,
                    Arguments = @"{'QRCodeStyle':'Redirect','GatewayDomainName':'http://localhost:61830//tsg/requestfromurl?'}",
                    AssemblyName = "Fake",
                    CommissionPercentage = ((decimal)0.0290),
                    HandlerType = "Fake.FakeDepositHandler",
                    MerchantId = 2,
                    PaymentPlatform = PaymentPlatform.All,
                    ProviderId = 1,
                    CreatedAt = DateTime.Now,
                    CallbackSuccessType = CallbackSuccessType.success,
                    ExtensionRoute = ExtensionRoute.Default,
                }
                );

            context.PaymentChannels.AddOrUpdate(
                r => r.Name,
                new PaymentChannel
                {
                    Name = "FakeWithdrawal",
                    Status = EntityStatus.Enabled,
                    MerchantUsername = "1001677743",
                    MinAmount = 1,
                    MaxAmount = 500,
                    MerchantKey = "TestErrorKey",
                    CallbackUrl = "http://localhost:60322/api/pcg/2/2/4/1/4",
                    RequestUrl = "http://localhost:60034/api/fake",
                    QueryUrl = "http://localhost:60034/api/fakequery",
                    PaymentType = PaymentType.Withdrawal,
                    PaymentMethod = PaymentMethod.OnlineBanking,
                    AssemblyName = "Fake",
                    CommissionPercentage = ((decimal)0.0290),
                    HandlerType = "Fake.FakeDepositHandler",
                    MerchantId = 2,
                    PaymentPlatform = PaymentPlatform.All,
                    ProviderId = 1,
                    CreatedAt = DateTime.Now,
                    CallbackSuccessType = CallbackSuccessType.success,
                    ExtensionRoute = ExtensionRoute.Default
                }
                );

            context.PaymentChannels.AddOrUpdate(
                r => r.Name,
                new PaymentChannel
                {
                    Name = "FakeAlipay",
                    Status = EntityStatus.Enabled,
                    MerchantUsername = "1001677743",
                    MinAmount = 1,
                    MaxAmount = 500,
                    MerchantKey = "TestErrorKey",
                    CallbackUrl = "http://localhost:60322/api/pcg/3/1/2/1/4",
                    RequestUrl = "http://localhost:60034/api/fake",
                    QueryUrl = "http://localhost:60034/api/fakequery",
                    PaymentType = PaymentType.Deposit,
                    PaymentMethod = PaymentMethod.Alipay,
                    Arguments = @"{'QRCodeStyle':'Redirect','GatewayDomainName':'http://localhost:61830//tsg/requestfromurl?'}",
                    AssemblyName = "Fake",
                    CommissionPercentage = ((decimal)0.0290),
                    HandlerType = "Fake.FakeDepositHandler",
                    MerchantId = 2,
                    PaymentPlatform = PaymentPlatform.All,
                    ProviderId = 1,
                    CreatedAt = DateTime.Now,
                    CallbackSuccessType = CallbackSuccessType.success,
                    ExtensionRoute = ExtensionRoute.Default,
                }
                );

            context.PaymentChannels.AddOrUpdate(
                r => r.Name,
                new PaymentChannel
                {
                    Name = "FakeOP",
                    Status = EntityStatus.Enabled,
                    MerchantUsername = "1001677743",
                    MinAmount = 1,
                    MaxAmount = 500,
                    MerchantKey = "TestErrorKey",
                    CallbackUrl = "http://localhost:60322/api/pcg/4/1/3/1/4",
                    RequestUrl = "http://localhost:60034/api/fake",
                    QueryUrl = "http://localhost:60034/api/fakequery",
                    PaymentType = PaymentType.Deposit,
                    PaymentMethod = PaymentMethod.OnlinePayment,
                    Arguments = @"{'QRCodeStyle':'Redirect','GatewayDomainName':'http://localhost:61830//tsg/requestfromurl?'}",
                    AssemblyName = "Fake",
                    CommissionPercentage = ((decimal)0.0290),
                    HandlerType = "Fake.FakeDepositHandler",
                    MerchantId = 2,
                    PaymentPlatform = PaymentPlatform.All,
                    ProviderId = 1,
                    CreatedAt = DateTime.Now,
                    CallbackSuccessType = CallbackSuccessType.success,
                    ExtensionRoute = ExtensionRoute.Default,
                }
                );

            context.PaymentChannels.AddOrUpdate(
                r => r.Name,
                new PaymentChannel
                {
                    Name = "FakeOB",
                    Status = EntityStatus.Enabled,
                    MerchantUsername = "1001677743",
                    MinAmount = 1,
                    MaxAmount = 500,
                    MerchantKey = "TestErrorKey",
                    CallbackUrl = "http://localhost:60322/api/pcg/5/1/4/1/4",
                    RequestUrl = "http://localhost:60034/api/fake",
                    QueryUrl = "http://localhost:60034/api/fakequery",
                    PaymentType = PaymentType.Deposit,
                    PaymentMethod = PaymentMethod.OnlineBanking,
                    Arguments = @"{'QRCodeStyle':'Redirect','GatewayDomainName':'http://localhost:61830//tsg/requestfromurl?'}",
                    AssemblyName = "Fake",
                    CommissionPercentage = ((decimal)0.0290),
                    HandlerType = "Fake.FakeDepositHandler",
                    MerchantId = 2,
                    PaymentPlatform = PaymentPlatform.All,
                    ProviderId = 1,
                    CreatedAt = DateTime.Now,
                    CallbackSuccessType = CallbackSuccessType.success,
                    ExtensionRoute = ExtensionRoute.Default,
                }
                );

            context.PaymentChannels.AddOrUpdate(
                r => r.Name,
                new PaymentChannel
                {
                    Name = "FakePrepaidCard",
                    Status = EntityStatus.Enabled,
                    MerchantUsername = "1001677743",
                    MinAmount = 1,
                    MaxAmount = 500,
                    MerchantKey = "TestErrorKey",
                    CallbackUrl = "http://localhost:60322/api/pcg/6/1/5/1/4",
                    RequestUrl = "http://localhost:60034/api/fake",
                    QueryUrl = "http://localhost:60034/api/fakequery",
                    PaymentType = PaymentType.Deposit,
                    PaymentMethod = PaymentMethod.PrepaidCard,
                    Arguments = @"{'QRCodeStyle':'Redirect','GatewayDomainName':'http://localhost:61830//tsg/requestfromurl?'}",
                    AssemblyName = "Fake",
                    CommissionPercentage = ((decimal)0.0290),
                    HandlerType = "Fake.FakeDepositHandler",
                    MerchantId = 2,
                    PaymentPlatform = PaymentPlatform.All,
                    ProviderId = 1,
                    CreatedAt = DateTime.Now,
                    CallbackSuccessType = CallbackSuccessType.success,
                    ExtensionRoute = ExtensionRoute.Default,
                }
                );

            context.PaymentChannels.AddOrUpdate(
                r => r.Name,
                new PaymentChannel
                {
                    Name = "FakeQQWallet",
                    Status = EntityStatus.Enabled,
                    MerchantUsername = "1001677743",
                    MinAmount = 1,
                    MaxAmount = 500,
                    MerchantKey = "TestErrorKey",
                    CallbackUrl = "http://localhost:60322/api/pcg/7/1/6/1/4",
                    RequestUrl = "http://localhost:60034/api/fake",
                    QueryUrl = "http://localhost:60034/api/fakequery",
                    PaymentType = PaymentType.Deposit,
                    PaymentMethod = PaymentMethod.QQWallet,
                    Arguments = @"{'QRCodeStyle':'Redirect','GatewayDomainName':'http://localhost:61830//tsg/requestfromurl?'}",
                    AssemblyName = "Fake",
                    CommissionPercentage = ((decimal)0.0290),
                    HandlerType = "Fake.FakeDepositHandler",
                    MerchantId = 2,
                    PaymentPlatform = PaymentPlatform.All,
                    ProviderId = 1,
                    CreatedAt = DateTime.Now,
                    CallbackSuccessType = CallbackSuccessType.success,
                    ExtensionRoute = ExtensionRoute.Default,
                }
                );

            context.PaymentChannels.AddOrUpdate(
                r => r.Name,
                new PaymentChannel
                {
                    Name = "FakeQP",
                    Status = EntityStatus.Enabled,
                    MerchantUsername = "1001677743",
                    MinAmount = 1,
                    MaxAmount = 500,
                    MerchantKey = "TestErrorKey",
                    CallbackUrl = "http://localhost:60322/api/pcg/8/1/7/1/4",
                    RequestUrl = "http://localhost:60034/api/fake",
                    QueryUrl = "http://localhost:60034/api/fakequery",
                    PaymentType = PaymentType.Deposit,
                    PaymentMethod = PaymentMethod.QuickPay,
                    Arguments = @"{'QRCodeStyle':'Redirect','GatewayDomainName':'http://localhost:61830//tsg/requestfromurl?'}",
                    AssemblyName = "Fake",
                    CommissionPercentage = ((decimal)0.0290),
                    HandlerType = "Fake.FakeDepositHandler",
                    MerchantId = 2,
                    PaymentPlatform = PaymentPlatform.All,
                    ProviderId = 1,
                    CreatedAt = DateTime.Now,
                    CallbackSuccessType = CallbackSuccessType.success,
                    ExtensionRoute = ExtensionRoute.Default,
                }
                );

            context.PaymentChannels.AddOrUpdate(
                r => r.Name,
                new PaymentChannel
                {
                    Name = "FakeOTBC",
                    Status = EntityStatus.Enabled,
                    MerchantUsername = "1001677743",
                    MinAmount = 1,
                    MaxAmount = 500,
                    MerchantKey = "TestErrorKey",
                    CallbackUrl = "http://localhost:60322/api/pcg/9/1/8/1/4",
                    RequestUrl = "http://localhost:60034/api/fake",
                    QueryUrl = "http://localhost:60034/api/fakequery",
                    PaymentType = PaymentType.Deposit,
                    PaymentMethod = PaymentMethod.OnlineToBankCard,
                    Arguments = @"{'QRCodeStyle':'Redirect','GatewayDomainName':'http://localhost:61830//tsg/requestfromurl?'}",
                    AssemblyName = "Fake",
                    CommissionPercentage = ((decimal)0.0290),
                    HandlerType = "Fake.FakeDepositHandler",
                    MerchantId = 2,
                    PaymentPlatform = PaymentPlatform.All,
                    ProviderId = 1,
                    CreatedAt = DateTime.Now,
                    CallbackSuccessType = CallbackSuccessType.success,
                    ExtensionRoute = ExtensionRoute.Default,
                }
                );

            context.PaymentChannels.AddOrUpdate(
                r => r.Name,
                new PaymentChannel
                {
                    Name = "FakeJD",
                    Status = EntityStatus.Enabled,
                    MerchantUsername = "1001677743",
                    MinAmount = 1,
                    MaxAmount = 500,
                    MerchantKey = "TestErrorKey",
                    CallbackUrl = "http://localhost:60322/api/pcg/10/1/9/1/4",
                    RequestUrl = "http://localhost:60034/api/fake",
                    QueryUrl = "http://localhost:60034/api/fakequery",
                    PaymentType = PaymentType.Deposit,
                    PaymentMethod = PaymentMethod.JDWallet,
                    Arguments = @"{'QRCodeStyle':'Redirect','GatewayDomainName':'http://localhost:61830//tsg/requestfromurl?'}",
                    AssemblyName = "Fake",
                    CommissionPercentage = ((decimal)0.0290),
                    HandlerType = "Fake.FakeDepositHandler",
                    MerchantId = 2,
                    PaymentPlatform = PaymentPlatform.All,
                    ProviderId = 1,
                    CreatedAt = DateTime.Now,
                    CallbackSuccessType = CallbackSuccessType.success,
                    ExtensionRoute = ExtensionRoute.Default,
                }
                );

            context.SaveChanges();
        }
    }
}
