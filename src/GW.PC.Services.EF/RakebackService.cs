﻿using GW.PC.Models;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using GW.PC.Services.EF.Gaming;
using Newtonsoft.Json;
using GW.PC.Core;

namespace GW.PC.Services.EF.Admin
{
    public class RakebackService : CreatableEntityService<Rakeback>
    {
        public async Task Audit(int id, bool result, int auditedById)
        {
            using (var ctx = new GWContext())
            {
                var rakeback = await EnsureEntity(id, ctx);
                var now = DateTime.Now;

                if (rakeback.Status == AuditStatus.AwaitApproval)
                {
                    rakeback.Status = result ? AuditStatus.Approved : AuditStatus.Declined;
                    rakeback.AuditedAt = now;
                    rakeback.AuditedById = auditedById;

                    var user = await EnsureEntity<User>(auditedById, ctx);
                    AddUserLog(ctx, user.Username, $"账户返水审核{(result ? "成功" : "失败")}");

                    if (result)
                    {
                        // Update customer wallet
                        var wallet = await EnsureEntity<Wallet>(rakeback.Username, ctx);
                        wallet.Balance += rakeback.RakebackAmount;
                        wallet.TotalRakebackAmount += rakeback.RakebackAmount;
                        wallet.AmountLastUpdatedAt = now;

                        // Add wallet transaction
                        AddWalletTransaction(ctx, rakeback.RakebackAmount, wallet.Balance, WalletTransactionType.Rakeback, rakeback.Username, rakeback.Id, now);

                        var customer = await EnsureEntity<Customer>(rakeback.Username, ctx, "Agent");
                        if (customer.Agent != null)
                        {
                            customer.Agent.CustomerAmountLastUpdatedAt = now;
                        }
                    }

                    await ctx.SaveChangesAsync();
                }
            }
        }

        public async Task<Tuple<int, IEnumerable<string>>> Generate(int createdById)
        {
            var rakebacks = new List<Rakeback>();
            var failedUsernames = new List<string>();
            var games = await new GameService().GetRakebackGames();
            var now = DateTime.Now.ToE8();

            using (var ctx = new GWContext())
            {
                foreach (var game in games)
                {
                    var periodsStartAt = GetPeriodStartAt(game);
                    if (periodsStartAt.Item1.HasValue)
                    {
                        // 上一周期的正常返水
                        await GenerateRakebacks(ctx, game, periodsStartAt.Item1.Value, RakebackType.Regular);
                    }
                    // 当前周期的提前返水
                    await GenerateRakebacks(ctx, game, periodsStartAt.Item2, RakebackType.Advanced);
                }

                ctx.Rakebacks.AddRange(rakebacks);

                await ctx.SaveChangesAsync();

                return Tuple.Create(rakebacks.Count, failedUsernames.AsEnumerable());
            }

            Tuple<DateTime?, DateTime> GetPeriodStartAt(Game game)
            {
                var rakebackAt = game.RakebackAt.Value;
                var rakebackDeadline = rakebackAt.AddMinutes(-5);
                if (now >= new DateTime(now.Year, now.Month, now.Day,
                    rakebackDeadline.Hour, rakebackDeadline.Minute, rakebackDeadline.Second) &&
                    now <= new DateTime(now.Year, now.Month, now.Day,
                    rakebackAt.Hour, rakebackAt.Minute, rakebackAt.Second))
                {
                    throw new NotSupportedException("该时间段内不能办理返水");
                }

                DateTime yesterday = now.AddDays(-1), currentPeriodStart;
                DateTime? lastPeriodStart = null;
                if (now > new DateTime(now.Year, now.Month, now.Day,
                    rakebackAt.Hour, rakebackAt.Millisecond, rakebackAt.Second))
                {
                    // 在当日返水时间点之后，可以做当前周期的提前返水，以及上一周期的正常返水
                    currentPeriodStart = new DateTime(now.Year, now.Month, now.Day, rakebackAt.Hour, rakebackAt.Minute, rakebackAt.Second);
                    lastPeriodStart = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day, rakebackAt.Hour, rakebackAt.Minute, rakebackAt.Second);
                }
                else
                {
                    // 在当日返水时间点之前，只能做当前周期的提前返水
                    currentPeriodStart = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day, rakebackAt.Hour, rakebackAt.Minute, rakebackAt.Second);
                }

                return Tuple.Create(lastPeriodStart, currentPeriodStart);
            }

            async Task GenerateRakebacks(GWContext ctx, Game game, DateTime periodStartsAt, RakebackType type)
            {
                var dataItems = await ctx.CustomerGameData.Join(
                    ctx.CustomerGameData.Where(
                        ddd => ddd.GameId == game.Id &&
                        ddd.PeriodStartsAt == periodStartsAt &&
                        ddd.Status == EntityStatus.Enabled)
                        .GroupBy(
                            g => new { g.Username, g.GameId },
                            (k, g) => new
                            {
                                k.Username,
                                k.GameId,
                                LastCreatedAt = g.Max(gg => gg.CreatedAt)
                            }),
                    d => d.Username,
                    dd => dd.Username,
                    (d, dd) => new { d, dd })
                    .Where(o => o.d.CreatedAt == o.dd.LastCreatedAt && o.d.GameId == game.Id &&
                    o.d.PeriodStartsAt == periodStartsAt &&
                    o.d.Status == EntityStatus.Enabled)
                    .Select(o => o.d)
                    .ToListAsync();

                var usernames = dataItems.Select(d => d.Username);
                var awaitingUsernames = ctx.Rakebacks.Where(
                    r => r.Status == AuditStatus.AwaitApproval &&
                    r.PeriodStartsAt == periodStartsAt &&
                    r.GameId == game.Id &&
                    usernames.Contains(r.Username))
                    .Select(r => r.Username)
                    .Distinct()
                    .ToList();
                failedUsernames.AddRange(awaitingUsernames);
                usernames = usernames.Except(awaitingUsernames);

                var existing = await ctx.Rakebacks.Where(
                    r => r.PeriodStartsAt == periodStartsAt &&
                    r.GameId == game.Id &&
                    usernames.Contains(r.Username))
                    .ToListAsync();
                var customers = await new CustomerService().GetByUsernames(usernames);
                var configurations = await GetConfigurations();

                dataItems = dataItems.Where(d => usernames.Contains(d.Username)).ToList();
                foreach (var item in dataItems)
                {
                    var declined = existing.Where(
                        d => d.Status == AuditStatus.Declined &&
                        d.Username.Equals(item.Username, StringComparison.OrdinalIgnoreCase));
                    if (declined.Any(d => d.ValidBetAmount == item.TotalBetAmount && d.ProfitAmount == item.ProfitAmount))
                    {
                        continue;
                    }

                    var customer = customers.SingleOrDefault(c => c.Username.Equals(item.Username, StringComparison.OrdinalIgnoreCase));
                    var configuration = configurations.SingleOrDefault(
                        c => c.CustomerGradeId == customer?.GradeId);
                    if (!string.IsNullOrEmpty(configuration?.Configurations))
                    {
                        var arguments = JsonConvert.DeserializeObject<IEnumerable<RakebackConfigurationItem>>(configuration.Configurations);
                        var argument = arguments.SingleOrDefault(c => c.GameName == game.Name);
                        var percentage = argument?.Percentage;

                        var existingApprovedAmount = existing.Where(
                            r => r.Status == AuditStatus.Approved &&
                            r.Username.Equals(item.Username, StringComparison.OrdinalIgnoreCase))
                            .Sum(r => (decimal?)r.RakebackAmount);

                        var rakeback = new Rakeback
                        {
                            CreatedAt = DateTime.Now.ToE8(),
                            CreatedById = createdById,
                            GameId = game.Id,
                            PeriodEndsAt = periodStartsAt.AddHours(23).AddMinutes(59).AddSeconds(59),
                            PeriodStartsAt = periodStartsAt,
                            ProfitAmount = item.ProfitAmount,
                            ValidBetAmount = item.TotalBetAmount,
                            Username = item.Username,
                            Type = type,
                            Status = AuditStatus.AwaitApproval
                        };
                        rakeback.RakebackPercentage = percentage.GetValueOrDefault();
                        rakeback.RakebackAmount = Math.Round(item.TotalBetAmount * (percentage ?? 0), 2) - (existingApprovedAmount ?? 0);

                        if (rakeback.RakebackAmount > 0)
                        {
                            rakebacks.Add(rakeback);
                        }
                        else
                        {
                            var last = existing.Where(
                                d => d.Status == AuditStatus.Approved &&
                                d.Username.Equals(item.Username, StringComparison.OrdinalIgnoreCase))
                                .OrderByDescending(d => d.CreatedAt)
                                .FirstOrDefault();

                            if (last == null ||
                                item.TotalBetAmount > last.ValidBetAmount ||
                                (item.TotalBetAmount == last.ValidBetAmount && item.ProfitAmount != last.ProfitAmount))
                            {
                                rakebacks.Add(rakeback);
                            }
                        }

                        //var maxAmount = argument?.MaxAmount;
                        //if (maxAmount.HasValue && rakeback.RakebackAmount > maxAmount)
                        //{
                        //    rakeback.RakebackAmount = maxAmount.Value;

                        //    rakebacks.Add(new Rakeback
                        //    {
                        //        CreatedAt = now,
                        //        GameId = rakeback.GameId,
                        //        PeriodEndsAt = rakeback.PeriodEndsAt,
                        //        PeriodStartsAt = rakeback.PeriodStartsAt,
                        //        RakebackAmount = rakeback.RakebackAmount - maxAmount.Value,
                        //        RakebackPercentage = rakeback.RakebackPercentage,
                        //        Status = AuditStatus.AwaitApproval,
                        //        Type = RakebackType.Split,
                        //        Username = customer.Username
                        //    });
                        //}
                    }
                }
            }
        }

        public async Task<IEnumerable<RakebackConfiguration>> GetConfigurations()
        {
            using (var ctx = new GWContext())
            {
                return await ctx.RakebackConfigurations.ToListAsync();
            }
        }
    }
}
