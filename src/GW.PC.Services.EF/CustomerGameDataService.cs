﻿using GW.PC.Models;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using GW.PC.Services.EF.Gaming;
using GW.PC.Core;
using Newtonsoft.Json;

namespace GW.PC.Services.EF
{
    public class CustomerGameDataService : CreatableEntityService<CustomerGameData>
    {
        public async Task<Tuple<int, IEnumerable<string>>> AddRange(IList<CustomerGameData> items)
        {
            var failedResults = new List<UploadResult>();

            // Exclude duplicate usernames in a single batch data.
            failedResults.AddRange(items.GroupBy(x => x.Username)
              .Where(g => g.Count() > 1)
              .Select(y => new UploadResult
              {
                  Username = y.Key,
                  Message = "文件中有重复账号数据"
              })
              .ToList());
            UpdateValidItems();

            using (var ctx = new GWContext())
            {
                var first = items.First();

                var gameService = new GameService();
                var game = await EnsureEntity<Game>(first.GameId, ctx);

                // Now item.Username is actually game username. Need to update it to customer username.
                var customerGames = await gameService.GetCustomerGames(items.Select(d => d.Username), game.Name);
                foreach (var item in items)
                {
                    var customerGame = customerGames.SingleOrDefault(g => g.GameUsername.Equals(item.Username, StringComparison.OrdinalIgnoreCase));
                    if (customerGame != null)
                    {
                        item.Username = customerGame.Username;
                    }
                    else
                    {
                        failedResults.Add(new UploadResult
                        {
                            Username = item.Username,
                            Message = "游戏账号无效"
                        });
                    }
                }
                UpdateValidItems();

                // Check if the uploaded amount is bigger than existing data.
                var itemUsernames = items.Select(d => d.Username);
                var existingData = await ctx.CustomerGameData.Where(
                    r => r.PeriodStartsAt == first.PeriodStartsAt &&
                    r.PeriodEndsAt == first.PeriodEndsAt &&
                    r.Status == EntityStatus.Enabled &&
                    r.GameId == first.GameId &&
                    itemUsernames.Contains(r.Username))
                    .ToListAsync();
                foreach (var item in items)
                {
                    var last = existingData.Where(
                        d => d.Username.Equals(item.Username, StringComparison.OrdinalIgnoreCase))
                        .OrderByDescending(d => d.CreatedAt)
                        .FirstOrDefault();

                    if (!(last == null ||
                        item.TotalBetAmount > last.TotalBetAmount ||
                        (item.TotalBetAmount == last.TotalBetAmount && item.ProfitAmount != last.ProfitAmount)))
                    {
                        failedResults.Add(new UploadResult
                        {
                            Username = item.Username,
                            Message = "有效投注额或输赢额度无效."
                        });
                    }
                }
                UpdateValidItems();

                await base.AddRange(items);

                return Tuple.Create(items.Count(),
                    failedResults.Select(r => JsonConvert.SerializeObject(r)));
            }

            void UpdateValidItems()
            {
                items = items.Where(
                    r => !failedResults.Any(f => f.Username.Equals(r.Username, StringComparison.OrdinalIgnoreCase))).ToList();
            }
        }

        public async Task<IEnumerable<CustomerGameData>> Get(DateTime periodStartsAt, DateTime periodEndsAt, params string[] usernames)
        {
            using (var ctx = new GWContext())
            {
                var kgPeriodStartsAt = periodStartsAt.AddHours(-12);
                var kgPeriodEndsAt = periodEndsAt.AddHours(-12);
                var kgId = (await ctx.Games.SingleAsync(g => g.Name == Constants.GameNames.KG)).Id;

                var inner = ctx.CustomerGameData.Where(d => d.Status == EntityStatus.Enabled &&
                    ((d.GameId != kgId && d.PeriodStartsAt >= periodStartsAt && d.PeriodEndsAt <= periodEndsAt) ||
                    (d.GameId == kgId && d.PeriodStartsAt >= kgPeriodStartsAt && d.PeriodEndsAt <= kgPeriodEndsAt)));
                if (usernames?.Length > 0)
                {
                    inner = inner.Where(o => usernames.Contains(o.Username));
                }
                var query = ctx.CustomerGameData.Join(
                    inner.GroupBy(
                        g => new { g.Username, g.GameId },
                        (k, g) => new
                        {
                            k.Username,
                            k.GameId,
                            LastCreatedAt = g.Max(gg => gg.CreatedAt)
                        }),
                    d => d.Username,
                    dd => dd.Username,
                    (d, dd) => new { d, dd })
                    .Where(o => o.d.CreatedAt == o.dd.LastCreatedAt &&
                    o.d.Status == EntityStatus.Enabled &&
                    ((o.d.GameId != kgId && o.d.PeriodStartsAt >= periodStartsAt && o.d.PeriodEndsAt <= periodEndsAt) ||
                    (o.d.GameId == kgId && o.d.PeriodStartsAt >= kgPeriodStartsAt && o.d.PeriodEndsAt <= kgPeriodEndsAt)));
                if (usernames?.Length > 0)
                {
                    query = query.Where(o => usernames.Contains(o.d.Username));
                }

                return await query.Select(o => o.d).ToListAsync();
            }
        }

        public async Task Discard(int id, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var data = await EnsureEntity(id, ctx);

                data.Status = EntityStatus.Deprecated;
                data.DiscardedAt = DateTime.Now;
                data.DiscardedById = operatorId;

                await ctx.SaveChangesAsync();
            }
        }

        private class UploadResult
        {
            public string Username { get; set; }
            public string Message { get; set; }
        }
    }
}
