﻿using GW.PC.Core;
using GW.PC.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class MerchantService : CreatableEntityService<Merchant>
    {
        public async Task Save(Merchant mt)
        {
            using (var ctx = new PCContext())
            {
                if (mt.Id == 0)
                {
                    try
                    {
                        await CheckUniqueness(c => c.Username == mt.Username);
                    }
                    catch (BusinessException)
                    {

                        throw new BusinessException
                            (string.Format(Constants.PCAdminMessages.UsernameAlreadyExists, mt.Username),
                            Constants.PCAdminMessages.DuplicateUsernameCode);
                    }

                    ctx.Merchants.Add(mt);
                }
                else
                {
                    var dbMt = await ctx.Merchants.FindAsync(mt.Id);

                    dbMt.Username = mt.Username;
                    dbMt.Name = mt.Name;
                    dbMt.MerchantKey = mt.MerchantKey;
                    dbMt.Notes = mt.Notes;
                    dbMt.Status = mt.Status;
                    dbMt.CreatedAt = mt.CreatedAt;
                    dbMt.CreatedById = mt.CreatedById;

                    dbMt.Status = mt.Status;
                }

                await ctx.SaveChangesAsync();
            }
        }

        public async Task UpdateEntityStatus(int id, EntityStatus status, int operatorId)
        {
            using (var ctx = new PCContext())
            {
                var entity = await EnsureEntity<Merchant>(id, ctx);
                var user = await EnsureEntity<User>(operatorId, ctx);

                entity.Status = status;

                AddUserLog(ctx, user.Username, "修改了状态");

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<Merchant> GetByUsername(string username, params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<Merchant> source = ctx.Merchants;

                foreach (var include in includes)
                {
                    source = source.Include(include);
                }

                var merchant = await source.SingleOrDefaultAsync(c => c.Username == username);

                return merchant ??
                    throw new BusinessException(Constants.Messages.MerchantNotFound);
            }
        }

        public async Task<IEnumerable<Merchant>> GetByUsernames(IEnumerable<string> usernames, params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                var source = ctx.Merchants.Where(c => usernames.Contains(c.Username));

                foreach (var include in includes)
                {
                    source = source.Include(include);
                }

                return await source.ToListAsync();
            }
        }

        public async Task<string> GetMerchantKey(string username)
        {
            using (var ctx = new PCContext())
            {
                return await ctx.Merchants.Where(c => c.Username == username).Select(r => r.MerchantKey).SingleOrDefaultAsync();
            }
        }
    }
}