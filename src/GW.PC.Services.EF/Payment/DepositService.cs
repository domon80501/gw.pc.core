﻿using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.PC.Services.EF.Payment
{
    public abstract class DepositService<TDeposit> : CreatableEntityService<TDeposit>
        where TDeposit : Deposit
    {
        public override async Task<TDeposit> Add(TDeposit deposit)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new PCContext())
                {
                    // Validate
                    var channel = await ctx.PaymentChannels.FindAsync(deposit.PaymentChannelId);
                    if (deposit.RequestedAmount < channel.MinAmount ||
                        deposit.RequestedAmount > channel.MaxAmount)
                    {
                        throw new BusinessException("存款金额低于最低额度或高于最高额度");
                    }

                    deposit.CreatedAt = DateTime.Now.ToE8();
                    deposit.Status = DepositStatus.PendingPayment;
                    deposit.PaymentCenterOrderNumber = $"{Constants.MerchantOrderNumberPrefixes.Deposit}{RandomGenerator.NewMerchantOrderNumber()}";

                    ctx.Entry(deposit).State = EntityState.Added;

                    await ctx.SaveChangesAsync();

                    EntityLogService.Add(
                        ctx,
                        EntityLogContent.Deposit_Created,
                        deposit.EntityLogTargetType,
                        deposit.Id,
                        deposit.PaymentMethod.GetDisplayName());

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return deposit;
                }
            }
        }

        public virtual async Task Callback(
            TDeposit deposit,
            bool succeeded,
            decimal actualAmount)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new PCContext())
                {
                    await FinalizeDeposit(ctx, deposit, succeeded ? DepositStatus.AutoSuccess : DepositStatus.PaymentFailed, actualAmount, null);
                }

                scope.Complete();
            }
        }

        public virtual async Task Audit(int id, bool result, int operatorId, decimal? gameBalance)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new PCContext())
                {
                    var deposit = await EnsureEntity<TDeposit>(id, ctx, "PaymentChannel.Provider");

                    if (deposit.Status != DepositStatus.PendingPayment)
                    {
                        throw new InvalidOperationException($"Invalid deposit status: {deposit.Status.GetDisplayName()}");
                    }

                    await FinalizeDeposit(ctx, deposit, result ? DepositStatus.ManualSuccess : DepositStatus.PaymentFailed, deposit.RequestedAmount, operatorId);
                }

                scope.Complete();
            }
        }

        public async Task<TDeposit> GetByMerchantOrderNumber(int merchantId, string merchantOrderNumber, params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<TDeposit> query = ctx.Set<TDeposit>();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }

                return await query.SingleOrDefaultAsync(d => d.MerchantId == merchantId && d.MerchantOrderNumber == merchantOrderNumber);
            }
        }

        public async Task<TDeposit> GetByPaymentCenterOrderNumber(string paymentCenterOrderNumber, params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<TDeposit> query = ctx.Set<TDeposit>();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }

                return await query.SingleOrDefaultAsync(d => d.PaymentCenterOrderNumber == paymentCenterOrderNumber);
            }
        }

        public async Task Fail(TDeposit deposit)
        {
            using (var ctx = new PCContext())
            {
                deposit.Status = DepositStatus.PaymentFailed;
                deposit.CompletedAt = DateTime.Now.ToE8();

                ctx.Entry(deposit).State = EntityState.Modified;

                await ctx.SaveChangesAsync();
            }
        }

        public override async Task<decimal> SearchAmount(Expression<Func<TDeposit, decimal?>> amountSelector, IEnumerable<Expression<Func<TDeposit, bool>>> query = null)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<TDeposit> result = ctx.Set<TDeposit>().Where(
                    d => d.Status == DepositStatus.AutoSuccess ||
                    d.Status == DepositStatus.ManualSuccess)
                .AsNoTracking();

                if (query != null)
                {
                    foreach (var predicate in query)
                    {
                        result = result.Where(predicate);
                    }
                }

                return (await result.SumAsync(amountSelector)).GetValueOrDefault();
            }
        }

        internal virtual async Task FinalizeDeposit(PCContext ctx, TDeposit deposit, DepositStatus status, decimal actualAmount, int? auditedById)
        {
            if (status == DepositStatus.ManualSuccess && !auditedById.HasValue)
            {
                throw new ArgumentException("No auditor");
            }

            var now = DateTime.Now.ToE8();
            var succeeded = status == DepositStatus.AutoSuccess || status == DepositStatus.ManualSuccess;

            if (succeeded)
            {
                if (status == DepositStatus.AutoSuccess)
                {
                    AutoSucceed(deposit, ctx, now);
                }
                else
                {
                    ManualSucceed(deposit, ctx, now, auditedById.Value);
                }

                // Update related payment channel
                UpdatePaymentChannel(ctx, deposit, actualAmount, now);
            }
            else
            {
                deposit.CompletedAt = now;
                deposit.Status = DepositStatus.PaymentFailed;
                ctx.Entry(deposit).State = EntityState.Modified;
            }

            // Add user log
            if (auditedById.HasValue)
            {
                var user = await EnsureEntity<User>(auditedById.Value, ctx);
                AddUserLog(ctx, user.Username, $"账户存款{deposit.Id}审核完成：{deposit.Status.GetDisplayName()}");
            }

            if (status == DepositStatus.AutoSuccess || status == DepositStatus.ManualSuccess)
            {
                deposit.ActualAmount = actualAmount;
            }

            await ctx.SaveChangesAsync();
        }

        protected virtual async Task OnFinalizeDeposit(PCContext ctx, TDeposit deposit, DepositStatus status, Merchant customer, decimal actualAmount, DateTime now, int? auditedById)
        {
            var reminder = await ctx.DepositReminders.SingleOrDefaultAsync(
                r => r.PaymentMethod == deposit.PaymentMethod &&
                r.DepositId == deposit.Id &&
                r.Status == DepositReminderStatus.AwaitApproval);
            if (reminder != null)
            {
                reminder.AuditedAt = now;
                reminder.AuditedById = auditedById;

                if (deposit.Status == DepositStatus.AutoSuccess)
                {
                    reminder.Status = DepositReminderStatus.DepositAutoSucceeded;
                }
                else if (deposit.Status == DepositStatus.ManualSuccess)
                {
                    reminder.Status = DepositReminderStatus.Approved;
                }
                else if (deposit.Status == DepositStatus.PaymentFailed)
                {
                    reminder.Status = DepositReminderStatus.DepositFailed;
                }
            }
        }

        protected void AutoSucceed(TDeposit deposit, PCContext ctx, DateTime now)
        {
            deposit.Status = DepositStatus.AutoSuccess;
            deposit.CompletedAt = now;
            ctx.Entry(deposit).State = EntityState.Modified;
        }

        protected void ManualSucceed(TDeposit deposit, PCContext ctx, DateTime now, int auditedById)
        {
            deposit.Status = DepositStatus.ManualSuccess;
            deposit.AuditedAt = now;
            deposit.CompletedAt = now;
            deposit.AuditedById = auditedById;
            ctx.Entry(deposit).State = EntityState.Modified;
        }

        protected virtual void UpdatePaymentChannel(PCContext ctx, TDeposit deposit, decimal actualAmount, DateTime now)
        {
            if (deposit.PaymentChannel != null)
            {
                if (deposit.PaymentChannel.LastUsedAt?.Date != now.Date)
                {
                    deposit.PaymentChannel.CurrentPeriodDepositAmount = actualAmount;
                }
                else
                {
                    deposit.PaymentChannel.CurrentPeriodDepositAmount += actualAmount;
                }

                deposit.PaymentChannel.LastUsedAt = now;

                deposit.Commission = new PaymentChannelService().CalculateCommission(deposit.PaymentChannel, actualAmount);
                var transaction = new PaymentChannelTransaction
                {
                    Amount = actualAmount,
                    BeforeBalance = deposit.PaymentChannel.Provider.Balance,
                    CreatedAt = now,
                    PaymentChannelId = deposit.PaymentChannelId.Value,
                    Type = deposit.GetType().Name == "InternalDeposit" ?
                        PaymentChannelTransactionType.InternalDeposit :
                        PaymentChannelTransactionType.MerchantDeposit,
                    Commission = deposit.Commission,
                    AfterBalance = deposit.PaymentChannel.Provider.Balance + actualAmount - deposit.Commission
                };
                deposit.PaymentChannel.Provider.Balance = transaction.AfterBalance;

                ctx.PaymentChannelTransactions.Add(transaction);
            }
        }

        internal async Task UpdateCallbackStatus(string paymentCenterOrderNumber)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new PCContext())
                {
                    IQueryable<TDeposit> query = ctx.Set<TDeposit>();

                    var deposit = await query.SingleOrDefaultAsync(d => d.PaymentCenterOrderNumber == paymentCenterOrderNumber);

                    if (deposit != null && deposit.MerchantCallbackStatus == MerchantCallbackStatus.Await)
                    {
                        deposit.MerchantCallbackStatus = MerchantCallbackStatus.Successed;

                        ctx.Entry(deposit).State = EntityState.Modified;

                        await ctx.SaveChangesAsync();
                    }
                }

                scope.Complete();
            }
        }

        public async Task<string> CalculateSuccessRate(DateTime PeriodStartsAt, DateTime PeriodEndsAt)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<TDeposit> query = ctx.Set<TDeposit>().Where(o => o.CreatedAt >= PeriodStartsAt && o.CreatedAt <= PeriodEndsAt);

                var totalCount = query.Count();
                if (totalCount == 0)
                    return string.Empty;

                var successCount = query.Where(o => o.Status == DepositStatus.AutoSuccess || o.Status == DepositStatus.ManualSuccess || o.Status == DepositStatus.Success).Count();

                return (successCount / totalCount).ToString("F2");
            }
        }
    }
}
