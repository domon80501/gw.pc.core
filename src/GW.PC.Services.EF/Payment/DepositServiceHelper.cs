﻿using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Models.Infrastructure;
using System;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Payment
{
    public static class DepositServiceHelper
    {
        public static async Task<Deposit> GetDeposit(PaymentMethod paymentMethod, int depositId, params string[] includes)
        {
            Deposit deposit = null;

            switch (paymentMethod)
            {
                case PaymentMethod.WeChat:
                    deposit = await new WeChatDepositService().Find(depositId, includes);

                    break;
                case PaymentMethod.Alipay:
                    deposit = await new AlipayDepositService().Find(depositId, includes);

                    break;
                case PaymentMethod.OnlinePayment:
                    deposit = await new OnlinePaymentDepositService().Find(depositId, includes);

                    break;
                case PaymentMethod.OnlineBanking:
                    deposit = await new OnlineBankingDepositService().Find(depositId, includes);

                    break;
                case PaymentMethod.PrepaidCard:
                    deposit = await new PrepaidCardDepositService().Find(depositId, includes);

                    break;
                case PaymentMethod.QQWallet:
                    deposit = await new QQWalletDepositService().Find(depositId, includes);

                    break;
                case PaymentMethod.QuickPay:
                    deposit = await new QuickPayDepositService().Find(depositId, includes);

                    break;
                case PaymentMethod.OnlineToBankCard:
                    deposit = await new OnlineToBankCardDepositService().Find(depositId, includes);

                    break;
                //case PaymentMethod.Internal:
                //    deposit = await new InternalDepositService().Find(depositId, includes);

                //    break;
                case PaymentMethod.JDWallet:
                    deposit = await new JDWalletDepositService().Find(depositId, includes);

                    break;
            }

            if (deposit == null)
            {
                throw new ArgumentException(string.Format(Constants.MessageTemplates.InvalidObject, "存款"));
            }

            return deposit;
        }

        public static async Task<Deposit> GetDeposit(int merchantId, PaymentMethod paymentMethod, string merchantOrderNumber, params string[] includes)
        {
            Deposit deposit = null;

            switch (paymentMethod)
            {
                case PaymentMethod.WeChat:
                    deposit = await new WeChatDepositService().GetByMerchantOrderNumber(merchantId, merchantOrderNumber, includes);

                    break;
                case PaymentMethod.Alipay:
                    deposit = await new AlipayDepositService().GetByMerchantOrderNumber(merchantId, merchantOrderNumber, includes);

                    break;
                case PaymentMethod.OnlinePayment:
                    deposit = await new OnlinePaymentDepositService().GetByMerchantOrderNumber(merchantId, merchantOrderNumber, includes);

                    break;
                case PaymentMethod.OnlineBanking:
                    deposit = await new OnlineBankingDepositService().GetByMerchantOrderNumber(merchantId, merchantOrderNumber, includes);

                    break;
                case PaymentMethod.PrepaidCard:
                    deposit = await new PrepaidCardDepositService().GetByMerchantOrderNumber(merchantId, merchantOrderNumber, includes);

                    break;
                case PaymentMethod.QQWallet:
                    deposit = await new QQWalletDepositService().GetByMerchantOrderNumber(merchantId, merchantOrderNumber, includes);

                    break;
                case PaymentMethod.QuickPay:
                    deposit = await new QuickPayDepositService().GetByMerchantOrderNumber(merchantId, merchantOrderNumber, includes);

                    break;
                case PaymentMethod.OnlineToBankCard:
                    deposit = await new OnlineToBankCardDepositService().GetByMerchantOrderNumber(merchantId, merchantOrderNumber, includes);

                    break;
                //case PaymentMethod.Internal:
                //    deposit = await new InternalDepositService().GetByMerchantOrderNumber(merchantOrderNumber, includes);

                //    break;
                case PaymentMethod.JDWallet:
                    deposit = await new JDWalletDepositService().GetByMerchantOrderNumber(merchantId, merchantOrderNumber, includes);

                    break;
            }

            return deposit;
        }

        public static async Task<Deposit> GetDeposit(PaymentMethod paymentMethod, string paymentCenterOrderNumber, params string[] includes)
        {
            Deposit deposit = null;

            switch (paymentMethod)
            {
                case PaymentMethod.WeChat:
                    deposit = await new WeChatDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case PaymentMethod.Alipay:
                    deposit = await new AlipayDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case PaymentMethod.OnlinePayment:
                    deposit = await new OnlinePaymentDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case PaymentMethod.OnlineBanking:
                    deposit = await new OnlineBankingDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case PaymentMethod.PrepaidCard:
                    deposit = await new PrepaidCardDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case PaymentMethod.QQWallet:
                    deposit = await new QQWalletDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case PaymentMethod.QuickPay:
                    deposit = await new QuickPayDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case PaymentMethod.OnlineToBankCard:
                    deposit = await new OnlineToBankCardDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                //case PaymentMethod.Internal:
                //    deposit = await new InternalDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                //    break;
                case PaymentMethod.JDWallet:
                    deposit = await new JDWalletDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
            }

            return deposit;
        }

        public static async Task<Deposit> GetDeposit(string type, string paymentCenterOrderNumber, params string[] includes)
        {
            Deposit deposit = null;

            switch (type.ToLower())
            {
                case "wechatdeposit":
                    deposit = await new WeChatDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case "alipaydeposit":
                    deposit = await new AlipayDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case "onlinepaymentdeposit":
                    deposit = await new OnlinePaymentDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case "onlinebankingdeposit":
                    deposit = await new OnlineBankingDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case "prepaidcarddeposit":
                    deposit = await new PrepaidCardDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case "qqwalletdeposit":
                    deposit = await new QQWalletDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case "quickpaydeposit":
                    deposit = await new QuickPayDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                case "onlinetobankcarddeposit":
                    deposit = await new OnlineToBankCardDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
                //case "internaldeposit":
                //    deposit = await new InternalDepositService().GetByPaymentCenterOrderNumber(merchantOrderNumber, includes);

                //    break;
                case "jdwalletdeposit":
                    deposit = await new JDWalletDepositService().GetByPaymentCenterOrderNumber(paymentCenterOrderNumber, includes);

                    break;
            }

            return deposit;
        }

        public static async Task FinalizeDeposit(PCContext ctx, Deposit deposit, DepositStatus status, decimal actualAmount, int? auditedById)
        {
            switch (deposit.GetType().Name.ToLower())
            {
                case "wechatdeposit":
                    await new WeChatDepositService().FinalizeDeposit(ctx, (WeChatDeposit)deposit, status, actualAmount, auditedById);

                    break;
                case "alipaydeposit":
                    await new AlipayDepositService().FinalizeDeposit(ctx, (AlipayDeposit)deposit, status, actualAmount, auditedById);

                    break;
                case "onlinebankingdeposit":
                    await new OnlineBankingDepositService().FinalizeDeposit(ctx, (OnlineBankingDeposit)deposit, status, actualAmount, auditedById);

                    break;
                case "onlinetobankcarddeposit":
                    await new OnlineToBankCardDepositService().FinalizeDeposit(ctx, (OnlineToBankCardDeposit)deposit, status, actualAmount, auditedById);

                    break;
                case "jdwalletdeposit":
                    await new JDWalletDepositService().FinalizeDeposit(ctx, (JDWalletDeposit)deposit, status, actualAmount, auditedById);

                    break;
                    //case "internaldeposit":
                    //    await new InternalDepositService().FinalizeDeposit(ctx, (InternalDeposit)deposit, status, actualAmount, auditedById);

                    //    break;
            }
        }

        public static async Task UpdateCallbackStatus(PaymentMethod paymentMethod, string paymentCenterOrderNumber)
        {
            using (var ctx = new PCContext())
            {
                Deposit deposit = null;

                switch (paymentMethod)
                {
                    case PaymentMethod.WeChat:
                        await new WeChatDepositService().UpdateCallbackStatus(paymentCenterOrderNumber);

                        break;
                    case PaymentMethod.Alipay:
                        await new AlipayDepositService().UpdateCallbackStatus(paymentCenterOrderNumber);

                        break;
                    case PaymentMethod.OnlinePayment:
                        await new OnlinePaymentDepositService().UpdateCallbackStatus(paymentCenterOrderNumber);

                        break;
                    case PaymentMethod.OnlineBanking:
                        await new OnlineBankingDepositService().UpdateCallbackStatus(paymentCenterOrderNumber);

                        break;
                    case PaymentMethod.PrepaidCard:
                        await new PrepaidCardDepositService().UpdateCallbackStatus(paymentCenterOrderNumber);

                        break;
                    case PaymentMethod.QQWallet:
                        await new QQWalletDepositService().UpdateCallbackStatus(paymentCenterOrderNumber);

                        break;
                    case PaymentMethod.QuickPay:
                        await new QuickPayDepositService().UpdateCallbackStatus(paymentCenterOrderNumber);

                        break;
                    case PaymentMethod.OnlineToBankCard:
                        await new OnlineToBankCardDepositService().UpdateCallbackStatus(paymentCenterOrderNumber);

                        break;
                    //case PaymentMethod.Internal:
                    //    await new InternalDepositService().UpdateCallbackStatus(paymentCenterOrderNumber);

                    //    break;
                    case PaymentMethod.JDWallet:
                        await new JDWalletDepositService().UpdateCallbackStatus(paymentCenterOrderNumber);

                        break;
                }
            }
        }

        public static async Task<Deposit> Update(Deposit deposit)
        {
            switch (deposit.PaymentMethod)
            {
                case PaymentMethod.WeChat:
                    return await new WeChatDepositService().Update((WeChatDeposit)deposit);
                case PaymentMethod.Alipay:
                    return await new AlipayDepositService().Update((AlipayDeposit)deposit);
                case PaymentMethod.OnlinePayment:
                    return await new OnlinePaymentDepositService().Update((OnlinePaymentDeposit)deposit);
                case PaymentMethod.OnlineBanking:
                    return await new OnlineBankingDepositService().Update((OnlineBankingDeposit)deposit);
                case PaymentMethod.PrepaidCard:
                    return await new PrepaidCardDepositService().Update((PrepaidCardDeposit)deposit);
                case PaymentMethod.QQWallet:
                    return await new QQWalletDepositService().Update((QQWalletDeposit)deposit);
                case PaymentMethod.QuickPay:
                    return await new QuickPayDepositService().Update((QuickPayDeposit)deposit);
                case PaymentMethod.OnlineToBankCard:
                    return await new OnlineToBankCardDepositService().Update((OnlineToBankCardDeposit)deposit);
                case PaymentMethod.JDWallet:
                    return await new JDWalletDepositService().Update((JDWalletDeposit)deposit);
                case PaymentMethod.Internal:
                    return await new InternalDepositService().Update((InternalDeposit)deposit);
                default:
                    return null;
            }
        }
    }
}
