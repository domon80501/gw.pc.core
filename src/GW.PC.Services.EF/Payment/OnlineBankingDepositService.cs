﻿using GW.PC.Models;
using System;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Payment
{
    public class OnlineBankingDepositService : DepositService<OnlineBankingDeposit>
    {
        public override Task Audit(int id, bool result, int operatorId, decimal? gameBalance)
        {
            if (result)
            {
                throw new NotSupportedException("Online banking deposits can only be made manual success from deposit reminder");
            }

            return base.Audit(id, false, operatorId, gameBalance);
        }

        protected override async Task OnFinalizeDeposit(PCContext ctx, OnlineBankingDeposit deposit, DepositStatus result, Merchant customer, decimal actualAmount, DateTime now, int? auditedById)
        {
            if (result == DepositStatus.AutoSuccess || result == DepositStatus.ManualSuccess)
            {
                deposit.ActualAmount = actualAmount;
            }

            await base.OnFinalizeDeposit(ctx, deposit, result, customer, actualAmount, now, auditedById);
        }
    }
}
