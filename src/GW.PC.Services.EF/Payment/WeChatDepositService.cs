﻿using GW.PC.Models;
using System;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Payment
{
    public class WeChatDepositService : DepositService<WeChatDeposit>
    {
        protected override Task OnFinalizeDeposit(PCContext ctx, WeChatDeposit deposit, DepositStatus result, Merchant customer, decimal actualAmount, DateTime now, int? auditedById)
        {
            if (result == DepositStatus.AutoSuccess || result == DepositStatus.ManualSuccess)
            {
                deposit.ActualAmount = actualAmount;
            }

            return base.OnFinalizeDeposit(ctx, deposit, result, customer, actualAmount, now, auditedById);
        }
    }
}
