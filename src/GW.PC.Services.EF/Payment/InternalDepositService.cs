﻿using System;
using System.Threading.Tasks;
using GW.PC.Core;
using GW.PC.Models;
using System.Data.Entity;

namespace GW.PC.Services.EF.Payment
{
    public class InternalDepositService : DepositService<InternalDeposit>
    {
        internal override async Task FinalizeDeposit(PCContext ctx, InternalDeposit deposit, DepositStatus status, decimal actualAmount, int? auditedById)
        {
            if (status == DepositStatus.ManualSuccess && !auditedById.HasValue)
            {
                throw new ArgumentException("No auditor");
            }

            var now = DateTime.Now.ToE8();
            var succeeded = status == DepositStatus.AutoSuccess || status == DepositStatus.ManualSuccess;

            if (succeeded)
            {
                if (status == DepositStatus.AutoSuccess)
                {
                    AutoSucceed(deposit, ctx, now);
                }
                else
                {
                    ManualSucceed(deposit, ctx, now, auditedById.Value);
                }

                // Update related payment channel
                deposit.DepositPaymentChannel = await ctx.PaymentChannels
                    .Include(c => c.Provider)
                    .SingleOrDefaultAsync(c => c.Id == deposit.DepositPaymentChannelId);
                UpdatePaymentChannel(ctx, deposit, actualAmount, now);
            }
            else
            {
                deposit.CompletedAt = now;
                deposit.Status = DepositStatus.PaymentFailed;
            }

            // Add user log
            if (auditedById.HasValue)
            {
                var user = await EnsureEntity<User>(auditedById.Value, ctx);
                AddUserLog(ctx, user.Username, $"账户存款{deposit.Id}审核完成：{deposit.Status.GetDisplayName()}");
            }

            await base.OnFinalizeDeposit(ctx, deposit, deposit.Status, null, actualAmount, now, auditedById);

            await ctx.SaveChangesAsync();
        }

        protected override void UpdatePaymentChannel(PCContext ctx, InternalDeposit deposit, decimal actualAmount, DateTime now)
        {
            if (deposit.DepositPaymentChannel != null)
            {
                deposit.DepositPaymentChannel.LastUsedAt = now;

                deposit.DepositCommission = new PaymentChannelService().CalculateCommission(deposit.DepositPaymentChannel, actualAmount);
                var transaction = new PaymentChannelTransaction
                {
                    Amount = 0 - actualAmount,
                    BeforeBalance = deposit.DepositPaymentChannel.Provider.Balance,
                    CreatedAt = now,
                    PaymentChannelId = deposit.DepositPaymentChannelId,
                    Type = PaymentChannelTransactionType.InternalDeposit,
                    Commission = deposit.DepositCommission,
                    AfterBalance = deposit.DepositPaymentChannel.Provider.Balance - actualAmount - deposit.DepositCommission
                };
                deposit.DepositPaymentChannel.Provider.Balance = transaction.AfterBalance;

                ctx.PaymentChannelTransactions.Add(transaction);
            }

            base.UpdatePaymentChannel(ctx, deposit, actualAmount, now);
        }
    }
}
