﻿using System.Threading.Tasks;
using GW.PC.Models;
using System.Data.Entity;
using System.Linq;
using System;
using GW.PC.Core;
using System.Collections;
using System.Collections.Generic;

namespace GW.PC.Services.EF
{
    public class MerchantMessageService : CreatableEntityService<MerchantMessage>
    {
        public async Task<MerchantMessage> FindReply(int id)
        {
            using (var ctx = new APContext())
            {
                return await ctx.MerchantMessages.SingleOrDefaultAsync(
                    m => m.ParentId == id);
            }
        }

        public async Task Reply(MerchantMessage reply)
        {
            using (var ctx = new APContext())
            {
                var message = await ctx.MerchantMessages.FindAsync(reply.ParentId);

                message.Status = MerchantMessageStatus.Replied;

                ctx.Entry(reply).State = reply.Id == 0 ? EntityState.Added : EntityState.Modified;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<MerchantMessage>> GetForMerchant(string username, int lastMessageId)
        {
            using (var ctx = new APContext())
            {
                return await ctx.MerchantMessages.Where(
                    m => m.ToUsername == username &&
                    m.Id > lastMessageId)
                    .ToListAsync();
            }
        }

        public override async Task<MerchantMessage> Add(MerchantMessage item)
        {
            if (item.Type == MerchantMessageType.MerchantMessage)
            {
                var todayStart = DateTime.Today;
                var todayEnd = todayStart.AddDays(1).AddSeconds(-1);
                using (var ctx = new APContext())
                {
                    if (await ctx.MerchantMessages.CountAsync(
                        m => m.Type == MerchantMessageType.MerchantMessage &&
                        m.FromUsername == item.FromUsername &&
                        m.CreatedAt >= todayStart &&
                        m.CreatedAt <= todayEnd) >= 3)
                    {
                        throw new BusinessException("今天发送的站内信个数不可以超过3个");
                    }
                }
            }

            return await base.Add(item);
        }

        protected override IQueryable<MerchantMessage> OrderBy(IQueryable<MerchantMessage> source, string orderBy = null, bool isAscending = false)
        {
            switch (orderBy?.ToLower())
            {
                case "createdat":
                    return isAscending ? source.OrderBy(c => c.CreatedAt) :
                        source.OrderByDescending(c => c.CreatedAt);
                default:
                    return base.OrderBy(source, orderBy, isAscending);
            }
        }
    }
}
