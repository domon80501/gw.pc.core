﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class PaymentProviderBankService : StatusfulEntityService<PaymentProviderBank>
    {
        public async Task<PaymentProviderBank> Get(string paymentProviderBankCode, int paymentProviderId, PaymentType paymentType, PaymentMethod paymentMethod)
        {
            using (var ctx = new PCContext())
            {
                return await ctx.PaymentProviderBanks.Include(ppb => ppb.Bank)
                    .SingleOrDefaultAsync(
                    ppb => ppb.Code == paymentProviderBankCode &&
                    ppb.PaymentProviderId == paymentProviderId &&
                    ppb.PaymentType == paymentType &&
                    ppb.PaymentMethod == paymentMethod);
            }
        }

        public async Task<PaymentProviderBank> GetByBankCode(string bankCode, int paymentProviderId, PaymentType paymentType, PaymentMethod paymentMethod)
        {
            using (var ctx = new PCContext())
            {
                return await ctx.PaymentProviderBanks.Include(ppb => ppb.Bank)
                    .SingleOrDefaultAsync(
                    ppb => ppb.Bank.Code == bankCode &&
                    ppb.PaymentProviderId == paymentProviderId &&
                    ppb.PaymentType == paymentType &&
                    ppb.PaymentMethod == paymentMethod);
            }
        }

        public async Task Save(PaymentProviderBank ppb)
        {
            using (var ctx = new PCContext())
            {
                if (ppb.Id == 0)
                {
                    if (await ctx.PaymentProviderBanks.AnyAsync(
                        b => b.PaymentProviderId == ppb.PaymentProviderId &&
                        b.PaymentType == ppb.PaymentType &&
                        b.PaymentMethod == ppb.PaymentMethod &&
                        b.BankId == ppb.BankId))
                    {
                        throw new BusinessException("该支付银行已存在");
                    }

                    ppb.Status = EntityStatus.Enabled;

                    ctx.PaymentProviderBanks.Add(ppb);
                }
                else
                {
                    var dbPpb = await EnsureEntity<PaymentProviderBank>(ppb.Id, ctx);

                    dbPpb.BankId = ppb.BankId;
                    dbPpb.Order = ppb.Order;
                    dbPpb.PaymentType = ppb.PaymentType;
                    dbPpb.PaymentMethod = ppb.PaymentMethod;
                    dbPpb.Code = ppb.Code;
                    dbPpb.PaymentProviderId = ppb.PaymentProviderId;
                    dbPpb.Status = ppb.Status;
                }

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<PaymentProviderBank> GetByBankId(int bankId, int paymentProviderId, PaymentType paymentType, PaymentMethod paymentMethod)
        {
            using (var ctx = new PCContext())
            {
                return await ctx.PaymentProviderBanks.Include(ppb => ppb.Bank)
                    .SingleOrDefaultAsync(
                    ppb => ppb.BankId == bankId &&
                    ppb.PaymentProviderId == paymentProviderId &&
                    ppb.PaymentType == paymentType &&
                    ppb.PaymentMethod == paymentMethod);
            }
        }

        public async Task<IEnumerable<Bank>> GetBanks(int id)
        {
            using (var ctx = new PCContext())
            {
                var provider = await EnsureEntity<PaymentProvider>(id, ctx, "PaymentProviderBanks.Bank");
                if (provider.Status != EntityStatus.Enabled)
                {
                    throw new BusinessException("该通道未启用");
                }

                return provider.PaymentProviderBanks.Where(
                    ppb => ppb.Status == EntityStatus.Enabled &&
                    ppb.PaymentType == PaymentType.Withdrawal)
                    .Select(ppb => new Bank
                    {
                        Id = ppb.BankId,
                        Name = ppb.Bank.Name
                    });
            }
        }

        public async Task<IEnumerable<PaymentProviderBank>> Get(int paymentProviderId, PaymentType paymentType, PaymentMethod paymentMethod)
        {
            using (var ctx = new PCContext())
            {
                return await ctx.PaymentProviderBanks.Include(ppb => ppb.Bank)
                    .Where(
                    ppb =>
                    ppb.PaymentProviderId == paymentProviderId &&
                    ppb.PaymentType == paymentType &&
                    ppb.PaymentMethod == paymentMethod).ToListAsync();
            }
        }

        public async Task<IEnumerable<PaymentProviderBank>> Get(string bankCode, IEnumerable<int> paymentProviderIds, PaymentType paymentType, PaymentMethod paymentMethod)
        {
            using (var ctx = new PCContext())
            {
                return await ctx.PaymentProviderBanks.Include(ppb => ppb.Bank)
                    .Where(
                    ppb => ppb.Bank.Code == bankCode &&
                    paymentProviderIds.Contains(ppb.PaymentProviderId) &&
                    ppb.PaymentType == paymentType &&
                    ppb.PaymentMethod == paymentMethod).ToListAsync();
            }
        }
    }
}
