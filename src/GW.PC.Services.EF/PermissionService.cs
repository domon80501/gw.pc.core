﻿using GW.PC.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Admin
{
    public class PermissionService : EntityService<Permission>
    {
        public async Task<IEnumerable<PermissionGroup>> GetGroups()
        {
            using (var ctx = new PCContext())
            {
                return await ctx.PermissionGroups.Include(p => p.Permissions)
                    .ToListAsync();
            }
        }
    }
}
