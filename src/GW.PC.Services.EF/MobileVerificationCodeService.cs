﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class MobileVerificationCodeService : EntityService<MobileVerificationCode>
    {
        public async Task<MobileVerificationCode> New(string mobile)
        {
            using (var ctx = new GWContext())
            {
                var item = new MobileVerificationCode
                {
                    Code = RandomGenerator.RandomNumber(6),
                    CreatedAt = DateTime.Now.ToE8(),
                    Mobile = mobile
                };

                ctx.MobileVerificationCodes.Add(item);

                await ctx.SaveChangesAsync();

                return item;
            }
        }

        public async Task Verify(string mobile, string verificationCode)
        {
            CheckNull(mobile, nameof(mobile));
            CheckNull(verificationCode, nameof(verificationCode));

            using (var ctx = new GWContext())
            {
                var item = await ctx.MobileVerificationCodes
                    .Where(c => c.Mobile == mobile)
                    .OrderByDescending(c => c.Id)
                    .FirstOrDefaultAsync();
                var now = DateTime.Now.ToE8();

                if (item == null ||
                    item.VerifiedAt.HasValue ||
                    now > item.CreatedAt.AddMinutes(10) ||
                    item.Code != verificationCode)
                {
                    throw new BusinessException(Constants.Messages.InvalidVerificationCode, "1");
                }

                item.VerifiedAt = now;

                await ctx.SaveChangesAsync();
            }
        }
    }
}
