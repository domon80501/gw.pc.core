﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.PC.Services.EF
{
    public class MerchantWithdrawalService : CreatableEntityService<MerchantWithdrawal>
    {
        private static readonly Random random = new Random();

        public async Task<MerchantWithdrawal> AddChildren(MerchantWithdrawal withdrawal, IEnumerable<MerchantWithdrawal> children)
        {
            using (var ctx = new PCContext())
            {
                withdrawal.HasChildren = true;
                withdrawal.PaymentChannelId = null;
                withdrawal.PaymentChannel = null;

                ctx.MerchantWithdrawals.AddRange(children);
                ctx.Entry(withdrawal).State = EntityState.Modified;

                await ctx.SaveChangesAsync();

                return withdrawal;
            }
        }

        public async Task<MerchantWithdrawal> GetByMerchantOrderNumber(int merchantId, string merchantOrderNumber, params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<MerchantWithdrawal> query = ctx.Set<MerchantWithdrawal>();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }

                return await query.SingleOrDefaultAsync(d => d.MerchantId == merchantId && d.MerchantOrderNumber == merchantOrderNumber);
            }
        }

        public async Task<MerchantWithdrawal> GetByPaymentCenterOrderNumber(string paymentCenterOrderNumber, params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<MerchantWithdrawal> query = ctx.Set<MerchantWithdrawal>();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }

                return await query.SingleOrDefaultAsync(d => d.PaymentCenterOrderNumber == paymentCenterOrderNumber);
            }
        }

        public async Task<MerchantWithdrawal> AuditClose(int id, string notes, int? operatorId = null, string customerMessage = null)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new PCContext())
                {
                    var withdrawal = await EnsureEntity(id, ctx);
                    if (withdrawal.Status != WithdrawalStatus.AwaitingApproval)
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                    }

                    withdrawal.AuditedById = operatorId;
                    withdrawal.AuditedAt = DateTime.Now.ToE8();
                    withdrawal.DeclineNotes = notes;

                    await Fail(ctx, withdrawal, operatorId, customerMessage);

                    await Close(ctx, withdrawal, operatorId);

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return withdrawal;
                }
            }
        }

        public async Task<MerchantWithdrawal> AutoFail(int id, string declineNotes, string customerMessage = null, string logDetails = null)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new PCContext())
                {                    
                    var withdrawal = await EnsureEntity<MerchantWithdrawal>(id, ctx, "PaymentChannel.Provider", "Merchant");
                    if (withdrawal.Status != WithdrawalStatus.AutoPaymentInProgress)
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                    }

                    withdrawal.Status = WithdrawalStatus.AutoPaymentFailed;
                    withdrawal.PaidAt = DateTime.Now.ToE8();
                    withdrawal.DeclineNotes = declineNotes;

                    await Fail(ctx, withdrawal, null, customerMessage, logDetails);

                    await Close(ctx, withdrawal, null);

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return withdrawal;
                }
            }
        }

        public async Task<MerchantWithdrawal> AutoSucceed(int id)
        {
            using (var ctx = new PCContext())
            {
                var withdrawal = await EnsureEntity<MerchantWithdrawal>(id, ctx, "PaymentChannel.Provider", "Merchant");
                if (withdrawal.Status != WithdrawalStatus.AutoPaymentInProgress)
                {
                    throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                }

                withdrawal.Status = WithdrawalStatus.AutoPaymentSuccessful;

                await Succeed(ctx, withdrawal, DateTime.Now.ToE8(), null);

                await ctx.SaveChangesAsync();

                return withdrawal;
            }
        }

        public async Task<MerchantWithdrawal> AuditApprove(int id, int? operatorId, int? lastDepositPeriodId = null)
        {
            using (var ctx = new PCContext())
            {
                var withdrawal = await EnsureEntity(id, ctx);
                if (withdrawal.Status != WithdrawalStatus.AwaitingApproval)
                {
                    throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                }

                withdrawal.Status = WithdrawalStatus.AwaitingAutoPayment;

                var now = DateTime.Now.ToE8();

                withdrawal.AuditedAt = now;
                withdrawal.AuditedById = operatorId;

                if (operatorId.HasValue)
                {
                    // Add user log
                    var user = await EnsureEntity<User>(operatorId.Value, ctx);
                    AddUserLog(ctx, user.Username, BuildUserLogContent(withdrawal, user.Username));
                }

                EntityLogService.Add(
                    ctx,
                    EntityLogContent.MerchantWithdrawal_AmountFrozen,
                    EntityLogTargetType.MerchantWithdrawal,
                    withdrawal.Id,
                    null,
                    withdrawal.Status.GetDisplayName(), withdrawal.Amount);

                await ctx.SaveChangesAsync();

                return withdrawal;
            }
        }

        public async Task<MerchantWithdrawal> ManualConfirm(int id, bool result, string notes, int operatorId)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new PCContext())
                {
                    var withdrawal = await EnsureEntity<MerchantWithdrawal>(id, ctx, "PaymentChannel.Provider", "Reference");
                    if (withdrawal.Status != WithdrawalStatus.AutoPaymentInProgress)
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                    }

                    if (result)
                    {
                        withdrawal.Status = WithdrawalStatus.ManualConfirmed;

                        await Succeed(ctx, withdrawal, DateTime.Now.ToE8(), operatorId, notes);
                    }
                    else
                    {
                        withdrawal.Status = WithdrawalStatus.AutoPaymentManualConfirmedFailed;
                        withdrawal.ManualConfirmedAt = DateTime.Now.ToE8();
                        withdrawal.ManualConfirmedById = operatorId;
                        withdrawal.ManualConfirmNotes = notes;

                        await Fail(ctx, withdrawal, operatorId);

                        if (!withdrawal.HasChildren &&
                            !await ctx.MerchantWithdrawals.AnyAsync(
                            w => w.Username == withdrawal.Username &&
                            (w.Status == WithdrawalStatus.AutoPaymentSuccessful ||
                            w.Status == WithdrawalStatus.ManualConfirmed ||
                            w.Status == WithdrawalStatus.RetryAutoPaymentSuccessful)))
                        {
                            await Close(ctx, withdrawal, null);
                        }
                    }

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return withdrawal;
                }
            }
        }

        public async Task<IEnumerable<MerchantWithdrawal>> GetChildren(int id)
        {
            using (var ctx = new PCContext())
            {
                return await ctx.MerchantWithdrawals.Include(w => w.PaymentChannel).Where(w => w.ParentId == id).ToListAsync();
            }
        }

        public async Task<PaymentChannel> SelectWithdrawChannel(int merchantId, decimal amount, int payeeBankId, PaymentPlatform platform)
        {
            using (var ctx = new PCContext())
            {
                PaymentChannel channel = null;

                var channels = await ctx.PaymentChannels
                    .Include(c => c.Provider.PaymentProviderBanks)
                    .Where(
                    c => c.MerchantId == merchantId &&
                    c.PaymentType == PaymentType.Withdrawal &&
                    c.Status == EntityStatus.Enabled &&
                    amount >= c.MinAmount && amount <= c.MaxAmount)
                    .ToListAsync();

                channels = channels.Where(
                    c => c.Provider.PaymentProviderBanks
                    .Where(b => b.Status == EntityStatus.Enabled &&
                    b.PaymentMethod == c.PaymentMethod)
                    .Select(b => b.BankId)
                    .Contains(payeeBankId) &&
                    (!c.MinimumWithdrawInterval.HasValue ||
                        Convert.ToDecimal(DateTime.Now.ToE8().Subtract(c.LastUsedAt.GetValueOrDefault()).TotalSeconds) >= c.MinimumWithdrawInterval.Value))
                    .ToList();

                var commissions = channels.Select(c => new
                {
                    Channel = c,
                    Commission = new PaymentChannelService().CalculateCommission(c, amount)
                })
                .Where(c => c.Channel.Provider.Balance >= amount + c.Commission + 100)
                .ToList();

                if (commissions.Count() == 0)
                {
                    return null;
                }
                else
                {
                    if (commissions.Count == 1)
                    {
                        channel = commissions.Single().Channel;
                    }
                    else
                    {
                        var minCommissions = commissions.Where(
                            c => c.Commission == commissions.Min(cc => cc.Commission))
                            .ToList();

                        var randomDouble = random.NextDouble();

                        if (randomDouble < 0.5)
                        {
                            var index = random.Next(0, minCommissions.Count);

                            channel = minCommissions.ElementAt(index).Channel;
                        }
                        else
                        {
                            var index = random.Next(0, commissions.Count);

                            channel = commissions.ElementAt(index).Channel;
                        }
                    }
                }
                await ctx.SaveChangesAsync();

                return channel;
            }
        }

        public async Task<MerchantWithdrawal> Retry(int id, int operatorId)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new PCContext())
                {
                    var withdrawal = await EnsureEntity<MerchantWithdrawal>(id, ctx, "PaymentChannel");
                    if (withdrawal.HasChildren)
                    {
                        throw new BusinessException("总提款不能重试，请对各拆分提款分别重试");
                    }

                    if (withdrawal.Status != WithdrawalStatus.AutoPaymentFailed &&
                        withdrawal.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed)
                    {
                        throw new BusinessException(
                            string.Format(
                                Constants.MessageTemplates.InvalidCurrentStatus,
                                withdrawal.Status.GetDisplayName()));
                    }

                    if (await ctx.MerchantWithdrawals.AnyAsync(
                        w => w.ReferenceId == withdrawal.Id &&
                        w.Status != WithdrawalStatus.AutoPaymentFailed &&
                        w.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed &&
                        w.Status != WithdrawalStatus.AutoPaymentSuccessful &&
                        w.Status != WithdrawalStatus.ManualConfirmed))
                    {
                        throw new BusinessException("该提款已经在重试中，请刷新查看");
                    }

                    withdrawal.Status = WithdrawalStatus.FailedAndClosed;

                    var newWithdrawal = new MerchantWithdrawal
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        Amount = withdrawal.Amount,
                        Status = WithdrawalStatus.AwaitingAutoPayment,
                        CreatedAt = DateTime.Now.ToE8(),
                        ParentId = withdrawal.ParentId,
                        PayeeBankId = withdrawal.PayeeBankId,
                        PayeeCardAddress = withdrawal.PayeeCardAddress,
                        PayeeCardNumber = withdrawal.PayeeCardNumber,
                        PayeeName = withdrawal.PayeeName,
                        PaymentChannel = withdrawal.PaymentChannel,
                        PaymentChannelId = withdrawal.PaymentChannelId,
                        Username = withdrawal.Username,
                        WithdrawalIP = withdrawal.WithdrawalIP,
                        WithdrawalAddress = withdrawal.WithdrawalAddress,
                        ReferenceId = withdrawal.Id
                    };

                    ctx.MerchantWithdrawals.Add(newWithdrawal);

                    await ctx.SaveChangesAsync();

                    EntityLogService.Add(
                        ctx,
                        EntityLogContent.MerchantWithdrawal_RetryCreated,
                        EntityLogTargetType.MerchantWithdrawal,
                        newWithdrawal.Id,
                        null,
                        newWithdrawal.Status.GetDisplayName(), withdrawal.Amount);

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return newWithdrawal;
                }
            }
        }

        public async Task<MerchantWithdrawal> Close(int id, int? operatorId)
        {
            using (var ctx = new PCContext())
            {
                var withdrawal = await EnsureEntity<MerchantWithdrawal>(id, ctx);

                return await Close(ctx, withdrawal, operatorId);
            }
        }

        public async Task UpdateCallbackStatus(string paymentCenterOrderNumber)
        {
            using (var ctx = new PCContext())
            {
                var withdrawal = await ctx.MerchantWithdrawals.SingleOrDefaultAsync(r => r.PaymentCenterOrderNumber == paymentCenterOrderNumber);

                if (withdrawal.MerchantCallbackStatus == MerchantCallbackStatus.Await)
                {
                    withdrawal.MerchantCallbackStatus = MerchantCallbackStatus.Successed;

                    await ctx.SaveChangesAsync();
                }
            }
        }

        private async Task<MerchantWithdrawal> Close(PCContext ctx, MerchantWithdrawal withdrawal, int? operatorId)
        {
            if (withdrawal.Status != WithdrawalStatus.AwaitingApproval &&
                withdrawal.Status != WithdrawalStatus.AutoPaymentFailed &&
                withdrawal.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed)
            {
                throw new BusinessException(
                    string.Format(
                        Constants.MessageTemplates.InvalidCurrentStatus,
                        withdrawal.Status.GetDisplayName()));
            }

            withdrawal.Status = WithdrawalStatus.FailedAndClosed;

            EntityLogService.Add(
                ctx,
                EntityLogContent.MerchantWithdrawal_FailedAndClosed,
                EntityLogTargetType.MerchantWithdrawal,
                withdrawal.Id,
                null,
                withdrawal.Amount);

            await ctx.SaveChangesAsync();

            return withdrawal;
        }

        private async Task Succeed(PCContext ctx, MerchantWithdrawal withdrawal, DateTime now, int? operatorId, string notes = null)
        {
            if (withdrawal.Status == WithdrawalStatus.ManualConfirmed)
            {
                withdrawal.ManualConfirmedAt = now;
                withdrawal.ManualConfirmedById = operatorId;
                withdrawal.ManualConfirmNotes = notes;
                if (withdrawal.Reference != null)
                {
                    withdrawal.Reference.Status = WithdrawalStatus.RetryManualConfirmed;
                    withdrawal.Reference.ManualConfirmedById = operatorId;
                    withdrawal.Reference.ManualConfirmedAt = now;
                    withdrawal.Reference.ManualConfirmNotes = notes;
                }
            }
            else if (withdrawal.Status == WithdrawalStatus.AutoPaymentSuccessful)
            {
                withdrawal.PaidAt = now;
                if (withdrawal.Reference != null)
                {
                    withdrawal.Reference.DeclineNotes = null;
                    withdrawal.Reference.Status = WithdrawalStatus.RetryAutoPaymentSuccessful;
                    withdrawal.Reference.PaidAt = now;
                }
            }

            // Update payment channel
            if (withdrawal.PaymentChannel != null)
            {
                UpdatePaymentChannel(ctx, withdrawal, now);
            }

            if (operatorId.HasValue)
            {
                // Add user log
                var user = await EnsureEntity<User>(operatorId.Value, ctx);
                AddUserLog(ctx, user.Username, BuildUserLogContent(withdrawal, user.Username));
            }
        }

        private async Task Fail(PCContext ctx, MerchantWithdrawal withdrawal, int? operatorId, string customerMessage = null, string logDetails = null)
        {
            var now = DateTime.Now.ToE8();

            withdrawal.MerchantMessage = customerMessage;

            if (operatorId.HasValue)
            {
                // Add user log
                var user = await EnsureEntity<User>(operatorId.Value, ctx);
                AddUserLog(ctx, user.Username, BuildUserLogContent(withdrawal, user.Username));
            }

            // Update parent withdrawal, if needed
            if (withdrawal.ParentId > 0)
            {
                var parent = await new MerchantWithdrawalService().Find(withdrawal.ParentId.Value);
                var children = await new MerchantWithdrawalService().GetChildren(parent.Id);

                if (children.Where(c => c.Id != withdrawal.Id).All(
                    c => c.Status == WithdrawalStatus.AutoPaymentFailed ||
                    c.Status == WithdrawalStatus.AutoPaymentManualConfirmedFailed))
                {
                    parent.Status = withdrawal.Status;

                    ctx.Entry(parent).State = EntityState.Modified;
                }
            }


            EntityLogService.Add(
                ctx,
                EntityLogContent.MerchantWithdrawal_Failed,
                EntityLogTargetType.MerchantWithdrawal,
                withdrawal.Id,
                logDetails,
                withdrawal.Status.GetDisplayName(), withdrawal.Amount);
        }

        private string BuildUserLogContent(MerchantWithdrawal withdrawal, string operatorUsername)
        {
            switch (withdrawal.Status)
            {
                //case WithdrawalStatus.AwaitingApproval:
                //    break;
                case WithdrawalStatus.AwaitingAutoPayment:
                    return $"{withdrawal.Id}账户{withdrawal.Username}提款审核批准，正在等待自动支付";
                //case WithdrawalStatus.AutoPaymentInProgress:
                //    break;
                //case WithdrawalStatus.AutoPaymentFailed:
                //    break;
                case WithdrawalStatus.AutoPaymentSuccessful:
                    return $"{withdrawal.Id}账户{withdrawal.Username}提款{withdrawal.Amount}自动支付成功";
                case WithdrawalStatus.ManualConfirmed:
                    return $"{withdrawal.Id}账户{withdrawal.Username}提款{withdrawal.Amount}被{operatorUsername}补单成功";
                case WithdrawalStatus.Declined:
                    return $"{withdrawal.Id}账户{withdrawal.Username}提款{withdrawal.Amount}被{operatorUsername}拒绝";
                case WithdrawalStatus.AutoPaymentManualConfirmedFailed:
                    return $"{withdrawal.Id}账户{withdrawal.Username}提款{withdrawal.Amount}被{operatorUsername}补单失败";
                default:
                    return string.Empty;
            }
        }

        private void UpdatePaymentChannel(PCContext ctx, MerchantWithdrawal withdrawal, DateTime now)
        {
            withdrawal.PaymentChannel.LastUsedAt = now;

            withdrawal.Commission = new PaymentChannelService().CalculateCommission(withdrawal.PaymentChannel, withdrawal.Amount);
            var transaction = new PaymentChannelTransaction
            {
                Amount = 0 - withdrawal.Amount,
                BeforeBalance = withdrawal.PaymentChannel.Provider.Balance,
                CreatedAt = now,
                PaymentChannelId = withdrawal.PaymentChannelId.Value,
                Type = PaymentChannelTransactionType.MerchantWithdrawal,
                Commission = withdrawal.Commission,
                AfterBalance = withdrawal.PaymentChannel.Provider.Balance - withdrawal.Amount - withdrawal.Commission
            };
            withdrawal.PaymentChannel.Provider.Balance = transaction.AfterBalance;

            ctx.PaymentChannelTransactions.Add(transaction);
        }
    }
}
