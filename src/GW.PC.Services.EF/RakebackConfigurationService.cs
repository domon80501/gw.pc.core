﻿using System.Threading.Tasks;
using GW.PC.Models;
using System.Data.Entity;
using GW.PC.Core;
using System.Linq;

namespace GW.PC.Services.EF.Admin
{
    public class RakebackConfigurationService : StatusfulEntityService<RakebackConfiguration>
    {
        public async Task Save(RakebackConfiguration configuration)
        {
            if (configuration.Id == 0)
            {
                if (configuration.Status == EntityStatus.Enabled)
                {
                    using (var ctx = new GWContext())
                    {
                        var existing = await ctx.RakebackConfigurations.SingleOrDefaultAsync(
                            c => c.CustomerGradeId == configuration.CustomerGradeId &&
                            c.Status == EntityStatus.Enabled);
                        if (existing.Id != configuration.Id)
                        {
                            throw new BusinessException("已存在启用的等级");
                        }
                    }
                }

                await base.Add(configuration);
            }
            else
            {
                using (var ctx = new GWContext())
                {
                    var dbConfiguration = await ctx.RakebackConfigurations.FindAsync(configuration.Id);

                    dbConfiguration.CustomerGradeId = configuration.CustomerGradeId;
                    dbConfiguration.Configurations = configuration.Configurations;

                    await ctx.SaveChangesAsync();
                }
            }
        }

        public override async Task UpdateEntityStatus(int id, EntityStatus status, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var configuration = await EnsureEntity(id, ctx);

                if (status == EntityStatus.Enabled && await ctx.RakebackConfigurations.AnyAsync(
                    c => c.CustomerGradeId == configuration.CustomerGradeId &&
                    c.Status == EntityStatus.Enabled &&
                    c.Id != id))
                {
                    throw new BusinessException("已存在启用的等级");
                }

                configuration.Status = status;

                await ctx.SaveChangesAsync();
            }
        }

        protected override IQueryable<RakebackConfiguration> OrderBy(IQueryable<RakebackConfiguration> source, string orderBy = null, bool isAscending = false)
        {
            return isAscending ? source.OrderBy(c => c.CustomerGrade.Level) :
                source.OrderByDescending(c => c.CustomerGrade.Level);
        }
    }
}
