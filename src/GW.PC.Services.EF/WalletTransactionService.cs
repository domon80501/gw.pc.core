﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF.Admin;
using GW.PC.Services.EF.Payment;

namespace GW.PC.Services.EF
{
    public class WalletTransactionService : CreatableEntityService<WalletTransaction>
    {
        public static async Task<object> GetSource(WalletTransactionType type, int sourceId)
        {
            if (sourceId <= 0)
            {
                return null;
            }

            switch (type)
            {
                case WalletTransactionType.WeChatDeposit:
                    return await DepositServiceHelper.GetDeposit(PaymentMethod.WeChat, sourceId, "PaymentChannel");
                case WalletTransactionType.AlipayDeposit:
                    return await DepositServiceHelper.GetDeposit(PaymentMethod.Alipay, sourceId, "PaymentChannel");
                case WalletTransactionType.OnlineBankingDeposit:
                    return await DepositServiceHelper.GetDeposit(PaymentMethod.OnlineBanking, sourceId, "PaymentChannel");
                case WalletTransactionType.OnlinePaymentDeposit:
                    return await DepositServiceHelper.GetDeposit(PaymentMethod.OnlinePayment, sourceId, "PaymentChannel");
                case WalletTransactionType.PrepaidCardDeposit:
                    return await DepositServiceHelper.GetDeposit(PaymentMethod.PrepaidCard, sourceId, "PaymentChannel");
                case WalletTransactionType.QQWalletDeposit:
                    return await DepositServiceHelper.GetDeposit(PaymentMethod.QQWallet, sourceId, "PaymentChannel");
                case WalletTransactionType.QuickPayDeposit:
                    return await DepositServiceHelper.GetDeposit(PaymentMethod.QuickPay, sourceId, "PaymentChannel");
                case WalletTransactionType.GameDeposit:
                case WalletTransactionType.GameWithdrawal:
                    return await new GameTransferService().Find(sourceId);
                case WalletTransactionType.BalanceAdjustment:
                    return await new BalanceAdjustmentService().Find(sourceId);
                case WalletTransactionType.CustomerWithdrawal:
                case WalletTransactionType.CustomerWithdrawalRefund:
                    return await new CustomerWithdrawalService().Find(sourceId);
                case WalletTransactionType.Bonus:
                    return await new BonusService().Find(sourceId);
                case WalletTransactionType.OnlineToBankCardDeposit:
                case WalletTransactionType.OnlineToBankCardCommissionRefund:
                    return await DepositServiceHelper.GetDeposit(PaymentMethod.OnlineToBankCard, sourceId, "PaymentChannel");
                case WalletTransactionType.AgentWithdrawal:
                case WalletTransactionType.AgentWithdrawalRefund:
                    return await new AgentWithdrawalService().Find(sourceId);
                case WalletTransactionType.AgentSettlement:
                    return await new AgentSettlementService().Find(sourceId);
                case WalletTransactionType.AgentAdjustment:
                    return await new AgentAdjustmentService().Find(sourceId);
                case WalletTransactionType.JDWalletDeposit:
                    return await DepositServiceHelper.GetDeposit(PaymentMethod.JDWallet, sourceId, "PaymentChannel");
                case WalletTransactionType.GameBet:
                case WalletTransactionType.GameWin:
                case WalletTransactionType.GameBetRefund:
                    return await new GameTransactionService().GetByGameId(sourceId);
                default:
                    throw new NotSupportedException($"Invalid wallet transaction type: {type.GetDisplayName()}");
            }
        }

        public async Task<int?> GetLastGameId()
        {
            using (var ctx = new GWContext())
            {
                return (await ctx.GameTransactions
                    .OrderByDescending(t => t.GameId)
                    .FirstOrDefaultAsync())?.GameId;
            }
        }
    }
}
