﻿using System.Threading.Tasks;
using GW.PC.Models;
using System.Data.Entity;
using System.Linq;
using System;
using GW.PC.Core;
using System.Collections.Generic;

namespace GW.PC.Services.EF
{
    public class AgentMessageService : CreatableEntityService<AgentMessage>
    {
        public async Task<int> GetUnreadCount(string username)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.AgentMessages.CountAsync(
                    m => m.ToUsername == username && m.Status == AgentMessageStatus.Unread);
            }
        }

        public async Task Delete(int id)
        {
            using (var ctx = new GWContext())
            {
                var message = await EnsureEntity(id, ctx);

                ctx.AgentMessages.Remove(message);

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<AgentMessage>> GetAgentMessages(string username, int pageNum, int pageSize)
        {
            using (var ctx = new GWContext())
            {
                var unread = ctx.AgentMessages.Where(x => x.ToUsername == username && x.Status == AgentMessageStatus.Unread);
                var now = DateTime.Now.ToE8();
                foreach (var item in unread)
                {
                    item.ReadAt = now;
                    item.Status = AgentMessageStatus.Read;
                    await Update(item);
                }

                return await ctx.AgentMessages.Where(x => x.ToUsername == username).OrderByDescending(x => x.CreatedAt).Skip(pageNum * pageSize).Take(pageSize).ToListAsync();
            }
        }

        protected override IQueryable<AgentMessage> OrderBy(IQueryable<AgentMessage> source, string orderBy = null, bool isAscending = false)
        {
            switch (orderBy?.ToLower())
            {
                case "createdat":
                    return isAscending ? source.OrderBy(c => c.CreatedAt) :
                        source.OrderByDescending(c => c.CreatedAt);
                default:
                    return base.OrderBy(source, orderBy, isAscending);
            }
        }
    }
}
