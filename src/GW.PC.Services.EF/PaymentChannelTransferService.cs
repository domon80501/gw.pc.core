﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.PC.Services.EF
{
    public class PaymentChannelTransferService : CreatableEntityService<PaymentChannelTransfer>
    {
        public override Task<PaymentChannelTransfer> Add(PaymentChannelTransfer item)
        {
            throw new NotSupportedException("Use the other overload instead");
        }

        public async Task<PaymentChannelTransfer> Add(PaymentChannelTransfer transfer, int fromProviderId, int toProviderId)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var pcService = new PaymentChannelService();
                    var from = await pcService.Get(fromProviderId, PaymentType.Withdrawal);
                    var to = await pcService.Get(toProviderId, PaymentType.InternalDeposit);

                    ctx.Entry(from).State = EntityState.Unchanged;
                    ctx.Entry(to).State = EntityState.Unchanged;
                    ctx.Entry(from.Provider).State = EntityState.Unchanged;
                    ctx.Entry(to.Provider).State = EntityState.Unchanged;

                    CheckNull(from);
                    CheckNull(to);

                    transfer.FromId = from.Id;
                    transfer.From = from;
                    transfer.ToId = to.Id;
                    transfer.To = to;
                    transfer.FromCommission = pcService.CalculateCommission(from, transfer.Amount);
                    transfer.ToCommission = to.IsBankCard ? 0 : pcService.CalculateCommission(to, transfer.Amount);

                    if (transfer.Status == PaymentChannelTransferStatus.AutoPaymentInProgress)
                    {
                        if (from.Status != EntityStatus.Enabled)
                        {
                            throw new BusinessException("通道未启用，无法新增互转");
                        }

                        // Validate transfer amount against from channel
                        if (transfer.Amount < from.MinAmount ||
                            transfer.Amount > from.MaxAmount)
                        {
                            throw new BusinessException(
                                string.Format(
                                    Constants.MessageTemplates.InvalidAmount,
                                    from.MinAmount,
                                    from.MaxAmount));
                        }
                    }
                    else if (transfer.Status == PaymentChannelTransferStatus.Created)
                    {
                        UpdatePaymentChannel(ctx, transfer, DateTime.Now.ToE8());
                    }

                    ctx.PaymentChannelTransfers.Add(transfer);

                    await ctx.SaveChangesAsync();

                    EntityLogService.Add(
                        ctx,
                        EntityLogContent.PaymentChannelTransfer_Created,
                        EntityLogTargetType.PaymentChannelTransfer,
                        transfer.Id,
                        null,
                        transfer.Amount,
                        transfer.Status.GetDisplayName());

                    scope.Complete();

                    return transfer;
                }
            }
        }

        public async Task<PaymentChannelTransfer> AutoSucceed(int id)
        {
            using (var ctx = new GWContext())
            {
                var transfer = await EnsureEntity<PaymentChannelTransfer>(id, ctx, "From.Provider", "To.Provider");
                CheckNull(transfer.From?.Provider);
                CheckNull(transfer.To?.Provider);
                if (transfer.Status != PaymentChannelTransferStatus.AutoPaymentInProgress)
                {
                    throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, transfer.Status.GetDisplayName()));
                }

                transfer.Status = PaymentChannelTransferStatus.AutoPaymentSuccessful;

                Succeed(ctx, transfer, DateTime.Now, null);

                await ctx.SaveChangesAsync();

                return transfer;
            }
        }

        public async Task<PaymentChannelTransfer> AutoFail(int id, string declineNotes, string logDetails = null)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var transfer = await EnsureEntity(id, ctx);
                    if (transfer.Status != PaymentChannelTransferStatus.AutoPaymentInProgress)
                    {
                        throw new BusinessException(
                            string.Format(
                                Constants.MessageTemplates.InvalidCurrentStatus,
                                transfer.Status.GetDisplayName()));
                    }

                    transfer.Status = PaymentChannelTransferStatus.AutoPaymentFailed;

                    Fail(ctx, transfer, declineNotes, null, logDetails);

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return transfer;
                }
            }
        }

        public async Task<PaymentChannelTransfer> GetByMerchantOrderNumber(string merchantOrderNumber, params string[] includes)
        {
            using (var ctx = new GWContext())
            {
                IQueryable<PaymentChannelTransfer> query = ctx.Set<PaymentChannelTransfer>();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }

                return await query.SingleOrDefaultAsync(d => d.MerchantOrderNumber == merchantOrderNumber);
            }
        }

        private void Succeed(GWContext ctx, PaymentChannelTransfer transfer, DateTime now, int? operatorId, string notes = null)
        {
            if (transfer.Status == PaymentChannelTransferStatus.ManualConfirmed)
            {
                transfer.ManualConfirmedAt = now;
                transfer.ManualConfirmedById = operatorId;
                //transfer.ManualConfirmNotes = notes;
            }
            else if (transfer.Status == PaymentChannelTransferStatus.AutoPaymentSuccessful)
            {
                transfer.PaidAt = now;
            }

            // Update payment channel
            UpdatePaymentChannel(ctx, transfer, now);
        }

        private void Fail(GWContext ctx, PaymentChannelTransfer transfer, string notes, int? operatorId, string logDetails = null)
        {
            var now = DateTime.Now;

            // Update transfer
            if (transfer.Status == PaymentChannelTransferStatus.AutoPaymentManualConfirmedFailed)
            {
                transfer.ManualConfirmedAt = now;
                transfer.ManualConfirmedById = operatorId;
            }
            else if (transfer.Status == PaymentChannelTransferStatus.AutoPaymentFailed)
            {
                transfer.PaidAt = now;
            }
            else
            {
                throw new Exception($"Invalid status to Fail: {transfer.Status.GetDisplayName()}");
            }
            transfer.Notes = notes;

            EntityLogService.Add(
                ctx,
                EntityLogContent.PaymentChannelTransfer_Failed,
                EntityLogTargetType.PaymentChannelTransfer,
                transfer.Id,
                logDetails,
                transfer.Amount, transfer.Status.GetDisplayName());
        }

        private void UpdatePaymentChannel(GWContext ctx, PaymentChannelTransfer transfer, DateTime now)
        {
            transfer.From.LastUsedAt = now;

            var fromTransaction = new PaymentChannelTransaction
            {
                Amount = 0 - transfer.Amount,
                BeforeBalance = transfer.From.Provider.Balance,
                Commission = transfer.FromCommission,
                AfterBalance = transfer.From.Provider.Balance - transfer.Amount - transfer.FromCommission,
                CreatedAt = DateTime.Now.ToE8(),
                PaymentChannelId = transfer.FromId,
                Type = PaymentChannelTransactionType.PaymentChannelTransfer
            };
            var toTransaction = new PaymentChannelTransaction
            {
                Amount = transfer.Amount,
                BeforeBalance = transfer.To.Provider.Balance,
                Commission = transfer.ToCommission,
                AfterBalance = transfer.To.Provider.Balance + transfer.Amount - transfer.ToCommission,
                CreatedAt = DateTime.Now.ToE8(),
                PaymentChannelId = transfer.ToId,
                Type = PaymentChannelTransactionType.PaymentChannelTransfer
            };

            transfer.From.Provider.Balance = fromTransaction.AfterBalance;
            transfer.To.Provider.Balance = toTransaction.AfterBalance;

            ctx.PaymentChannelTransactions.Add(fromTransaction);
            ctx.PaymentChannelTransactions.Add(toTransaction);
        }
    }
}