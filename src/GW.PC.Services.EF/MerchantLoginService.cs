﻿using System.Threading.Tasks;
using GW.PC.Models;
using System.Data.Entity.Migrations;
using System;
using GW.PC.Core;

namespace GW.PC.Services.EF
{
    public class MerchantLoginService : EntityService<MerchantLogin>
    {
        public async Task<string> RefreshToken(string username)
        {
            using (var ctx = new PCContext())
            {
                var login = new MerchantLogin
                {
                    Username = username,
                    Token = Guid.NewGuid().ToString(),
                    ExpiresAt = DateTime.Now.ToE8().AddDays(30)
                };

                ctx.MerchantLogins.AddOrUpdate(l => l.Username, login);

                await ctx.SaveChangesAsync();

                return login.Token;
            }
        }
    }
}
