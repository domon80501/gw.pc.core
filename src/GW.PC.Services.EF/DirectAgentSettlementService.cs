﻿using GW.PC.Models;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class DirectAgentSettlementService : EntityService<DirectAgentSettlement>
    {
        public async Task<DirectAgentSettlement> Get(int settlementId)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.DirectAgentSettlements.SingleOrDefaultAsync(s => s.SettlementId == settlementId);
            }
        }
    }
}
