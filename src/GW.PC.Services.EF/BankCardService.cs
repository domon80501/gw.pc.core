﻿using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF.Validation;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class BankCardService : EntityService<BankCard>
    {
        public async Task ToggleAgent(int id, bool enabled, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var bankCard = await EnsureEntity(id, ctx);
                if (enabled)
                {
                    if (await ctx.BankCards.AnyAsync(
                        c => c.Id != bankCard.Id &&
                        c.CardNumber == bankCard.CardNumber &&
                        c.AgentId.HasValue &&
                        c.Status == EntityStatus.Enabled))
                    {
                        throw new BusinessException("相同银行卡号已被其它代理账号绑定启用");
                    }
                }

                bankCard.Status = enabled ? EntityStatus.Enabled : EntityStatus.Disabled;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task ToggleCustomer(int id, bool enabled, int operatorId)
        {
            if (enabled)
            {
                throw new NotSupportedException("Not supported for enabling customer bank cards");
            }

            using (var ctx = new GWContext())
            {
                var bankCard = await EnsureEntity(id, ctx);

                bankCard.Status = EntityStatus.Disabled;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task Validate(string cardNumber)
        {
            await Validate<BankCardValidator>(new BankCard { CardNumber = cardNumber }, "VerifyBankCard");
        }
    }
}
