﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class EntityLogService : CreatableEntityService<EntityLog>
    {
        public static async Task Add(EntityLogContent content, EntityLogTargetType targetType, int? targetId, string details = null, params object[] data)
        {
            using (var ctx = new PCContext())
            {
                ctx.EntityLogs.Add(new EntityLog
                {
                    Content = content,
                    CreatedAt = DateTime.Now.ToE8(),
                    TargetType = targetType,
                    TargetId = targetId,
                    Data = EntityLogContents.SetData(data),
                    Details = details
                });

                await ctx.SaveChangesAsync();
            }
        }

        internal static void Add(PCContext ctx, EntityLogContent content, EntityLogTargetType targetType, int? targetId, string details = null, params object[] data)
        {
            ctx.EntityLogs.Add(new EntityLog
            {
                Content = content,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = targetType,
                TargetId = targetId,
                Data = EntityLogContents.SetData(data),
                Details = details
            });
        }
    }
}
