﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.PC.Services.EF
{
    public class PaymentChannelAdjustmentService : CreatableEntityService<PaymentChannelAdjustment>
    {
        public async Task<PaymentChannelAdjustment> Save(PaymentChannelAdjustment adjustment, int? providerId = null)
        {
            PaymentChannelAdjustment result = null;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    if (adjustment.Id == 0)
                    {
                        CheckNull(providerId);

                        var pcService = new PaymentChannelService();
                        var channel = await pcService.Get(providerId.Value, adjustment.Amount > 0 ? PaymentType.InternalDeposit : PaymentType.Withdrawal);

                        ctx.Entry(channel).State = EntityState.Unchanged;
                        ctx.Entry(channel.Provider).State = EntityState.Unchanged;

                        CheckNull(channel?.Provider);

                        adjustment.PaymentChannelId = channel.Id;
                        adjustment.PaymentChannel = channel;
                        if (!channel.IsBankCard || (channel.IsBankCard && adjustment.Amount < 0))
                        {
                            adjustment.Commission = pcService.CalculateCommission(channel, Math.Abs(adjustment.Amount));
                        }

                        if (adjustment.Type == PaymentChannelAdjustmentType.Auto)
                        {
                            if (channel.Status != EntityStatus.Enabled)
                            {
                                throw new BusinessException("通道未启用，无法新增微调");
                            }

                            // Validate transfer amount against from channel
                            if (Math.Abs(adjustment.Amount) < channel.MinAmount ||
                                Math.Abs(adjustment.Amount) > channel.MaxAmount)
                            {
                                throw new BusinessException(
                                    string.Format(
                                        Constants.MessageTemplates.InvalidAmount,
                                        channel.MinAmount,
                                        channel.MaxAmount));
                            }
                        }
                        else if (adjustment.Status == PaymentChannelAdjustmentStatus.Created)
                        {
                            UpdatePaymentChannel(ctx, adjustment, DateTime.Now.ToE8());
                        }
                        else
                        {
                            throw new Exception($"Invalid adjustment status: {adjustment.Status.GetDisplayName()}");
                        }

                        ctx.PaymentChannelAdjustments.Add(adjustment);

                        await ctx.SaveChangesAsync();

                        EntityLogService.Add(
                            ctx,
                            EntityLogContent.PaymentChannelAdjustment_Created,
                            EntityLogTargetType.PaymentChannelAdjustment,
                            adjustment.Id,
                            null,
                            adjustment.Amount,
                            adjustment.Status.GetDisplayName());

                        result = adjustment;
                    }
                    else
                    {
                        var dbAdjustment = await ctx.PaymentChannelAdjustments.FindAsync(adjustment.Id);

                        dbAdjustment.Usage = adjustment.Usage;
                        dbAdjustment.Notes = adjustment.Notes;

                        result = dbAdjustment;

                        await ctx.SaveChangesAsync();
                    }

                    scope.Complete();

                    return result;
                }
            }
        }

        public async Task<PaymentChannelAdjustment> AutoSucceed(int id)
        {
            using (var ctx = new GWContext())
            {
                var adjustment = await EnsureEntity<PaymentChannelAdjustment>(id, ctx, "PaymentChannel.Provider");
                CheckNull(adjustment.PaymentChannel?.Provider);
                if (adjustment.Status != PaymentChannelAdjustmentStatus.AutoPaymentInProgress)
                {
                    throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, adjustment.Status.GetDisplayName()));
                }

                adjustment.Status = PaymentChannelAdjustmentStatus.AutoPaymentSuccessful;

                Succeed(ctx, adjustment, DateTime.Now, null);

                await ctx.SaveChangesAsync();

                return adjustment;
            }
        }

        public async Task<PaymentChannelAdjustment> AutoFail(int id, string declineNotes, string logDetails = null)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var adjustment = await EnsureEntity(id, ctx);
                    if (adjustment.Status != PaymentChannelAdjustmentStatus.AutoPaymentInProgress)
                    {
                        throw new BusinessException(
                            string.Format(
                                Constants.MessageTemplates.InvalidCurrentStatus,
                                adjustment.Status.GetDisplayName()));
                    }

                    adjustment.Status = PaymentChannelAdjustmentStatus.AutoPaymentFailed;

                    Fail(ctx, adjustment, declineNotes, null, logDetails);

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return adjustment;
                }
            }
        }

        public async Task<PaymentChannelAdjustment> GetByMerchantOrderNumber(string merchantOrderNumber, params string[] includes)
        {
            using (var ctx = new GWContext())
            {
                IQueryable<PaymentChannelAdjustment> query = ctx.Set<PaymentChannelAdjustment>();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }

                return await query.SingleOrDefaultAsync(d => d.MerchantOrderNumber == merchantOrderNumber);
            }
        }

        private void UpdatePaymentChannel(GWContext ctx, PaymentChannelAdjustment adjustment, DateTime now)
        {
            adjustment.PaymentChannel.LastUsedAt = now;

            var transaction = new PaymentChannelTransaction
            {
                BeforeBalance = adjustment.PaymentChannel.Provider.Balance,
                Amount = adjustment.Amount,
                Commission = adjustment.Commission,
                AfterBalance = adjustment.PaymentChannel.Provider.Balance + adjustment.Amount - adjustment.Commission,
                CreatedAt = now,
                Notes = adjustment.Notes,
                PaymentChannelId = adjustment.PaymentChannelId,
                Type = PaymentChannelTransactionType.PaymentChannelAdjustment
            };

            adjustment.PaymentChannel.Provider.Balance = transaction.AfterBalance;

            ctx.PaymentChannelTransactions.Add(transaction);
        }

        private void Succeed(GWContext ctx, PaymentChannelAdjustment adjustment, DateTime now, int? operatorId, string notes = null)
        {
            if (adjustment.Status == PaymentChannelAdjustmentStatus.ManualConfirmed)
            {
                adjustment.ManualConfirmedAt = now;
                adjustment.ManualConfirmedById = operatorId;
                //transfer.ManualConfirmNotes = notes;
            }
            else if (adjustment.Status == PaymentChannelAdjustmentStatus.AutoPaymentSuccessful)
            {
                adjustment.PaidAt = now;
            }

            // Update payment channel
            UpdatePaymentChannel(ctx, adjustment, now);
        }

        private void Fail(GWContext ctx, PaymentChannelAdjustment adjustment, string notes, int? operatorId, string logDetails = null)
        {
            var now = DateTime.Now;

            // Update transfer
            if (adjustment.Status == PaymentChannelAdjustmentStatus.AutoPaymentManualConfirmedFailed)
            {
                adjustment.ManualConfirmedAt = now;
                adjustment.ManualConfirmedById = operatorId;
            }
            else if (adjustment.Status == PaymentChannelAdjustmentStatus.AutoPaymentFailed)
            {
                adjustment.PaidAt = now;
            }
            else
            {
                throw new Exception($"Invalid status to Fail: {adjustment.Status.GetDisplayName()}");
            }
            adjustment.PaymentNotes = notes;

            EntityLogService.Add(
                ctx,
                EntityLogContent.PaymentChannelAdjustment_Failed,
                EntityLogTargetType.PaymentChannelAdjustment,
                adjustment.Id,
                logDetails,
                adjustment.Amount, adjustment.Status.GetDisplayName());
        }
    }
}
