﻿using System.Threading.Tasks;
using GW.PC.Models;
using System.Data.Entity.Migrations;
using System;

namespace GW.PC.Services.EF
{
    public class CustomerLoginService : EntityService<CustomerLogin>
    {
        public async Task<string> RefreshToken(string username)
        {
            using (var ctx = new GWContext())
            {
                var login = new CustomerLogin
                {
                    Username = username,
                    Token = Guid.NewGuid().ToString(),
                    ExpiresAt = DateTime.Now.AddDays(30)
                };

                ctx.CustomerLogins.AddOrUpdate(l => l.Username, login);

                await ctx.SaveChangesAsync();

                return login.Token;
            }
        }
    }
}
