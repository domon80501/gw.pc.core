﻿using GW.PC.Models;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class AgentPromotionService : CreatableEntityService<AgentPromotion>
    {
        public async Task Save(AgentPromotion ap)
        {
            using (var ctx = new GWContext())
            {
                if (ap.Id == 0)
                {
                    ctx.AgentPromotions.Add(ap);
                }
                else
                {
                    var dbGW = await ctx.AgentPromotions.FindAsync(ap.Id);

                    dbGW.AttachmentFileNames = ap.AttachmentFileNames;
                    dbGW.Title = ap.Title;
                    dbGW.Content = ap.Content;
                }

                await ctx.SaveChangesAsync();
            }
        }

        public async Task Reorder(string orders)
        {
            using (var ctx = new GWContext())
            {
                await ctx.Database.ExecuteSqlCommandAsync(@"update agentpromotions set orders = @orders",
                    new SqlParameter("@orders", orders));
            }
        }
    }
}
