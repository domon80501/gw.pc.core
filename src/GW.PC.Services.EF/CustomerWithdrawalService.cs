﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.Services.EF
{
    public class CustomerWithdrawalService : CreatableEntityService<CustomerWithdrawal>
    {
        private static readonly Random random = new Random();

        public async Task<CustomerWithdrawal> AddChildren(CustomerWithdrawal withdrawal, IEnumerable<CustomerWithdrawal> children)
        {
            using (var ctx = new GWContext())
            {
                withdrawal.HasChildren = true;
                withdrawal.PaymentChannelId = null;
                withdrawal.PaymentChannel = null;

                ctx.CustomerWithdrawals.AddRange(children);
                ctx.Entry(withdrawal).State = EntityState.Modified;

                await ctx.SaveChangesAsync();

                return withdrawal;
            }
        }

        public async Task<CustomerWithdrawal> GetByMerchantOrderNumber(string merchantOrderNumber, params string[] includes)
        {
            using (var ctx = new GWContext())
            {
                IQueryable<CustomerWithdrawal> query = ctx.Set<CustomerWithdrawal>();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }

                return await query.SingleOrDefaultAsync(d => d.MerchantOrderNumber == merchantOrderNumber);
            }
        }

        public async Task<CustomerWithdrawal> AuditClose(int id, string notes, int? operatorId = null, string customerMessage = null)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var withdrawal = await EnsureEntity(id, ctx);
                    if (withdrawal.Status != WithdrawalStatus.AwaitingApproval)
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                    }

                    withdrawal.AuditedById = operatorId;
                    withdrawal.AuditedAt = DateTime.Now.ToE8();
                    withdrawal.DeclineNotes = notes;

                    await Fail(ctx, withdrawal, operatorId, customerMessage);

                    await Close(ctx, withdrawal, operatorId);

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return withdrawal;
                }
            }
        }

        public async Task<CustomerWithdrawal> AutoFail(int id, string declineNotes, string customerMessage = null, string logDetails = null)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var withdrawal = await EnsureEntity(id, ctx);
                    if (withdrawal.Status != WithdrawalStatus.AutoPaymentInProgress)
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                    }

                    withdrawal.Status = WithdrawalStatus.AutoPaymentFailed;
                    withdrawal.PaidAt = DateTime.Now.ToE8();
                    withdrawal.DeclineNotes = declineNotes;

                    await Fail(ctx, withdrawal, null, customerMessage, logDetails);

                    if (!withdrawal.HasChildren &&
                        !withdrawal.ParentId.HasValue &&
                        !await ctx.CustomerWithdrawals.AnyAsync(
                            w => w.Username == withdrawal.Username &&
                            (w.Status == WithdrawalStatus.AutoPaymentSuccessful ||
                            w.Status == WithdrawalStatus.ManualConfirmed ||
                            w.Status == WithdrawalStatus.RetryAutoPaymentSuccessful)))
                    {
                        await Close(ctx, withdrawal, null);
                    }

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return withdrawal;
                }
            }
        }

        public async Task<CustomerWithdrawal> AutoSucceed(int id)
        {
            using (var ctx = new GWContext())
            {
                var withdrawal = await EnsureEntity<CustomerWithdrawal>(id, ctx, "PaymentChannel.Provider", "Reference");
                if (withdrawal.Status != WithdrawalStatus.AutoPaymentInProgress)
                {
                    throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                }

                withdrawal.Status = WithdrawalStatus.AutoPaymentSuccessful;

                await Succeed(ctx, withdrawal, DateTime.Now, null);

                await ctx.SaveChangesAsync();

                return withdrawal;
            }
        }

        public async Task<CustomerWithdrawal> AuditApprove(int id, int? operatorId, int? lastDepositPeriodId = null)
        {
            using (var ctx = new GWContext())
            {
                var withdrawal = await EnsureEntity(id, ctx);
                if (withdrawal.Status != WithdrawalStatus.AwaitingApproval)
                {
                    throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                }

                withdrawal.Status = WithdrawalStatus.AwaitingAutoPayment;

                var now = DateTime.Now;

                withdrawal.AuditedAt = now;
                withdrawal.AuditedById = operatorId;

                if (operatorId.HasValue)
                {
                    // Add user log
                    var user = await EnsureEntity<User>(operatorId.Value, ctx);
                    AddUserLog(ctx, user.Username, BuildUserLogContent(withdrawal, user.Username));
                }

                // Update wallet
                var wallet = await EnsureEntity<Wallet>(withdrawal.Username, ctx);
                if (wallet.Balance < withdrawal.Amount)
                {
                    throw new BusinessException(Constants.Messages.InsufficientWalletBalance);
                }
                wallet.Balance -= withdrawal.Amount;
                wallet.FrozenAmount += withdrawal.Amount;

                AddWalletTransaction(ctx, 0 - withdrawal.Amount, wallet.Balance, WalletTransactionType.CustomerWithdrawal, withdrawal.Username, withdrawal.Id);

                EntityLogService.Add(
                    ctx,
                    EntityLogContent.CustomerWithdrawal_AmountFrozen,
                    EntityLogTargetType.CustomerWithdrawal,
                    withdrawal.Id,
                    null,
                    withdrawal.Status.GetDisplayName(), wallet.Balance, wallet.FrozenAmount, withdrawal.Amount);

                // Update customer deposit period
                if (lastDepositPeriodId.HasValue)
                {
                    var lastDepositPeriod = await EnsureEntity<CustomerDepositPeriod>(lastDepositPeriodId.Value, ctx);
                    if (lastDepositPeriod?.Completed == false)
                    {
                        lastDepositPeriod.Completed = true;
                        lastDepositPeriod.EndsAt = now;
                    }
                }

                await ctx.SaveChangesAsync();

                return withdrawal;
            }
        }

        public async Task<CustomerWithdrawal> ManualConfirm(int id, bool result, string notes, int operatorId)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var withdrawal = await EnsureEntity<CustomerWithdrawal>(id, ctx, "PaymentChannel.Provider", "Reference");
                    if (withdrawal.Status != WithdrawalStatus.AutoPaymentInProgress)
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                    }

                    if (result)
                    {
                        withdrawal.Status = WithdrawalStatus.ManualConfirmed;

                        await Succeed(ctx, withdrawal, DateTime.Now.ToE8(), operatorId, notes);
                    }
                    else
                    {
                        withdrawal.Status = WithdrawalStatus.AutoPaymentManualConfirmedFailed;
                        withdrawal.ManualConfirmedAt = DateTime.Now.ToE8();
                        withdrawal.ManualConfirmedById = operatorId;
                        withdrawal.ManualConfirmNotes = notes;

                        await Fail(ctx, withdrawal, operatorId);

                        if (!withdrawal.HasChildren &&
                            !await ctx.CustomerWithdrawals.AnyAsync(
                            w => w.Username == withdrawal.Username &&
                            (w.Status == WithdrawalStatus.AutoPaymentSuccessful ||
                            w.Status == WithdrawalStatus.ManualConfirmed ||
                            w.Status == WithdrawalStatus.RetryAutoPaymentSuccessful)))
                        {
                            await Close(ctx, withdrawal, null);
                        }
                    }

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return withdrawal;
                }
            }
        }

        public async Task<IEnumerable<CustomerWithdrawal>> GetChildren(int id)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.CustomerWithdrawals.Include(w => w.PaymentChannel).Where(w => w.ParentId == id).ToListAsync();
            }
        }

        public async Task<CustomerWithdrawal> SelectWithdrawChannel(int id)
        {
            using (var ctx = new GWContext())
            {
                var withdrawal = await EnsureEntity(id, ctx);
                var config = await SystemConfigurationService.Get();
                var divideThreshold = config.Withdrawal.DivideThreshold ?? 45000;

                if (withdrawal.Status == WithdrawalStatus.AwaitingAutoPayment)
                {
                    PaymentChannel channel = null;

                    var channels = await ctx.PaymentChannels
                        .Include(c => c.Provider.PaymentProviderBanks)
                        .Where(
                        c => c.PaymentType == PaymentType.Withdrawal &&
                        c.Status == EntityStatus.Enabled &&
                        c.MinAmount <= withdrawal.Amount &&
                        ((withdrawal.Amount <= divideThreshold && c.MaxAmount >= withdrawal.Amount) ||
                        withdrawal.Amount > divideThreshold))
                        .ToListAsync();

                    NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 1 - Amount:{withdrawal.Amount}; channels:{string.Join("|", channels.Select(c => c.Name))}");
                    channels = channels.Where(
                        c => c.Provider.PaymentProviderBanks
                        .Where(b => b.Status == EntityStatus.Enabled &&
                        b.PaymentMethod == c.PaymentMethod)
                        .Select(b => b.BankId)
                        .Contains(withdrawal.PayeeBankId.GetValueOrDefault()) &&
                        (!c.MinimumWithdrawInterval.HasValue ||
                            Convert.ToDecimal(DateTime.Now.ToE8().Subtract(c.LastUsedAt.GetValueOrDefault()).TotalSeconds) >= c.MinimumWithdrawInterval.Value))
                        .ToList();
                    NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 2 - PayeeBankId:{withdrawal.PayeeBankId}; channels:{string.Join("|", channels.Select(c => c.Name))}");
                    var commissions = channels.Select(c => new
                    {
                        Channel = c,
                        Commission = new PaymentChannelService().CalculateCommission(c, withdrawal.Amount)
                    })
                    .Where(c => c.Channel.Provider.Balance >= withdrawal.Amount + c.Commission + 100)
                    .ToList();
                    NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 3 - channels:{(string.Join("|", commissions.Select(c => new { c.Channel.Name, c.Channel.Provider.Balance, c.Commission })))}");

                    if (commissions.Count == 0)
                    {
                        NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 4 - No channel");

                        withdrawal.Status = WithdrawalStatus.AutoPaymentFailed;

                        await Fail(ctx, withdrawal, null, "无可用提款渠道");
                    }
                    else
                    {
                        if (commissions.Count == 1)
                        {
                            channel = commissions.Single().Channel;
                            NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 4 - Single channel:{channel.Name}");
                        }
                        else
                        {
                            var minCommissions = commissions.Where(
                                c => c.Commission == commissions.Min(cc => cc.Commission))
                                .ToList();
                            NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 4 - minCommissions:{(string.Join("|", minCommissions.Select(c => new { c.Channel.Name, c.Commission })))}");
                            var randomDouble = random.NextDouble();
                            NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 5 - random double:{randomDouble}");
                            if (randomDouble < 0.5)
                            {
                                var index = random.Next(0, minCommissions.Count);
                                NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 6 - [0, {minCommissions.Count}]:{index}");
                                channel = minCommissions.ElementAt(index).Channel;
                            }
                            else
                            {
                                var index = random.Next(0, commissions.Count);
                                NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 6 - [0, {commissions.Count}]:{index}");
                                channel = commissions.ElementAt(index).Channel;
                            }
                            NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 7 - selected: {channel.Name}");
                        }

                        withdrawal.Status = WithdrawalStatus.AutoPaymentInProgress;
                        withdrawal.PaymentChannelId = channel.Id;
                        withdrawal.PaymentChannel = channel;
                    }

                    await ctx.SaveChangesAsync();
                }

                return withdrawal;
            }
        }

        public async Task<CustomerWithdrawal> Retry(int id, int operatorId)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var withdrawal = await EnsureEntity<CustomerWithdrawal>(id, ctx, "PaymentChannel");
                    if (withdrawal.HasChildren)
                    {
                        throw new BusinessException("总提款不能重试，请对各拆分提款分别重试");
                    }

                    if (withdrawal.Status != WithdrawalStatus.AutoPaymentFailed &&
                        withdrawal.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed)
                    {
                        throw new BusinessException(
                            string.Format(
                                Constants.MessageTemplates.InvalidCurrentStatus,
                                withdrawal.Status.GetDisplayName()));
                    }

                    if (await ctx.CustomerWithdrawals.AnyAsync(
                        w => w.ReferenceId == withdrawal.Id &&
                        w.Status != WithdrawalStatus.AutoPaymentFailed &&
                        w.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed &&
                        w.Status != WithdrawalStatus.AutoPaymentSuccessful &&
                        w.Status != WithdrawalStatus.ManualConfirmed))
                    {
                        throw new BusinessException("该提款已经在重试中，请刷新查看");
                    }

                    withdrawal.Status = WithdrawalStatus.FailedAndClosed;

                    var newWithdrawal = new CustomerWithdrawal
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        Amount = withdrawal.Amount,
                        Status = WithdrawalStatus.AwaitingAutoPayment,
                        CreatedAt = DateTime.Now.ToE8(),
                        ParentId = withdrawal.ParentId,
                        PayeeBankId = withdrawal.PayeeBankId,
                        PayeeCardAddress = withdrawal.PayeeCardAddress,
                        PayeeCardNumber = withdrawal.PayeeCardNumber,
                        PayeeName = withdrawal.PayeeName,
                        PaymentChannel = withdrawal.PaymentChannel,
                        PaymentChannelId = withdrawal.PaymentChannelId,
                        Username = withdrawal.Username,
                        WithdrawalIP = withdrawal.WithdrawalIP,
                        WithdrawalAddress = withdrawal.WithdrawalAddress,
                        ReferenceId = withdrawal.Id
                    };

                    ctx.CustomerWithdrawals.Add(newWithdrawal);

                    await ctx.SaveChangesAsync();

                    var wallet = await EnsureEntity<Wallet>(withdrawal.Username, ctx);
                    EntityLogService.Add(
                        ctx,
                        EntityLogContent.CustomerWithdrawal_RetryCreated,
                        EntityLogTargetType.CustomerWithdrawal,
                        newWithdrawal.Id,
                        null,
                        newWithdrawal.Status.GetDisplayName(), wallet.Balance, wallet.FrozenAmount, withdrawal.Amount);

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return newWithdrawal;
                }
            }
        }

        public async Task<CustomerWithdrawal> Close(int id, int? operatorId)
        {
            using (var ctx = new GWContext())
            {
                var withdrawal = await EnsureEntity<CustomerWithdrawal>(id, ctx);

                return await Close(ctx, withdrawal, operatorId);
            }
        }

        private async Task<CustomerWithdrawal> Close(GWContext ctx, CustomerWithdrawal withdrawal, int? operatorId)
        {
            if (withdrawal.ParentId.HasValue)
            {
                throw new NotSupportedException("Cannot close a child withdrawal");
            }

            if (withdrawal.Status != WithdrawalStatus.AwaitingApproval &&
                withdrawal.Status != WithdrawalStatus.AutoPaymentFailed &&
                withdrawal.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed)
            {
                throw new BusinessException(
                    string.Format(
                        Constants.MessageTemplates.InvalidCurrentStatus,
                        withdrawal.Status.GetDisplayName()));
            }

            // Update wallet
            var wallet = await EnsureEntity<Wallet>(withdrawal.Username, ctx);
            if (withdrawal.Status == WithdrawalStatus.AutoPaymentFailed ||
                withdrawal.Status == WithdrawalStatus.AutoPaymentManualConfirmedFailed)
            {
                if (wallet.FrozenAmount < withdrawal.Amount)
                {
                    throw new BusinessException(Constants.Messages.InsufficientFrozenAmount);
                }

                wallet.FrozenAmount -= withdrawal.Amount;
                wallet.Balance += withdrawal.Amount;

                AddWalletTransaction(ctx, withdrawal.Amount, wallet.Balance, WalletTransactionType.CustomerWithdrawalRefund, withdrawal.Username, withdrawal.Id);
            }

            withdrawal.Status = WithdrawalStatus.FailedAndClosed;

            var children = ctx.CustomerWithdrawals.Where(w => w.ParentId == withdrawal.Id);
            foreach (var child in children)
            {
                if (child.Status != WithdrawalStatus.AutoPaymentFailed &&
                    child.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed)
                {
                    throw new BusinessException("该提款有拆分提款不是失败状态，不能关闭");
                }

                child.Status = WithdrawalStatus.FailedAndClosed;
            }

            var references = ctx.CustomerWithdrawals.Where(w => w.ReferenceId == withdrawal.Id);
            foreach (var reference in references)
            {
                if (reference.Status != WithdrawalStatus.AutoPaymentFailed &&
                    reference.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed)
                {
                    throw new BusinessException("该提款有重试提款不是失败状态，不能关闭");
                }

                reference.Status = WithdrawalStatus.FailedAndClosed;
            }

            await new GameTransferService().Add(ctx, withdrawal.Username, withdrawal.Amount, true, null, null, GameTransferSource.CustomerWithdrawalRefund, withdrawal.Id);

            EntityLogService.Add(
                ctx,
                EntityLogContent.CustomerWithdrawal_FailedAndClosed,
                EntityLogTargetType.CustomerWithdrawal,
                withdrawal.Id,
                null,
                withdrawal.Amount, wallet.Balance, wallet.FrozenAmount);

            await ctx.SaveChangesAsync();

            return withdrawal;
        }

        private async Task Succeed(GWContext ctx, CustomerWithdrawal withdrawal, DateTime now, int? operatorId, string notes = null)
        {
            if (withdrawal.Status == WithdrawalStatus.ManualConfirmed)
            {
                withdrawal.ManualConfirmedAt = now;
                withdrawal.ManualConfirmedById = operatorId;
                withdrawal.ManualConfirmNotes = notes;
                if (withdrawal.Reference != null)
                {
                    withdrawal.Reference.Status = WithdrawalStatus.RetryManualConfirmed;
                    withdrawal.Reference.ManualConfirmedById = operatorId;
                    withdrawal.Reference.ManualConfirmedAt = now;
                    withdrawal.Reference.ManualConfirmNotes = notes;
                }
            }
            else if (withdrawal.Status == WithdrawalStatus.AutoPaymentSuccessful)
            {
                withdrawal.PaidAt = now;
                if (withdrawal.Reference != null)
                {
                    withdrawal.Reference.DeclineNotes = null;
                    withdrawal.Reference.Status = WithdrawalStatus.RetryAutoPaymentSuccessful;
                    withdrawal.Reference.PaidAt = now;
                }
            }

            // Update wallet
            var wallet = await EnsureEntity<Wallet>(withdrawal.Username, ctx);
            if (wallet.FrozenAmount < withdrawal.Amount)
            {
                throw new BusinessException(Constants.Messages.InsufficientFrozenAmount);
            }

            wallet.FrozenAmount -= withdrawal.Amount;
            wallet.FirstWithdrawalAt = wallet.FirstWithdrawalAt ?? now;
            wallet.LastWithdrawalAt = now;
            wallet.TotalWithdrawAmount += withdrawal.Amount;
            wallet.TotalWithdrawCount += 1;
            wallet.AmountLastUpdatedAt = now;

            // Update payment channel
            if (withdrawal.PaymentChannel != null)
            {
                UpdatePaymentChannel(ctx, withdrawal, now);
            }

            if (operatorId.HasValue)
            {
                // Add user log
                var user = await EnsureEntity<User>(operatorId.Value, ctx);
                AddUserLog(ctx, user.Username, BuildUserLogContent(withdrawal, user.Username));
            }

            var customer = await EnsureEntity<Customer>(withdrawal.Username, ctx, "Agent");
            if (customer.Agent != null)
            {
                customer.Agent.CustomerAmountLastUpdatedAt = now;
            }

            // Update customer's bank cards
            var bankCard = await ctx.BankCards.SingleOrDefaultAsync(
                c => c.CardNumber == withdrawal.PayeeCardNumber &&
                c.WalletId.HasValue &&
                c.Status == EntityStatus.Enabled);
            if (bankCard == null)
            {
                ctx.BankCards.Add(new BankCard
                {
                    AccountName = withdrawal.PayeeName,
                    BankId = withdrawal.PayeeBankId.Value,
                    CardNumber = withdrawal.PayeeCardNumber,
                    CreatedAt = DateTime.Now.ToE8(),
                    Status = EntityStatus.Enabled,
                    Verified = true,
                    WalletId = customer.WalletId
                });
            }
            else
            {
                if (bankCard.WalletId == customer.WalletId && bankCard.Verified == false)
                {
                    bankCard.Verified = true;
                }
            }

            // Update parent withdrawal, if needed
            if (withdrawal.ParentId > 0)
            {
                var parent = await new CustomerWithdrawalService().Find(withdrawal.ParentId.Value);
                var children = await new CustomerWithdrawalService().GetChildren(parent.Id);

                if (children.Where(c => c.Id != withdrawal.Id).All(
                    c => c.Status == WithdrawalStatus.AutoPaymentSuccessful ||
                    c.Status == WithdrawalStatus.RetryAutoPaymentSuccessful ||
                    c.Status == WithdrawalStatus.ManualConfirmed ||
                    c.Status == WithdrawalStatus.RetryManualConfirmed))
                {
                    parent.Status = WithdrawalStatus.AutoPaymentSuccessful;

                    ctx.Entry(parent).State = EntityState.Modified;
                }
            }

            // Send customer message
            SendCustomerMessage(ctx, withdrawal.Username, "您的提款成功到账", $"您申请的提款金额{withdrawal.Amount}元，已成功派发至您的银行账户.请及时查看.");
        }

        private async Task Fail(GWContext ctx, CustomerWithdrawal withdrawal, int? operatorId, string customerMessage = null, string logDetails = null)
        {
            var now = DateTime.Now.ToE8();

            withdrawal.CustomerMessage = customerMessage;

            if (operatorId.HasValue)
            {
                // Add user log
                var user = await EnsureEntity<User>(operatorId.Value, ctx);
                AddUserLog(ctx, user.Username, BuildUserLogContent(withdrawal, user.Username));
            }

            // Update parent withdrawal, if needed
            if (withdrawal.ParentId > 0)
            {
                var parent = await new CustomerWithdrawalService().Find(withdrawal.ParentId.Value);
                var children = await new CustomerWithdrawalService().GetChildren(parent.Id);

                if (children.Where(c => c.Id != withdrawal.Id).All(
                    c => c.Status == WithdrawalStatus.AutoPaymentFailed ||
                    c.Status == WithdrawalStatus.AutoPaymentManualConfirmedFailed))
                {
                    parent.Status = withdrawal.Status;

                    ctx.Entry(parent).State = EntityState.Modified;
                }
            }

            var wallet = await EnsureEntity<Wallet>(withdrawal.Username, ctx);
            EntityLogService.Add(
                ctx,
                EntityLogContent.CustomerWithdrawal_Failed,
                EntityLogTargetType.CustomerWithdrawal,
                withdrawal.Id,
                logDetails,
                withdrawal.Status.GetDisplayName(), wallet.Balance, wallet.FrozenAmount, withdrawal.Amount);
        }

        private string BuildUserLogContent(CustomerWithdrawal withdrawal, string operatorUsername)
        {
            switch (withdrawal.Status)
            {
                //case WithdrawalStatus.AwaitingApproval:
                //    break;
                case WithdrawalStatus.AwaitingAutoPayment:
                    return $"{withdrawal.Id}账户{withdrawal.Username}提款审核批准，正在等待自动支付";
                //case WithdrawalStatus.AutoPaymentInProgress:
                //    break;
                //case WithdrawalStatus.AutoPaymentFailed:
                //    break;
                case WithdrawalStatus.AutoPaymentSuccessful:
                    return $"{withdrawal.Id}账户{withdrawal.Username}提款{withdrawal.Amount}自动支付成功";
                case WithdrawalStatus.ManualConfirmed:
                    return $"{withdrawal.Id}账户{withdrawal.Username}提款{withdrawal.Amount}被{operatorUsername}补单成功";
                case WithdrawalStatus.Declined:
                    return $"{withdrawal.Id}账户{withdrawal.Username}提款{withdrawal.Amount}被{operatorUsername}拒绝";
                case WithdrawalStatus.AutoPaymentManualConfirmedFailed:
                    return $"{withdrawal.Id}账户{withdrawal.Username}提款{withdrawal.Amount}被{operatorUsername}补单失败";
                default:
                    return string.Empty;
            }
        }

        private void UpdatePaymentChannel(GWContext ctx, CustomerWithdrawal withdrawal, DateTime now)
        {
            withdrawal.PaymentChannel.LastUsedAt = now;

            withdrawal.Commission = new PaymentChannelService().CalculateCommission(withdrawal.PaymentChannel, withdrawal.Amount);
            var transaction = new PaymentChannelTransaction
            {
                Amount = 0 - withdrawal.Amount,
                BeforeBalance = withdrawal.PaymentChannel.Provider.Balance,
                CreatedAt = now,
                PaymentChannelId = withdrawal.PaymentChannelId.Value,
                Type = PaymentChannelTransactionType.CustomerWithdrawal,
                Commission = withdrawal.Commission,
                AfterBalance = withdrawal.PaymentChannel.Provider.Balance - withdrawal.Amount - withdrawal.Commission
            };
            withdrawal.PaymentChannel.Provider.Balance = transaction.AfterBalance;

            ctx.PaymentChannelTransactions.Add(transaction);
        }
    }
}
