﻿using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Models.Infrastructure;
using GW.PC.Services.EF.Admin;
using GW.PC.Services.EF.Validation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using X.PagedList;

namespace GW.PC.Services.EF
{
    public class CustomerService : CreatableEntityService<Customer>
    {
        public override Task<Customer> Add(Customer item)
        {
            item.Password = PasswordUtility.Encrypt(item.Password);
            item.Wallet = item.Wallet ?? new Wallet
            {
                Username = item.Username,
                CreatedAt = DateTime.Now,
            };

            return base.Add(item);
        }

        public async Task ToggleStatus(int id, bool enabled, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity(id, ctx);

                customer.Status = enabled ? AccountStatus.Normal : AccountStatus.Frozen;

                if (enabled)
                {
                    customer.FailedLoginCount = 0;
                }

                await ctx.SaveChangesAsync();
            }
        }

        public async Task Register(Customer customer, string verificationCode, string agentPromotionCode)
        {
            await Validate<CustomerValidator>(customer, "Register");

            if (!string.IsNullOrEmpty(verificationCode))
            {
                await new MobileVerificationCodeService().Verify(customer.Mobile, verificationCode);
            }

            using (var ctx = new GWContext())
            {
                if (await ctx.Customers.AnyAsync(c => c.Username == customer.Username))
                {
                    throw new BusinessException(string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "账号"), "1");
                }

                if (await ctx.Customers.AnyAsync(c => c.Mobile == customer.Mobile))
                {
                    throw new BusinessException(string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "电话"), "1");
                }

                if (await ctx.Customers.AnyAsync(
                    c => c.RegistrationIP == customer.RegistrationIP &&
                    c.Name == customer.Name))
                {
                    throw new BusinessException("重复注册，忘记帐号可手机号登录", "1");
                }

                if (customer.ReferrerId.HasValue)
                {
                    if (!await ctx.Customers.AnyAsync(c => c.Id == customer.ReferrerId.Value))
                    {
                        throw new BusinessException("无效推荐码", "1");
                    }
                }

                Agent agent = null;
                if (!string.IsNullOrEmpty(agentPromotionCode))
                {
                    agent = await ctx.Agents.SingleOrDefaultAsync(a => a.PromotionCode == agentPromotionCode);

                    customer.AgentId = agent?.Id;
                }
                if (!customer.AgentId.HasValue && !string.IsNullOrEmpty(customer.RegistrationIP))
                {
                    var referral = await ctx.ReferralRecognitions.SingleOrDefaultAsync(r => r.ClientIP == customer.RegistrationIP);
                    if (!string.IsNullOrEmpty(referral?.AgentPromotionCode))
                    {
                        agent = await ctx.Agents.SingleOrDefaultAsync(a => a.PromotionCode == referral.AgentPromotionCode);

                        customer.AgentId = agent?.Id;
                    }
                    if (!customer.AgentId.HasValue && !string.IsNullOrEmpty(referral?.Url))
                    {
                        customer.AgentId = await new AgentService().GetIdByPromotionSite(referral.Url);
                    }
                }
                if (!customer.AgentId.HasValue && !string.IsNullOrEmpty(customer.RegistrationUrl))
                {
                    customer.AgentId = await new AgentService().GetIdByPromotionSite(customer.RegistrationUrl);
                }
                if (!customer.AgentId.HasValue)
                {
                    agent = await EnsureEntity<Agent>(Constants.DefaultAgentUsername, ctx);
                    customer.AgentId = agent.Id;
                }

                if (!customer.ReferrerId.HasValue)
                {
                    var referral = await ctx.ReferralRecognitions.SingleOrDefaultAsync(r => r.ClientIP == customer.RegistrationIP);

                    customer.ReferrerId = referral?.CustomerId;
                }

                customer.AccountType = CustomerAccountType.Normal;
                customer.Password = PasswordUtility.Encrypt(customer.Password);
                customer.Gender = Gender.NotSet;
                customer.CreatedAt = DateTime.Now;
                customer.Status = AccountStatus.Normal;
                customer.DateOfBirth = new DateTime(1900, 1, 1);
                customer.Grade = ctx.CustomerGrades.Single(g => g.Name == "普通会员");
                customer.AllowWithdraw = true;
                customer.AllowRakeback = true;
                customer.AllowPromotion = true;
                customer.MobileBound = true;
                customer.Wallet = new Wallet
                {
                    Username = customer.Username,
                    CreatedAt = DateTime.Now
                };

                ctx.Customers.Add(customer);

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<Customer> Login(string username, string password, string deviceId = null)
        {
            await Validate<CustomerValidator>(new Customer { Username = username, Password = password }, "Login");

            var now = DateTime.Now.ToE8();

            using (var ctx = new GWContext())
            {
                var customer = await ctx.Customers.FirstOrDefaultAsync(
                    c => c.Username == username ||
                        (c.MobileBound && c.Mobile == username) ||
                        (c.EmailBound && c.Email == username));

                CheckLoginIdentity(customer);

                if (!string.IsNullOrEmpty(customer.Password))
                {
                    if (PasswordUtility.Decrypt(customer.Password) == password)
                    {
                        customer.LastLoginAt = now;

                        ctx.CustomerLoginLogs.Add(new CustomerLoginLog
                        {
                            Username = customer.Username,
                            CreatedAt = now,
                            DeviceIdentifier = deviceId,
                            Succeeded = true
                        });

                        await ctx.SaveChangesAsync();

                        await UpdateReturnedCustomer(customer);

                        return customer;
                    }
                    else
                    {
                        ctx.FailedCustomerLogins.Add(new FailedCustomerLogin
                        {
                            Username = username,
                            CreatedAt = now
                        });

                        var start = now.AddMinutes(-30);
                        if (await ctx.FailedCustomerLogins.CountAsync(
                            l => l.Username == username &&
                            l.CreatedAt >= start) > 10)
                        {
                            customer.Status = AccountStatus.Frozen;
                        }

                        await ctx.SaveChangesAsync();

                        throw new BusinessException(Constants.Messages.InvalidLogin, "1");
                    }
                }

                throw new BusinessException(Constants.Messages.InvalidLogin, "1");

                async Task UpdateReturnedCustomer(Customer cust)
                {
                    cust.MD5Password = cust.Password = cust.TempPasswordKey = string.Empty;

                    await ctx.Entry(cust).Reference(c => c.Wallet).LoadAsync();
                    await ctx.Entry(cust).Reference(c => c.Grade).LoadAsync();
                    await ctx.Entry(cust).Reference(c => c.Referrer).LoadAsync();
                    await ctx.Entry(cust).Reference(c => c.Agent).LoadAsync();
                }
            }
        }

        public async Task<Customer> LoginByMobile(string mobile)
        {
            await Validate<CustomerValidator>(new Customer { Mobile = mobile }, "LoginByMobile");

            using (var ctx = new GWContext())
            {
                var customers = await ctx.Customers.Where(
                    c => c.MobileBound &&
                    c.Mobile == mobile)
                    .ToListAsync();
                if (customers.Count > 1)
                {
                    throw new BusinessException(
                        string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "手机号码"));
                }

                var customer = customers.SingleOrDefault();

                CheckLoginIdentity(customer);

                customer.LastLoginAt = DateTime.Now;

                await ctx.SaveChangesAsync();

                await UpdateReturnedCustomer(customer);

                return customer;

                async Task UpdateReturnedCustomer(Customer cust)
                {
                    cust.MD5Password = cust.Password = cust.TempPasswordKey = string.Empty;

                    await ctx.Entry(cust).Reference(c => c.Wallet).LoadAsync();
                    await ctx.Entry(cust).Reference(c => c.Grade).LoadAsync();
                    await ctx.Entry(cust).Reference(c => c.Referrer).LoadAsync();
                    await ctx.Entry(cust).Reference(c => c.Agent).LoadAsync();
                }
            }
        }

        public async Task ResetPassword(string username, string newPassword)
        {
            await Validate<CustomerValidator>(new Customer { Password = newPassword }, "ManagePassword");

            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);

                customer.MD5Password = MD5Hash.GetMd5(newPassword, false);
                customer.Password = PasswordUtility.Encrypt(newPassword);

                await ctx.SaveChangesAsync();
            }
        }

        public async Task ResetPassword(string mobile, string verificationCode, string newPassword)
        {
            await Validate<CustomerValidator>(new Customer { Password = newPassword }, "ManagePassword");

            await new MobileVerificationCodeService().Verify(mobile, verificationCode);

            using (var ctx = new GWContext())
            {
                var customer = await ctx.Customers.SingleOrDefaultAsync(c => c.Mobile == mobile);

                if (customer == null)
                {
                    throw new BusinessException(
                        string.Format(Constants.MessageTemplates.InvalidObject, "手机号码"),
                        "1");
                }

                customer.Password = PasswordUtility.Encrypt(newPassword);

                await ctx.SaveChangesAsync();
            }
        }

        public async Task ChangeMobile(string username, string password, string mobile, string verificationCode)
        {
            await new MobileVerificationCodeService().Verify(mobile, verificationCode);

            await Login(username, password);

            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);

                if (await ctx.Customers.AnyAsync(
                    c => c.Mobile == mobile &&
                    c.Id != customer.Id))
                {
                    throw new BusinessException("", "1");
                }

                customer.Mobile = mobile;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<Customer> GetByUsername(string username, params string[] includes)
        {
            using (var ctx = new GWContext())
            {
                IQueryable<Customer> source = ctx.Customers;

                foreach (var include in includes)
                {
                    source = source.Include(include);
                }

                var customer = await source.SingleOrDefaultAsync(c => c.Username == username);

                return customer ??
                    throw new BusinessException(Constants.Messages.CustomerNotFound);
            }
        }

        public async Task<IEnumerable<Customer>> GetByUsernames(IEnumerable<string> usernames, params string[] includes)
        {
            using (var ctx = new GWContext())
            {
                var source = ctx.Customers.Where(c => usernames.Contains(c.Username));

                foreach (var include in includes)
                {
                    source = source.Include(include);
                }

                return await source.ToListAsync();
            }
        }

        public async Task<Customer> GetByMobile(string mobile, params string[] includes)
        {
            using (var ctx = new GWContext())
            {
                IQueryable<Customer> source = ctx.Customers;

                foreach (var include in includes)
                {
                    source = source.Include(include);
                }

                try
                {
                    var customer = await source.SingleOrDefaultAsync(c => c.Mobile == mobile);

                    return customer;
                }
                catch
                {
                    throw new Exception("Duplicate phone number");
                }
            }
        }

        public async Task<IEnumerable<Customer>> GetByGrade(int gradeId)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Customers.Where(c => c.GradeId == gradeId).ToListAsync();
            }
        }

        public async Task<IPagedList<AggregateDeposit>> GetDeposits(string username, int pageNumber, int pageSize, DepositStatus? status = null)
        {
            using (var ctx = new GWContext())
            {
                var deposits = ctx.WeChatDeposits.Where(d => d.Username == username).Select(d => new AggregateDeposit
                {
                    Id = d.Id,
                    RequestedAmount = d.RequestedAmount,
                    ActualAmount = d.RequestedAmount,
                    CommissionRefund = 0,
                    CreatedAt = d.CreatedAt,
                    CompletedAt = d.CompletedAt,
                    Type = WalletTransactionType.WeChatDeposit,
                    Status = d.Status
                })
                .Concat(ctx.AlipayDeposits.Where(d => d.Username == username).Select(d => new AggregateDeposit
                {
                    Id = d.Id,
                    RequestedAmount = d.RequestedAmount,
                    ActualAmount = d.RequestedAmount,
                    CommissionRefund = 0,
                    CreatedAt = d.CreatedAt,
                    CompletedAt = d.CompletedAt,
                    Type = WalletTransactionType.AlipayDeposit,
                    Status = d.Status
                }))
                .Concat(ctx.OnlinePaymentDeposits.Where(d => d.Username == username).Select(d => new AggregateDeposit
                {
                    Id = d.Id,
                    RequestedAmount = d.RequestedAmount,
                    ActualAmount = d.RequestedAmount,
                    CommissionRefund = 0,
                    CreatedAt = d.CreatedAt,
                    CompletedAt = d.CompletedAt,
                    Type = WalletTransactionType.OnlinePaymentDeposit,
                    Status = d.Status
                }))
                .Concat(ctx.OnlineBankingDeposits.Where(d => d.Username == username).Select(d => new AggregateDeposit
                {
                    Id = d.Id,
                    RequestedAmount = d.RequestedAmount,
                    ActualAmount = d.ActualAmount,
                    CommissionRefund = d.CommissionRefund,
                    CreatedAt = d.CreatedAt,
                    CompletedAt = d.CompletedAt,
                    Type = WalletTransactionType.OnlineBankingDeposit,
                    Status = d.Status
                }))
                .Concat(ctx.PrepaidCardDeposits.Where(d => d.Username == username).Select(d => new AggregateDeposit
                {
                    Id = d.Id,
                    RequestedAmount = d.RequestedAmount,
                    ActualAmount = d.RequestedAmount,
                    CommissionRefund = 0,
                    CreatedAt = d.CreatedAt,
                    CompletedAt = d.CompletedAt,
                    Type = WalletTransactionType.PrepaidCardDeposit,
                    Status = d.Status
                }))
                .Concat(ctx.QQWalletDeposits.Where(d => d.Username == username).Select(d => new AggregateDeposit
                {
                    Id = d.Id,
                    RequestedAmount = d.RequestedAmount,
                    ActualAmount = d.RequestedAmount,
                    CommissionRefund = 0,
                    CreatedAt = d.CreatedAt,
                    CompletedAt = d.CompletedAt,
                    Type = WalletTransactionType.QQWalletDeposit,
                    Status = d.Status
                }))
                .Concat(ctx.QuickPayDeposits.Where(d => d.Username == username).Select(d => new AggregateDeposit
                {
                    Id = d.Id,
                    RequestedAmount = d.RequestedAmount,
                    ActualAmount = d.RequestedAmount,
                    CommissionRefund = 0,
                    CreatedAt = d.CreatedAt,
                    CompletedAt = d.CompletedAt,
                    Type = WalletTransactionType.QuickPayDeposit,
                    Status = d.Status
                }))
                .Concat(ctx.OnlineToBankCardDeposits.Where(d => d.Username == username).Select(d => new AggregateDeposit
                {
                    Id = d.Id,
                    RequestedAmount = d.RequestedAmount,
                    ActualAmount = d.ActualAmount,
                    CommissionRefund = 0,
                    CreatedAt = d.CreatedAt,
                    CompletedAt = d.CompletedAt,
                    Type = WalletTransactionType.OnlineToBankCardDeposit,
                    Status = d.Status
                }))
                .Concat(ctx.JDWalletDeposits.Where(d => d.Username == username).Select(d => new AggregateDeposit
                {
                    Id = d.Id,
                    RequestedAmount = d.RequestedAmount,
                    ActualAmount = d.RequestedAmount,
                    CommissionRefund = 0,
                    CreatedAt = d.CreatedAt,
                    CompletedAt = d.CompletedAt,
                    Type = WalletTransactionType.JDWalletDeposit,
                    Status = d.Status
                }));

                switch (status)
                {
                    case DepositStatus.Success:
                        deposits = deposits.Where(d => d.Status == DepositStatus.AutoSuccess || d.Status == DepositStatus.ManualSuccess);
                        break;
                    case DepositStatus.PaymentFailed:
                        deposits = deposits.Where(d => d.Status == DepositStatus.PaymentFailed);
                        break;
                    case DepositStatus.PendingPayment:
                        deposits = deposits.Where(d => d.Status == DepositStatus.PendingPayment);
                        break;
                }

                return await deposits.OrderByDescending(d => d.CreatedAt).ToPagedListAsync(pageNumber, pageSize);
            }
        }

        public async Task<CustomerWithdrawal> PreWithdraw(string username, decimal amount, string cardNumber, int payeeBankId, string ip, string address)
        {
            if (amount < 100)
            {
                throw new BusinessException(Constants.Messages.WithdrawAmountOutOfRange, "1");
            }

            if (cardNumber.Length < 16 || cardNumber.Length > 21 || !cardNumber.All(c => char.IsNumber(c)))
            {
                throw new BusinessException(Constants.Messages.InvalidBankCardNumber);
            }

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var customer = await EnsureEntity<Customer>(username, ctx, "Wallet");

                    if (customer.Status != AccountStatus.Normal || !customer.AllowWithdraw)
                    {
                        throw new BusinessException(Constants.Messages.WithdrawNotAllowed, "1");
                    }

                    if (await ctx.BankCards.AnyAsync(
                        c => c.CardNumber == cardNumber &&
                        c.Status == EntityStatus.Enabled &&
                        c.WalletId.HasValue &&
                        c.WalletId != customer.WalletId))
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "银行卡"), "1");
                    }

                    var withdrawal = new CustomerWithdrawal
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        Amount = amount,
                        CreatedAt = DateTime.Now.ToE8(),
                        PayeeBankId = payeeBankId,
                        PayeeCardNumber = cardNumber,
                        PayeeName = customer.Name,
                        Status = WithdrawalStatus.AwaitingApproval,
                        Username = username,
                        WithdrawalIP = ip,
                        WithdrawalAddress = address
                    };
                    ctx.CustomerWithdrawals.Add(withdrawal);

                    await ctx.SaveChangesAsync();

                    EntityLogService.Add(
                        ctx,
                        EntityLogContent.CustomerWithdrawal_Created,
                        EntityLogTargetType.CustomerWithdrawal,
                        withdrawal.Id,
                        null,
                        withdrawal.Status.GetDisplayName(), customer.Wallet.Balance, customer.Wallet.FrozenAmount, withdrawal.Amount);

                    await new GameTransferService().Add(ctx, withdrawal.Username, withdrawal.Amount, false, null, null, GameTransferSource.CustomerWithdrawal, withdrawal.Id);

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return withdrawal;
                }
            }
        }

        public async Task AddBankCard(BankCard bankCard, string username, string cityCode)
        {
            await AddBankCard(bankCard, username);
        }

        public async Task DisableBankCard(string username, string cardNumber)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);
                var bankCard = await ctx.BankCards.SingleOrDefaultAsync(
                    c => c.WalletId == customer.WalletId &&
                    c.CardNumber == cardNumber &&
                    c.Status == EntityStatus.Enabled);

                CheckNull(bankCard);

                bankCard.Status = EntityStatus.Disabled;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task DisableBankCard(string username, int id)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);
                var bankCard = await ctx.BankCards.SingleOrDefaultAsync(
                    c => c.WalletId == customer.WalletId &&
                    c.Id == id &&
                    c.Status == EntityStatus.Enabled);

                CheckNull(bankCard);

                bankCard.Status = EntityStatus.Disabled;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<IPagedList<CustomerWithdrawal>> GetWithdrawals(string username, int pageNumber, int pageSize, WithdrawalStatus? status = null)
        {
            using (var ctx = new GWContext())
            {
                var withdrawals = ctx.CustomerWithdrawals.Where(
                    w => w.Username == username &&
                    !w.ParentId.HasValue);

                switch (status)
                {
                    case WithdrawalStatus.AutoPaymentSuccessful:
                        withdrawals = withdrawals.Where(w => w.Status == WithdrawalStatus.AutoPaymentSuccessful || w.Status == WithdrawalStatus.ManualConfirmed || w.Status == WithdrawalStatus.RetryAutoPaymentSuccessful || w.Status == WithdrawalStatus.RetryManualConfirmed);
                        break;
                    case WithdrawalStatus.AutoPaymentFailed:
                        withdrawals = withdrawals.Where(w => w.Status == WithdrawalStatus.FailedAndClosed || w.Status == WithdrawalStatus.Declined);
                        break;
                    case WithdrawalStatus.AutoPaymentInProgress:
                        withdrawals = withdrawals.Where(w => w.Status == WithdrawalStatus.AutoPaymentInProgress || w.Status == WithdrawalStatus.AwaitingApproval || w.Status == WithdrawalStatus.AwaitingAutoPayment || w.Status == WithdrawalStatus.AutoPaymentFailed || w.Status == WithdrawalStatus.AutoPaymentManualConfirmedFailed);
                        break;
                }

                return await withdrawals.OrderByDescending(w => w.CreatedAt).ToPagedListAsync(pageNumber, pageSize);
            }
        }

        public async Task<IPagedList<GameTransfer>> GetGameTransfers(string username, int pageNumber, int pageSize, GameTransferStatus? status = null)
        {
            using (var ctx = new GWContext())
            {
                var gameTransfers = ctx.GameTransfers.Include(g => g.From).Include(g => g.To).Where(g => g.Username == username);

                switch (status)
                {
                    case GameTransferStatus.Successful:
                        gameTransfers = gameTransfers.Where(g => g.Status == GameTransferStatus.Successful);
                        break;
                    case GameTransferStatus.Failed:
                        gameTransfers = gameTransfers.Where(g => g.Status == GameTransferStatus.Failed);
                        break;
                    case GameTransferStatus.InProgress:
                        gameTransfers = gameTransfers.Where(g => g.Status == GameTransferStatus.InProgress);
                        break;
                }

                return await gameTransfers.OrderByDescending(g => g.CreatedAt).ToPagedListAsync(pageNumber, pageSize);
            }
        }

        public async Task<IPagedList<Bonus>> GetBonus(string username, int pageNumber, int pageSize, AuditStatus status = 0)
        {
            using (var ctx = new GWContext())
            {
                var bonus = ctx.Bonus.Where(b => b.Username == username);

                switch (status)
                {
                    case AuditStatus.Approved:
                        bonus = bonus.Where(b => b.Status == AuditStatus.Approved);
                        break;
                    case AuditStatus.Declined:
                        bonus = bonus.Where(b => b.Status == AuditStatus.Declined);
                        break;
                    case AuditStatus.AwaitApproval:
                        bonus = bonus.Where(b => b.Status == AuditStatus.AwaitApproval);
                        break;
                }

                return await bonus.OrderByDescending(b => b.CreatedAt).ToPagedListAsync(pageNumber, pageSize);
            }
        }

        public async Task<IPagedList<Rakeback>> GetRakebacks(string username, int pageNumber, int pageSize, AuditStatus? status = null)
        {
            using (var ctx = new GWContext())
            {
                var rakebacks = ctx.Rakebacks.Include(r => r.Game).Where(r => r.Username == username);

                switch (status)
                {
                    case AuditStatus.Approved:
                        rakebacks = rakebacks.Where(r => r.Status == AuditStatus.Approved);
                        break;
                    case AuditStatus.Declined:
                        rakebacks = rakebacks.Where(r => r.Status == AuditStatus.Declined);
                        break;
                    case AuditStatus.AwaitApproval:
                        rakebacks = rakebacks.Where(r => r.Status == AuditStatus.AwaitApproval);
                        break;
                }

                return await rakebacks.OrderByDescending(r => r.CreatedAt).ToPagedListAsync(pageNumber, pageSize);
            }
        }

        public async Task SetPassword(string username, string password, int operatorId)
        {
            await Validate<CustomerValidator>(new Customer { Password = password }, "ManagePassword");

            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);

                customer.Password = PasswordUtility.Encrypt(password);

                await ctx.SaveChangesAsync();
            }
        }

        public async Task ChangePassword(string username, string oldPassword, string newPassword)
        {
            await Login(username, oldPassword);

            await Validate<CustomerValidator>(new Customer { Password = newPassword }, "ManagePassword");

            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);

                customer.Password = PasswordUtility.Encrypt(newPassword);

                await ctx.SaveChangesAsync();
            }
        }

        public async Task UpdateAgent(string username, string agentUsername)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);

                if (string.IsNullOrWhiteSpace(agentUsername))
                {
                    //customer.AgentId = null;
                    throw new BusinessException("上级代理不能为空");
                }
                else
                {
                    var agent = await ctx.Agents.SingleOrDefaultAsync(
                        a => a.Username.Equals(agentUsername, StringComparison.OrdinalIgnoreCase));
                    if (agent == null)
                    {
                        throw new BusinessException(Constants.Messages.AgentNotFound);
                    }

                    if (agent.Status != AccountStatus.Normal)
                    {
                        throw new BusinessException("代理不是正常状态");
                    }

                    customer.AgentId = agent.Id;
                }

                await ctx.SaveChangesAsync();
            }
        }

        public async Task ToggleBind(string username, CustomerAccountBindTarget type, bool isBound)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);

                switch (type)
                {
                    case CustomerAccountBindTarget.Mobile:
                        customer.MobileBound = isBound;

                        break;

                    case CustomerAccountBindTarget.Email:
                        customer.EmailBound = isBound;

                        break;

                    case CustomerAccountBindTarget.QQ:
                        customer.QQBound = isBound;

                        break;

                    case CustomerAccountBindTarget.WeChat:
                        customer.WeChatBound = isBound;

                        break;

                    default:
                        throw new ArgumentOutOfRangeException($"Invalid account binding type: {type.ToString()}");
                }

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<BankCard> GetBankCard(string username, string cardNumber)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);

                return await ctx.BankCards.SingleOrDefaultAsync(
                    c => c.WalletId == customer.WalletId &&
                    c.CardNumber == cardNumber);
            }
        }

        public async Task<IEnumerable<BankCard>> GetBankCards(string username, bool includeDisabled = true)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);

                if (includeDisabled)
                {
                    return await ctx.BankCards.Include(c => c.Bank).Where(
                        c => c.WalletId == customer.WalletId).ToListAsync();
                }
                else
                {
                    return await ctx.BankCards.Include(c => c.Bank).Where(
                        c => c.WalletId == customer.WalletId &&
                        c.Status == EntityStatus.Enabled).ToListAsync();
                }
            }
        }

        public async Task<IEnumerable<BankCard>> GetBankCardsForWithdrawal(string username)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);
                var lastWithdrawal = await ctx.CustomerWithdrawals.Where(
                    w => w.Username == username &&
                    (w.Status == WithdrawalStatus.AutoPaymentSuccessful ||
                    w.Status == WithdrawalStatus.ManualConfirmed))
                    .OrderByDescending(w => w.Id)
                    .FirstOrDefaultAsync();
                var bankCards = await ctx.BankCards.Include(c => c.Bank).Where(
                    c => c.WalletId == customer.WalletId &&
                    c.Status == EntityStatus.Enabled)
                    .OrderByDescending(c => c.Id)
                    .ToListAsync();

                if (lastWithdrawal == null)
                {
                    return bankCards;
                }
                else
                {
                    var lastBankCard = bankCards.SingleOrDefault(b => b.CardNumber == lastWithdrawal.PayeeCardNumber);
                    if (lastBankCard != null)
                    {
                        return new[] { lastBankCard }.Concat(bankCards.Where(b => b.Id != lastBankCard.Id));
                    }
                    else
                    {
                        return bankCards;
                    }
                }
            }
        }

        public async Task AddTag(string username, int tagId)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx, "Tags");

                if (!customer.Tags.Any(t => t.Id == tagId))
                {
                    var tag = await ctx.Tags.FindAsync(tagId);

                    customer.Tags.Add(tag);

                    await ctx.SaveChangesAsync();
                }
            }
        }

        public async Task RemoveTag(string username, int tagId)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx, "Tags");

                if (customer.Tags.Any(t => t.Id == tagId))
                {
                    var tag = await ctx.Tags.FindAsync(tagId);

                    customer.Tags.Remove(tag);

                    await ctx.SaveChangesAsync();
                }
            }
        }

        public async Task RemoveAllTags(string username)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx, "Tags");

                customer.Tags.Clear();

                await ctx.SaveChangesAsync();
            }
        }

        public async Task UpdateNotes(string username, string content)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);

                customer.Notes = content;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task SaveFollowUp(FollowUp followUp, int[] receiverIds)
        {
            bool isAdd = followUp.Id == 0;

            using (var ctx = new GWContext())
            {
                if (isAdd)
                {
                    followUp.Receivers = await new UserService().GetUsers(receiverIds, ctx);

                    ctx.FollowUps.Add(followUp);
                }
                else
                {
                    var dbFollowUp = await EnsureEntity<FollowUp>(followUp.Id, ctx, "Receivers");

                    dbFollowUp.Content = followUp.Content;
                    dbFollowUp.AttachmentFileNames = followUp.AttachmentFileNames;

                    var dbUserIds = dbFollowUp.Receivers.Select(u => u.Id).ToList();
                    var newReceiverIds = receiverIds.Except(dbUserIds);
                    foreach (var id in newReceiverIds)
                    {
                        dbFollowUp.Receivers.Add(ctx.Users.Find(id));
                    }
                    var deletedReceiverIds = dbUserIds.Except(receiverIds).ToList();
                    foreach (var id in deletedReceiverIds)
                    {
                        dbFollowUp.Receivers.Remove(dbFollowUp.Receivers.Single(p => p.Id == id));
                    }
                }

                await ctx.SaveChangesAsync();
            }

            if (isAdd)
            {
                // Add associated alarm(s).
                await new UserService().AddAlarm(new Alarm
                {
                    FollowUpId = followUp.Id,
                    Content = "你有新跟进需要处理",
                    Read = false,
                    CreatedAt = DateTime.Now,
                    CreatedById = followUp.CreatedById,
                    StartedAt = DateTime.Now
                },
                receiverIds);
            }
        }

        public async Task DeleteFollowUp(int followUpId, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var followUp = await EnsureEntity<FollowUp>(followUpId, ctx, "Comments");

                ctx.FollowUpComments.RemoveRange(followUp.Comments);
                ctx.FollowUps.Remove(followUp);

                var alarm = await ctx.Alarms.SingleOrDefaultAsync(a => a.FollowUpId == followUpId);
                if (alarm != null)
                {
                    ctx.Alarms.Remove(alarm);
                }

                await ctx.SaveChangesAsync();
            }
        }

        public async Task SaveFollowUpComment(FollowUpComment comment)
        {
            bool isAdd = comment.Id == 0;

            using (var ctx = new GWContext())
            {
                if (isAdd)
                {
                    ctx.FollowUpComments.Add(comment);
                }
                else
                {
                    var dbComment = await EnsureEntity<FollowUpComment>(comment.Id, ctx);

                    dbComment.Content = comment.Content;
                    dbComment.AttachmentFileNames = comment.AttachmentFileNames;
                }

                await ctx.SaveChangesAsync();
            }
        }

        public async Task DeleteFollowUpComment(int commentId, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var comment = await EnsureEntity<FollowUpComment>(commentId, ctx);

                ctx.FollowUpComments.Remove(comment);

                await ctx.SaveChangesAsync();
            }
        }

        public async Task SolveFollowUp(int followUpId, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var followUp = await EnsureEntity<FollowUp>(followUpId, ctx);

                followUp.IsSolved = true;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<decimal> GetCurrentMonthDepositAmount(string username)
        {
            using (var ctx = new GWContext())
            {
                var now = DateTime.Now;
                var month = new DateTime(now.Year, now.Month, 1);

                return (await ctx.WeChatDeposits.Where(
                    d => d.Username == username &&
                    (d.Status == DepositStatus.AutoSuccess || d.Status == DepositStatus.ManualSuccess) &&
                    d.CompletedAt >= month)
                    .Select(d => (decimal?)d.RequestedAmount)
                    .Concat(ctx.AlipayDeposits.Where(
                        d => d.Username == username &&
                        (d.Status == DepositStatus.AutoSuccess || d.Status == DepositStatus.ManualSuccess) &&
                        d.CompletedAt >= month)
                        .Select(d => (decimal?)d.RequestedAmount))
                    .Concat(ctx.OnlinePaymentDeposits.Where(
                        d => d.Username == username &&
                        (d.Status == DepositStatus.AutoSuccess || d.Status == DepositStatus.ManualSuccess) &&
                        d.CompletedAt >= month)
                        .Select(d => (decimal?)d.RequestedAmount))
                    .Concat(ctx.OnlineBankingDeposits.Where(
                        d => d.Username == username &&
                        (d.Status == DepositStatus.AutoSuccess || d.Status == DepositStatus.ManualSuccess) &&
                        d.CompletedAt >= month)
                        .Select(d => d.ActualAmount))
                    .Concat(ctx.QQWalletDeposits.Where(
                        d => d.Username == username &&
                        (d.Status == DepositStatus.AutoSuccess || d.Status == DepositStatus.ManualSuccess) &&
                        d.CompletedAt >= month)
                        .Select(d => (decimal?)d.RequestedAmount))
                    .Concat(ctx.QuickPayDeposits.Where(
                        d => d.Username == username &&
                        (d.Status == DepositStatus.AutoSuccess || d.Status == DepositStatus.ManualSuccess) &&
                        d.CompletedAt >= month)
                        .Select(d => (decimal?)d.RequestedAmount))
                    .Concat(ctx.PrepaidCardDeposits.Where(
                        d => d.Username == username &&
                        (d.Status == DepositStatus.AutoSuccess || d.Status == DepositStatus.ManualSuccess) &&
                        d.CompletedAt >= month)
                        .Select(d => (decimal?)d.RequestedAmount))
                    .Concat(ctx.OnlineToBankCardDeposits.Where(
                        d => d.Username == username &&
                        (d.Status == DepositStatus.AutoSuccess || d.Status == DepositStatus.ManualSuccess) &&
                        d.CompletedAt >= month)
                        .Select(d => d.ActualAmount))
                    .Concat(ctx.JDWalletDeposits.Where(
                        d => d.Username == username &&
                        (d.Status == DepositStatus.AutoSuccess || d.Status == DepositStatus.ManualSuccess) &&
                        d.CompletedAt >= month)
                        .Select(d => (decimal?)d.RequestedAmount))
                    .SumAsync()) ?? 0;
            }
        }

        public async Task ValidateForUpdate(Customer customer, string username, string birthday, string qq, string email, string mobile, string weChat)
        {
            using (var ctx = new GWContext())
            {
                var duplicates = ctx.Customers.Where(a => a.Username != username);

                if (!string.IsNullOrEmpty(qq))
                {
                    if (await duplicates.AnyAsync(a => a.QQ == qq))
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "QQ"));
                    }

                    if (!string.IsNullOrEmpty(customer.QQ))
                    {
                        throw new BusinessException("QQ已设置，不能修改");
                    }
                }

                if (!string.IsNullOrEmpty(email))
                {
                    if (await duplicates.AnyAsync(a => a.Email == email))
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "邮箱"));
                    }

                    if (customer.EmailBound)
                    {
                        throw new BusinessException("邮箱已绑定，请联系客服进行修改");
                    }
                }

                if (!string.IsNullOrEmpty(mobile))
                {
                    if (await duplicates.AnyAsync(a => a.Mobile == mobile))
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "手机号码"));
                    }

                    if (customer.MobileBound)
                    {
                        throw new BusinessException("手机已绑定，请联系客服进行修改");
                    }
                }

                if (!string.IsNullOrEmpty(weChat) && await duplicates.AnyAsync(a => a.WeChat == weChat))
                {
                    throw new BusinessException(string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "微信"));
                }

                if (!string.IsNullOrEmpty(birthday) && customer.DateOfBirth != new DateTime(1900, 1, 1))
                {
                    throw new BusinessException("生日已经设置，请联系客服进行修改");
                }
            }
        }

        public async Task<Wallet> GetWallet(string username)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Wallets.SingleOrDefaultAsync(w => w.Username == username);
            }
        }

        public async Task UpdateBirthday(string username, DateTime dob)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(username, ctx);

                customer.DateOfBirth = dob;

                await ctx.SaveChangesAsync();
            }
        }

        protected override IQueryable<Customer> OrderBy(IQueryable<Customer> source,
            string orderBy = null,
            bool isAscending = false)
        {
            if (string.IsNullOrEmpty(orderBy))
            {
                orderBy = "registeredat";
            }

            switch (orderBy.ToLower())
            {
                case "username":
                    return isAscending ? source.OrderBy(c => c.Username) :
                        source.OrderByDescending(c => c.Username);

                case "name":
                    return isAscending ? source.OrderBy(c => c.Name) :
                        source.OrderByDescending(c => c.Name);

                case "registeredat":
                    return isAscending ? source.OrderBy(c => c.CreatedAt) :
                        source.OrderByDescending(c => c.CreatedAt);

                case "depositcount":
                    return isAscending ? source.OrderBy(c => c.Wallet.TotalDepositCount) :
                        source.OrderByDescending(c => c.Wallet.TotalDepositCount);

                case "depositamount":
                    return isAscending ? source.OrderBy(c => c.Wallet.TotalDepositAmount) :
                        source.OrderByDescending(c => c.Wallet.TotalDepositAmount);

                case "withdrawalcount":
                    return isAscending ? source.OrderBy(c => c.Wallet.TotalWithdrawCount) :
                        source.OrderByDescending(c => c.Wallet.TotalWithdrawCount);

                case "withdrawalamount":
                    return isAscending ? source.OrderBy(c => c.Wallet.TotalWithdrawAmount) :
                        source.OrderByDescending(c => c.Wallet.TotalWithdrawAmount);

                case "totalprofit":
                    return isAscending ? source.OrderBy(c => c.Wallet.TotalWithdrawAmount - c.Wallet.TotalDepositAmount) :
                        source.OrderByDescending(c => c.Wallet.TotalWithdrawAmount - c.Wallet.TotalDepositAmount);

                case "totalbonus":
                    return isAscending ? source.OrderBy(c => c.Wallet.TotalBonusAmount) :
                        source.OrderByDescending(c => c.Wallet.TotalBonusAmount);

                case "totalrakeback":
                    return isAscending ? source.OrderBy(c => c.Wallet.TotalRakebackAmount) :
                        source.OrderByDescending(c => c.Wallet.TotalRakebackAmount);

                case "firstdeposit":
                    return isAscending ? source.OrderBy(c => c.Wallet.FirstDepositAt) :
                        source.OrderByDescending(c => c.Wallet.FirstDepositAt);

                default:
                    return source.OrderByDescending(c => c.CreatedAt);
            }
        }

        private void CheckLoginIdentity(Customer customer)
        {
            if (customer == null)
            {
                throw new BusinessException(Constants.Messages.InvalidLogin, "1");
            }

            if (customer.Status != AccountStatus.Normal)
            {
                throw new BusinessException(Constants.Messages.InvalidAccountStatus, "1");
            }
        }

        private async Task AddBankCard(BankCard bankCard, string username)
        {
            using (var ctx = new GWContext())
            {
                await VerifyBankCardBeforeAdd(ctx, bankCard);

                var customer = await EnsureEntity<Customer>(username, ctx);

                bankCard.AccountName = customer.Name;
                bankCard.WalletId = customer.WalletId;
                bankCard.CreatedAt = DateTime.Now;
                bankCard.Status = EntityStatus.Enabled;

                ctx.BankCards.Add(bankCard);

                await ctx.SaveChangesAsync();
            }
        }

        private async Task VerifyBankCardBeforeAdd(GWContext ctx, BankCard bankCard)
        {
            await new BankCardService().Validate(bankCard.CardNumber);

            if (await ctx.BankCards.AnyAsync(c => c.CardNumber == bankCard.CardNumber && c.Status == EntityStatus.Enabled))
            {
                throw new BusinessException(string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "银行卡"));
            }
        }

        #region Wap
        public async Task<string> ValidateLoginToken(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new BusinessException(Constants.Messages.InvalidToken, "401");
            }

            using (var ctx = new GWContext())
            {
                var now = DateTime.Now.ToE8();
                var login = await ctx.CustomerLogins.SingleOrDefaultAsync(
                    l => l.Token == token && now <= l.ExpiresAt);
                if (login == null)
                {
                    throw new BusinessException(Constants.Messages.InvalidToken, "401");
                }

                return login.Username;
            }
        }
        #endregion
    }
}