﻿using GW.PC.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class ProvinceService : EntityService<Province>
    {
        public async Task<IEnumerable<City>> GetCities(int provinceId)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Cities.Where(c => c.ProvinceId == provinceId).ToListAsync();
            }
        }

        public async Task<IEnumerable<Province>> GetProvinces()
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Provinces.ToListAsync();
            }
        }

        public async Task<Province> GetProvince(string code)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Provinces.SingleOrDefaultAsync(p => p.Code == code);
            }
        }

        public async Task<City> GetCity(string code)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Cities.Include(c => c.Province)
                    .SingleOrDefaultAsync(c => c.Code == code);
            }
        }

        public async Task<IEnumerable<City>> GetCities(string provinceCode)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Cities.Where(c => c.Province.Code == provinceCode).ToListAsync();
            }
        }
    }
}
