﻿using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Models.Infrastructure;
using GW.PC.Services.EF.Gaming;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.PC.Services.EF
{
    public class AgentService : CreatableEntityService<Agent>
    {
        public async Task<Agent> Login(string username, string password, string safeCode = null, bool clean = true, params string[] includes)
        {
            using (var ctx = new GWContext())
            {
                IQueryable<Agent> query = ctx.Agents;

                foreach (var include in includes)
                {
                    query = query.Include(include);
                }

                Agent agent = null;
                if (string.IsNullOrEmpty(safeCode))
                {
                    agent = await query.SingleOrDefaultAsync(c => c.Username == username);
                }
                else
                {
                    agent = await query.SingleOrDefaultAsync(c => c.Username == username && c.SafeCode == safeCode);
                }

                if (agent == null)
                {
                    throw new BusinessException(Constants.Messages.UsernameNotFound);
                }

                if (agent.Status != AccountStatus.Normal)
                {
                    throw new BusinessException(Constants.Messages.InvalidAccountStatus);
                }

                if (!string.IsNullOrEmpty(agent.Password))
                {
                    if (PasswordUtility.VerifyHash(password, agent.Password))
                    {
                        agent.LastLoginAt = DateTime.Now;

                        await ctx.SaveChangesAsync();

                        if (clean)
                        {
                            UpdateReturnedAgent();
                        }

                        return agent;
                    }
                    else
                    {
                        throw new BusinessException(Constants.Messages.InvalidLogin);
                    }
                }
                else
                {

                    throw new BusinessException(Constants.Messages.InvalidLogin);
                }

                void UpdateReturnedAgent()
                {
                    agent.Password = agent.TempPasswordKey = string.Empty;
                }
            }
        }

        public async Task<string> ValidateLoginToken(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new BusinessException(Constants.Messages.InvalidToken);
            }

            using (var ctx = new GWContext())
            {
                var login = await ctx.AgentLogins.SingleOrDefaultAsync(l => l.Token == token);
                if (login == null)
                {
                    throw new BusinessException(Constants.Messages.InvalidToken);
                }

                return login.Username;
            }
        }

        public async Task ToggleStatus(int id, bool enabled, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var agent = await EnsureEntity(id, ctx);

                agent.Status = enabled ? AccountStatus.Normal : AccountStatus.Frozen;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<Agent> GetByUsername(string username, params string[] includes)
        {
            using (var ctx = new GWContext())
            {
                return await EnsureEntity<Agent>(username, ctx, includes);
            }
        }

        public async Task<IEnumerable<Agent>> GetByUsernames(IEnumerable<string> usernames, params string[] includes)
        {
            if ((usernames?.Count() ?? 0) == 0)
            {
                return Enumerable.Empty<Agent>();
            }

            using (var ctx = new GWContext())
            {
                IQueryable<Agent> query = ctx.Set<Agent>();

                if (includes != null)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }

                var result = await query.Where(e => usernames.Contains(e.Username)).ToListAsync();

                return result;
            }
        }

        public async Task<int?> GetIdByPromotionSite(string url)
        {
            using (var ctx = new GWContext())
            {
                var result = await ctx.Agents.Where(
                    a => a.Status == AccountStatus.Normal &&
                    a.PromotionWebsites.Contains(url))
                    .Select(a => new
                    {
                        a.Id,
                        a.PromotionWebsites
                    })
                    .ToListAsync();
                // Further filtering
                result = result.Where(a =>
                {
                    var sites = a.PromotionWebsites.Split('|');
                    if (sites.Any(
                        s => s == url ||
                        $"http://{url}" == s ||
                        $"https://{url}" == s))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                })
                .ToList();

                if (result.Count == 0)
                {
                    return null;
                }
                else if (result.Count == 1)
                {
                    return result.Single().Id;
                }
                else
                {
                    throw new BusinessException($"推广链接重复{url}");
                }
            }
        }

        public async Task SaveGames(string username, string gameSettings)
        {
            using (var ctx = new GWContext())
            {
                var agent = await EnsureEntity<Agent>(username, ctx);

                agent.GameSettings = gameSettings;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task Audit(int id, bool result, int auditedById)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var agent = await EnsureEntity(id, ctx);
                    if (agent.Status != AccountStatus.AwaitApproval)
                    {
                        throw new BusinessException(string.Format(
                            Constants.MessageTemplates.InvalidCurrentStatus,
                            agent.Status.GetDisplayName()));
                    }

                    if (result)
                    {
                        agent.PromotionCode = RandomGenerator.RandomNumber(6);
                        agent.Status = AccountStatus.Normal;
                    }
                    else
                    {
                        agent.Status = AccountStatus.Declined;
                    }

                    agent.AuditedById = auditedById;
                    agent.AuditedAt = DateTime.Now.ToE8();

                    await ctx.SaveChangesAsync();

                    // Make sure PromotionCode is unique
                    if (await ctx.Agents.CountAsync(a => a.PromotionCode == agent.PromotionCode) > 1)
                    {
                        agent.PromotionCode = null;
                        agent.Status = AccountStatus.AwaitApproval;
                        agent.AuditedAt = null;
                        agent.AuditedById = null;

                        await ctx.SaveChangesAsync();

                        throw new BusinessException($"Failed generating promotion code: {agent.PromotionCode}");
                    }

                    scope.Complete();
                }
            }
        }

        public async Task<IEnumerable<string>> GetEnabledUsernames()
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Agents.Where(a => a.Status == AccountStatus.Normal)
                    .Select(a => a.Username).ToListAsync();
            }
        }

        public async Task<IEnumerable<BankCard>> GetBankCards(string username, EntityStatus? status = null)
        {
            using (var ctx = new GWContext())
            {
                var agent = await EnsureEntity<Agent>(username, ctx);

                if (status.HasValue)
                {
                    return await ctx.BankCards.Include(c => c.Bank).Where(
                        c => c.AgentId == agent.Id && c.Status == status.Value)
                    .ToListAsync();
                }
                else
                {
                    return await ctx.BankCards.Include(c => c.Bank).Where(c =>
                        c.AgentId == agent.Id).ToListAsync();
                }
            }
        }

        public async Task SaveBankCard(BankCard bc, string agentUsername)
        {
            if (bc.Id == 0)
            {
                using (var ctx = new GWContext())
                {
                    var agent = await EnsureEntity<Agent>(agentUsername, ctx, "BankCards");

                    if (agent.BankCards.Count(c => c.Status == EntityStatus.Enabled) == 3)
                    {
                        throw new BusinessException("最多只能启用3张银行卡");
                    }

                    if (ctx.BankCards.Any(c => c.CardNumber == bc.CardNumber &&
                        c.AccountName == bc.AccountName &&
                        c.Status == EntityStatus.Enabled))
                    {
                        throw new BusinessException("该银行卡已存在");
                    }

                    bc.AgentId = agent.Id;

                    ctx.BankCards.Add(bc);

                    await ctx.SaveChangesAsync();
                }
            }
            else
            {
                using (var ctx = new GWContext())
                {
                    if (ctx.BankCards.Any(c => c.CardNumber == bc.CardNumber &&
                        c.AccountName == bc.AccountName &&
                        c.Status == EntityStatus.Enabled &&
                        c.Id != bc.Id))
                    {
                        throw new BusinessException("该银行卡已存在");
                    }

                    var dbBandCard = await ctx.BankCards.FindAsync(bc.Id);

                    dbBandCard.BankId = bc.BankId;
                    dbBandCard.CityId = bc.CityId;
                    dbBandCard.CardNumber = bc.CardNumber;
                    dbBandCard.RegistrationAddress = bc.RegistrationAddress;

                    dbBandCard.AccountName = bc.AccountName;

                    await ctx.SaveChangesAsync();
                }
            }
        }

        public async Task DisableBankCard(string username, int id)
        {
            using (var ctx = new GWContext())
            {
                var agent = await EnsureEntity<Agent>(username, ctx);
                var bankCard = await ctx.BankCards.SingleOrDefaultAsync(
                    c => c.AgentId == agent.Id &&
                    c.Id == id &&
                    c.Status == EntityStatus.Enabled);

                CheckNull(bankCard);

                bankCard.Status = EntityStatus.Disabled;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Agent>> GetSubordinates(string username)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Agents.Where(a => a.Parent.Username == username)
                    .ToListAsync();
            }
        }

        public async Task<IEnumerable<string>> GetSubordinateUsernames(string username)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Agents.Where(a => a.Parent.Username == username)
                    .Select(a => a.Username)
                    .ToListAsync();
            }
        }

        public async Task ChangePassword(string username, string oldPassword, string newPassword)
        {
            using (var ctx = new GWContext())
            {
                var agent = await EnsureEntity<Agent>(username, ctx);

                if (PasswordUtility.VerifyHash(oldPassword, agent.Password))
                {
                    agent.Password = PasswordUtility.Hash(newPassword);

                    await ctx.SaveChangesAsync();
                }
                else
                {
                    throw new BusinessException(Constants.Messages.InvalidLogin);
                }
            }
        }

        public async Task<int> GetCustomerCount(int agentId)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Customers.CountAsync(c => c.AgentId == agentId);
            }
        }

        public async Task ValidateContactInfo(string username, string qq, string email, string mobile, string weChat)
        {
            if (string.IsNullOrEmpty(qq) &&
                string.IsNullOrEmpty(email) &&
                string.IsNullOrEmpty(mobile) &&
                string.IsNullOrEmpty(weChat))
            {
                return;
            }

            using (var ctx = new GWContext())
            {
                var duplicates = ctx.Agents.Where(a => a.Username != username);

                if (!string.IsNullOrEmpty(qq) && await duplicates.AnyAsync(a => a.QQ == qq))
                {
                    throw new BusinessException(
                        string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "QQ"));
                }

                if (!string.IsNullOrEmpty(email) && await duplicates.AnyAsync(a => a.Email == email))
                {
                    throw new BusinessException(
                        string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "邮箱"));
                }

                if (!string.IsNullOrEmpty(mobile) && await duplicates.AnyAsync(a => a.Mobile == mobile))
                {
                    throw new BusinessException(
                        string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "手机号码"));
                }

                if (!string.IsNullOrEmpty(weChat) && await duplicates.AnyAsync(a => a.WeChat == weChat))
                {
                    throw new BusinessException(
                        string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "微信"));
                }
            }
        }

        public async Task ValidatePromotionWebSites(string username, string promotionWebsite1, string promotionWebsite2, string promotionWebsite3, string promotionWebsite4)
        {
            using (var ctx = new GWContext())
            {
                var promotionWebSites = await ctx.Agents.Where(
                    a => a.Username != username)
                    .Select(a => a.PromotionWebsites)
                    .ToListAsync();

                if (promotionWebSites.Any(s =>
                {
                    if (string.IsNullOrEmpty(s))
                    {
                        return false;
                    }

                    var sites = s.Split('|');

                    return sites.Contains(promotionWebsite1) ||
                    sites.Contains(promotionWebsite2) ||
                    sites.Contains(promotionWebsite3) ||
                    sites.Contains(promotionWebsite4);
                }))
                {
                    throw new BusinessException(
                        string.Format(Constants.MessageTemplates.ObjectAlreadyExists, "推广链接"));
                }
            }
        }

        public async Task<int> GetNewRegisterCustomerCount(int agentId)
        {
            var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            using (var ctx = new GWContext())
            {
                return await ctx.Customers.CountAsync(c => c.AgentId == agentId && (c.CreatedAt >= firstDayOfMonth));
            }
        }

        public async Task<IEnumerable<Customer>> GetCustomers(int agentId, string username)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Customers.Where(c => c.AgentId == agentId && c.Username.Contains(username)).ToListAsync();
            }
        }

        public async Task VerifySecurityCode(string safeCode)
        {
            using (var ctx = new GWContext())
            {
                var agentCount = await ctx.Agents.Where(a => a.SafeCode == safeCode).CountAsync();

                if (agentCount < 1)
                {
                    throw new BusinessException("安全码错误");
                }
            }
        }

        public async Task<IEnumerable<Wallet>> GetCustomerBusinessData(DateTime periodStart, DateTime periodEnd, IEnumerable<string> usernames, int agentId = 0)
        {
            using (var ctx = new GWContext())
            {
                var deposits =
                    ((
                    from alipay in ctx.AlipayDeposits
                    where ((alipay.CompletedAt >= periodStart && alipay.CompletedAt <= periodEnd)
                     && (alipay.Status == DepositStatus.AutoSuccess || alipay.Status == DepositStatus.ManualSuccess))
                    group new { alipay } by new { alipay.Username } into g
                    select new AgentCustomerAssetsDto { Username = g.Key.Username, Amount = g.Sum(x => x.alipay == null ? 0 : (decimal)x.alipay.ActualAmount) }
                    ).Concat(
                        from onlineBanking in ctx.OnlineBankingDeposits
                        where ((onlineBanking.CompletedAt >= periodStart && onlineBanking.CompletedAt <= periodEnd)
                         && (onlineBanking.Status == DepositStatus.AutoSuccess || onlineBanking.Status == DepositStatus.ManualSuccess))
                        group new { onlineBanking } by new { onlineBanking.Username } into g
                        select new AgentCustomerAssetsDto { Username = g.Key.Username, Amount = g.Sum(x => x.onlineBanking == null ? 0 : (decimal)x.onlineBanking.ActualAmount) }
                        ).Concat(
                        from onlinePayment in ctx.OnlinePaymentDeposits
                        where ((onlinePayment.CompletedAt >= periodStart && onlinePayment.CompletedAt <= periodEnd)
                         && (onlinePayment.Status == DepositStatus.AutoSuccess || onlinePayment.Status == DepositStatus.ManualSuccess))
                        group new { onlinePayment } by new { onlinePayment.Username } into g
                        select new AgentCustomerAssetsDto { Username = g.Key.Username, Amount = g.Sum(x => x.onlinePayment == null ? 0 : x.onlinePayment.RequestedAmount) }
                        ).Concat(
                        from onlineToBankCard in ctx.OnlineToBankCardDeposits
                        where ((onlineToBankCard.CompletedAt >= periodStart && onlineToBankCard.CompletedAt <= periodEnd)
                         && (onlineToBankCard.Status == DepositStatus.AutoSuccess || onlineToBankCard.Status == DepositStatus.ManualSuccess))
                        group new { onlineToBankCard } by new { onlineToBankCard.Username } into g
                        select new AgentCustomerAssetsDto { Username = g.Key.Username, Amount = g.Sum(x => x.onlineToBankCard == null ? 0 : (decimal)x.onlineToBankCard.ActualAmount) }
                        )
                        .Concat(
                        from prepaidCard in ctx.PrepaidCardDeposits
                        where ((prepaidCard.CompletedAt >= periodStart && prepaidCard.CompletedAt <= periodEnd)
                         && (prepaidCard.Status == DepositStatus.AutoSuccess || prepaidCard.Status == DepositStatus.ManualSuccess))
                        group new { prepaidCard } by new { prepaidCard.Username } into g
                        select new AgentCustomerAssetsDto { Username = g.Key.Username, Amount = g.Sum(x => x.prepaidCard == null ? 0 : x.prepaidCard.RequestedAmount) }
                        )
                        .Concat(
                        from qqWallet in ctx.QQWalletDeposits
                        where ((qqWallet.CompletedAt >= periodStart && qqWallet.CompletedAt <= periodEnd)
                         && (qqWallet.Status == DepositStatus.AutoSuccess || qqWallet.Status == DepositStatus.ManualSuccess))
                        group new { qqWallet } by new { qqWallet.Username } into g
                        select new AgentCustomerAssetsDto { Username = g.Key.Username, Amount = g.Sum(x => x.qqWallet == null ? 0 : (decimal)x.qqWallet.ActualAmount) }
                        ).Concat(
                        from quickPay in ctx.QuickPayDeposits
                        where ((quickPay.CompletedAt >= periodStart && quickPay.CompletedAt <= periodEnd)
                         && (quickPay.Status == DepositStatus.AutoSuccess || quickPay.Status == DepositStatus.ManualSuccess))
                        group new { quickPay } by new { quickPay.Username } into g
                        select new AgentCustomerAssetsDto { Username = g.Key.Username, Amount = g.Sum(x => x.quickPay == null ? 0 : (decimal)x.quickPay.ActualAmount) }
                        ).Concat(
                        from weChat in ctx.WeChatDeposits
                        where ((weChat.CompletedAt >= periodStart && weChat.CompletedAt <= periodEnd)
                         && (weChat.Status == DepositStatus.AutoSuccess || weChat.Status == DepositStatus.ManualSuccess))
                        group new { weChat } by new { weChat.Username } into g
                        select new AgentCustomerAssetsDto { Username = g.Key.Username, Amount = g.Sum(x => x.weChat == null ? 0 : (decimal)x.weChat.ActualAmount) }
                        ).Concat(
                        from jdWallet in ctx.JDWalletDeposits
                        where ((jdWallet.CompletedAt >= periodStart && jdWallet.CompletedAt <= periodEnd)
                         && (jdWallet.Status == DepositStatus.AutoSuccess || jdWallet.Status == DepositStatus.ManualSuccess))
                        group new { jdWallet } by new { jdWallet.Username } into g
                        select new AgentCustomerAssetsDto { Username = g.Key.Username, Amount = g.Sum(x => x.jdWallet == null ? 0 : (decimal)x.jdWallet.ActualAmount) }

                        )).GroupBy(g => g.Username).Select(x => new AgentCustomerAssetsDto
                        {
                            Username = x.Key,
                            Amount = x.Sum(g => g.Amount)
                        });

                var withdrawals = from withdrawal in ctx.CustomerWithdrawals
                                  where ((withdrawal.PaidAt >= periodStart && withdrawal.PaidAt <= periodEnd)
                                  && (withdrawal.Status == WithdrawalStatus.AutoPaymentSuccessful || withdrawal.Status == WithdrawalStatus.ManualConfirmed))
                                  group new { withdrawal } by new { withdrawal.Username } into g
                                  select new AgentCustomerAssetsDto { Username = g.Key.Username, Amount = g.Sum(x => x.withdrawal == null ? 0 : x.withdrawal.Amount) }
                                  ;

                var bonuses = from bonus in ctx.Bonus
                              where ((bonus.AuditedAt >= periodStart && bonus.AuditedAt <= periodEnd)
                              && (bonus.Status == AuditStatus.Approved))
                              group new { bonus } by new { bonus.Username } into g
                              select new AgentCustomerAssetsDto { Username = g.Key.Username, Amount = g.Sum(x => x.bonus == null ? 0 : x.bonus.BonusAmount) }
                            ;

                var data = from cust in ctx.Customers
                           join deposit in deposits on cust.Username equals deposit.Username into a
                           from deposit in a.DefaultIfEmpty()
                           join withdrawal in withdrawals on cust.Username equals withdrawal.Username into b
                           from withdrawal in b.DefaultIfEmpty()
                           join bonus in bonuses on cust.Username equals bonus.Username into c
                           from bonus in c.DefaultIfEmpty()
                           where (usernames.Any() ? usernames.Contains(cust.Username) : true)
                           && (agentId > 0 ? cust.AgentId == agentId : true)
                           select new AgentCustomerBusinessDto
                           {
                               Username = cust.Username,
                               TotalDepositAmount = deposit == null ? 0 : deposit.Amount,
                               TotalWithdrawalAmount = withdrawal == null ? 0 : withdrawal.Amount,
                               TotalBonusAmount = bonus == null ? 0 : bonus.Amount
                           };

                var transfer = await data.ToListAsync();

                var result = transfer.Select(x => new Wallet
                {
                    Username = x.Username,
                    TotalDepositAmount = x.TotalDepositAmount,
                    TotalWithdrawAmount = x.TotalWithdrawalAmount,
                    TotalBonusAmount = x.TotalBonusAmount
                }).ToList();

                return result;
            }
        }

        public async Task<IEnumerable<AgentAggregateTransaction>> GetAgentAggregateTransactions(string username, WalletTransactionType type, int pageNum, int pageSize, DateTime? dtStart = null, DateTime? dtEnd = null)
        {
            using (var ctx = new GWContext())
            {
                var agentWithdrawals = from w in ctx.AgentWithdrawals.Include("PayeeBank")
                                       where w.Username == username
                                       join wt in ctx.WalletTransactions.Where(x => x.Type == WalletTransactionType.AgentWithdrawal)
                                       on new { Username = w.Username, Date = w.PaidAt } equals new { Username = wt.Username, Date = (DateTime?)wt.CreatedAt } into temp
                                       from wtList in temp.DefaultIfEmpty()
                                       select new AgentAggregateTransaction
                                       {
                                           TransactionType = WalletTransactionType.AgentWithdrawal,
                                           PayeeName = w.PayeeName,
                                           PayeeBankName = w.PayeeBank.Name,
                                           PayeeCardNumber = w.PayeeCardNumber,
                                           Amount = w.Amount,
                                           Status = w.Status,
                                           Balance = wtList == null ? default(decimal?) : wtList.Balance,
                                           MerchantOrderNumber = w.MerchantOrderNumber,
                                           CreatedAt = w.CreatedAt,
                                           AuditedAt = w.PaidAt,
                                           CommissionDate = null,
                                           DeclineMessage = w.AgentMessage
                                       };

                var agentCommission = from s in ctx.AgentSettlements.Where(x => (dtStart == null && dtEnd == null) ? true : x.PeriodStartsAt == dtStart && x.PeriodEndsAt == dtEnd)
                                      where s.Username == username
                                      join wt in ctx.WalletTransactions.Where(x => x.Type == WalletTransactionType.AgentSettlement)
                                      on new { Username = s.Username, Date = s.AuditedAt } equals new { Username = wt.Username, Date = (DateTime?)wt.CreatedAt } into temp
                                      from wtList in temp.DefaultIfEmpty()
                                      select new AgentAggregateTransaction
                                      {
                                          TransactionType = WalletTransactionType.AgentSettlement,
                                          PayeeName = "",
                                          PayeeBankName = "",
                                          PayeeCardNumber = "",
                                          Amount = s.DirectCommission + s.SubordinateCommissions,
                                          Status = s.Status == AgentSettlementStatus.Approved ? WithdrawalStatus.AutoPaymentSuccessful : WithdrawalStatus.AwaitingApproval,
                                          Balance = wtList == null ? default(decimal?) : wtList.Balance,
                                          MerchantOrderNumber = "",
                                          CreatedAt = s.CreatedAt,
                                          AuditedAt = s.AuditedAt,
                                          CommissionDate = s.PeriodStartsAt,
                                          DeclineMessage = s.Notes
                                      };

                switch (type)
                {
                    case WalletTransactionType.AgentWithdrawal:
                        return await agentWithdrawals.OrderByDescending(x => x.CreatedAt).Skip(pageNum * pageSize).Take(pageSize).ToListAsync();
                    case WalletTransactionType.AgentSettlement:
                        return await agentCommission.OrderByDescending(x => x.CreatedAt).Skip(pageNum * pageSize).Take(pageSize).ToListAsync();
                    default:
                        return await agentCommission.Concat(agentWithdrawals).OrderByDescending(x => x.CreatedAt).Skip(pageNum * pageSize).Take(pageSize).ToListAsync();
                }
            }
        }

        public async Task<string> GetTokenByMobile(string mobile)
        {
            using (var ctx = new GWContext())
            {
                var agents = await ctx.Agents.Where(x => x.Mobile == mobile).ToListAsync();

                if (agents.Count > 1)
                {
                    throw new BusinessException("重复的手机号码，请联络客服协助处理");
                }

                var agent = agents.SingleOrDefault();

                return await ctx.AgentLogins.Where(x => x.Username == agent.Username).Select(x => x.Token).SingleOrDefaultAsync();
            }
        }

        public async Task ResetPassword(string username, string newPassword)
        {
            using (var ctx = new GWContext())
            {
                var agent = await EnsureEntity<Agent>(username, ctx);

                agent.Password = PasswordUtility.Hash(newPassword);

                await ctx.SaveChangesAsync();
            }
        }

        public async Task ChangeSafeCode(string username, string oldSafeCode, string newSafeCode)
        {
            using (var ctx = new GWContext())
            {
                var agent = await EnsureEntity<Agent>(username, ctx);

                await VerifySecurityCode(oldSafeCode);

                agent.SafeCode = newSafeCode;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<decimal> GetNetProfitLastMonth(string username)
        {
            var now = DateTime.Now;
            var periodStart = new DateTime(now.Year, now.Month, 1).AddMonths(-1);
            var periodEnd = new DateTime(now.Year, now.Month, 1).AddSeconds(-1);

            Expression<Func<AgentSettlement, bool>> query =
                q => q.Username == username &&
                q.PeriodStartsAt >= periodStart &&
                q.PeriodEndsAt <= periodEnd &&
                q.Status == AgentSettlementStatus.Approved;

            var agentSettlement = await new AgentSettlementService().SearchAll(new[] { query });

            if (agentSettlement.SingleOrDefault() == null)
            {
                return 0;
            }
            else
            {
                var agentSettlementId = agentSettlement.SingleOrDefault().Id;
                var direct = await new DirectAgentSettlementService().Get(agentSettlementId);
                var directBaseAmount = (direct == null ? 0 : direct.BaseAmount);

                Expression<Func<SubordinateAgentSettlement, bool>> querySub = null;
                querySub = q => q.SettlementId == agentSettlementId;
                var sub = (await new SubordinateAgentSettlementService().SearchAll(new[] { querySub })).ToList();
                var subBaseAmount = sub.Sum(s => s.BaseAmount);

                return directBaseAmount + subBaseAmount;
            }
        }

        public async Task<decimal> GetAgentDynamicPercentage(decimal dynamicPercentage)
        {
            var config = await SystemConfigurationService.Get();

            var stages = config.Agent.DynamicPercentage.Stages;
            var percentages = config.Agent.DynamicPercentage.Percentages;
            decimal stageOne = stages.One, stageTwo = stages.Two, stageThree = stages.Three, stageFour = stages.Four,
                levelOne = percentages.One, levelTwo = percentages.Two, levelThree = percentages.Three, levelFour = percentages.Four, levelFive = percentages.Five;

            var conditions = new Dictionary<Predicate<decimal>, decimal>()
                {
                    { (decimal temp)=>{ return temp < stageOne; }, levelOne },
                    { (decimal temp)=>{ return stageOne <= temp && temp < stageTwo; }, levelTwo },
                    { (decimal temp)=>{ return stageTwo <= temp && temp < stageThree; }, levelThree },
                    { (decimal temp)=>{ return stageThree <= temp && temp < stageFour; }, levelFour },
                    { (decimal temp)=>{ return stageFour <= temp; }, levelFive }
                };
            return ((conditions.Where(x => x.Key(dynamicPercentage)).FirstOrDefault().Value) / 100);
        }

        public async Task<BankCard> GetBankCard(string username, string cardNumber)
        {
            using (var ctx = new GWContext())
            {
                var agent = await EnsureEntity<Agent>(username, ctx);

                return await ctx.BankCards.SingleOrDefaultAsync(
                    c => c.AgentId == agent.Id &&
                    c.CardNumber == cardNumber);
            }
        }

        public async Task RegisterFromCustomer(string username, string qq, string introduction = null, string website = null)
        {
            using (var ctx = new GWContext())
            {
                var existing = await ctx.Agents.SingleOrDefaultAsync(a => a.Username == username);
                if (existing != null)
                {
                    if (existing.Status == AccountStatus.Normal || existing.Status == AccountStatus.Frozen)
                    {
                        throw new BusinessException("您已经是代理，代理专员qq 2946583990", "3");
                    }
                    else if (existing.Status == AccountStatus.Declined)
                    {
                        throw new BusinessException("您的申请已经被拒绝，期待下一次与您合作，如有疑问您联系在线客服", "2");
                    }
                    else if (existing.Status == AccountStatus.AwaitApproval)
                    {
                        throw new BusinessException("您的申请已经提交，等待代理专员联系您，如有疑问请咨询客服", "1");
                    }
                }

                if (string.IsNullOrEmpty(qq))
                {
                    throw new BusinessException("请填写QQ", "1");
                }

                var config = await SystemConfigurationService.Get();
                var game = await new GameService().GetByName(Constants.GameNames.ES);
                var customer = await EnsureEntity<Customer>(username, ctx);
                var agent = new Agent
                {
                    CreatedAt = DateTime.Now.ToE8(),
                    AgentPercentage = config.Agent.DefaultPercentage,
                    GameSettings = JsonConvert.SerializeObject(new[]
                    {
                        new AgentGameSettingItem
                        {
                            GameId = game.Id,
                            SettlementType = AgentSettlementType.ProfitAmount,
                            SettlementPercentage = config.Agent.DefaultSettlementPercentage
                        }
                    }),
                    Name = customer.Name,
                    Username = customer.Username,
                    Password = PasswordUtility.Hash(PasswordUtility.Decrypt(customer.Password)),
                    Status = AccountStatus.AwaitApproval,
                    QQ = qq,
                    Introduction = introduction,
                    Website = website,
                    SafeCode = $"{RandomGenerator.RandomNumber(4)}+{RandomGenerator.RandomNumber(4)}",
                    BankCards = (await ctx.BankCards.Where(
                        c => c.WalletId == customer.WalletId &&
                        c.Status == EntityStatus.Enabled &&
                        c.Verified == true)
                        .ToListAsync())
                        .Select(c => new BankCard
                        {
                            AccountName = c.AccountName,
                            BankId = c.BankId,
                            Verified = true,
                            Status = EntityStatus.Enabled,
                            CardNumber = c.CardNumber,
                            CityId = c.CityId,
                            CreatedAt = DateTime.Now.ToE8(),
                            RegistrationAddress = c.RegistrationAddress
                        })
                        .ToList()
                };

                ctx.Agents.Add(agent);

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<Agent> TryGet(string username)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Agents.SingleOrDefaultAsync(a => a.Username == username);
            }
        }

        private class AgentCustomerBusinessDto
        {
            public string Username { get; set; }
            public decimal TotalDepositAmount { get; set; }
            public decimal TotalWithdrawalAmount { get; set; }
            public decimal TotalBonusAmount { get; set; }
        }

        private class AgentCustomerAssetsDto
        {
            public string Username { get; set; }
            public decimal Amount { get; set; }
        }
    }
}
