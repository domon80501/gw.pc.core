﻿using GW.PC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Owin
{
    public class RoleService : RoleManager<Role, int>
    {
        public RoleService(IRoleStore<Role, int> store) : base(store)
        {

        }

        public static RoleService Create(IdentityFactoryOptions<RoleService> options, IOwinContext context)
        {
            return new RoleService(new RoleStore<Role, int, UserRole>(context.Get<PCContext>()));
        }

        public async Task<IEnumerable<Role>> GetRoles(string roleName, EntityStatus? status)
        {
            var result = Roles;

            if (!string.IsNullOrEmpty(roleName))
            {
                result = result.Where(o => o.Name.Contains(roleName));
            }

            if (status.HasValue)
            {
                result = result.Where(o => o.Status == status.Value);
            }

            return await result.ToListAsync();
        }

        public virtual async Task<Role> Find(int id, params string[] includes)
        {
            if (id <= 0)
            {
                return null;
            }

            using (var ctx = new PCContext())
            {
                IQueryable<Role> source = ctx.Set<Role>();

                foreach (var include in includes)
                {
                    source = source.Include(include);
                }

                return await source.SingleOrDefaultAsync(o => o.Id == id);
            }
        }






        public virtual async Task<Role> Update2(Role item)
        {
            using (var ctx = new PCContext())
            {
                ctx.Entry(item).State = EntityState.Modified;

                await ctx.SaveChangesAsync();

                return item;
            }
        }


        public async Task Save(Role role, List<int> permissionIds)
        {
            using (var ctx = new PCContext())
            {
                //if (role.Id == 0)
                //{
                //    await CheckUniqueness(r => r.Name.Equals(role.Name, StringComparison.OrdinalIgnoreCase));

                //    if (!string.IsNullOrEmpty(permissionIds))
                //    {
                //        var ids = permissionIds.Split(';').Select(s => Convert.ToInt32(s));
                //        role.Permissions = await ctx.Permissions.Where(
                //            p => ids.Contains(p.Id)).ToListAsync();
                //    }

                //    ctx.Roles.Add(role);
                //}
                //else
                //{
                // var dbRole = await EnsureEntity<Role>(role.Id, ctx, "Permissions");



                var dbRole = await Find(role.Id, "Permissions", "CreatedBy");

                dbRole.Name = "ADMIN444";
                dbRole.Permissions.Clear();



                foreach (var id in permissionIds)
                {
                    var pm = ctx.Permissions.Find(id);
                    dbRole.Permissions.Add(pm);
                }

                

                ctx.Entry(dbRole).State = EntityState.Modified;

                await ctx.SaveChangesAsync();


             //   if (string.IsNullOrEmpty(permissionIds))
             //       {
             //           dbRole.Permissions.Clear();
             //       }
             //       else
             //       {
             //           var dbPermissionIds = dbRole.Permissions.Select(p => p.Id).ToList();
             //           var newPermissionIds = permissionIds.Split(';').Select(s => Convert.ToInt32(s)).Except(dbPermissionIds);
             //           foreach (var id in newPermissionIds)
             //           {
             //               dbRole.Permissions.Add(ctx.Permissions.Find(id));
             //           }
             //           var deletedPermissionIds = dbPermissionIds.Except(
             //               permissionIds.Split(';').Select(s => Convert.ToInt32(s))).ToList();
             //           foreach (var id in deletedPermissionIds)
             //           {
             //               dbRole.Permissions.Remove(dbRole.Permissions.Single(p => p.Id == id));
             //           }
             //       }
             ////   }

             //   await ctx.SaveChangesAsync();
            }
        }


    }
}
