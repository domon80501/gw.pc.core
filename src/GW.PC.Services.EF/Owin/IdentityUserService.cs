﻿using GW.PC.Core;
using GW.PC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Owin
{
    public class IdentityUserService : UserManager<AdminIdentityUser>
    {
        public IdentityUserService(IUserStore<AdminIdentityUser> store) : base(store)
        {
        }

        public static IdentityUserService Create(IdentityFactoryOptions<IdentityUserService> options,
            IOwinContext context)
        {
            var store = new UserStore<AdminIdentityUser>(context.Get<PCContext>());
            var manager = new IdentityUserService(store);

            //    var manager = new UserStore<AdminIdentityUser>(context.Get<PCContext>());

            //    //// Configure validation logic for usernames
            //    //manager.UserValidator = new UserValidator<AdminIdentityUser>(manager)
            //    //{
            //    //    AllowOnlyAlphanumericUserNames = false,
            //    //    RequireUniqueEmail = true
            //    //};

            //    //var dataProtectionProvider = options.DataProtectionProvider;
            //    //if (dataProtectionProvider != null)
            //    //{
            //    //    manager.UserTokenProvider = new DataProtectorTokenProvider<AdminIdentityUser>
            //    //                                (dataProtectionProvider.Create("ASP.NET Identity"));
            //    //}
            return manager;
        }


        
    }

}
