﻿using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.PC.Services.EF
{
    public class GameTransactionService : EntityService<GameTransaction>
    {
        public override async Task AddRange(IEnumerable<GameTransaction> items)
        {
            using (var ctx = new GWContext())
            {
                ctx.GameTransactions.AddRange(items);

                ctx.WalletTransactions.AddRange(items.Select(t => new WalletTransaction
                {
                    Amount = t.Amount,
                    CreatedAt = t.CreatedAt,
                    SourceId = t.GameId,
                    Type = GameTransactionTypeToWalletTransactionType(t.Type),
                    Username = t.Username,
                    Balance = t.Balance
                }));

                await ctx.SaveChangesAsync();
            }

            WalletTransactionType GameTransactionTypeToWalletTransactionType(GameTransactionType gtt)
            {
                switch (gtt)
                {
                    case GameTransactionType.Win:
                        return WalletTransactionType.GameWin;
                    case GameTransactionType.BetRefund:
                        return WalletTransactionType.GameBetRefund;
                    case GameTransactionType.Bet:
                        return WalletTransactionType.GameBet;
                    default:
                        throw new NotSupportedException();
                }
            }
        }

        public async Task<GameTransaction> GetByGameId(int gameId)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.GameTransactions.SingleOrDefaultAsync(t => t.GameId == gameId);
            }
        }
    }
}
