﻿using GW.PC.Models;
using System;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class AgentAdjustmentService : CreatableEntityService<AgentAdjustment>
    {
        public override async Task<AgentAdjustment> Add(AgentAdjustment item)
        {
            using (var ctx = new GWContext())
            {
                await EnsureEntity<Agent>(item.Username, ctx);

                ctx.AgentAdjustments.Add(item);

                await ctx.SaveChangesAsync();

                return item;
            }
        }

        public async Task Audit(int id, bool result, int auditedById)
        {
            using (var ctx = new GWContext())
            {
                var aa = await EnsureEntity(id, ctx);

                aa.Status = result ? AuditStatus.Approved : AuditStatus.Declined;
                aa.AuditedAt = DateTime.Now;
                aa.AuditedById = auditedById;

                // Update agent's balance.
                if (result && aa.Type == AgentAdjustmentType.Balance)
                {
                    var agent = await EnsureEntity<Agent>(aa.Username, ctx);
                    agent.Balance += aa.Amount;

                    AddWalletTransaction(ctx, aa.Amount, agent.Balance, WalletTransactionType.AgentAdjustment, aa.Username, id);
                }

                await ctx.SaveChangesAsync();
            }
        }
    }
}
