﻿using GW.PC.Models;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Gaming
{
    public class EAService : GameService
    {
        public async Task<CustomerEA> Validate(string username, string token, string clientIP)
        {
            using (var ctx = new GWContext())
            {
                var customerGame = await ctx.CustomerEAs.SingleOrDefaultAsync(
                    s => s.GameUsername.ToLower() == username.ToLower() &&
                        s.LoginToken == token &&
                        DateTime.Now < s.LoginTokenExpiredAt
                );
                if (customerGame != null)
                {
                    customerGame.ClientIP = clientIP;

                    await ctx.SaveChangesAsync();
                }

                return customerGame;
            }
        }
    }
}
