﻿using GW.PC.Models;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Gaming
{
    public class LTService : GameService
    {
        public async Task<CustomerLT> ValidateToken(string token)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.CustomerLTs.SingleOrDefaultAsync(
                    s => s.LoginToken == token &&
                    DateTime.Now < s.LoginTokenExpiredAt
                );
            }
        }
    }
}
