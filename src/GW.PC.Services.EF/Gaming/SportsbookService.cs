﻿using GW.PC.Models;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Gaming
{
    public class SportsbookService : GameService
    {
        public async Task<CustomerSportsbook> ValidateToken(string username, string token)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.CustomerSportsbooks.SingleOrDefaultAsync(
                    s => s.GameUsername.ToLower() == username.ToLower() &&
                    s.LoginToken == token &&
                    DateTime.Now < s.LoginTokenExpiredAt
                );
            }
        }
    }
}
