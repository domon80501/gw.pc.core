﻿using GW.PC.Models;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Gaming
{
    public class LBService : GameService
    {
        public async Task<CustomerLB> ValidateToken(string token)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.CustomerLBs.SingleOrDefaultAsync(
                    s => s.LoginToken == token &&
                    DateTime.Now < s.LoginTokenExpiredAt
                );
            }
        }
    }
}
