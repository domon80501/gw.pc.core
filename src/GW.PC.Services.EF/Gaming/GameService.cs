﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace GW.PC.Services.EF.Gaming
{
    public class GameService : StatusfulEntityService<Game>
    {
        public async Task<IEnumerable<Game>> GetAll(bool enabled)
        {
            using (var ctx = new GWContext())
            {
                if (enabled)
                {
                    return await ctx.Games.Where(g => g.Status == EntityStatus.Enabled).ToListAsync();
                }
                else
                {
                    return await ctx.Games.ToListAsync();
                }
            }
        }

        public async Task<IEnumerable<Game>> GetRakebackGames()
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Games.Where(
                    g => g.Status == EntityStatus.Enabled &&
                    g.RakebackAt.HasValue)
                    .AsNoTracking().ToListAsync();
            }
        }

        public async Task<Game> GetByName(string gameName)
        {
            if (string.IsNullOrWhiteSpace(gameName))
            {
                return null;
            }

            using (var ctx = new GWContext())
            {
                return await ctx.Games.SingleOrDefaultAsync(
                    g => g.Name.ToLower() == gameName.ToLower());
            }
        }

        public async Task<Game> GetByCode(string gameCode)
        {
            if (string.IsNullOrWhiteSpace(gameCode))
            {
                return null;
            }

            using (var ctx = new GWContext())
            {
                return await ctx.Games.SingleOrDefaultAsync(
                    g => g.Code == gameCode);
            }
        }

        public async Task<T> GetCustomerGame<T>(string username)
            where T : CustomerGameBase
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                return null;
            }

            using (var ctx = new GWContext())
            {
                return await ctx.Set<T>().SingleOrDefaultAsync(c => c.Username == username);
            }
        }

        public async Task<IEnumerable<CustomerGameBase>> GetCustomerGames(IEnumerable<string> gameUsernames, string gameName)
        {
            using (var ctx = new GWContext())
            {
                switch (gameName)
                {
                    case Constants.GameNames.AG:
                        return await ctx.CustomerAGs.Where(g => gameUsernames.Contains(g.GameUsername)).ToListAsync();
                    case Constants.GameNames.Bbin:
                        return await ctx.CustomerBbins.Where(g => gameUsernames.Contains(g.GameUsername)).ToListAsync();
                    case Constants.GameNames.EA:
                        return await ctx.CustomerEAs.Where(g => gameUsernames.Contains(g.GameUsername)).ToListAsync();
                    case Constants.GameNames.QT:
                        return await ctx.CustomerQTs.Where(g => gameUsernames.Contains(g.GameUsername)).ToListAsync();
                    case Constants.GameNames.PT:
                        return await ctx.CustomerPTs.Where(g => gameUsernames.Contains(g.GameUsername)).ToListAsync();
                    case Constants.GameNames.LB:
                        return await ctx.CustomerLBs.Where(g => gameUsernames.Contains(g.GameUsername)).ToListAsync();
                    case Constants.GameNames.KG:
                        return await ctx.CustomerKGs.Where(g => gameUsernames.Contains(g.GameUsername)).ToListAsync();
                    case Constants.GameNames.Sportsbook:
                        return await ctx.CustomerSportsbooks.Where(g => gameUsernames.Contains(g.GameUsername)).ToListAsync();
                    case Constants.GameNames.ES:
                        return await ctx.CustomerESs.Where(g => gameUsernames.Contains(g.GameUsername)).ToListAsync();
                    default:
                        throw new ArgumentOutOfRangeException(nameof(gameName));
                }
            }
        }

        public async Task UpdateCustomerGame<T>(T item)
            where T : CustomerGameBase
        {
            using (var ctx = new GWContext())
            {
                ctx.Entry(item).State = EntityState.Modified;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task AddCustomerGame<T>(string username, T item)
            where T : CustomerGameBase
        {
            using (var ctx = new GWContext())
            {
                ctx.Entry(item).State = EntityState.Added;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task Save(Game game)
        {
            if (game.Id == 0)
            {
                await base.Add(game);
            }
            else
            {
                using (var ctx = new GWContext())
                {
                    var dbGame = await ctx.Games.FindAsync(game.Id);

                    dbGame.Arguments = game.Arguments;
                    dbGame.Code = game.Code;
                    dbGame.Name = game.Name;
                    dbGame.Status = game.Status;
                    dbGame.UsernamePrefix = game.UsernamePrefix;

                    await ctx.SaveChangesAsync();
                }
            }
        }

        public async Task ToggleAllowTransfer(int id, bool enabled, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var entity = await EnsureEntity(id, ctx);

                entity.AllowTransfer = enabled;

                await ctx.SaveChangesAsync();
            }
        }
    }
}
