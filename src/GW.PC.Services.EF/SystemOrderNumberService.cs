﻿using GW.PC.Core;
using GW.PC.Models;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class SystemOrderNumberService : EntityService<SystemOrderNumber>
    {
        public static async Task<string> Next()
        {
            var ctx = new PCContext();

            try
            {
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        var number = new SystemOrderNumber
                        {
                            Value = RandomGenerator.NewSystemOrderNumber()
                        };

                        ctx.SystemOrderNumbers.Add(number);

                        await ctx.SaveChangesAsync();

                        return number.Value;
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
            finally
            {
                ctx.Dispose();
            }

            throw new BusinessException("无法生成系统订单号");
        }
    }
}
