﻿using GW.PC.Models;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class CustomerDepositPeriodService : EntityService<CustomerDepositPeriod>
    {
        public async Task<CustomerDepositPeriod> GetLast(string username)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.CustomerDepositPeriods
                    .Where(p => p.Username == username)
                    .OrderByDescending(p => p.Id)
                    .FirstOrDefaultAsync();
            }
        }
    }
}
