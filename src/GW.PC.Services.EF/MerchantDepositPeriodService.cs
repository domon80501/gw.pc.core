﻿using GW.PC.Models;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class MerchantDepositPeriodService : EntityService<MerchantDepositPeriod>
    {
        public async Task<MerchantDepositPeriod> GetLast(string username)
        {
            using (var ctx = new APContext())
            {
                return await ctx.MerchantDepositPeriods
                    .Where(p => p.Username == username)
                    .OrderByDescending(p => p.Id)
                    .FirstOrDefaultAsync();
            }
        }
    }
}
