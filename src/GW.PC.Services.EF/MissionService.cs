﻿using System.Data.Entity;
using System.Threading.Tasks;
using GW.PC.Core;
using GW.PC.Models;

namespace GW.PC.Services.EF
{
    public class MissionService : StatusfulEntityService<Mission>
    {
        public async Task Save(Mission mission)
        {
            using (var ctx = new GWContext())
            {
                if (await ctx.Missions.AnyAsync(
                    m => m.Name == mission.Name &&
                    m.Id != mission.Id))
                {
                    throw new BusinessException(Constants.Messages.ObjectDuplicated);
                }

                if (mission.Id == 0)
                {
                    ctx.Missions.Add(mission);
                }
                else
                {
                    var dbMission = await EnsureEntity(mission.Id, ctx);
                    ctx.Entry(dbMission).State = EntityState.Detached;

                    mission.CreatedAt = dbMission.CreatedAt;
                    mission.CreatedById = dbMission.CreatedById;
                    mission.Status = dbMission.Status;

                    ctx.Entry(mission).State = EntityState.Modified;
                }

                await ctx.SaveChangesAsync();
            }
        }
    }
}
