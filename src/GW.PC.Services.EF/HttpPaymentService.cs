﻿using GW.PC.Models;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class HttpPaymentService : EntityService<HttpPayment>
    {
        public async Task<T> GetSource<T>(HttpPayment payment, params string[] includes)
            where T : EntityBase
        {
            using (var ctx = new PCContext())
            {
                var entity = ctx.Set<T>().Where(o => o.Id == payment.Id);

                foreach (var include in includes)
                {
                    entity = entity.Include(include);
                }

                return await entity.SingleOrDefaultAsync();
            }
        }
    }
}
