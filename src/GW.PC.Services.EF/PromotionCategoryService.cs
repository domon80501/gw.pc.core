﻿using System.Data.Entity;
using System.Threading.Tasks;
using GW.PC.Core;
using GW.PC.Models;

namespace GW.PC.Services.EF
{
    public class PromotionCategoryService : CreatableEntityService<PromotionCategory>
    {
        public async Task<PromotionCategory> Save(PromotionCategory category)
        {
            if (category.Id == 0)
            {
                await CheckUniqueness(c => c.Name == category.Name);

                return await base.Add(category);
            }
            else
            {
                using (var ctx = new GWContext())
                {
                    if (await ctx.PromotionCategories.AnyAsync(
                        c => c.Name == category.Name &&
                        c.Id != category.Id))
                    {
                        throw new BusinessException(Constants.Messages.ObjectDuplicated);
                    }

                    var dbCategory = await ctx.PromotionCategories.FindAsync(category.Id);

                    dbCategory.Name = category.Name;
                    dbCategory.SequenceNumber = category.SequenceNumber;

                    await ctx.SaveChangesAsync();

                    return dbCategory;
                }
            }
        }
    }
}
