﻿using GW.PC.Core;
using GW.PC.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class PaymentChannelService : StatusfulEntityService<PaymentChannel>
    {
        public async Task<PaymentChannel> Save(PaymentChannel pc)
        {
            using (var ctx = new PCContext())
            {
                if (pc.Id == 0)
                {
                    var result = ctx.PaymentChannels.Add(pc);
                    await ctx.SaveChangesAsync();
                    return result;
                }
                else
                {
                    var dbPC = await ctx.PaymentChannels.FindAsync(pc.Id);

                    dbPC.CallbackUrl = pc.CallbackUrl;
                    dbPC.MaxAmount = pc.MaxAmount;
                    dbPC.MerchantKey = pc.MerchantKey;
                    dbPC.MerchantUsername = pc.MerchantUsername;
                    dbPC.MinAmount = pc.MinAmount;
                    dbPC.Name = pc.Name;
                    dbPC.PaymentMethod = pc.PaymentMethod;
                    dbPC.PaymentType = pc.PaymentType;
                    dbPC.PaymentPlatform = pc.PaymentPlatform;
                    dbPC.ProviderId = pc.ProviderId;
                    dbPC.RequestUrl = pc.RequestUrl;
                    dbPC.TotalLimit = pc.TotalLimit;
                    dbPC.MinimumWithdrawInterval = pc.MinimumWithdrawInterval;
                    dbPC.Arguments = pc.Arguments;
                    dbPC.AssemblyName = pc.AssemblyName;
                    dbPC.HandlerType = pc.HandlerType;
                    dbPC.CommissionConstants = pc.CommissionConstants;
                    dbPC.CommissionMinValue = pc.CommissionMinValue;
                    dbPC.CommissionPercentage = pc.CommissionPercentage;
                    dbPC.Notes = pc.Notes;
                    dbPC.QueryUrl = pc.QueryUrl;
                    dbPC.MerchantId = pc.MerchantId;
                    dbPC.ExtensionRoute = pc.ExtensionRoute;
                    dbPC.CallbackSuccessType = pc.CallbackSuccessType;

                    await ctx.SaveChangesAsync();
                    return dbPC;
                }
            }
        }

        //public async Task<PaymentChannel> GetEnabled(PaymentMethod method, PaymentPlatform platform)
        //{
        //    using (var ctx = new PCContext())
        //    {
        //        return await ctx.PaymentChannels.SingleOrDefaultAsync(
        //            p => p.PaymentMethod == method &&
        //            p.PaymentPlatform == platform &&
        //            p.PaymentType == PaymentType.Deposit &&
        //            p.Status == EntityStatus.Enabled);
        //    }
        //}

        public override async Task UpdateEntityStatus(int id, EntityStatus status, int operatorId)
        {
            using (var ctx = new PCContext())
            {
                var entity = await EnsureEntity(id, ctx);
                var user = await EnsureEntity<User>(operatorId, ctx);

                entity.Status = status;

                AddUserLog(ctx, user.Username, "修改了渠道状态");

                await ctx.SaveChangesAsync();
            }
        }

        //public async Task<IEnumerable<PaymentProviderBank>> GetEnabledBanks(PaymentType type, PaymentMethod method, PaymentPlatform platform)
        //{
        //    using (var ctx = new PCContext())
        //    {
        //        var channel = await GetEnabled(method, platform);
        //        if (channel == null)
        //        {
        //            throw new Exception("No enabled channel found.");
        //        }

        //        return await ctx.PaymentProviderBanks.Include(ppb => ppb.Bank).Where(
        //            ppb => ppb.PaymentProviderId == channel.ProviderId &&
        //            ppb.Status == EntityStatus.Enabled &&
        //            ppb.PaymentType == type &&
        //            ppb.PaymentMethod == method)
        //            .OrderBy(ppb => ppb.Order)
        //            .ToListAsync();
        //    }
        //}

        public decimal CalculateCommission(PaymentChannel pc, decimal amount)
        {
            if (pc == null)
            {
                throw new ArgumentNullException(nameof(pc));
            }

            var original = Math.Max(
                (pc.CommissionPercentage ?? 0) * amount + (pc.CommissionConstants ?? 0),
                (pc.CommissionMinValue ?? 0));

            if (!string.IsNullOrEmpty(pc.Arguments))
            {
                dynamic args = JObject.Parse(pc.Arguments);
                if (args.CommissionCalculation == "Ceiling")
                {
                    return Math.Ceiling(original * 100) / 100;
                }
            }

            return Math.Round(original, 2, MidpointRounding.AwayFromZero);
        }

        //public async Task<IEnumerable<PaymentChannel>> GetByType(PaymentType type)
        //{
        //    using (var ctx = new PCContext())
        //    {
        //        return await ctx.PaymentChannels.Where(
        //            c => c.PaymentType == type)
        //            .ToListAsync();
        //    }
        //}

        public async Task<PaymentChannel> SelectDepositChannel(
             int merchantId,
             PaymentMethod paymentMethod,
             PaymentPlatform paymentPlatform,
             decimal amount, string bankCode)
        {
            using (var ctx = new PCContext())
            {
                var channels = await ctx.PaymentChannels.Where(
                    c => c.Status == EntityStatus.Enabled &&
                    c.MerchantId == merchantId &&
                    c.PaymentType == PaymentType.Deposit &&
                    (c.PaymentPlatform == paymentPlatform || c.PaymentPlatform == PaymentPlatform.All) &&
                    c.MinAmount <= amount &&
                    c.MaxAmount >= amount &&
                    c.PaymentMethod == paymentMethod)
                    .ToListAsync();

                channels = channels.Where(c => c.Amounts == null || c.Amounts.Any(x => Convert.ToDecimal(x.Keys) == amount)).ToList();

                if (channels.Count == 0)
                {
                    return null;
                }

                if (paymentMethod == PaymentMethod.OnlinePayment || paymentMethod == PaymentMethod.OnlineBanking || paymentMethod == PaymentMethod.PrepaidCard)
                {
                    var ppbs = await new PaymentProviderBankService().Get(
                       bankCode,
                       channels.Select(c => c.ProviderId),
                       PaymentType.Deposit,
                       paymentMethod);

                    channels = channels.Join(ppbs, c => c.ProviderId, ppb => ppb.PaymentProviderId,
                        (c, ppb) => new { c }).Select(r => r.c).ToList();

                    if (channels.Count < 1)
                    {
                        throw new RequestException(paymentMethod == PaymentMethod.PrepaidCard
                            ? Constants.Messages.PaymentProviderCardsInMaintenance : Constants.Messages.PaymentProviderBanksInMaintenance,
                            NoticeType.PaymentCenter);
                    }
                }

                if (channels.Count == 1)
                {
                    return channels.Single();
                }

                return channels.ElementAt(new Random().Next(0, channels.Count));
            }
        }

        public async Task<Tuple<string, string>> GetHandlerConfig(int paymentChannelId)
        {
            using (var ctx = new PCContext())
            {
                var config = await ctx.PaymentChannels.Where(c => c.Id == paymentChannelId)
                    .Select(r => new { r.AssemblyName, r.HandlerType }).SingleOrDefaultAsync();

                return Tuple.Create(config.AssemblyName, config.HandlerType);
            }
        }

        //public async Task<PaymentChannel> Get(int providerId, PaymentType paymentType)
        //{
        //    using (var ctx = new PCContext())
        //    {
        //        var provider = await EnsureEntity<PaymentProvider>(providerId, ctx, "PaymentChannels");
        //        var channels = provider.PaymentChannels.Where(p => p.Status != EntityStatus.Deprecated);
        //        if (channels.Count() == 1 && channels.Single().IsBankCard)
        //        {
        //            return channels.Single();
        //        }

        //        channels = channels.Where(c => c.PaymentType == paymentType).ToList();
        //        if (channels.Count() > 1)
        //        {
        //            throw new BusinessException($"Multiple channels are found with provider {provider.Name} and type {paymentType.GetDisplayName()}");
        //        }
        //        if (channels.Count() == 0)
        //        {
        //            throw new BusinessException($"No channels are found with provider {provider.Name} and type {paymentType.GetDisplayName()}");
        //        }

        //        return channels.Single();
        //    }
        //}

        //public async Task<IEnumerable<Bank>> GetBanksForWithdrawal(PaymentPlatform paymentPlatform)
        //{
        //    using (var ctx = new PCContext())
        //    {
        //        return await ctx.PaymentChannels.Where(
        //            c => c.Status == EntityStatus.Enabled &&
        //            (c.PaymentPlatform == paymentPlatform || c.PaymentPlatform == PaymentPlatform.All) &&
        //            c.PaymentType == PaymentType.Withdrawal)
        //            .Select(c => c.Provider)
        //            .SelectMany(p => p.PaymentProviderBanks)
        //            .Where(ppb => ppb.Status == EntityStatus.Enabled &&
        //            ppb.PaymentType == PaymentType.Withdrawal)
        //            .Select(b => b.Bank)
        //            .Distinct()
        //            .OrderBy(b => b.CreatedAt)
        //            .ToListAsync();
        //    }
        //}

        //public async Task Discard(int id, int operatorId)
        //{
        //    using (var ctx = new PCContext())
        //    {
        //        var pc = await EnsureEntity(id, ctx);

        //        pc.Status = EntityStatus.Deprecated;

        //        await ctx.SaveChangesAsync();
        //    }
        //}

        //protected override IQueryable<PaymentChannel> OrderBy(IQueryable<PaymentChannel> source, string orderBy = null, bool isAscending = false)
        //{
        //    return isAscending ? source.OrderBy(o => o.CreatedAt) :
        //        source.OrderByDescending(o => o.CreatedAt);
        //}

    }
}
