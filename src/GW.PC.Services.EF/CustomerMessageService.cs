﻿using System.Threading.Tasks;
using GW.PC.Models;
using System.Data.Entity;
using System.Linq;
using System;
using GW.PC.Core;
using System.Collections;
using System.Collections.Generic;

namespace GW.PC.Services.EF
{
    public class CustomerMessageService : CreatableEntityService<CustomerMessage>
    {
        public async Task<CustomerMessage> FindReply(int id)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.CustomerMessages.SingleOrDefaultAsync(
                    m => m.ParentId == id);
            }
        }

        public async Task Reply(CustomerMessage reply)
        {
            using (var ctx = new GWContext())
            {
                var message = await ctx.CustomerMessages.FindAsync(reply.ParentId);

                message.Status = CustomerMessageStatus.Replied;

                ctx.Entry(reply).State = reply.Id == 0 ? EntityState.Added : EntityState.Modified;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<CustomerMessage>> GetForCustomer(string username, int lastMessageId)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.CustomerMessages.Where(
                    m => m.ToUsername == username &&
                    m.Id > lastMessageId)
                    .ToListAsync();
            }
        }

        public override async Task<CustomerMessage> Add(CustomerMessage item)
        {
            if (item.Type == CustomerMessageType.CustomerMessage)
            {
                var todayStart = DateTime.Today;
                var todayEnd = todayStart.AddDays(1).AddSeconds(-1);
                using (var ctx = new GWContext())
                {
                    if (await ctx.CustomerMessages.CountAsync(
                        m => m.Type == CustomerMessageType.CustomerMessage &&
                        m.FromUsername == item.FromUsername &&
                        m.CreatedAt >= todayStart &&
                        m.CreatedAt <= todayEnd) >= 3)
                    {
                        throw new BusinessException("今天发送的站内信个数不可以超过3个");
                    }
                }
            }

            return await base.Add(item);
        }

        protected override IQueryable<CustomerMessage> OrderBy(IQueryable<CustomerMessage> source, string orderBy = null, bool isAscending = false)
        {
            switch (orderBy?.ToLower())
            {
                case "createdat":
                    return isAscending ? source.OrderBy(c => c.CreatedAt) :
                        source.OrderByDescending(c => c.CreatedAt);
                default:
                    return base.OrderBy(source, orderBy, isAscending);
            }
        }
    }
}
