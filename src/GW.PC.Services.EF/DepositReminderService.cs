﻿using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF.Payment;
using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.PC.Services.EF.Admin
{
    public class DepositReminderService : CreatableEntityService<DepositReminder>
    {
        public override async Task<DepositReminder> Add(DepositReminder item)
        {
            using (var ctx = new GWContext())
            {
                var deposit = await DepositServiceHelper.GetDeposit(item.PaymentMethod, item.DepositId);

                if (await ctx.DepositReminders.AnyAsync(
                    r => r.PaymentMethod == item.PaymentMethod &&
                    r.DepositId == item.DepositId &&
                    r.Status == DepositReminderStatus.AwaitApproval))
                {
                    throw new BusinessException("你提交的表单正在处理,我们尽快核实!");
                }

                if (deposit.Status != DepositStatus.PendingPayment)
                {
                    throw new BusinessException("你提交的表单不存在或已经处理,请联系我们客服!");
                }

                deposit.Status = DepositStatus.AwaitReminderApproval;

                ctx.DepositReminders.Add(item);
                ctx.Entry(deposit).State = EntityState.Modified;

                await ctx.SaveChangesAsync();

                return item;
            }
        }

        public async Task Audit(int id, bool result, int auditedById, decimal? gameBalance)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var reminder = await EnsureEntity(id, ctx);
                    if (reminder.Status != DepositReminderStatus.AwaitApproval)
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, reminder.Status.GetDisplayName()));
                    }

                    var deposit = await DepositServiceHelper.GetDeposit(reminder.PaymentMethod, reminder.DepositId, "PaymentChannel.Provider");
                    if (deposit.Status != DepositStatus.AwaitReminderApproval)
                    {
                        if (deposit.Status == DepositStatus.AutoSuccess)
                        {
                            reminder.Status = DepositReminderStatus.DepositAutoSucceeded;
                        }
                        else if (deposit.Status == DepositStatus.ManualSuccess)
                        {
                            reminder.Status = DepositReminderStatus.Approved;
                        }
                        else if (deposit.Status == DepositStatus.PaymentFailed)
                        {
                            reminder.Status = DepositReminderStatus.DepositFailed;
                        }

                        reminder.AuditedAt = DateTime.Now.ToE8();
                        reminder.AuditedById = auditedById;

                        await ctx.SaveChangesAsync();

                        return;
                    }

                    if (result)
                    {
                        await DepositServiceHelper.FinalizeDeposit(ctx, deposit, DepositStatus.ManualSuccess, reminder.ActualAmount, gameBalance, auditedById);
                    }
                    else
                    {
                        reminder.Status = DepositReminderStatus.Declined;
                        reminder.AuditedAt = DateTime.Now;
                        reminder.AuditedById = auditedById;

                        deposit.Status = DepositStatus.PendingPayment;
                        ctx.Entry(deposit).State = EntityState.Modified;

                        await ctx.SaveChangesAsync();
                    }
                }

                scope.Complete();
            }
        }
    }
}