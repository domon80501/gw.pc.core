﻿using GW.PC.Models;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class CustomerGradeService : EntityService<CustomerGrade>
    {
        public async Task<CustomerGrade> Get(int level)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.CustomerGrades.SingleOrDefaultAsync(g => g.Level == level);
            }
        }
    }
}
