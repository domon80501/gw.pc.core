﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.PC.Services.EF
{
    public class BonusService : CreatableEntityService<Bonus>
    {
        public override async Task<Bonus> Add(Bonus bonus)
        {
            using (var ctx = new GWContext())
            {
                var customer = await EnsureEntity<Customer>(bonus.Username, ctx);

                if (customer.AllowPromotion == false)
                {
                    throw new BusinessException("客户不允许红利");
                }

                bonus.Username = customer.Username;
                bonus.SystemOrderNumber = await SystemOrderNumberService.Next();

                return await base.Add(bonus);
            }
        }

        public async Task Audit(int id, bool result, decimal? gameBalance, int? auditedById)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var bonus = await EnsureEntity<Bonus>(id, ctx, "Mission");

                    if (bonus.Status != AuditStatus.AwaitApproval)
                    {
                        throw new BusinessException(
                            string.Format(
                                Constants.MessageTemplates.InvalidCurrentStatus, bonus.Status.GetDisplayName()));
                    }

                    var customerBonus = await EnsureEntity<Customer>(bonus.Username, ctx);

                    if (customerBonus.AllowPromotion == false && result)
                    {
                        throw new BusinessException("客户不允许红利");
                    }

                    var now = DateTime.Now;

                    bonus.Status = result ? AuditStatus.Approved : AuditStatus.Declined;
                    bonus.AuditedAt = now;
                    bonus.AuditedById = auditedById;

                    if (auditedById.HasValue)
                    {
                        var user = await EnsureEntity<User>(auditedById.Value, ctx);
                        var log = new UserLog
                        {
                            Username = user.Username,
                            Type = UserLogType.Audit,
                            Content = $"账户[{bonus.Username}]红利[{bonus.Id}][{bonus.BonusAmount}]由[{user.Username}]审核{(result ? "成功" : "失败")}",
                            CreatedAt = now
                        };
                        ctx.UserLogs.Add(log);
                    }

                    if (result)
                    {
                        // Update customer wallet
                        var wallet = await EnsureEntity<Wallet>(bonus.Username, ctx);
                        wallet.Balance += bonus.BonusAmount;
                        wallet.TotalBonusAmount += bonus.BonusAmount;
                        wallet.AmountLastUpdatedAt = now;

                        // Add wallet transaction
                        AddWalletTransaction(ctx, bonus.BonusAmount, wallet.Balance, WalletTransactionType.Bonus, bonus.Username, bonus.Id, now);

                        // Add customer message
                        SendCustomerMessage(ctx, bonus.Username, "您的红利成功到账", "您申请的红利彩金已经成功派发至[中心钱包]，请及时查看");

                        await UpdateCustomerBonusPeriod(ctx, bonus, gameBalance, now);

                        // Add game transfer
                        await new GameTransferService().Add(ctx, bonus.Username, bonus.BonusAmount, true, null, null, GameTransferSource.BonusApproved, bonus.Id);

                        var customer = await EnsureEntity<Customer>(bonus.Username, ctx, "Agent");
                        if (customer.Agent != null)
                        {
                            customer.Agent.CustomerAmountLastUpdatedAt = now;
                        }

                        if (bonus.Mission != null)
                        {
                            var customerMission = await ctx.CustomerMissions.SingleOrDefaultAsync(
                                cm => cm.Username == customer.Username &&
                                cm.MissionId == bonus.MissionId);
                            if (customerMission == null)
                            {
                                throw new BusinessException($"Cannot find customer mission: {customer.Username} + {bonus.Mission.Name}");
                            }

                            customerMission.Status = CustomerMissionStatus.Applied;
                        }
                    }

                    await ctx.SaveChangesAsync();
                }

                scope.Complete();
            }
        }

        public async Task<Bonus> ApplyFromMission(Customer customer, Mission mission, decimal? gameBalance)
        {
            Bonus bonus = null;
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    if (await ctx.Bonus.AnyAsync(
                        b => b.Username == customer.Username &&
                        b.MissionId == mission.Id &&
                        (b.Status == AuditStatus.Approved ||
                        b.Status == AuditStatus.AwaitApproval)))
                    {
                        throw new BusinessException("您已申请过或正在等待审核中", "1");
                    }

                    bonus = new Bonus
                    {
                        BonusAmount = mission.Bonus.GetValueOrDefault(),
                        CreatedAt = DateTime.Now.ToE8(),
                        MissionId = mission.Id,
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        Username = customer.Username,
                        WithdrawalBetMultiplier = mission.Multiplier,
                        Status = AuditStatus.AwaitApproval
                    };

                    ctx.Bonus.Add(bonus);

                    await ctx.SaveChangesAsync();

                    if (await ctx.Bonus.CountAsync(
                        b => b.Username == customer.Username &&
                        b.MissionId == mission.Id &&
                        (b.Status == AuditStatus.Approved ||
                        b.Status == AuditStatus.AwaitApproval)) > 1)
                    {
                        throw new BusinessException("Duplicate application");
                    }

                    scope.Complete();
                }
            }

            if (mission.AutoAuditBonus)
            {
                await Audit(bonus.Id, true, gameBalance, null);
            }

            return bonus;
        }

        protected override IQueryable<Bonus> OrderBy(IQueryable<Bonus> source, string orderBy = null, bool isAscending = false)
        {
            switch (orderBy)
            {
                case "createdat":
                    return isAscending ? source.OrderBy(c => c.CreatedAt) :
                        source.OrderByDescending(c => c.CreatedAt);
                default:
                    return base.OrderBy(source, orderBy, isAscending);
            }
        }

        public override async Task<decimal> SearchAmount(Expression<Func<Bonus, decimal?>> amountSelector, IEnumerable<Expression<Func<Bonus, bool>>> query = null)
        {
            using (var ctx = new GWContext())
            {
                IQueryable<Bonus> result = ctx.Set<Bonus>().Where(
                    d => d.Status == AuditStatus.Approved)
                .AsNoTracking();

                if (query != null)
                {
                    foreach (var predicate in query)
                    {
                        result = result.Where(predicate);
                    }
                }

                return (await result.SumAsync(amountSelector)).GetValueOrDefault();
            }
        }

        private async Task UpdateCustomerBonusPeriod(GWContext ctx, Bonus bonus, decimal? gameBalance, DateTime now)
        {
            var lastPeriod = await new CustomerDepositPeriodService().GetLast(bonus.Username);
            if (lastPeriod == null || lastPeriod.Completed)
            {
                NLogLogger.Logger.Debug($"{bonus.Id} no last period found.");
                CreateNewPeriod();
            }
            else
            {
                NLogLogger.Logger.Debug($"{bonus.Id} last period found.");
                var wallet = await EnsureEntity<Wallet>(bonus.Username, ctx);
                var originalBalance = ctx.Entry(wallet).Property(w => w.Balance).OriginalValue;
                NLogLogger.Logger.Debug($"{bonus.Id} wallet balance: {originalBalance}; game balance: {gameBalance}");

                if (!gameBalance.HasValue || gameBalance.Value + originalBalance < 20)
                {
                    NLogLogger.Logger.Debug("Completing and starting a new period.");

                    lastPeriod.Completed = true;
                    lastPeriod.EndsAt = now;

                    CreateNewPeriod();
                }
                else
                {
                    NLogLogger.Logger.Debug("Adding to current period.");

                    UpdatePeriod(lastPeriod);
                }

                ctx.Entry(lastPeriod).State = EntityState.Modified;
            }

            void CreateNewPeriod()
            {
                var period = new CustomerDepositPeriod
                {
                    Username = bonus.Username,
                    CreatedAt = now,
                };
                UpdatePeriod(period);

                ctx.CustomerDepositPeriods.Add(period);
            }

            void UpdatePeriod(CustomerDepositPeriod period)
            {
                if (bonus.Mission == null)
                {
                    period.TotalAmount += bonus.BonusAmount;
                }
                else
                {
                    switch (bonus.Mission.WithdrawalRequiredAmountType)
                    {
                        case WithdrawalRequiredAmountType.TotalBet:
                            period.TotalAmount += GetAmount();

                            break;
                        case WithdrawalRequiredAmountType.ValidBet:
                            period.ValidBetAmount += GetAmount();

                            break;
                        default:
                            break;
                    }
                }
            }

            decimal GetAmount()
            {
                switch (bonus.Mission.WithdrawalCalculationType)
                {
                    case WithdrawalRequirementCalculationType.BonusMultiplier:
                        return bonus.BonusAmount * (bonus.Mission.Multiplier ?? 1);
                    case WithdrawalRequirementCalculationType.BonusPrincipalMultiplier:
                        return (bonus.BonusAmount + bonus.Mission.Principal.GetValueOrDefault()) * (bonus.Mission.Multiplier ?? 1);
                    case WithdrawalRequirementCalculationType.PrincipalMultiplier:
                        return bonus.Mission.Principal.GetValueOrDefault() * (bonus.Mission.Multiplier ?? 1);
                    default:
                        throw new InvalidOperationException();
                }
            }
        }
    }
}
