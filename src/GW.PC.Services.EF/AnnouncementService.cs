﻿using System.Threading.Tasks;
using GW.PC.Models;
using System.Linq;
using System.Data.Entity;
using System;
using System.Collections.Generic;

namespace GW.PC.Services.EF.Admin
{
    public class AnnouncementService : StatusfulEntityService<Announcement>
    {
        public async Task<Announcement> Save(Announcement announcement)
        {
            if (announcement.Id == 0)
            {
                announcement.Status = EntityStatus.Enabled;

                return await base.Add(announcement);
            }
            else
            {
                using (var ctx = new GWContext())
                {
                    var dbAnnouncement = await ctx.Announcements.FindAsync(announcement.Id);

                    dbAnnouncement.Title = announcement.Title;
                    dbAnnouncement.Content = announcement.Content;
                    dbAnnouncement.ExpiresAt = announcement.ExpiresAt;

                    await ctx.SaveChangesAsync();

                    return dbAnnouncement;
                }
            }
        }

        public async Task<IEnumerable<Announcement>> GetActive()
        {
            using (var ctx = new GWContext())
            {
                return await ctx.Announcements.Where(
                    a => a.ExpiresAt > DateTime.Now &&
                    a.Status == EntityStatus.Enabled)
                    .ToListAsync();
            }
        }

        public async Task Delete(int id, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var announcement = await EnsureEntity(id, ctx);

                ctx.Entry(announcement).State = EntityState.Deleted;

                await ctx.SaveChangesAsync();
            }
        }

        protected override IQueryable<Announcement> OrderBy(IQueryable<Announcement> source, string orderBy, bool isAscending)
        {
            return isAscending ? source.OrderBy(u => u.CreatedAt) :
                source.OrderByDescending(u => u.CreatedAt);
        }
    }
}
