﻿using System.Threading.Tasks;
using GW.PC.Models;
using System.Linq;
using System.Data.Entity;
using System;
using GW.PC.Core;

namespace GW.PC.Services.EF
{
    public class BankService : CreatableEntityService<Bank>
    {
        public async Task<int> GetBankId(string bankCode)
        {
            using (var ctx = new PCContext())
            {
                return (await ctx.Banks.Where(b => b.Code == bankCode).SingleOrDefaultAsync()).Id;
            }
        }


        public async Task<Bank> Save(Bank bank)
        {
            if (bank.Id == 0)
            {
                try
                {
                    await CheckUniqueness(b => b.Code == bank.Code,
                        b => b.Name.Equals(bank.Name, StringComparison.OrdinalIgnoreCase));
                }
                catch (BusinessException)
                {

                    throw new BusinessException
                        (string.Format(Constants.PCAdminMessages.UsernameAlreadyExists, bank.Name),
                        Constants.PCAdminMessages.DuplicateUsernameCode);
                }

                return await base.Add(bank);
            }
            else
            {
                using (var ctx = new PCContext())
                {
                    var dbBank = await ctx.Banks.FindAsync(bank.Id);

                    dbBank.Code = bank.Code;
                    dbBank.Name = bank.Name;
                    dbBank.Url = bank.Url;

                    await ctx.SaveChangesAsync();

                    return dbBank;
                }
            }
        }
    }
}
