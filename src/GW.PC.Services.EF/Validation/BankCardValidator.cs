﻿using FluentValidation;
using GW.PC.Models;

namespace GW.PC.Services.EF.Validation
{
    public class BankCardValidator : AbstractValidator<BankCard>
    {
        public BankCardValidator()
        {
            ValidatorOptions.CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleSet("VerifyBankCard", () =>
            {
                RuleFor(x => x.CardNumber)
                    .Length(16, 21)
                    .WithMessage($"卡号长度限制16~21个字符");
            });
        }
    }
}