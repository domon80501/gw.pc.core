﻿using System;
using System.Linq;
using FluentValidation;
using GW.PC.Models;
using GW.PC.Core;
using System.Text.RegularExpressions;

namespace GW.PC.Services.EF.Validation
{
    public class MerchantValidator : AbstractValidator<Merchant>
    {
        public MerchantValidator()
        {
            ValidatorOptions.CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleSet("VerifyEmail", () =>
            {
                RuleFor(x => x.EmailBound)
                    .Equal(false)
                    .WithMessage("账号已绑定邮箱");
            });

            RuleSet("Register", () =>
            {
                var today = DateTime.Today;

                RuleFor(x => x.Username)
                    .Length(Constants.MemberConditionParameters.UsernameMinLength, Constants.MemberConditionParameters.UsernameMaxLength)
                    .Matches("^[a-zA-Z0-9]+$")
                    .WithMessage($"用户名限制{Constants.MemberConditionParameters.UsernameMinLength}~{Constants.MemberConditionParameters.UsernameMaxLength}个英文/数字字符");

                RuleFor(x => x.Password)
                    .Length(Constants.MemberConditionParameters.PasswordMinLength, Constants.MemberConditionParameters.PasswordMaxLength)
                    .Matches("^[a-zA-Z0-9]+$")
                    .WithMessage($"密码限制{Constants.MemberConditionParameters.PasswordMinLength}~{Constants.MemberConditionParameters.PasswordMaxLength}个英文/数字字符")
                    .NotEqual(x => x.Username)
                    .WithMessage("用户名与密码不可重复");

                RuleFor(x => x.Name)
                    .MaximumLength(Constants.MemberConditionParameters.NameMaxLength)
                    .WithMessage($"姓名不能超过{Constants.MemberConditionParameters.NameMaxLength}个字符")
                    .Must(s => s.All(c => new Regex("^[\u4e00-\u9fa5]$").IsMatch(c.ToString())))
                    .WithMessage("姓名只能输入中文");

                RuleFor(x => x.Mobile)
                    .Length(Constants.MemberConditionParameters.MobileLength)
                    .Must(x => long.TryParse(x, out long s) && s > 0)
                    .WithMessage(string.Format(Constants.MessageTemplates.FormatError, "手机"));
            });

            RuleSet("Login", () =>
            {
                RuleFor(x => x.Username)
                    .Length(Constants.MemberConditionParameters.UsernameMinLength, Constants.MemberConditionParameters.UsernameMaxLength)
                    .WithMessage($"用户名限制{Constants.MemberConditionParameters.UsernameMinLength}~{Constants.MemberConditionParameters.UsernameMaxLength}个字符");

                RuleFor(x => x.Password)
                    .Length(Constants.MemberConditionParameters.PasswordMinLength, Constants.MemberConditionParameters.PasswordMaxLength)
                    .WithMessage($"密码限制{Constants.MemberConditionParameters.PasswordMinLength}~{Constants.MemberConditionParameters.PasswordMaxLength}个字符");
            });

            RuleSet("LoginByMobile", () =>
            {
                RuleFor(x => x.Mobile)
                    .Length(Constants.MemberConditionParameters.MobileLength)
                    .WithMessage(string.Format(Constants.MessageTemplates.FormatError, "手机"));
            });

            RuleSet("ConfirmMobile", () =>
            {
                RuleFor(x => x.Mobile)
                    .Length(Constants.MemberConditionParameters.MobileLength)
                    .WithMessage(string.Format(Constants.MessageTemplates.FormatError, "手机"));
            });

            RuleSet("ManagePassword", () =>
            {
                RuleFor(x => x.Password)
                    .Length(Constants.MemberConditionParameters.PasswordMinLength, Constants.MemberConditionParameters.PasswordMaxLength)
                    .Matches("^[a-zA-Z0-9]+$")
                    .WithMessage($"密码限制{Constants.MemberConditionParameters.PasswordMinLength}~{Constants.MemberConditionParameters.PasswordMaxLength}个英文/数字字符");
            });
        }
    }
}