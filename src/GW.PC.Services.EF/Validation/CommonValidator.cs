﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace GW.PC.Services.EF.Validation
{
    public class CommonValidator
    {
        /// <summary>
        /// 是否为正整数数字
        /// </summary>
        /// <param name="integer"></param>
        /// <returns></returns>
        public bool IsPositiveInteger(string integer)
        {
            return Regex.IsMatch(integer, @"^\d+$");
        }

        /// <summary>
        /// 是否为整数数字
        /// </summary>
        /// <param name="integer"></param>
        /// <returns></returns>
        public bool IsInteger(string integer)
        {
            return Regex.IsMatch(integer, @"^-?\d+$");
        }

        /// <summary>
        /// 是否为金额允许格式
        /// </summary>
        /// <param name="integer"></param>
        /// <returns></returns>
        public bool IsDecimal(string amount)
        {
            return decimal.TryParse(amount, out decimal result);
        }

        public bool IsValidEmail(string strIn)
        {
            var invalid = false;
            if (string.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid email format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            domainName = idn.GetAscii(domainName);

            return match.Groups[1].Value + domainName;
        }
    }
}
