﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Admin
{
    public class UserService : StatusfulEntityService<User>
    {
        public async Task<User> Create(User item, int[] roleIds)
        {
            using (var ctx = new PCContext())
            {
                item.Roles = await ctx.Roles.Where(r => roleIds.Contains(r.Id)).ToListAsync();

                item.CreatedAt = DateTime.Now.ToE8();
                item.Password = PasswordUtility.Hash(item.Password);
                item.Status = EntityStatus.Enabled;

                ctx.Users.Add(item);

                await ctx.SaveChangesAsync();

                return item;
            }
        }

        public async Task<User> Login(string username, string password)
        {
            using (var ctx = new PCContext())
            {
                var user = await ctx.Users.SingleOrDefaultAsync(
                    u => u.Username.Equals(username, StringComparison.OrdinalIgnoreCase));
                if (user == null)
                {
                    throw new BusinessException(Constants.Messages.InvalidLogin);
                }

                if (PasswordUtility.VerifyHash(password, user.Password))
                {
                    if (user.Status != EntityStatus.Enabled)
                    {
                        throw new BusinessException($"{Constants.Messages.InvalidAccountStatus}: {user.Status.ToString()}");
                    }
                }
                else
                {
                    throw new BusinessException(Constants.Messages.InvalidLogin);
                }

                return user;
            }
        }

        public async Task ChangePassword(string username, string oldPassword, string newPassword)
        {
            using (var ctx = new PCContext())
            {
                var user = await EnsureUser(username, ctx);

                if (PasswordUtility.VerifyHash(oldPassword, user.Password))
                {
                    if (user.Status == EntityStatus.Enabled)
                    {
                        user.Password = PasswordUtility.Hash(newPassword);

                        await ctx.SaveChangesAsync();
                    }
                    else
                    {
                        throw new BusinessException($"{Constants.Messages.InvalidAccountStatus}: {user.Status.ToString()}");
                    }
                }
                else
                {
                    throw new BusinessException(Constants.Messages.InvalidLogin);
                }
            }
        }

        public async Task<User> Get(string username, bool clearPassword = true, params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                var user = await EnsureUser(username, ctx, includes);

                if (clearPassword)
                {
                    // Clear the password for security concern.
                    user.Password = string.Empty;
                }

                return user;
            }
        }

        public async Task ResetPassword(string username, string newPassword)
        {
            using (var ctx = new PCContext())
            {
                var user = await EnsureUser(username, ctx);

                user.Password = PasswordUtility.Hash(newPassword);

                await ctx.SaveChangesAsync();
            }
        }

        public async Task<bool> HasPermission(string username, PermissionCode permissionCode)
        {
            using (var ctx = new PCContext())
            {
                var user = await EnsureUser(username, ctx, "Roles.Permissions");

                return user.Roles.Any(r => (r.Permissions?.Any(p => p.Code == permissionCode)).GetValueOrDefault());
            }
        }

        public async Task<IEnumerable<PermissionCode>> GetPermissionCodes(int userId)
        {
            using (var ctx = new PCContext())
            {
                var user = await EnsureEntity<User>(userId, ctx, "Roles.Permissions");

                return user.Roles.SelectMany(r => r.Permissions.Select(p => p.Code)).ToList();
            }
        }

        //public async Task AddAlarm(Alarm alarm, int[] receiverIds)
        //{
        //    using (var ctx = new PCContext())
        //    {
        //        alarm.Receivers = await GetUsers(receiverIds, ctx);

        //        ctx.Alarms.Add(alarm);

        //        await ctx.SaveChangesAsync();
        //    }
        //}

        //public async Task<IEnumerable<Alarm>> GetLayoutAlarms(int userId)
        //{
        //    using (var ctx = new PCContext())
        //    {
        //        var user = await EnsureEntity<User>(userId, ctx, "Alarms");
        //        foreach (var alarm in user.Alarms)
        //        {
        //            alarm.Read = true;
        //        }

        //        await ctx.SaveChangesAsync();

        //        return await ctx.Entry(user).Collection(u => u.Alarms).Query()
        //            .Include(a => a.CreatedBy)
        //            .Include(a => a.FollowUp)
        //            .OrderByDescending(a => a.Id)
        //            .Take(Constants.LayoutAlarmNumber)
        //            .ToListAsync();

        //    }
        //}

        //public async Task<int> GetUnreadAlarmsCount(int userId)
        //{
        //    using (var ctx = new PCContext())
        //    {
        //        var user = await EnsureEntity(userId, ctx);

        //        return await ctx.Entry(user).Collection(u => u.Alarms).Query()
        //            .CountAsync(a => !a.Read);
        //    }
        //}

        public async Task Update(User user, int[] roleIds)
        {
            using (var ctx = new PCContext())
            {
                var dbUser = await EnsureEntity<User>(user.Id, ctx, "Roles");

                dbUser.Status = user.Status;

                var roles = await ctx.Roles.Where(r => roleIds.Contains(r.Id)).ToListAsync();
                var newRoles = roles.Except(dbUser.Roles);
                foreach (var role in newRoles)
                {
                    dbUser.Roles.Add(role);
                }
                var deletedRoles = dbUser.Roles.Except(roles);
                for (int i = 0; i < deletedRoles.Count(); i++)
                {
                    dbUser.Roles.Remove(deletedRoles.ElementAt(i));
                }

                await ctx.SaveChangesAsync();
            }
        }

        internal async Task<List<User>> GetUsers(int[] ids, PCContext ctx)
        {
            var list = new List<User>();

            foreach (var id in ids)
            {
                list.Add(await ctx.Users.FindAsync(id));
            }

            return list;
        }

        private async Task<User> EnsureUser(string username, PCContext ctx, params string[] includes)
        {
            IQueryable<User> source = ctx.Users;

            foreach (var include in includes)
            {
                source = source.Include(include);
            }

            var user = await source.SingleOrDefaultAsync(c =>
                c.Username.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (user == null)
            {
                throw new BusinessException(Constants.Messages.UserNotFound);
            }

            return user;
        }
        
        protected override IQueryable<User> OrderBy(IQueryable<User> source, string orderBy, bool isAscending)
        {
            return isAscending ? source.OrderBy(u => u.CreatedAt) :
                source.OrderByDescending(u => u.CreatedAt);
        }
    }
}