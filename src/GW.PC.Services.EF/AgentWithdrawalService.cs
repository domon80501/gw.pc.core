﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.PC.Services.EF
{
    public class AgentWithdrawalService : CreatableEntityService<AgentWithdrawal>
    {
        private static readonly Random random = new Random();

        public async Task<AgentWithdrawal> Add(string username, string password, decimal amount, int bankId, string cardNumber, string accountName)
        {
            if (amount < 100)
            {
                throw new BusinessException(Constants.Messages.WithdrawAmountOutOfRange);
            }
            if (cardNumber.Length < 16 || cardNumber.Length > 21 || !cardNumber.All(c => char.IsNumber(c)))
            {
                throw new BusinessException(Constants.Messages.InvalidBankCardNumber);
            }

            var agent = await new AgentService().Login(username, password, clean: false);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    if (agent.Balance < amount)
                    {
                        throw new BusinessException(Constants.Messages.InsufficientWalletBalance);
                    }

                    agent.Balance -= amount;
                    agent.FrozenAmount += amount;

                    var withdrawal = new AgentWithdrawal
                    {
                        Amount = amount,
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        CreatedAt = DateTime.Now.ToE8(),
                        PayeeBankId = bankId,
                        PayeeCardNumber = cardNumber,
                        PayeeName = accountName,
                        Status = WithdrawalStatus.AwaitingAutoPayment,
                        Username = username
                    };

                    ctx.AgentWithdrawals.Add(withdrawal);

                    ctx.Entry(agent).State = EntityState.Modified;

                    await ctx.SaveChangesAsync();

                    AddWalletTransaction(ctx, 0 - amount, agent.Balance, WalletTransactionType.AgentWithdrawal, username, withdrawal.Id);

                    EntityLogService.Add(
                        ctx,
                        EntityLogContent.AgentWithdrawal_Created,
                        EntityLogTargetType.AgentWithdrawal,
                        withdrawal.Id,
                        null,
                        withdrawal.Amount);

                    scope.Complete();

                    return withdrawal;
                }
            }
        }

        public async Task<AgentWithdrawal> AddChildren(AgentWithdrawal withdrawal, IEnumerable<AgentWithdrawal> children)
        {
            using (var ctx = new GWContext())
            {
                withdrawal.HasChildren = true;

                ctx.AgentWithdrawals.AddRange(children);
                ctx.Entry(withdrawal).State = EntityState.Modified;

                await ctx.SaveChangesAsync();

                return withdrawal;
            }
        }

        public async Task<AgentWithdrawal> GetByMerchantOrderNumber(string merchantOrderNumber, params string[] includes)
        {
            using (var ctx = new GWContext())
            {
                IQueryable<AgentWithdrawal> query = ctx.Set<AgentWithdrawal>();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }

                return await query.SingleOrDefaultAsync(d => d.MerchantOrderNumber == merchantOrderNumber);
            }
        }

        public async Task<IEnumerable<AgentWithdrawal>> GetChildren(int id)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.AgentWithdrawals.Include(w => w.PaymentChannel).Where(w => w.ParentId == id).ToListAsync();
            }
        }

        public async Task<AgentWithdrawal> AutoSucceed(int id)
        {
            using (var ctx = new GWContext())
            {
                var withdrawal = await EnsureEntity<AgentWithdrawal>(id, ctx, "PaymentChannel.Provider", "Reference");
                if (withdrawal.Status != WithdrawalStatus.AutoPaymentInProgress)
                {
                    throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                }

                withdrawal.Status = WithdrawalStatus.AutoPaymentSuccessful;

                await Succeed(ctx, withdrawal, DateTime.Now, null);

                await ctx.SaveChangesAsync();

                return withdrawal;
            }
        }

        public async Task<AgentWithdrawal> SelectWithdrawChannel(int id)
        {
            using (var ctx = new GWContext())
            {
                var withdrawal = await EnsureEntity(id, ctx);
                var config = await SystemConfigurationService.Get();
                var divideThreshold = config.Withdrawal.DivideThreshold ?? 45000;

                if (withdrawal.Status == WithdrawalStatus.AwaitingAutoPayment)
                {
                    PaymentChannel channel = null;

                    var channels = await ctx.PaymentChannels
                        .Include(c => c.Provider.PaymentProviderBanks)
                        .Where(
                        c => c.PaymentType == PaymentType.Withdrawal &&
                        c.Status == EntityStatus.Enabled &&
                        c.MinAmount <= withdrawal.Amount &&
                        ((withdrawal.Amount <= divideThreshold && c.MaxAmount >= withdrawal.Amount) ||
                        withdrawal.Amount > divideThreshold))
                        .ToListAsync();

                    NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 1 - Amount:{withdrawal.Amount}; channels:{string.Join("|", channels.Select(c => c.Name))}");
                    channels = channels.Where(
                        c => c.Provider.PaymentProviderBanks
                        .Where(b => b.Status == EntityStatus.Enabled)
                        .Select(b => b.BankId)
                        .Contains(withdrawal.PayeeBankId.GetValueOrDefault()) &&
                        (!c.MinimumWithdrawInterval.HasValue ||
                            Convert.ToDecimal(DateTime.Now.ToE8().Subtract(c.LastUsedAt.GetValueOrDefault()).TotalSeconds) >= c.MinimumWithdrawInterval.Value))
                        .ToList();
                    NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 2 - PayeeBankId:{withdrawal.PayeeBankId}; channels:{string.Join("|", channels.Select(c => c.Name))}");
                    var commissions = channels.Select(c => new
                    {
                        Channel = c,
                        Commission = new PaymentChannelService().CalculateCommission(c, withdrawal.Amount)
                    })
                    .Where(c => c.Channel.Provider.Balance >= withdrawal.Amount + c.Commission + 100)
                    .ToList();
                    NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 3 - channels:{(string.Join("|", commissions.Select(c => new { c.Channel.Name, c.Channel.Provider.Balance, c.Commission })))}");

                    if (commissions.Count == 0)
                    {
                        NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 4 - No channel");

                        withdrawal.Status = WithdrawalStatus.AutoPaymentFailed;
                        withdrawal.DeclineNotes = Constants.Messages.NoWithdrawalChannel;
                    }
                    else
                    {
                        if (commissions.Count == 1)
                        {
                            channel = commissions.Single().Channel;
                            NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 4 - Single channel:{channel.Name}");
                        }
                        else
                        {
                            var minCommissions = commissions.Where(
                                c => c.Commission == commissions.Min(cc => cc.Commission))
                                .ToList();
                            NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 4 - minCommissions:{(string.Join("|", minCommissions.Select(c => new { c.Channel.Name, c.Commission })))}");
                            var randomDouble = random.NextDouble();
                            NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 5 - random double:{randomDouble}");
                            if (randomDouble < 0.5)
                            {
                                var index = random.Next(0, minCommissions.Count);
                                NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 6 - [0, {minCommissions.Count}]:{index}");
                                channel = minCommissions.ElementAt(index).Channel;
                            }
                            else
                            {
                                var index = random.Next(0, commissions.Count);
                                NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 6 - [0, {commissions.Count}]:{index}");
                                channel = commissions.ElementAt(index).Channel;
                            }
                            NLogLogger.Logger.Debug($"{withdrawal.Id} Withdrawal selection log 7 - selected: {channel.Name}");
                        }

                        withdrawal.Status = WithdrawalStatus.AutoPaymentInProgress;
                        withdrawal.PaymentChannelId = channel.Id;
                        withdrawal.PaymentChannel = channel;
                    }

                    await ctx.SaveChangesAsync();
                }

                return withdrawal;
            }
        }

        public async Task<AgentWithdrawal> AutoFail(int id, string declineNotes, string customerMessage = null, string logDetails = null)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var withdrawal = await EnsureEntity(id, ctx);
                    if (withdrawal.Status != WithdrawalStatus.AutoPaymentInProgress)
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                    }

                    withdrawal.Status = WithdrawalStatus.AutoPaymentFailed;
                    withdrawal.PaidAt = DateTime.Now.ToE8();
                    withdrawal.DeclineNotes = declineNotes;

                    await Fail(ctx, withdrawal, null, customerMessage, logDetails);

                    if (!withdrawal.HasChildren &&
                        !withdrawal.ParentId.HasValue &&
                        !await ctx.CustomerWithdrawals.AnyAsync(
                            w => w.Username == withdrawal.Username &&
                            (w.Status == WithdrawalStatus.AutoPaymentSuccessful ||
                            w.Status == WithdrawalStatus.ManualConfirmed ||
                            w.Status == WithdrawalStatus.RetryAutoPaymentSuccessful)))
                    {
                        await Close(ctx, withdrawal, null);
                    }

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return withdrawal;
                }
            }
        }

        public async Task<AgentWithdrawal> Close(int id, int? operatorId)
        {
            using (var ctx = new GWContext())
            {
                var withdrawal = await EnsureEntity<AgentWithdrawal>(id, ctx);

                return await Close(ctx, withdrawal, operatorId);
            }
        }

        public async Task<AgentWithdrawal> Retry(int id, int operatorId)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var withdrawal = await EnsureEntity<AgentWithdrawal>(id, ctx, "PaymentChannel");
                    if (withdrawal.HasChildren)
                    {
                        throw new BusinessException("总提款不能重试，请对各拆分提款分别重试");
                    }

                    if (withdrawal.Status != WithdrawalStatus.AutoPaymentFailed &&
                        withdrawal.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed)
                    {
                        throw new BusinessException(
                            string.Format(
                                Constants.MessageTemplates.InvalidCurrentStatus,
                                withdrawal.Status.GetDisplayName()));
                    }

                    if (await ctx.AgentWithdrawals.AnyAsync(
                        w => w.ReferenceId == withdrawal.Id &&
                        w.Status != WithdrawalStatus.AutoPaymentFailed &&
                        w.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed &&
                        w.Status != WithdrawalStatus.AutoPaymentSuccessful &&
                        w.Status != WithdrawalStatus.ManualConfirmed))
                    {
                        throw new BusinessException("该提款已经在重试中，请刷新查看");
                    }

                    withdrawal.Status = WithdrawalStatus.FailedAndClosed;

                    var newWithdrawal = new AgentWithdrawal
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        Amount = withdrawal.Amount,
                        Status = WithdrawalStatus.AwaitingAutoPayment,
                        CreatedAt = DateTime.Now.ToE8(),
                        ParentId = withdrawal.ParentId,
                        PayeeBankId = withdrawal.PayeeBankId,
                        PayeeCardAddress = withdrawal.PayeeCardAddress,
                        PayeeCardNumber = withdrawal.PayeeCardNumber,
                        PayeeName = withdrawal.PayeeName,
                        Username = withdrawal.Username,
                        WithdrawalIP = withdrawal.WithdrawalIP,
                        WithdrawalAddress = withdrawal.WithdrawalAddress,
                        ReferenceId = withdrawal.Id
                    };

                    ctx.AgentWithdrawals.Add(newWithdrawal);

                    await ctx.SaveChangesAsync();

                    var agent = await EnsureEntity<Agent>(withdrawal.Username, ctx);
                    EntityLogService.Add(
                        ctx,
                        EntityLogContent.AgentWithdrawal_RetryCreated,
                        EntityLogTargetType.AgentWithdrawal,
                        newWithdrawal.Id,
                        null,
                        newWithdrawal.Status.GetDisplayName(), agent.Balance, agent.FrozenAmount, withdrawal.Amount);

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return newWithdrawal;
                }
            }
        }

        public async Task<AgentWithdrawal> ManualConfirm(int id, bool result, string notes, int operatorId)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var withdrawal = await EnsureEntity<AgentWithdrawal>(id, ctx, "PaymentChannel.Provider", "Reference");
                    if (withdrawal.Status != WithdrawalStatus.AutoPaymentInProgress)
                    {
                        throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, withdrawal.Status.GetDisplayName()));
                    }

                    if (result)
                    {
                        withdrawal.Status = WithdrawalStatus.ManualConfirmed;

                        await Succeed(ctx, withdrawal, DateTime.Now.ToE8(), operatorId, notes);
                    }
                    else
                    {
                        withdrawal.Status = WithdrawalStatus.AutoPaymentManualConfirmedFailed;
                        withdrawal.ManualConfirmedAt = DateTime.Now.ToE8();
                        withdrawal.ManualConfirmedById = operatorId;
                        withdrawal.ManualConfirmNotes = notes;

                        await Fail(ctx, withdrawal, operatorId);

                        if (!withdrawal.HasChildren &&
                            !await ctx.CustomerWithdrawals.AnyAsync(
                            w => w.Username == withdrawal.Username &&
                            (w.Status == WithdrawalStatus.AutoPaymentSuccessful ||
                            w.Status == WithdrawalStatus.ManualConfirmed ||
                            w.Status == WithdrawalStatus.RetryAutoPaymentSuccessful)))
                        {
                            await Close(ctx, withdrawal, null);
                        }
                    }

                    await ctx.SaveChangesAsync();

                    scope.Complete();

                    return withdrawal;
                }
            }
        }

        private async Task<AgentWithdrawal> Close(GWContext ctx, AgentWithdrawal withdrawal, int? operatorId)
        {
            if (withdrawal.ParentId.HasValue)
            {
                throw new NotSupportedException("Cannot close a child withdrawal");
            }

            if (withdrawal.Status != WithdrawalStatus.AwaitingApproval &&
                withdrawal.Status != WithdrawalStatus.AutoPaymentFailed &&
                withdrawal.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed)
            {
                throw new BusinessException(
                    string.Format(
                        Constants.MessageTemplates.InvalidCurrentStatus,
                        withdrawal.Status.GetDisplayName()));
            }

            // Update wallet
            var agent = await EnsureEntity<Agent>(withdrawal.Username, ctx);
            if (withdrawal.Status == WithdrawalStatus.AutoPaymentFailed ||
                withdrawal.Status == WithdrawalStatus.AutoPaymentManualConfirmedFailed)
            {
                if (agent.FrozenAmount < withdrawal.Amount)
                {
                    throw new BusinessException(Constants.Messages.InsufficientFrozenAmount);
                }

                agent.FrozenAmount -= withdrawal.Amount;
                agent.Balance += withdrawal.Amount;

                AddWalletTransaction(ctx, withdrawal.Amount, agent.Balance, WalletTransactionType.AgentWithdrawalRefund, withdrawal.Username, withdrawal.Id);
            }

            withdrawal.Status = WithdrawalStatus.FailedAndClosed;

            var children = ctx.AgentWithdrawals.Where(w => w.ParentId == withdrawal.Id);
            foreach (var child in children)
            {
                if (child.Status != WithdrawalStatus.AutoPaymentFailed &&
                    child.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed)
                {
                    throw new BusinessException("该提款有拆分提款不是失败状态，不能关闭");
                }

                child.Status = WithdrawalStatus.FailedAndClosed;
            }

            var references = ctx.AgentWithdrawals.Where(w => w.ReferenceId == withdrawal.Id);
            foreach (var reference in references)
            {
                if (reference.Status != WithdrawalStatus.AutoPaymentFailed &&
                    reference.Status != WithdrawalStatus.AutoPaymentManualConfirmedFailed)
                {
                    throw new BusinessException("该提款有重试提款不是失败状态，不能关闭");
                }

                reference.Status = WithdrawalStatus.FailedAndClosed;
            }

            EntityLogService.Add(
                ctx,
                EntityLogContent.AgentWithdrawal_FailedAndClosed,
                EntityLogTargetType.AgentWithdrawal,
                withdrawal.Id,
                null,
                withdrawal.Amount, agent.Balance, agent.FrozenAmount);

            await ctx.SaveChangesAsync();

            return withdrawal;
        }

        private async Task Succeed(GWContext ctx, AgentWithdrawal withdrawal, DateTime now, int? operatorId, string notes = null)
        {
            if (withdrawal.Status == WithdrawalStatus.ManualConfirmed)
            {
                withdrawal.ManualConfirmedAt = now;
                withdrawal.ManualConfirmedById = operatorId;
                withdrawal.ManualConfirmNotes = notes;
                if (withdrawal.Reference != null)
                {
                    withdrawal.Reference.Status = WithdrawalStatus.RetryManualConfirmed;
                    withdrawal.Reference.ManualConfirmedById = operatorId;
                    withdrawal.Reference.ManualConfirmedAt = now;
                    withdrawal.Reference.ManualConfirmNotes = notes;
                }
            }
            else if (withdrawal.Status == WithdrawalStatus.AutoPaymentSuccessful)
            {
                withdrawal.PaidAt = now;
                if (withdrawal.Reference != null)
                {
                    withdrawal.Reference.DeclineNotes = null;
                    withdrawal.Reference.Status = WithdrawalStatus.RetryAutoPaymentSuccessful;
                    withdrawal.Reference.PaidAt = now;
                }
            }

            // Update wallet
            var agent = await EnsureEntity<Agent>(withdrawal.Username, ctx);
            if (agent.FrozenAmount < withdrawal.Amount)
            {
                throw new BusinessException(Constants.Messages.InsufficientFrozenAmount);
            }

            agent.FrozenAmount -= withdrawal.Amount;

            // Update payment channel
            if (withdrawal.PaymentChannel != null)
            {
                UpdatePaymentChannel(ctx, withdrawal, now);
            }

            // Update agent's bank cards
            var bankCard = await ctx.BankCards.SingleOrDefaultAsync(
                c => c.CardNumber == withdrawal.PayeeCardNumber &&
                c.AgentId.HasValue &&
                c.Status == EntityStatus.Enabled);
            if (bankCard == null)
            {
                ctx.BankCards.Add(new BankCard
                {
                    AccountName = withdrawal.PayeeName,
                    BankId = withdrawal.PayeeBankId.Value,
                    CardNumber = withdrawal.PayeeCardNumber,
                    CreatedAt = DateTime.Now.ToE8(),
                    Status = EntityStatus.Enabled,
                    Verified = true,
                    AgentId = agent.Id
                });
            }
            else
            {
                if (bankCard.AgentId == agent.Id && bankCard.Verified == false)
                {
                    bankCard.Verified = true;
                }
            }

            // Update parent withdrawal, if needed
            if (withdrawal.ParentId > 0)
            {
                var parent = await new AgentWithdrawalService().Find(withdrawal.ParentId.Value);
                var children = await new AgentWithdrawalService().GetChildren(parent.Id);

                if (children.Where(c => c.Id != withdrawal.Id).All(
                    c => c.Status == WithdrawalStatus.AutoPaymentSuccessful))
                {
                    parent.Status = WithdrawalStatus.AutoPaymentSuccessful;

                    ctx.Entry(parent).State = EntityState.Modified;
                }
            }
        }

        private void UpdatePaymentChannel(GWContext ctx, AgentWithdrawal withdrawal, DateTime now)
        {
            withdrawal.PaymentChannel.LastUsedAt = now;

            withdrawal.Commission = new PaymentChannelService().CalculateCommission(withdrawal.PaymentChannel, withdrawal.Amount);
            var transaction = new PaymentChannelTransaction
            {
                Amount = 0 - withdrawal.Amount,
                BeforeBalance = withdrawal.PaymentChannel.Provider.Balance,
                CreatedAt = now,
                PaymentChannelId = withdrawal.PaymentChannelId.Value,
                Type = PaymentChannelTransactionType.CustomerWithdrawal,
                Commission = withdrawal.Commission,
                AfterBalance = withdrawal.PaymentChannel.Provider.Balance - withdrawal.Amount - withdrawal.Commission
            };
            withdrawal.PaymentChannel.Provider.Balance = transaction.AfterBalance;

            ctx.PaymentChannelTransactions.Add(transaction);
        }

        private async Task Fail(GWContext ctx, AgentWithdrawal withdrawal, int? operatorId, string customerMessage = null, string logDetails = null)
        {
            var now = DateTime.Now.ToE8();

            withdrawal.AgentMessage = customerMessage;

            // Update parent withdrawal, if needed
            if (withdrawal.ParentId > 0)
            {
                var parent = await new AgentWithdrawalService().Find(withdrawal.ParentId.Value);
                var children = await new AgentWithdrawalService().GetChildren(parent.Id);

                if (children.Where(c => c.Id != withdrawal.Id).All(
                    c => c.Status == WithdrawalStatus.AutoPaymentFailed ||
                    c.Status == WithdrawalStatus.AutoPaymentManualConfirmedFailed))
                {
                    parent.Status = withdrawal.Status;

                    ctx.Entry(parent).State = EntityState.Modified;
                }
            }

            var agent = await EnsureEntity<Agent>(withdrawal.Username, ctx);
            EntityLogService.Add(
                ctx,
                EntityLogContent.AgentWithdrawal_Failed,
                EntityLogTargetType.AgentWithdrawal,
                withdrawal.Id,
                logDetails,
                withdrawal.Status.GetDisplayName(), agent.Balance, agent.FrozenAmount, withdrawal.Amount);
        }
    }
}
