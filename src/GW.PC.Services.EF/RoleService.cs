﻿using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF.Admin
{
    public class RoleService : StatusfulEntityService<Role>
    {
        public async Task Save(Role role, List<int> permissionIds)
        {
            using (var ctx = new PCContext())
            {
                if (role.Id == 0)
                {
                    await CheckUniqueness(r => r.Name.Equals(role.Name, StringComparison.OrdinalIgnoreCase));

                    if (permissionIds != null && permissionIds.Count() > 0)
                    {
                        role.Permissions = await ctx.Permissions.Where(
                            p => permissionIds.Contains(p.Id)).ToListAsync();
                    }

                    ctx.Roles.Add(role);
                }
                else
                {
                    var dbRole = await EnsureEntity<Role>(role.Id, ctx, "Permissions");

                    dbRole.Name = role.Name;

                    if (permissionIds == null || permissionIds.Count() == 0)
                    {
                        dbRole.Permissions.Clear();
                    }
                    else
                    {
                        var dbPermissionIds = dbRole.Permissions.Select(p => p.Id).ToList();
                        var newPermissionIds = permissionIds.Except(dbPermissionIds);
                        foreach (var id in newPermissionIds)
                        {
                            dbRole.Permissions.Add(ctx.Permissions.Find(id));
                        }
                        var deletedPermissionIds = dbPermissionIds.Except(permissionIds).ToList();
                        foreach (var id in deletedPermissionIds)
                        {
                            dbRole.Permissions.Remove(dbRole.Permissions.Single(p => p.Id == id));
                        }
                    }
                }

                await ctx.SaveChangesAsync();
            }
        }
        
        //public override async Task<Role> Update(Role item)
        //{
        //    using (var ctx = new PCContext())
        //    {
        //        var role = await ctx.Roles.FindAsync(item.Id);

        //        role.Name = item.Name;

        //        await ctx.SaveChangesAsync();

        //        return item;
        //    }
        //}

        protected override IQueryable<Role> OrderBy(IQueryable<Role> source, string orderBy, bool isAscending)
        {
            return isAscending ? source.OrderBy(u => u.CreatedAt) :
                source.OrderByDescending(u => u.CreatedAt);
        }
    }
}
