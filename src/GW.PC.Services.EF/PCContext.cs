﻿using GW.PC.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace GW.PC.Services.EF
{
    public class PCContext : IdentityDbContext<AdminIdentityUser>//DbContext
    {
        /// <summary>
        /// Enforces the connection string to be present in the configuration file.
        /// </summary>
        public PCContext() : base("name=PCContext")
        {
            //Development period
            Database.SetInitializer(new PCDbInitializer());
            //Database.SetInitializer<PCContext>(new DropCreateDatabaseIfModelChanges<PCContext>());

            //Operation period
            //Database.SetInitializer<PCContext>(null);

            // Disable lazy loading because most controller actions simply call
            // web api methods to do business. Therefore, lazy loading won't help
            // but rather hinder the serialization.
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public static PCContext Create()
        {
            return new PCContext();
        }

        //public DbSet<Province> Provinces { get; set; }
        //public DbSet<City> Cities { get; set; }
        //public DbSet<CustomerLogin> CustomerLogins { get; set; }
        public DbSet<PermissionGroup> PermissionGroups { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Bank> Banks { get; set; }
        //public DbSet<BankCard> BankCards { get; set; }
        //public DbSet<NewPromotion> NewPromotions { get; set; }
        //public DbSet<Customer> Customers { get; set; }
        //public DbSet<Agent> Agents { get; set; }
        //public DbSet<AgentLogin> AgentLogins { get; set; }
        //public DbSet<AgentMessage> AgentMessages { get; set; }
        //public DbSet<AgentSettlement> AgentSettlements { get; set; }
        //public DbSet<DirectAgentSettlement> DirectAgentSettlements { get; set; }
        //public DbSet<SubordinateAgentSettlement> SubordinateAgentSettlements { get; set; }
        //public DbSet<AgentAdjustment> AgentAdjustments { get; set; }
        //public DbSet<AgentWithdrawal> AgentWithdrawals { get; set; }
        //public DbSet<CustomerMonthlyDeposit> CustomerMonthlyDeposits { get; set; }
        //public DbSet<AgentPromotion> AgentPromotions { get; set; }
        //public DbSet<Wallet> Wallets { get; set; }
        //public DbSet<Game> Games { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        //public DbSet<Alarm> Alarms { get; set; }
        //public DbSet<Tag> Tags { get; set; }
        //public DbSet<Bonus> Bonus { get; set; }
        //public DbSet<Rakeback> Rakebacks { get; set; }
        //public DbSet<FollowUp> FollowUps { get; set; }
        //public DbSet<FollowUpComment> FollowUpComments { get; set; }
        //public DbSet<BalanceAdjustment> BalanceAdjustments { get; set; }
        //public DbSet<Announcement> Announcements { get; set; }
        //public DbSet<CustomerRelevance> CustomerRelevances { get; set; }
        //public DbSet<CustomerGrade> CustomerGrades { get; set; }
        //public DbSet<CustomerMessage> CustomerMessages { get; set; }
        //public DbSet<CustomerWithdrawal> CustomerWithdrawals { get; set; }
        //public DbSet<FinancialReport> FinancialReports { get; set; }
        //public DbSet<CustomerLog> CustomerLogs { get; set; }
        public DbSet<UserLog> UserLogs { get; set; }
        //public DbSet<WalletTransaction> WalletTransactions { get; set; }
        //public DbSet<GameTransfer> GameTransfers { get; set; }
        //public DbSet<RakebackConfiguration> RakebackConfigurations { get; set; }
        public DbSet<OnlinePaymentDeposit> OnlinePaymentDeposits { get; set; }
        public DbSet<OnlineBankingDeposit> OnlineBankingDeposits { get; set; }
        public DbSet<OnlineToBankCardDeposit> OnlineToBankCardDeposits { get; set; }
        public DbSet<WeChatDeposit> WeChatDeposits { get; set; }
        public DbSet<AlipayDeposit> AlipayDeposits { get; set; }
        public DbSet<DepositReminder> DepositReminders { get; set; }
        public DbSet<PrepaidCardDeposit> PrepaidCardDeposits { get; set; }
        public DbSet<QQWalletDeposit> QQWalletDeposits { get; set; }
        public DbSet<QuickPayDeposit> QuickPayDeposits { get; set; }
        public DbSet<JDWalletDeposit> JDWalletDeposits { get; set; }
        public DbSet<PaymentChannel> PaymentChannels { get; set; }
        public DbSet<PaymentProvider> PaymentProviders { get; set; }
        public DbSet<PaymentProviderBank> PaymentProviderBanks { get; set; }
        //public DbSet<CustomerSportsbook> CustomerSportsbooks { get; set; }
        //public DbSet<CustomerBbin> CustomerBbins { get; set; }
        //public DbSet<CustomerAG> CustomerAGs { get; set; }
        //public DbSet<CustomerEA> CustomerEAs { get; set; }
        //public DbSet<CustomerQT> CustomerQTs { get; set; }
        //public DbSet<CustomerPT> CustomerPTs { get; set; }
        //public DbSet<CustomerKG> CustomerKGs { get; set; }
        //public DbSet<CustomerLB> CustomerLBs { get; set; }
        //public DbSet<CustomerLT> CustomerLTs { get; set; }
        //public DbSet<CustomerES> CustomerESs { get; set; }
        //public DbSet<CustomerGameData> CustomerGameData { get; set; }
        //public DbSet<OnlineToBankCardDepositReminder> OnlineToBankCardDepositReminders { get; set; }
        //public DbSet<PaymentChannelTransfer> PaymentChannelTransfers { get; set; }
        //public DbSet<PaymentChannelAdjustment> PaymentChannelAdjustments { get; set; }
        public DbSet<PaymentChannelTransaction> PaymentChannelTransactions { get; set; }
        public DbSet<InternalDeposit> InternalDeposits { get; set; }
        public DbSet<HttpPayment> HttpPayments { get; set; }
        //public DbSet<CustomerDepositPeriod> CustomerDepositPeriods { get; set; }
        public DbSet<EntityLog> EntityLogs { get; set; }
        //public DbSet<FailedCustomerLogin> FailedCustomerLogins { get; set; }
        public DbSet<SystemConfiguration> SystemConfigurations { get; set; }
        public DbSet<SystemOrderNumber> SystemOrderNumbers { get; set; }
        //public DbSet<CustomerLoginLog> CustomerLoginLogs { get; set; }
        //public DbSet<MobileVerificationCode> MobileVerificationCodes { get; set; }
        //public DbSet<GameTransaction> GameTransactions { get; set; }
        //public DbSet<PromotionCategory> PromotionCategories { get; set; }
        //public DbSet<ReferralRecognition> ReferralRecognitions { get; set; }
        //public DbSet<Mission> Missions { get; set; }
        //public DbSet<CustomerMission> CustomerMissions { get; set; }
        public DbSet<Merchant> Merchants { get; set; }
        public DbSet<MerchantLogin> MerchantLogins { get; set; }
        public DbSet<MerchantWithdrawal> MerchantWithdrawals { get; set; }
        public DbSet<FailedMerchantLogin> FailedMerchantLogins { get; set; }
        public DbSet<MerchantLoginLog> MerchantLoginLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Properties<string>().Configure(
                c => c.HasMaxLength(500));

            //modelBuilder.Entity<Agent>().Property(a => a.Username)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //    new IndexAnnotation(new IndexAttribute("IX_Username")
            //    {
            //        IsUnique = true
            //    }));
            //modelBuilder.Entity<Agent>().Property(a => a.Name)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //    new IndexAnnotation(new IndexAttribute("IX_Name")
            //    {
            //        IsUnique = true
            //    }));
            //modelBuilder.Entity<Agent>().Property(a => a.Email)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //    new IndexAnnotation(new IndexAttribute("IX_Email")
            //    {
            //        IsUnique = true
            //    }));
            //modelBuilder.Entity<Agent>().Property(a => a.QQ)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //    new IndexAnnotation(new IndexAttribute("IX_QQ")
            //    {
            //        IsUnique = true
            //    }));
            //modelBuilder.Entity<Agent>().Property(a => a.Mobile)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //    new IndexAnnotation(new IndexAttribute("IX_Mobile")
            //    {
            //        IsUnique = true
            //    }));
            //modelBuilder.Entity<Agent>().Property(a => a.WeChat)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //    new IndexAnnotation(new IndexAttribute("IX_WeChat")
            //    {
            //        IsUnique = true
            //    }));
            //modelBuilder.Entity<Agent>().Property(a => a.GameSettings)
            //    .HasMaxLength(1000);
            //modelBuilder.Entity<Agent>().Property(a => a.AgentPercentage)
            //    .HasPrecision(18, 3);
            //modelBuilder.Entity<Agent>().Property(a => a.DepositFeePercentage)
            //    .HasPrecision(18, 3);
            //modelBuilder.Entity<Agent>().Property(a => a.ParentBonusPercentage)
            //    .HasPrecision(18, 3);
            //modelBuilder.Entity<Agent>().Property(a => a.PlatformFeePercentage)
            //    .HasPrecision(18, 3);
            //modelBuilder.Entity<Agent>().Property(a => a.WithdrawalFeePercentage)
            //    .HasPrecision(18, 3);

            //modelBuilder.Entity<RakebackConfiguration>().Property(c => c.Configurations)
            //    .HasMaxLength(1000);

            //modelBuilder.Entity<Rakeback>().Property(r => r.RakebackPercentage)
            //    .HasPrecision(18, 3);

            //modelBuilder.Entity<Customer>().HasMany(c => c.Tags)
            //    .WithMany(t => t.Customers)
            //    .Map(c =>
            //        c.ToTable("CustomerTags")
            //            .MapLeftKey("CustomerId")
            //            .MapRightKey("TagId")
            //    );

            modelBuilder.Entity<Role>().HasMany(r => r.Permissions)
             .WithMany()
             .Map(c => c.ToTable("RolePermissions")
                 .MapLeftKey("RoleId")
                 .MapRightKey("PermissionId"));

            modelBuilder.Entity<User>().HasMany(u => u.Roles)
              .WithMany(r => r.Users)
              .Map(c =>
                  c.ToTable("UserRoles")
                      .MapLeftKey("UserId")
                      .MapRightKey("RoleId")
              );

            modelBuilder.Entity<Merchant>().HasMany(u => u.PaymentProviders)
                .WithRequired(u => u.Merchant);
            modelBuilder.Entity<Merchant>().HasMany(u => u.PaymentProviderBanks)
                .WithRequired(u => u.Merchant);
            modelBuilder.Entity<Merchant>().HasMany(u => u.PaymentChannels)
                .WithRequired(u => u.Merchant);
            modelBuilder.Entity<Merchant>().HasMany(u => u.Users)
                .WithMany(r => r.Merchants)
                .Map(c =>
                    c.ToTable("MerchantUsers")
                    .MapLeftKey("MerchantId")
                    .MapRightKey("UserId")
                    );

            //modelBuilder.Entity<Merchant>().HasMany(u => u.AlipayDeposits)
            //  .WithRequired(u => u.Merchant);
            //modelBuilder.Entity<Merchant>().HasMany(u => u.MerchantWithdrawals)
            // .WithRequired(u => u.Merchant);

            //modelBuilder.Entity<User>().HasMany(u => u.Alarms)
            //    .WithMany(n => n.Receivers)
            //    .Map(c =>
            //        c.ToTable("UserAlarms")
            //            .MapLeftKey("UserId")
            //            .MapRightKey("AlarmId")
            //    );

            //modelBuilder.Entity<FollowUp>().HasMany(f => f.Receivers)
            //    .WithMany()
            //    .Map(c =>
            //        c.ToTable("UserFollowUps")
            //            .MapLeftKey("FollowUpId")
            //            .MapRightKey("UserId")
            //    );

            //modelBuilder.Entity<Customer>().Property(c => c.Username)
            //    .HasMaxLength(50)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_Username")
            //        {
            //            IsUnique = true
            //        }));

            modelBuilder.Entity<User>().Property(u => u.Username)
                .HasMaxLength(50)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_Username")
                    {
                        IsUnique = true
                    }));
            
            modelBuilder.Entity<Permission>().Property(p => p.Code)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_Code")
                    {
                        IsUnique = true
                    }));


            //modelBuilder.Entity<Tag>().Property(t => t.Name)
            //   .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //       new IndexAnnotation(new IndexAttribute("IX_Name")
            //       {
            //           IsUnique = true
            //       }));

            //modelBuilder.Entity<CustomerMonthlyDeposit>().Property(d => d.Month)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_Unique")
            //        {
            //            IsUnique = true,
            //            Order = 1
            //        }));
            //modelBuilder.Entity<CustomerMonthlyDeposit>().Property(r => r.Username)
            //.HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //    new IndexAnnotation(new IndexAttribute("IX_Unique")
            //    {
            //        IsUnique = true,
            //        Order = 2
            //    }));

            modelBuilder.Entity<PaymentProvider>().Property(p => p.Name)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_Name")
                    {
                        IsUnique = true
                    }));

            modelBuilder.Entity<Bank>().Property(p => p.Name)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_Name")
                    {
                        IsUnique = true
                    }));
            modelBuilder.Entity<Bank>().Property(b => b.Code)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_Code")
                    {
                        IsUnique = true
                    }));

            modelBuilder.Entity<Bank>().Property(b => b.Code)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_Code")
                    {
                        IsUnique = true
                    }));

            //modelBuilder.Entity<CustomerAG>().Property(p => p.Username)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_Username")
            //        {
            //            IsUnique = true
            //        }));
            //modelBuilder.Entity<CustomerAG>().Property(p => p.GameUsername)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_GameUsername")
            //        {
            //            IsUnique = true
            //        }));

            //modelBuilder.Entity<CustomerEA>().Property(p => p.Username)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_Username")
            //        {
            //            IsUnique = true
            //        }));
            //modelBuilder.Entity<CustomerEA>().Property(p => p.GameUsername)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_GameUsername")
            //        {
            //            IsUnique = true
            //        }));

            //modelBuilder.Entity<CustomerBbin>().Property(p => p.Username)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_Username")
            //        {
            //            IsUnique = true
            //        }));
            //modelBuilder.Entity<CustomerBbin>().Property(p => p.GameUsername)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_GameUsername")
            //        {
            //            IsUnique = true
            //        }));

            //modelBuilder.Entity<CustomerSportsbook>().Property(p => p.Username)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_Username")
            //        {
            //            IsUnique = true
            //        }));
            //modelBuilder.Entity<CustomerSportsbook>().Property(p => p.GameUsername)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_GameUsername")
            //        {
            //            IsUnique = true
            //        }));

            //modelBuilder.Entity<CustomerPT>().Property(p => p.Username)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_Username")
            //        {
            //            IsUnique = true
            //        }));
            //modelBuilder.Entity<CustomerPT>().Property(p => p.GameUsername)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_GameUsername")
            //        {
            //            IsUnique = true
            //        }));

            //modelBuilder.Entity<CustomerQT>().Property(p => p.Username)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_Username")
            //        {
            //            IsUnique = true
            //        }));
            //modelBuilder.Entity<CustomerQT>().Property(p => p.GameUsername)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_GameUsername")
            //        {
            //            IsUnique = true
            //        }));

            //modelBuilder.Entity<CustomerLB>().Property(p => p.Username)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_Username")
            //        {
            //            IsUnique = true
            //        }));
            //modelBuilder.Entity<CustomerLB>().Property(p => p.GameUsername)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_GameUsername")
            //        {
            //            IsUnique = true
            //        }));

            //modelBuilder.Entity<CustomerKG>().Property(p => p.Username)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_Username")
            //        {
            //            IsUnique = true
            //        }));
            //modelBuilder.Entity<CustomerKG>().Property(p => p.GameUsername)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(new IndexAttribute("IX_GameUsername")
            //        {
            //            IsUnique = true
            //        }));

            modelBuilder.Entity<PaymentChannel>().Property(p => p.Name)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_Name")
                    {
                        IsUnique = true
                    }));
            modelBuilder.Entity<PaymentChannel>().Property(p => p.CommissionPercentage)
                .HasPrecision(18, 4);

            modelBuilder.Entity<HttpPayment>().Property(p => p.Response).HasMaxLength(2000);

            //modelBuilder.Entity<EntityLog>().Property(p => p.Data).HasMaxLength(4000);
            //modelBuilder.Entity<EntityLog>().Property(p => p.Details).HasMaxLength(4000);

            modelBuilder.Entity<SystemConfiguration>().Property(c => c.Value).HasMaxLength(8000);

            //modelBuilder.Entity<CustomerWithdrawal>()
            //    .HasOptional(w => w.Reference)
            //    .WithMany();

            modelBuilder.Entity<SystemOrderNumber>().HasIndex(n => n.Value).IsUnique();

            //modelBuilder.Entity<AgentWithdrawal>()
            //    .HasOptional(w => w.Reference)
            //    .WithMany();

            //modelBuilder.Entity<GameTransaction>()
            //    .HasIndex(t => t.GameId)
            //    .IsUnique();

            //modelBuilder.Entity<Agent>().HasIndex(a => a.SafeCode).IsUnique();

            //modelBuilder.Entity<NewPromotion>().Property(p => p.Platforms).IsMaxLength();

            modelBuilder.Entity<Merchant>().Property(c => c.Username)
                .HasMaxLength(50)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_Username")
                    {
                        IsUnique = true
                    }));

            modelBuilder.Entity<MerchantWithdrawal>()
                .HasOptional(w => w.Reference)
                .WithMany();
        }
    }
}
