﻿using GW.PC.Core;
using GW.PC.Models;
using Newtonsoft.Json;
using System;

namespace GW.PC.Services.EF.Admin
{
    public class CustomerLogService : CreatableEntityService<CustomerLog>
    {
        public static void Add(GWContext ctx, CustomerLogType type, string username, DateTime? createdAt = null, params object[] contents)
        {
            ctx.CustomerLogs.Add(new CustomerLog
            {
                Content = JsonConvert.SerializeObject(contents, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                }),
                CreatedAt = createdAt ?? DateTime.Now.ToE8(),
                Type = type,
                Username = username
            });
        }
    }
}
