﻿using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class PaymentProviderService : CreatableEntityService<PaymentProvider>
    {
        public async Task<PaymentProvider> Save(PaymentProvider pp)
        {
            PaymentProvider result = null;

            using (var ctx = new PCContext())
            {
                if (pp.Id == 0)
                {
                    await CheckUniqueness(p => p.Name.Equals(pp.Name, StringComparison.OrdinalIgnoreCase));

                    pp.Status = EntityStatus.Enabled;

                    ctx.PaymentProviders.Add(pp);

                    result = pp;
                }
                else
                {
                    var dbPP = await EnsureEntity(pp.Id, ctx);

                    dbPP.Name = pp.Name;
                    dbPP.Status = pp.Status;

                    result = dbPP;
                }

                await ctx.SaveChangesAsync();

                return result;
            }
        }

        public decimal GetEnabledTotalBalance()
        {
            using (var ctx = new PCContext())
            {
                return ctx.PaymentProviders.Where(p => p.Status == EntityStatus.Enabled)
                    .Sum(p => p.Balance);
            }
        }

        protected override IQueryable<PaymentProvider> OrderBy(IQueryable<PaymentProvider> source, string orderBy = null, bool isAscending = false)
        {
            return isAscending ? source.OrderBy(o => o.CreatedAt) :
                source.OrderByDescending(o => o.CreatedAt);
        }
    }
}
