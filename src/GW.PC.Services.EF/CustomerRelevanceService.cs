﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GW.PC.Models;
using System.Linq;
using System;
using GW.PC.Core;

namespace GW.PC.Services.EF.Admin
{
    public class CustomerRelevanceService : CreatableEntityService<CustomerRelevance>
    {
        public override async Task AddRange(IEnumerable<CustomerRelevance> items)
        {
            using (var ctx = new GWContext())
            {
                if (items.Any(
                    i => !ctx.Customers.Any(
                        c => c.Username.Equals(i.PrincipalCustomerUsername, StringComparison.OrdinalIgnoreCase)) ||
                    !ctx.Customers.Any(
                        c => c.Username.Equals(i.AssociateCustomerUsername, StringComparison.OrdinalIgnoreCase))))
                {
                    throw new BusinessException(Constants.Messages.CustomerNotFound);
                }

                ctx.CustomerRelevances.AddRange(items);

                await ctx.SaveChangesAsync();
            }
        }
    }
}
