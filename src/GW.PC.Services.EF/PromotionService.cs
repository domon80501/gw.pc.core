﻿using System.Threading.Tasks;
using GW.PC.Models;
using System.Data.Entity;
using GW.PC.Core;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json;

namespace GW.PC.Services.EF
{
    public class PromotionService : StatusfulEntityService<NewPromotion>
    {
        public async Task Save(NewPromotion promotion)
        {
            using (var ctx = new GWContext())
            {
                if (await ctx.NewPromotions.AnyAsync(
                    p => p.Name == promotion.Name &&
                    p.Id != promotion.Id))
                {
                    throw new BusinessException(Constants.Messages.ObjectDuplicated);
                }
            }

            if (promotion.Id == 0)
            {
                promotion.Code = RandomGenerator.RandomNumber(8);

                await base.Add(promotion);
            }
            else
            {
                using (var ctx = new GWContext())
                {
                    var dbPromotion = await ctx.NewPromotions.FindAsync(promotion.Id);

                    dbPromotion.EndsAt = promotion.EndsAt;
                    dbPromotion.Name = promotion.Name;
                    dbPromotion.Order = promotion.Order;
                    dbPromotion.Platforms = promotion.Platforms;
                    dbPromotion.StartsAt = promotion.StartsAt;
                    dbPromotion.Url = promotion.Url;
                    dbPromotion.PopUp = promotion.PopUp;

                    await ctx.SaveChangesAsync();
                }
            }
        }

        public async Task<IEnumerable<NewPromotion>> GetPromotions(EntityStatus? status, DateTime? now, bool popUp)
        {
            using (var ctx = new GWContext())
            {
                IQueryable<NewPromotion> query = ctx.NewPromotions.Include(p => p.Missions)
                    .Where(p => p.PopUp == popUp);

                if (now.HasValue)
                {
                    query = query.Where(p => now >= p.StartsAt && (!p.EndsAt.HasValue || now <= p.EndsAt.Value));
                }
                if (status.HasValue)
                {
                    query = query.Where(p => p.Status == status.Value);
                }

                return await query.ToListAsync();
            }
        }
    }
}
