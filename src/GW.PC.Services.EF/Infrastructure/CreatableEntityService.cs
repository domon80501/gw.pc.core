﻿using GW.PC.Models;
using System.Linq;

namespace GW.PC.Services.EF
{
    public abstract class CreatableEntityService<T> : EntityService<T>
        where T : class, ICreatedAt, IEntity
    {
        protected override IQueryable<T> OrderBy(IQueryable<T> source, string orderBy = null, bool isAscending = false)
        {
            return isAscending ? source.OrderBy(o => o.CreatedAt)
                : source.OrderByDescending(o => o.CreatedAt);
        }
    }
}