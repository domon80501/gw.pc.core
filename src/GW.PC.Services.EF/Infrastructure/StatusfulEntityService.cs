﻿using GW.PC.Models;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public abstract class StatusfulEntityService<T> : CreatableEntityService<T>
        where T : class, IEntity, IEntityStatus , ICreatedAt
    {
        public async virtual Task UpdateEntityStatus(int id, EntityStatus status, int operatorId)
        {
            using (var ctx = new PCContext())
            {
                var entity = await EnsureEntity<T>(id, ctx);
                var user = await EnsureEntity<User>(operatorId, ctx);

                entity.Status = status;

                AddUserLog(ctx, user.Username, "修改了状态");

                await ctx.SaveChangesAsync();
            }
        }
    }
}