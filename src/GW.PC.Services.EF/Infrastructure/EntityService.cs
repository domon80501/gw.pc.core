﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using X.PagedList;
using FluentValidation;

namespace GW.PC.Services.EF
{
    public abstract class EntityService<T>
        where T : class, IEntity
    {
        public async Task<IEnumerable<TResult>> SqlQuery<TResult>(string sql, params SqlParameter[] parameters)
        {
            using (var ctx = new PCContext())
            {
                return await ctx.Database.SqlQuery<TResult>(sql, parameters.Cast<ICloneable>().Select(p => p.Clone()).ToArray()).ToListAsync();
            }
        }

        public async Task<IEnumerable<TResult>> SqlQueryOutput<TResult>(string sql, params SqlParameter[] parameters)
        {
            using (var ctx = new PCContext())
            {
                return await ctx.Database.SqlQuery<TResult>(sql, parameters).ToListAsync();
            }
        }

        public virtual async Task<T> Add(T item)
        {
            using (var ctx = new PCContext())
            {
                ctx.Set<T>().Add(item);

                await ctx.SaveChangesAsync();

                return item;
            }
        }

        public virtual async Task AddRange(IEnumerable<T> items)
        {
            using (var ctx = new PCContext())
            {
                ctx.Set<T>().AddRange(items);

                await ctx.SaveChangesAsync();
            }
        }

        public virtual async Task<IEnumerable<T>> GetAll()
        {
            using (var ctx = new PCContext())
            {
                return await ctx.Set<T>().AsNoTracking().ToListAsync();
            }
        }

        public virtual async Task<IEnumerable<T>> GetAll(params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<T> query = ctx.Set<T>();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        query = query.Include(include);
                    }
                }

                return await query.ToListAsync();
            }
        }

        public virtual async Task<T> Find(int id)
        {
            using (var ctx = new PCContext())
            {
                return await ctx.Set<T>().FindAsync(id);
            }
        }

        public virtual async Task<T> Find(int id, params string[] includes)
        {
            if (id <= 0)
            {
                return null;
            }

            using (var ctx = new PCContext())
            {
                IQueryable<T> source = ctx.Set<T>();

                foreach (var include in includes)
                {
                    source = source.Include(include);
                }

                return await source.SingleOrDefaultAsync(o => o.Id == id);
            }
        }

        public virtual async Task<IEnumerable<T>> FindByIds(List<int> ids)
        {
            using (var ctx = new PCContext())
            {
                return await ctx.Set<T>().AsNoTracking().Where(o => ids.Contains(o.Id)).ToListAsync();
            }
        }

        public virtual async Task<IEnumerable<T>> FindByIds(List<int> ids, params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                var source = ctx.Set<T>().AsNoTracking();

                foreach (var include in includes)
                {
                    source = source.Include(include);
                }

                return await source.Where(o => ids.Contains(o.Id)).ToListAsync();
            }
        }

        public virtual async Task<T> Update(T item)
        {
            using (var ctx = new PCContext())
            {
                ctx.Entry(item).State = EntityState.Modified;

                await ctx.SaveChangesAsync();

                return item;
            }
        }

        public virtual async Task<IPagedList<T>> Search(
            IEnumerable<Expression<Func<T, bool>>> query = null,
            int pageNumber = 1,
            int pageSize = 10,
            string orderBy = null,
            bool isAscending = false,
            params string[] includes)
        {
            pageNumber = pageNumber > 0 ? pageNumber : 1;
            pageSize = pageSize > 0 ? pageSize : 10;

            using (var ctx = new PCContext())
            {
                IQueryable<T> result = ctx.Set<T>().AsNoTracking();

                if (query != null)
                {
                    foreach (var predicate in query)
                    {
                        result = result.Where(predicate);
                    }
                }

                var recordCount = await result.CountAsync();
                var pageCount = (int)Math.Ceiling((double)recordCount / pageSize);
                if (pageCount > 0 && pageNumber > pageCount)
                {
                    pageNumber = pageCount;
                }

                if (includes != null)
                {
                    foreach (var include in includes)
                    {
                        result = result.Include(include);
                    }
                }

                var data = await OrderBy(result, orderBy, isAscending)
                    .ToPagedListAsync(pageNumber, pageSize);

                return data;
            }
        }

        public virtual async Task<IEnumerable<T>> SearchAll(
            IEnumerable<Expression<Func<T, bool>>> query = null,
            string orderBy = null,
            bool isAscending = false,
            params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<T> result = ctx.Set<T>().AsNoTracking();

                if (query != null)
                {
                    foreach (var predicate in query)
                    {
                        result = result.Where(predicate);
                    }
                }

                if (includes != null)
                {
                    foreach (var include in includes)
                    {
                        result = result.Include(include);
                    }
                }

                var data = await OrderBy(result, orderBy, isAscending).ToListAsync();

                return data;
            }
        }

        public virtual async Task<decimal> SearchAmount(Expression<Func<T, decimal?>> amountSelector,
            IEnumerable<Expression<Func<T, bool>>> query = null)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<T> result = ctx.Set<T>().AsNoTracking();

                if (query != null)
                {
                    foreach (var predicate in query)
                    {
                        result = result.Where(predicate);
                    }
                }

                return (await result.SumAsync(amountSelector)).GetValueOrDefault();
            }
        }

        public async Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> query)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<T> result = ctx.Set<T>().AsNoTracking();

                return await result.Where(query).ToListAsync();
            }
        }

        public async Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> query, params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<T> result = ctx.Set<T>().AsNoTracking();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        result = result.Include(include);
                    }
                }

                return await result.Where(query).ToListAsync();
            }
        }

        public async Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> query, int pageNumber, int pageSize, string prpotyName, bool descending, params string[] includes)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<T> result = ctx.Set<T>().AsNoTracking();

                if (includes?.Length > 0)
                {
                    foreach (var include in includes)
                    {
                        result = result.Include(include);
                    }
                }

                result = result.Where(query);

                if (!string.IsNullOrEmpty(prpotyName))
                {
                    result = descending ? result.OrderByDescending(o => prpotyName) : result.OrderBy(o => prpotyName);
                }
                else
                {
                    result = result.OrderByDescending(o => o.Id);
                }

                result = result.Skip(--pageNumber * pageSize).Take(pageSize);

                return await result.ToListAsync();
            }
        }

        public async Task<int> QueryCountAsync(Expression<Func<T, bool>> query)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<T> result = ctx.Set<T>().AsNoTracking();

                return await result.Where(query).CountAsync();
            }
        }

        public virtual async Task<int> SearchCount(params Expression<Func<T, bool>>[] query)
        {
            using (var ctx = new PCContext())
            {
                IQueryable<T> result = ctx.Set<T>().AsNoTracking();

                if (query != null)
                {
                    foreach (var predicate in query)
                    {
                        result = result.Where(predicate);
                    }
                }

                return await result.CountAsync();
            }
        }

        protected virtual IQueryable<T> OrderBy(
            IQueryable<T> source,
            string orderBy = null,
            bool isAscending = false)
        {
            return isAscending ? source.OrderBy(c => c.Id) :
                source.OrderByDescending(c => c.Id);
        }

        protected virtual async Task<T> EnsureEntity(int id, PCContext ctx)
        {
            var entity = await ctx.Set<T>().FindAsync(id);
            if (entity == null)
            {
                throw new Exception(Constants.Messages.NotFound);
            }

            return entity;
        }

        protected virtual async Task<TEntity> EnsureEntity<TEntity>(int id, PCContext ctx, params string[] includes)
            where TEntity : class, IEntity
        {
            IQueryable<TEntity> query = ctx.Set<TEntity>();

            if (includes != null)
            {
                foreach (var include in includes)
                {
                    query = query.Include(include);
                }
            }

            var entity = await query.SingleOrDefaultAsync(e => e.Id == id);
            if (entity == null)
            {
                throw new Exception(Constants.Messages.NotFound);
            }

            return entity;
        }

        protected virtual async Task<TEntity> EnsureEntity<TEntity>(string username, PCContext ctx, params string[] includes)
            where TEntity : class, IEntity, IUsername
        {
            IQueryable<TEntity> query = ctx.Set<TEntity>();

            if (includes != null)
            {
                foreach (var include in includes)
                {
                    query = query.Include(include);
                }
            }

            var entity = await query.SingleOrDefaultAsync(e => e.Username.Equals(username, StringComparison.OrdinalIgnoreCase));
            if (entity == null)
            {
                throw new BusinessException(Constants.Messages.UsernameNotFound);
            }

            return entity;
        }

        protected async Task CheckUniqueness(params Expression<Func<T, bool>>[] predicates)
        {
            using (var ctx = new PCContext())
            {
                foreach (var predicate in predicates)
                {
                    if (await ctx.Set<T>().AnyAsync(predicate))
                    {
                        throw new BusinessException(Constants.Messages.ObjectDuplicated);
                    }
                }
            }
        }

        protected void AddUserLog(PCContext ctx, string username, string content)
        {
            var log = new UserLog
            {
                Username = username,
                Type = UserLogType.Audit,
                CreatedAt = DateTime.Now.ToE8(),
                Content = content
            };

            ctx.UserLogs.Add(log);
        }

        //protected void SendCustomerMessage(PCContext ctx, string username, string title, string content)
        //{
        //    var message = new CustomerMessage
        //    {
        //        Content = content,
        //        CreatedAt = DateTime.Now,
        //        FromUsername = Constants.SystemAccountUsername,
        //        Status = CustomerMessageStatus.Unread,
        //        Title = title,
        //        ToUsername = username,
        //        Type = CustomerMessageType.SystemNotification
        //    };

        //    ctx.CustomerMessages.Add(message);
        //}

        protected async Task Validate<TValidator>(T entity, string ruleSet)
            where TValidator : AbstractValidator<T>, new()
        {
            var result = await new TValidator().ValidateAsync(entity, ruleSet: ruleSet);

            if (!result.IsValid)
            {
                throw new BusinessException(string.Join("\r\n", result.Errors.Select(x => x.ErrorMessage)), "1");
            }
        }

        protected void CheckNull<TEntity>(TEntity entity)
        {
            if (entity == null)
            {
                throw new BusinessException(Constants.Messages.NotFound);
            }
        }

        protected void CheckNull<TEntity>(TEntity entity, string parameterName)
        {
            if (string.IsNullOrEmpty(entity?.ToString()))
            {
                throw new BusinessException(
                    string.Format(Constants.MessageTemplates.NullObject, parameterName));
            }
        }
    }
}