﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class GameTransferService : CreatableEntityService<GameTransfer>
    {
        public async Task Audit(int id, bool result, int auditedById)
        {
            using (var ctx = new GWContext())
            {
                var transfer = await EnsureEntity(id, ctx);

                if (transfer.Status == GameTransferStatus.InProgress)
                {
                    transfer.Status = result ? GameTransferStatus.Successful : GameTransferStatus.Failed;

                    await Finalize(ctx, transfer, auditedById);

                    await ctx.SaveChangesAsync();
                }
            }
        }

        public override async Task<GameTransfer> Update(GameTransfer transfer)
        {
            using (var ctx = new GWContext())
            {
                if (transfer.Status == GameTransferStatus.Successful ||
                    transfer.Status == GameTransferStatus.Failed)
                {
                    await Finalize(ctx, transfer, null);
                }

                ctx.Entry(transfer).State = EntityState.Modified;

                await ctx.SaveChangesAsync();

                return transfer;
            }
        }

        public async Task<GameTransfer> UpdateGameBalanceAfterTranser(int id, decimal balance)
        {
            using (var ctx = new GWContext())
            {
                var transfer = await EnsureEntity(id, ctx);

                if (transfer.IsDeposit)
                {
                    transfer.AfterBalanceTo = balance;
                }
                else
                {
                    transfer.AfterBalanceFrom = balance;
                }

                ctx.Entry(transfer).State = EntityState.Modified;

                await ctx.SaveChangesAsync();

                return transfer;
            }
        }

        public override Task<GameTransfer> Add(GameTransfer item)
        {
            throw new NotSupportedException();
        }

        internal async Task<GameTransfer> Add(GWContext ctx, string username, decimal amount, bool isDeposit, string ip, string ipAddress, GameTransferSource? source, int? sourceId)
        {
            var wallet = await EnsureEntity<Wallet>(username, ctx);
            var games = await ctx.Games.ToListAsync();

            var transfer = new GameTransfer
            {
                Amount = amount,
                CreatedAt = DateTime.Now.ToE8(),
                MerchantOrderNumber = RandomGenerator.NewMerchantOrderNumber(),
                Status = GameTransferStatus.InProgress,
                Username = username,
                TransferIP = ip,
                TransferAddress = ipAddress,
                Source = source,
                SourceId = sourceId
            };
            if (isDeposit)
            {
                transfer.FromId = null;

                var toGame = games.SingleOrDefault(g => g.Name == Constants.GameNames.Default);
                if (toGame == null ||
                    toGame.Status != EntityStatus.Enabled ||
                    !toGame.AllowTransfer)
                {
                    throw new BusinessException("该游戏已下线或禁止转账");
                }

                transfer.ToId = toGame.Id;
            }
            else
            {
                transfer.ToId = null;

                var fromGame = games.SingleOrDefault(g => g.Name == Constants.GameNames.Default);
                if (fromGame == null ||
                    fromGame.Status != EntityStatus.Enabled ||
                    !fromGame.AllowTransfer)
                {
                    throw new BusinessException("该游戏已下线或禁止转账");
                }

                transfer.FromId = fromGame.Id;
            }

            if (!transfer.IsValid)
            {
                throw new ArgumentException($"Invalid transfer from {transfer.FromId} to {transfer.ToId}");
            }

            if (transfer.IsDeposit)
            {
                transfer.BeforeBalanceFrom = wallet.Balance;
            }
            else
            {
                transfer.BeforeBalanceTo = wallet.Balance;
            }

            ctx.GameTransfers.Add(transfer);

            await ctx.SaveChangesAsync();

            EntityLogService.Add(
                ctx,
                EntityLogContent.GameTransfer_Created,
                EntityLogTargetType.GameTransfer,
                transfer.Id,
                null,
                transfer.Source.GetDisplayName(), transfer.Amount);

            return transfer;
        }

        internal async Task<GameTransfer> AddPending(GWContext ctx, string username, decimal amount, bool isDeposit, string ip, string ipAddress, GameTransferSource? source, int? sourceId)
        {
            var wallet = await EnsureEntity<Wallet>(username, ctx);
            var games = await ctx.Games.ToListAsync();

            var transfer = new GameTransfer
            {
                Amount = amount,
                CreatedAt = DateTime.Now.ToE8(),
                MerchantOrderNumber = RandomGenerator.NewMerchantOrderNumber(),
                Status = GameTransferStatus.Pending,
                Username = username,
                TransferIP = ip,
                TransferAddress = ipAddress,
                Source = source,
                SourceId = sourceId
            };
            if (isDeposit)
            {
                transfer.FromId = null;

                var toGame = games.SingleOrDefault(g => g.Name == Constants.GameNames.Default);
                if (toGame == null ||
                    toGame.Status != EntityStatus.Enabled ||
                    !toGame.AllowTransfer)
                {
                    throw new BusinessException("该游戏已下线或禁止转账");
                }

                transfer.ToId = toGame.Id;
            }
            else
            {
                transfer.ToId = null;

                var fromGame = games.SingleOrDefault(g => g.Name == Constants.GameNames.Default);
                if (fromGame == null ||
                    fromGame.Status != EntityStatus.Enabled ||
                    !fromGame.AllowTransfer)
                {
                    throw new BusinessException("该游戏已下线或禁止转账");
                }

                transfer.FromId = fromGame.Id;
            }

            if (!transfer.IsValid)
            {
                throw new ArgumentException($"Invalid transfer from {transfer.FromId} to {transfer.ToId}");
            }

            if (transfer.IsDeposit)
            {
                transfer.BeforeBalanceFrom = wallet.Balance;
            }
            else
            {
                transfer.BeforeBalanceTo = wallet.Balance;
            }

            ctx.GameTransfers.Add(transfer);

            await ctx.SaveChangesAsync();

            EntityLogService.Add(
                ctx,
                EntityLogContent.GameTransfer_Created,
                EntityLogTargetType.GameTransfer,
                transfer.Id,
                null,
                transfer.Source.GetDisplayName(), transfer.Amount);

            return transfer;
        }

        public async Task<GameTransfer> ProcessNext()
        {
            using (var ctx = new GWContext())
            {
                var transfer = await ctx.GameTransfers.FirstOrDefaultAsync(t => t.Status == GameTransferStatus.Pending);
                if (transfer != null)
                {
                    return transfer;
                }

                transfer = await ctx.GameTransfers.FirstOrDefaultAsync(t => t.Status == GameTransferStatus.Failed);
                if (transfer != null)
                {
                    return transfer;
                }

                return await ctx.GameTransfers.FirstOrDefaultAsync(t => t.Status == GameTransferStatus.InProgress);
            }
        }

        public async Task<GameTransfer> PreTransfer(int id, decimal gameBalance)
        {
            using (var ctx = new GWContext())
            {
                var transfer = await EnsureEntity(id, ctx);

                if (transfer.IsDeposit)
                {
                    if (!transfer.HasFrozen)
                    {
                        var wallet = await EnsureEntity<Wallet>(transfer.Username, ctx);

                        if (wallet.Balance < transfer.Amount)
                        {
                            throw new BusinessException(Constants.Messages.InsufficientWalletBalance);
                        }

                        wallet.Balance -= transfer.Amount;
                        wallet.FrozenAmount += transfer.Amount;

                        AddWalletTransaction(ctx, 0 - transfer.Amount, wallet.Balance, WalletTransactionType.GameDeposit, transfer.Username, transfer.Id);

                        transfer.HasFrozen = true;
                    }

                    transfer.BeforeBalanceTo = gameBalance;
                }
                else
                {
                    transfer.BeforeBalanceFrom = gameBalance;
                }

                ctx.Entry(transfer).State = EntityState.Modified;

                await ctx.SaveChangesAsync();

                return transfer;
            }
        }

        public async Task<GameTransfer> PreTransfer(int id)
        {
            using (var ctx = new GWContext())
            {
                var transfer = await EnsureEntity(id, ctx);

                if (transfer.Status != GameTransferStatus.Pending)
                {
                    throw new BusinessException(string.Format(Constants.MessageTemplates.InvalidCurrentStatus, transfer.Status.GetDisplayName()));
                }

                transfer.Status = GameTransferStatus.InProgress;

                if (transfer.IsDeposit)
                {
                    var wallet = await EnsureEntity<Wallet>(transfer.Username, ctx);

                    if (wallet.Balance < transfer.Amount)
                    {
                        throw new BusinessException(Constants.Messages.InsufficientWalletBalance);
                    }

                    wallet.Balance -= transfer.Amount;
                    wallet.FrozenAmount += transfer.Amount;

                    AddWalletTransaction(ctx, 0 - transfer.Amount, wallet.Balance, WalletTransactionType.GameDeposit, transfer.Username, transfer.Id);
                }

                ctx.Entry(transfer).State = EntityState.Modified;

                await ctx.SaveChangesAsync();

                return transfer;
            }
        }

        public async Task<GameTransfer> Get(string username, GameTransferSource source, int sourceId)
        {
            using (var ctx = new GWContext())
            {
                return await ctx.GameTransfers.SingleOrDefaultAsync(
                    t => t.Source == source &&
                    t.SourceId == sourceId);
            }
        }

        public async Task Close(int id, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var transfer = await EnsureEntity(id, ctx);

                if (transfer.Status != GameTransferStatus.Failed)
                {
                    throw new BusinessException(
                        string.Format(
                            Constants.MessageTemplates.InvalidCurrentStatus, transfer.Status.GetDisplayName()));
                }
                if (transfer.IsDeposit)
                {
                    throw new NotSupportedException("Can't close a deposit transfer");
                }

                transfer.Status = GameTransferStatus.FailedAndClosed;
                transfer.ClosedById = operatorId;
                transfer.ClosedAt = DateTime.Now.ToE8();

                await ctx.SaveChangesAsync();
            }
        }

        private async Task Finalize(GWContext ctx, GameTransfer transfer, int? auditedById)
        {
            var now = DateTime.Now;

            transfer.CompletedAt = now;
            if (auditedById.HasValue)
            {
                transfer.AuditedById = auditedById;

                // Add user log
                var user = await EnsureEntity<User>(auditedById.Value, ctx);
                AddUserLog(ctx, user.Username, $"游戏转账补单审核{(transfer.Status == GameTransferStatus.Successful ? "成功" : "失败")}[{transfer.Username}]");
            }

            // Update wallet
            var wallet = await EnsureEntity<Wallet>(transfer.Username, ctx);
            if (transfer.Status == GameTransferStatus.Successful)
            {
                if (transfer.IsDeposit)
                {
                    wallet.FrozenAmount -= transfer.Amount;
                    transfer.AfterBalanceFrom = wallet.Balance;
                }
                else
                {
                    wallet.Balance += transfer.Amount;
                    transfer.AfterBalanceTo = wallet.Balance;

                    AddWalletTransaction(ctx, transfer.Amount, wallet.Balance, WalletTransactionType.GameWithdrawal, transfer.Username, transfer.Id, now);
                }
            }
            else if (transfer.Status == GameTransferStatus.Failed)
            {
                if (transfer.IsDeposit)
                {
                    transfer.AfterBalanceFrom = wallet.Balance;
                }
                else
                {
                    transfer.Status = GameTransferStatus.FailedAndClosed;
                    transfer.AfterBalanceTo = wallet.Balance;
                }
            }
        }
    }
}
