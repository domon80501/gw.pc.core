﻿using System.Linq;
using GW.PC.Models;

namespace GW.PC.Services.EF.Admin
{
    public class FollowUpService : CreatableEntityService<FollowUp>
    {
        protected override IQueryable<FollowUp> OrderBy(IQueryable<FollowUp> source, string orderBy = null, bool isAscending = false)
        {
            var result = base.OrderBy(source, orderBy, isAscending);

            // 2nd ordering.
            result = result.OrderBy(f => f.IsSolved);

            return result;
        }
    }
}
