﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GW.PC.Models;
using System.Linq;
using System;
using GW.PC.Core;

namespace GW.PC.Services.EF.Admin
{
    public class MerchantRelevanceService : CreatableEntityService<MerchantRelevance>
    {
        public override async Task AddRange(IEnumerable<MerchantRelevance> items)
        {
            using (var ctx = new APContext())
            {
                if (items.Any(
                    i => !ctx.Merchants.Any(
                        c => c.Username.Equals(i.PrincipalMerchantUsername, StringComparison.OrdinalIgnoreCase)) ||
                    !ctx.Merchants.Any(
                        c => c.Username.Equals(i.AssociateMerchantUsername, StringComparison.OrdinalIgnoreCase))))
                {
                    throw new BusinessException(Constants.Messages.MerchantNotFound);
                }

                ctx.MerchantRelevances.AddRange(items);

                await ctx.SaveChangesAsync();
            }
        }
    }
}
