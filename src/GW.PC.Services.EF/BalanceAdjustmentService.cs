﻿using GW.PC.Core;
using GW.PC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Transactions;

namespace GW.PC.Services.EF.Admin
{
    public class BalanceAdjustmentService : CreatableEntityService<BalanceAdjustment>
    {
        public override async Task<BalanceAdjustment> Add(BalanceAdjustment item)
        {
            using (var ctx = new GWContext())
            {
                if (ctx.Customers.Any(c => c.Username == item.Username))
                {
                    item.SystemOrderNumber = await SystemOrderNumberService.Next();

                    return await base.Add(item);
                }
                else
                {
                    throw new BusinessException(Constants.Messages.CustomerNotFound);
                }
            }
        }

        public async Task Audit(int id, bool result, int auditedById)
        {
            var now = DateTime.Now.ToE8();

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (var ctx = new GWContext())
                {
                    var ba = await EnsureEntity(id, ctx);

                    ba.Status = result ? AuditStatus.Approved : AuditStatus.Declined;
                    ba.AuditedAt = now;
                    ba.AuditedById = auditedById;

                    if (result)
                    {
                        // Update customer's wallet balance.
                        var wallet = await ctx.Wallets.SingleOrDefaultAsync(w => w.Username == ba.Username);
                        if (wallet != null)
                        {
                            ba.BeforeBalance = wallet.Balance;

                            wallet.Balance += ba.Amount;

                            ba.AfterBalance = wallet.Balance;
                        }

                        AddWalletTransaction(ctx, ba.Amount, wallet.Balance, WalletTransactionType.BalanceAdjustment, ba.Username, ba.Id, now);

                        await new GameTransferService().AddPending(ctx, ba.Username, Math.Abs(ba.Amount), ba.Amount > 0, null, null, GameTransferSource.BalanceAdjustmentApproved, ba.Id);
                    }

                    await ctx.SaveChangesAsync();
                }

                scope.Complete();
            }
        }

        public override async Task<decimal> SearchAmount(Expression<Func<BalanceAdjustment, decimal?>> amountSelector, IEnumerable<Expression<Func<BalanceAdjustment, bool>>> query = null)
        {
            using (var ctx = new GWContext())
            {
                IQueryable<BalanceAdjustment> result = ctx.Set<BalanceAdjustment>().Where(
                    d => d.Status == AuditStatus.Approved)
                .AsNoTracking();

                if (query != null)
                {
                    foreach (var predicate in query)
                    {
                        result = result.Where(predicate);
                    }
                }

                return (await result.SumAsync(amountSelector)).GetValueOrDefault();
            }
        }
    }
}
