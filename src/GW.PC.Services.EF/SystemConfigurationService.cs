﻿using GW.PC.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.PC.Services.EF
{
    public class SystemConfigurationService : EntityService<SystemConfiguration>
    {
        public static async Task<SystemConfigurationDto> Get()
        {
            using (var ctx = new PCContext())
            {
                var config = await ctx.SystemConfigurations.SingleOrDefaultAsync();

                return JsonConvert.DeserializeObject<SystemConfigurationDto>(config.Value);
            }
        }

        public static async Task Set(SystemConfigurationDto config)
        {
            using (var ctx = new PCContext())
            {
                var dbConfig = await ctx.SystemConfigurations.SingleOrDefaultAsync();

                dbConfig.Value = JsonConvert.SerializeObject(config, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });

                await ctx.SaveChangesAsync();
            }
        }
    }
}
