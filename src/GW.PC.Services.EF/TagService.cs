﻿using GW.PC.Core;
using GW.PC.Models;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GW.Services.EF.Admin
{
    public class TagService : CreatableEntityService<Tag>
    {
        public async Task<Tag> Save(Tag tag)
        {
            using (var ctx = new GWContext())
            {
                if (tag.Id == 0)
                {
                    if (await ctx.Tags.AnyAsync(t => t.Name == tag.Name))
                    {
                        throw new BusinessException(Constants.Messages.ObjectDuplicated);
                    }

                    ctx.Tags.Add(tag);
                }
                else
                {
                    if (await ctx.Tags.AnyAsync(t => t.Name == tag.Name &&
                        t.Id != tag.Id))
                    {
                        throw new BusinessException(Constants.Messages.ObjectDuplicated);
                    }

                    var dbTag = await EnsureEntity(tag.Id, ctx);

                    dbTag.Name = tag.Name;
                }

                await ctx.SaveChangesAsync();

                return tag;
            }
        }

        public async Task Delete(int id, int operatorId)
        {
            using (var ctx = new GWContext())
            {
                var tag = await EnsureEntity(id, ctx);

                ctx.Tags.Remove(tag);

                await ctx.SaveChangesAsync();
            }
        }
    }
}
