﻿using System;

namespace GW.PC.Core
{
    public class ApiException : Exception
    {
        public ApiException(string message) : base(message) { }

        public ApiException(string message, Exception ex) : base(message, ex) { }
    }
}
