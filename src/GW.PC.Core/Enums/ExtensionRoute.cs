﻿
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Core
{
    public enum ExtensionRoute
    {
        Default = 1,
        [Display(Name = "JBP")]
        JBP = 2,
        [Display(Name = "CSF")]
        CSF = 3,
    }
}