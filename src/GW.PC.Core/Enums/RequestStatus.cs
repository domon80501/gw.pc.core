﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Core
{
    public enum RequestStatus
    {
        Unknow = 0,
        Success = 1,
        Fail = 2,
    }
}