﻿namespace GW.PC.Core
{
    public enum TransactionStatus
    {
        UnKnow = 0,
        Success = 1,
        Fail = 2,
        PendingPayment = 3
    }
}
