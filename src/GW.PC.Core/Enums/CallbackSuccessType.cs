﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GW.PC.Core
{
    public enum CallbackSuccessType
    {
        [Display(Name = "success")]
        success = 1,
        [Display(Name = "SUCCESS")]
        SUCCESS = 2,
        [Display(Name = "Success")]
        Success = 3,
        [Display(Name = "ok")]
        ok = 4,
        [Display(Name = "OK")]
        OK = 5,
        [Display(Name = "Ok")]
        Ok = 6,
        [Display(Name = "JBP")]
        JBP = 7,
    }
}