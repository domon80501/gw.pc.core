﻿using System.ComponentModel.DataAnnotations;

namespace GW.PC.Core
{
    /// <summary>
    /// 支付方式
    /// </summary>
    public enum PaymentMethod
    {
        [Display(Name = "微信", Order = 2)]
        WeChat = 1,
        [Display(Name = "支付宝", Order = 1)]
        Alipay = 2,
        [Display(Name = "在线支付")]
        OnlinePayment = 3,
        [Display(Name = "网银支付", Order = 6)]
        OnlineBanking = 4,
        [Display(Name = "充值卡", Order = 5)]
        PrepaidCard = 5,
        [Display(Name = "QQ钱包", Order = 3)]
        QQWallet = 6,
        [Display(Name = "银联支付", Order = 4)]
        QuickPay = 7,
        [Display(Name = "支付宝转卡", Order = 7)]
        OnlineToBankCard = 8,
        [Display(Name = "京东钱包", Order = 8)]
        JDWallet = 9,
        [Display(Name = "灌钱")]
        Internal = 101
    }
}