﻿using System;

namespace GW.PC.Core
{
    public static class Common
    {
        public static void CheckNull(object obj, string name)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(string.Format(Constants.MessageTemplates.NullObject, name));
            }
        }
    }
}
