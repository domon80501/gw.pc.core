﻿using Newtonsoft.Json;
using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GW.PC.Core
{
    public static class ExcelUtility
    {
        public static byte[] Export<T>(IEnumerable<T> data)
        {
            using (var package = new ExcelPackage())
            {
                var sheet = package.Workbook.Worksheets.Add("data");

                var properties = typeof(T).GetProperties()
                    .Where(pi => pi.GetCustomAttribute<ExportExcludeAttribute>(true) == null)
                    .OrderBy(pi =>
                    {
                        ExportOrderAttribute exportOrderAttribute = null;
                        if ((exportOrderAttribute = pi.GetCustomAttribute<ExportOrderAttribute>(true)) != null)
                        {
                            return exportOrderAttribute.Order;
                        }
                        else
                        {
                            return 0;
                        }
                    });

                WriteHeaders(sheet, properties);

                WriteValues(sheet, data, properties);

                sheet.Cells.AutoFitColumns();

                return package.GetAsByteArray();
            }
        }

        private static void WriteHeaders(ExcelWorksheet sheet, IEnumerable<PropertyInfo> properties)
        {
            for (int i = 0; i < properties.Count(); i++)
            {
                var pi = properties.ElementAt(i);

                JsonPropertyAttribute attribute = null;
                if ((attribute = pi.GetCustomAttribute<JsonPropertyAttribute>(true)) != null)
                {
                    sheet.SetValue(1, i + 1, attribute.PropertyName);
                }
                else
                {
                    sheet.SetValue(1, i + 1, pi.Name);
                }
            }
        }

        private static void WriteValues<T>(ExcelWorksheet sheet, IEnumerable<T> data, IEnumerable<PropertyInfo> properties)
        {
            for (int row = 0; row < data.Count(); row++)
            {
                for (int column = 0; column < properties.Count(); column++)
                {
                    sheet.SetValue(row + 2, column + 1, properties.ElementAt(column).GetValue(data.ElementAt(row)));
                }
            }
        }
    }
}
