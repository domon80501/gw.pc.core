﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Linq;

namespace GW.PC.Core
{
    public static class PasswordUtility
    {
        #region Encrypt/Decrypt
        // Used for accounts which need to retrieve the original plain text
        // for any reason.

        // This constant is used to determine the keysize of the encryption algorithm in bits.
        // We divide this by 8 within the code below to get the equivalent number of bytes.
        private const int Keysize = 256;


        // This constant determines the number of iterations for the password bytes generation function.
        private const int DerivationIterations = 1000;

        private const string Key = "14FFD873-9305-4824-BB41-8F793848AE6A";

        public static string Encrypt(string plainText)
        {
            // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
            // so that the same Salt and IV values can be used when decrypting.  
            var saltBytes = Generate256BitsOfRandomEntropy();
            var ivBytes = Generate256BitsOfRandomEntropy();
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            using (var password = new Rfc2898DeriveBytes(
                Key, saltBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var rm = GetRM())
                {
                    using (var encryptor = rm.CreateEncryptor(keyBytes, ivBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();

                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();

                                memoryStream.Close();
                                cryptoStream.Close();

                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        public static string Decrypt(string cipherText)
        {
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
            // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
            var saltBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            var ivBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).ToArray();

            using (var password = new Rfc2898DeriveBytes(
                Key, saltBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var rm = GetRM())
                {
                    using (var decryptor = rm.CreateDecryptor(keyBytes, ivBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

                                memoryStream.Close();
                                cryptoStream.Close();

                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        private static byte[] Generate256BitsOfRandomEntropy()
        {
            var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
            using (var rng = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rng.GetBytes(randomBytes);
            }

            return randomBytes;
        }

        private static RijndaelManaged GetRM()
        {
            return new RijndaelManaged
            {
                BlockSize = 256,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };
        }
        #endregion

        #region Hash
        // Used for agent accounts which don't have the requirements of retrieving
        // original plain text passwords.

        private const int SaltSize = 16;
        private const int HashSize = 20;
        private const int Iterations = 1000;

        public static string Hash(string password)
        {
            byte[] salt = null;
            byte[] hash = null;

            new RNGCryptoServiceProvider().GetBytes(salt = new byte[SaltSize]);
            hash = new Rfc2898DeriveBytes(password, salt, Iterations).GetBytes(HashSize);

            var result = new byte[SaltSize + HashSize];
            salt.CopyTo(result, 0);
            hash.CopyTo(result, SaltSize);

            return Convert.ToBase64String(result);
        }

        public static bool VerifyHash(string password, string hashedPassword)
        {
            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(hashedPassword))
            {
                return false;
            }

            var bytes = Convert.FromBase64String(hashedPassword);
            if (bytes.Length != SaltSize + HashSize)
            {
                return false;
            }

            var salt = new byte[SaltSize];
            Array.Copy(bytes, 0, salt, 0, SaltSize);
            var hash = new Rfc2898DeriveBytes(password, salt, Iterations).GetBytes(HashSize);

            for (int i = 0; i < HashSize; i++)
            {
                if (bytes[i + SaltSize] != hash[i])
                {
                    return false;
                }
            }

            return true;
        }
        #endregion
    }
}