﻿using Serilog;
using Serilog.Context;
using Serilog.Core;
using Serilog.Core.Enrichers;
using Serilog.Events;
using System;
using System.Collections.Generic;

namespace GW.PC.Core
{
    public class SerilogLogger
    {
        private static Func<ILogger> init = null;
        private volatile static ILogger logger = null;
        private volatile static SerilogLogger instance;
        private static readonly object lockHelper = new object();

        public static void Configure(Func<ILogger> func)
        {
            init = func;
        }

        private SerilogLogger()
        {

            if (init == null)
            {
                Serilog.Log.Logger = Serilog.Log.Logger ?? new LoggerConfiguration()
                    .WriteTo.File("..\\logs\\serilog.txt", outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Properties:j} {Message:lj}{NewLine}{Exception}", rollingInterval: RollingInterval.Day)
                    .CreateLogger();
            }
            else
            {
                Serilog.Log.Logger = init();

            }

            logger = Serilog.Log.Logger;
        }

        private static SerilogLogger CreateInstance()
        {
            if (instance == null)
            {
                lock (lockHelper)
                {
                    if (instance == null)
                        instance = new SerilogLogger();
                }
            }
            return instance;
        }

        public static SerilogLogger Logger => CreateInstance();

        public bool IsDebugEnabled => logger.IsEnabled(LogEventLevel.Debug);

        public bool IsInfoEnabled => logger.IsEnabled(LogEventLevel.Information);

        public bool IsWarnEnabled => logger.IsEnabled(LogEventLevel.Warning);

        public bool IsErrorEnabled => logger.IsEnabled(LogEventLevel.Error);

        public bool IsFatalEnabled => logger.IsEnabled(LogEventLevel.Fatal);

        public void LogError(string message, params object[] args)
        {
            if (IsErrorEnabled)
            {
                Log(LogEventLevel.Error, message, args);
            }
        }

        public void LogError(Exception exception, string message, params object[] args)
        {
            if (IsErrorEnabled)
            {
                Log(LogEventLevel.Error, message, args, exception);
            }
        }

        public void LogInformation(string message, params object[] args)
        {
            if (IsInfoEnabled)
            {
                Log(LogEventLevel.Information, message, args);
            }
        }

        public void LogInformation(Exception exception, string message, params object[] args)
        {
            if (IsInfoEnabled)
            {
                Log(LogEventLevel.Information, message, args, exception);
            }
        }

        public void LogDebug(string message, params object[] args)
        {
            if (IsDebugEnabled)
            {
                Log(LogEventLevel.Debug, message, args);
            }
        }

        public void LogDebug(Exception exception, string message, params object[] args)
        {
            if (IsDebugEnabled)
            {
                Log(LogEventLevel.Debug, message, args, exception);
            }
        }

        public void Log(LogEventLevel LogEventLevel, string message, Exception ex)
        {
            logger.Write(LogEventLevel, ex, message);
        }

        public void Log(LogEventLevel LogEventLevel, string messageTemplate, params object[] args)
        {
            logger.Write(LogEventLevel, messageTemplate, args);
        }

        public void Log(LogEventLevel LogEventLevel, string messageTemplate, object[] args, Exception ex)
        {
            logger.Write(LogEventLevel, ex, messageTemplate, args);
        }

        public IDisposable PushProperty(string name, object value, bool destructureObjects = false)
        {
            return LogContext.PushProperty(name, value, destructureObjects);
        }

        public IDisposable PushProperties(string[] names, object[] values)
        {
            if (names == null || values == null)
            {
                throw new ArgumentNullException($"Null {nameof(names)} or {nameof(values)}");
            }

            if (names.Length != values.Length)
            {
                throw new ArgumentOutOfRangeException($"names length: {names.Length}; values length: {values.Length}");
            }

            var list = new List<ILogEventEnricher>();

            for (int i = 0; i < names.Length; i++)
            {
                list.Add(new PropertyEnricher(names[i], values[i]));
            }

            return LogContext.Push(list.ToArray());
        }
    }
}
