﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GW.PC.Core
{
    public class BaseTask
    {
        public static Mutex EnsureSingleInstance(string processName)
        {
            var mutex = new Mutex(true, processName);
            if (!mutex.WaitOne(0, false))
            {
                Console.WriteLine("Instance Already Running!");

                return null;
            }

            return mutex;
        }
    }
}
