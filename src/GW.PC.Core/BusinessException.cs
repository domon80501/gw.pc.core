﻿using System;

namespace GW.PC.Core
{
    /// <summary>
    /// Represents the exceptions supposed to show the message to users.
    /// </summary>
    public class BusinessException : Exception
    {
        public BusinessException(string message) : base(message) { }

        public BusinessException(string message, string errorCode) : base(message)
        {
            ErrorCode = errorCode;
        }

        public string ErrorCode { get; set; }
    }
}
