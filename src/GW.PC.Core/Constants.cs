﻿using System.Configuration;

namespace GW.PC.Core
{
    public static class Constants
    {
        public const string SystemAccountUsername = "system";
        public const string AdministratorUsername = "administrator";
        public const string DefaultAgentUsername = "DEFAULT";
        public const string WebsiteName = "网站";
        public const string InvisibleText = "********";
        public const int LayoutAlarmNumber = 3;
        public const string AgentPromotionFilesUri = "https://1040846556.rsc.cdn77.org/agentpromotions/";
        public const string ExportedFileContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public static class AppSettings
        {
            public static string Get(AppSettingNames name)
            {
                return ConfigurationManager.AppSettings[name.ToString()];
            }
        }

        public static class ConnectionStrings
        {
            public static string Get(string name)
            {
                return ConfigurationManager.ConnectionStrings[name].ConnectionString;
            }
        }

        public static class FormatStrings
        {
            public const string MonthFormat = "yyyy/MM";
            public const string DateFormat = "yyyy/MM/dd";
            public const string YearFormat = "yyyy";
            public const string DateTimeFormat = "yyyy/MM/dd HH:mm:ss";
            public const string DateTimeRangeFormat = "yyyy/MM/dd HH:mm:ss";
            public const string DateTimeRangeValueString = "{0} - {1}";
            public const string AgentPromotionUrl = "https://{0}/?{1}";
            public const string DateTimeNoSecondFormat = "yyyy/MM/dd HH:mm";
        }

        public static class Messages
        {
            public const string UsernameNotFound = "无效用户名";
            public const string NotFound = "无效Id";
            public const string CustomerNotFound = "非法客户";
            public const string UserNotFound = "非法用户";
            public const string AgentNotFound = "非法代理";
            public const string MerchantNotFound = "非法商户";
            public const string InvalidLogin = "用户名或密码错误";
            public const string InvalidAccountStatus = "账户状态停用";
            public const string GameLoginNotAllowed = "账号被禁止登录游戏，请联系管理员";
            public const string ObjectDuplicated = "对象重复";
            public const string NoPermission = "你没有权限进行该操作";
            public const string InvalidSign = "Invalid sign";
            public const string UploadFileFailure = "上传文件失败";
            public const string InvalidToken = "无效token";
            public const string ApiConnectionError = "无法连接API服务器，请联系管理员";
            public const string InvalidMerchantOrderNumber = "Invalid merchant order number";
            public const string CustomerInGame = "操作失败，请点击退出按钮退出游戏后重试";
            public const string InsufficientGameBalance = "游戏余额不足，无法提款";
            public const string InsufficientWalletBalance = "中心钱包余额不足";
            public const string WithdrawNotAllowed = "您的账户暂时不能提款，请联系客服部";
            public const string WithdrawAmountOutOfRange = "提款金额最低100元";
            public const string InvalidBankCardNumber = "银行卡号只能是16-21位数字";
            public const string AgentNoGameSettings = "该代理无游戏设置";
            public const string PaymentChannelQueryNotSupported = "该通道不支持查询操作";
            public const string GeneralFailure = "网络异常，请重新提交";
            public const string GameTransferFailure = "游戏转账失败";
            public const string InsufficientFrozenAmount = "冻结金额不足";
            public const string NoWithdrawalChannel = "无可用提款通道";
            public const string InvalidVerificationCode = "验证码无效";
            public const string FailedReCaptcha = "人机验证失败";
            public const string PaymentSuccess = "支付成功";
            public const string OrderNumberDuplicated = "订单号重复";
            public const string PaymentProviderBanksNotFound = "未设置可用的支付银行";
            public const string PaymentProviderCardsNotFound = "未设置可用的充值卡类型";
            public const string ServerResponseFailure = "网络异常，请确认订单状态";
            public const string HandlerFailure = "支付组件处理异常，请确认订单状态";
            public const string PaymentProviderBanksInMaintenance = "目前该银行的服务维护中，请选择其他银行或稍后再试";
            public const string PaymentProviderCardsInMaintenance = "目前该充值卡类型的服务维护中，请选择其他充值卡类型或稍后再试";
            public const string NoDepositChannel = "无可用支付通道";
        }

        public static class MessageTemplates
        {
            public const string ObjectAlreadyExists = "{0}已存在";
            public const string InvalidCurrentStatus = "当前状态已更新为：{0}，请刷新页面重试";
            public const string InvalidObject = "无效{0}";
            public const string FormatError = "{0}格式错误";
            public const string NotEnoughTotalBetAmount = "总投注额差{0}元";
            public const string NotEnoughValidBetAmount = "有效投注额差{0}元";
            public const string WithdrawalFailure = "请核实您的卡号信息，重新提交。您提交的卡号：{0}，账户姓名：{1}。";
            public const string InvalidAmount = "申请金额应该在{0}-{1}之间";
            public const string NullObject = "Null {0}";
            public const string PluginSignFailure = "插件：{0}|{1}，验签失败";
            public const string PluginFailure = "插件{0}|{1}，发生错误：{2}";
            public const string PaymentPending = "支付中：{0}";
            public const string PaymentGeneralFailure = "网络异常，请重新提交。响应讯息：{0}";
            public const string PaymentRequestFailure = "网络异常，请再次查询该订单确认状态。响应讯息：{0}";

            public const string InvalidParameters = "无效参数：{0}";
        }

        public static class GameNames
        {
            public const string Default = "ESPORTS";

            public const string Sportsbook = "SSB";
            public const string Bbin = "BBIN";
            public const string AG = "AG";
            public const string EA = "EA";
            public const string QT = "QT";
            public const string KG = "KG";
            public const string PT = "PT";
            public const string LB = "LB";
            public const string LT = "LT";
            public const string ES = "ESPORTS";
        }

        public static class GatewayRequestDataItems
        {
            public const string Headers = "headers";
            public const string IgnoreServerCertificateValidation = "IgnoreServerCertificateValidation";
            public const string CertificateFilePath = "CertificateFilePath";
            public const string CertificatePassword = "CertificatePassword";
        }

        public static class CacheKeys
        {
            public const string FreeGameAccountCount = "FreeGameAccount";
        }

        public static class MemberConditionParameters
        {
            public const int UsernameMinLength = 6;
            public const int UsernameMaxLength = 12;
            public const int PasswordMinLength = 6;
            public const int PasswordMaxLength = 11;
            public const int AdultStandard = 18;
            public const int MobileLength = 11;
            public const int NameMaxLength = 10;
        }

        public static class SmsChannels
        {
            public const string emay = "1";
            public const string toushi = "2";
            public const string fegine = "3";
        }

        public static class MerchantOrderNumberPrefixes
        {
            public const string Deposit = "CK";
            //public const string CustomerWithdrawal = "TK";
            public const string MerchantWithdrawal = "TK";
            public const string PaymentChannelTransfer = "HZ";
            public const string PaymentChannelAdjustment = "WT";
            public const string AgentWithdrawal = "DT";

        }

        public static class PCAdminMessages
        {
            #region return Code
            public const string SuccessCode = "PC-000-000";
            public const string ExceptionCode = "PC-000-999";
            public const string PermissionErrorCode = "PC-000-100";
            public const string DuplicateUsernameCode = "PC-000-200";
            #endregion

            public const string InternalError = "系统内部错误, 请联络相关人员";
            public const string PermissionNotAllowed = "您没有被授权访问这个功能";
            public const string UsernameAlreadyExists = "重复的用户名: {0}";
        }
    }
}
