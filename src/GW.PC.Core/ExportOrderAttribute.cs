﻿using System;

namespace GW.PC.Core
{
    public class ExportOrderAttribute : Attribute
    {
        public ExportOrderAttribute(int order)
        {
            Order = order;
        }

        public int Order { get; set; }
    }
}
