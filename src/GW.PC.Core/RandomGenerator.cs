﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GW.PC.Core
{
    public static class RandomGenerator
    {
        private static readonly Random random = new Random();
        private static readonly List<string> todayNumbers = new List<string>();
        private static readonly object locker = new object();

        public static string NewMerchantOrderNumber(int randomLength = 4)
        {
            string attempt = string.Empty;

            lock (locker)
            {
                if (todayNumbers.Count > 1000)
                {
                    todayNumbers.RemoveAll(s => !s.StartsWith(DateTime.Today.ToString("yyMMdd")));
                }

                do
                {
                    attempt = $"{DateTime.Now.ToE8().ToString("yyMMddHHmmss")}{random.Next((int)Math.Pow(10, randomLength - 1), (int)Math.Pow(10, randomLength) - 1)}";
                }
                while (todayNumbers.Contains(attempt));

                todayNumbers.Add(attempt);
            }

            return attempt;
        }

        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string RandomNumber(int length)
        {
            const string chars = "0123456789";

            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string NewSystemOrderNumber()
            => $"{DateTime.Now.ToE8().ToString("yyMMddHHmmss")}{random.Next(99999999).ToString().PadLeft(8,'0')}";
    }
}
