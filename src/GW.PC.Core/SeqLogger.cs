﻿using Newtonsoft.Json;
using Serilog;
using Serilog.Events;
using System;
using System.Reflection;

namespace GW.PC.Core
{
    public class SeqLogger
    {
        private static Func<ILogger> init = null;
        private volatile static ILogger logger = null;
        private volatile static SeqLogger instance;
        private static readonly object lockHelper = new object();

        public static void Configure(Func<ILogger> func)
        {
            init = func;
        }

        private SeqLogger()
        {
            if (init == null)
            {
                init = () => new LoggerConfiguration()
                .Enrich.WithProperty("ApplicationName", "PaymentCenter")
                .Enrich.WithProperty("SourceContext", Assembly.GetExecutingAssembly().GetName().Name)
                .Enrich.WithProperty("Environment", "testing")
                .WriteTo.Seq("http://203.60.2.122:5341/").CreateLogger();
            }

            Log.Logger = init();

            logger = Log.Logger;
        }


        private static SeqLogger CreateInstance()
        {
            if (instance == null)
            {
                lock (lockHelper)
                {
                    if (instance == null)
                        instance = new SeqLogger();
                }
            }
            return instance;
        }

        public static SeqLogger Logger => CreateInstance();

        public bool IsDebugEnabled => logger.IsEnabled(LogEventLevel.Debug);

        public bool IsInfoEnabled => logger.IsEnabled(LogEventLevel.Information);

        public bool IsWarnEnabled => logger.IsEnabled(LogEventLevel.Warning);

        public bool IsErrorEnabled => logger.IsEnabled(LogEventLevel.Error);

        public bool IsFatalEnabled => logger.IsEnabled(LogEventLevel.Fatal);

        /// <summary>
        /// DebugLog
        /// </summary>
        /// <param name="id">OrderProcessId</param>
        /// <param name="content">Request/Response/CallbackRequest content</param>
        /// <param name="type">TransType</param>
        /// <param name="args">OtherMessage</param>
        public void LogDebug(Guid? id, string content, TransType type, params string[] args)
        {
            if (IsDebugEnabled)
            {
                WriteLog(id, content, LogEventLevel.Debug, type, args);
            }
        }

        /// <summary>
        /// InformationLog
        /// </summary>
        /// <param name="id">OrderProcessId</param>
        /// <param name="content">Request/Response/CallbackRequest content</param>
        /// <param name="type">TransType</param>
        /// <param name="args">OtherMessage</param>
        public void LogInfo(Guid? id, string content, TransType type, params string[] args)
        {
            if (IsInfoEnabled)
            {
                WriteLog(id, content, LogEventLevel.Information, type, args);
            }
        }

        /// <summary>
        /// ExceptionLog
        /// </summary>
        /// <param name="id">OrderProcessId</param>
        /// <param name="ex">Exception</param>
        /// <param name="type">TransType</param>
        /// <param name="args">OtherMessage</param>
        public void LogError(Guid? id, Exception ex, TransType type, params string[] args)
        {
            if (IsErrorEnabled)
            {
                WriteLog(id, ex, LogEventLevel.Fatal, type, args);
            }
        }

        /// <summary>
        /// BaseLog
        /// </summary>
        /// <param name="id">OrderProcessId</param>
        /// <param name="content">Content</param>
        /// <param name="level">LogLevel</param>
        /// <param name="type">TransType</param>
        /// <param name="symbol">OtherMessage</param>
        public void WriteLog(Guid? id, object content, LogEventLevel level, TransType type, string[] symbol)
        {
            string logStr = "{TransType} {ID} Content = {Content}";
            var log = new LogContent()
            {
                Id = id == null ? "" : id.ToString(),
                Level = level,
                TransType = type,
                Content = content.ToString(),
            };
            if (symbol.Length > 0)
            {
                logStr += " {Message}";
                logger.Write(log.Level, logStr,
                                log.TransType, log.Id, log.Content, symbol);
            }
            else
            {
                logger.Write(log.Level, logStr,
                                    log.TransType, log.Id, log.Content, symbol);
            }

        }
    }

    public enum TransType
    {
        Unknow = 0,
        DepositRequest = 1,
        DepositResponse = 2,
        DepositQueryRequest = 3,
        DepositQueryResponse = 4,
        DepositCallback = 5,
        WithdrawRequest = 6,
        WithdrawResponse = 7,
        WithdrawQueryRequest = 8,
        WithdrawQueryResponse = 9,
        WithdrawCallback = 10,
    }

    public class LogContent
    {
        /// <summary>
        /// 识别码
        /// </summary>
        [JsonProperty("Id")]
        public string Id { get; set; }
        /// <summary>
        /// 级别
        /// </summary>
        [JsonProperty("Level")]
        public LogEventLevel Level { get; set; }

        /// <summary>
        /// 交易类型
        /// </summary>
        [JsonProperty("Type")]
        public TransType TransType { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [JsonProperty("Content")]
        public string Content { get; set; }
    }
}
