﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace GW.PC.Core
{
    public static class EncryptDecryptUtility
    {
        private const string DefaultDesKey = "commonex";
        private static readonly byte[] DefaultDesIV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

        public static string Encrypt(string text)
        {
            return Convert.ToBase64String(DesEncrypt(text));
        }

        public static string DesEncrypt(string text, string key, CipherMode mode = CipherMode.CBC)
        {
            return Convert.ToBase64String(DesEncrypt(text, key, Encoding.ASCII.GetBytes(key), mode));
        }

        public static string Decrypt(string text)
        {
            var bytes = Convert.FromBase64String(text);

            return DesDecrypt(bytes);
        }

        public static string DesEncode(string text, string key)
        {
            return BitConverter.ToString(
                DesEncrypt(text, key, Encoding.ASCII.GetBytes(key)))
                .Replace("-", "");
        }

        /// <summary>
        /// For internal use and debugging purpose only.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string DesDecode(string text, string key)
        {
            int length = text.Length;
            var bytes = new byte[length / 2];
            for (int i = 0; i < length; i += 2)
                bytes[i / 2] = Convert.ToByte(text.Substring(i, 2), 16);

            return DesDecrypt(bytes, key, Encoding.ASCII.GetBytes(key));
        }

        private static byte[] DesEncrypt(string text,
            string key = DefaultDesKey,
            byte[] iv = null,
            CipherMode mode = CipherMode.CBC)
        {
            using (var des = new DESCryptoServiceProvider())
            {
                des.Key = Encoding.ASCII.GetBytes(key);
                des.IV = iv ?? DefaultDesIV;
                des.Mode = mode;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = des.CreateEncryptor();

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(text);
                        }
                    }

                    return msEncrypt.ToArray();
                }
            }
        }

        private static string DesDecrypt(byte[] bytes,
            string key = DefaultDesKey,
            byte[] iv = null)
        {
            using (var des = new DESCryptoServiceProvider())
            {
                des.Key = Encoding.ASCII.GetBytes(key);
                des.IV = iv ?? DefaultDesIV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = des.CreateDecryptor();

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(bytes))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            return srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
        }
    }
}
