﻿namespace GW.PC.Core
{
    public enum ClientPlatform
    {
        Unknown = 0,
        PC = 1,
        MobileWap = 2
    }
}
